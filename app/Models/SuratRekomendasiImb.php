<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratRekomendasiImb
 * @package App\Models
 * @version December 16, 2019, 7:36 am UTC
 *
 * @property \App\Models\Status status
 * @property string kode
 * @property string nik
 * @property string nama
 * @property string jenis_kelamin
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string kewarganegaraan
 * @property string agama
 * @property string status_perkawinan
 * @property string pekerjaan
 * @property string alamat
 * @property string maksud
 * @property string keperluan
 * @property string alamat_bangunan
 * @property string luas_bangunan
 * @property string luas_tanah
 * @property string nama_pemilik_tanah
 * @property string nomor_sertifikat_tanah
 * @property string bangunan_terbuat_dari
 * @property integer status_id
 */
class SuratRekomendasiImb extends Model
{
    use SoftDeletes;

    public $table = 'surat_rekomendasi_imb';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at' , 'tanggal_terbit' ];



    public $fillable = [
        'kode',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'maksud',
        'keperluan',
        'alamat_bangunan',
        'luas_bangunan',
        'luas_tanah',
        'nama_pemilik_tanah',
        'nomor_sertifikat_tanah',
        'bangunan_terbuat_dari',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kode' => 'string',
        'nik' => 'string|digits:16',
        'nama' => 'string',
        'jenis_kelamin' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'kewarganegaraan' => 'string',
        'agama' => 'string',
        'status_perkawinan' => 'string',
        'pekerjaan' => 'string',
        'alamat' => 'string',
        'maksud' => 'string',
        'keperluan' => 'string',
        'alamat_bangunan' => 'string',
        'luas_bangunan' => 'string',
        'luas_tanah' => 'string',
        'nama_pemilik_tanah' => 'string',
        'nomor_sertifikat_tanah' => 'string',
        'bangunan_terbuat_dari' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required',
        'nama' => 'required',
        'jenis_kelamin' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'kewarganegaraan' => 'required',
        'agama' => 'required',
        'status_perkawinan' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
        'maksud' => 'required',
        'keperluan' => 'required',
        'alamat_bangunan' => 'required',
        'luas_bangunan' => 'required',
        'luas_tanah' => 'required',
        'nama_pemilik_tanah' => 'required',
        'nomor_sertifikat_tanah' => 'required',
        'bangunan_terbuat_dari' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
