<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RiwayatKepemilikanHibah
 * @package App\Models
 * @version March 30, 2020, 1:40 pm +07
 *
 * @property \App\Models\SuratPernyataanHibah hibahan
 * @property integer hibahan_id
 * @property string alasan
 * @property string tahun
 * @property string nama
 */
class RiwayatKepemilikanHibah extends Model
{
    use SoftDeletes;

    public $table = 'riwayat_kepemilikan_hibah';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'hibahan_id',
        'alasan',
        'tahun',
        'nama'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'hibahan_id' => 'integer',
        'alasan' => 'string',
        'tahun' => 'string',
        'nama' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'hibahan_id' => 'required',
        'alasan' => 'required',
        'tahun' => 'required',
        'nama' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function hibahan()
    {
        return $this->belongsTo(\App\Models\SuratPernyataanHibah::class, 'hibahan_id');
    }
}
