<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganBedaNama
 * @package App\Models
 * @version January 31, 2020, 5:24 am UTC
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property integer user_id
 * @property string nik
 * @property string nama
 * @property string nama_kedua
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string jenis_kelamin
 * @property string status_perkawinan
 * @property string agama
 * @property string pekerjaan
 * @property string alamat
 * @property string tertera_di
 * @property string maksud
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganBedaNama extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_beda_nama';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik',
        'nama',
        'nama_kedua',
        'tempat_lahir',
        'tgl_lahir',
        'jenis_kelamin',
        'status_perkawinan',
        'agama',
        'pekerjaan',
        'alamat',
        'tertera_di',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik' => 'string',
        'nama' => 'string',
        'nama_kedua' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'jenis_kelamin' => 'string',
        'status_perkawinan' => 'string',
        'agama' => 'string',
        'pekerjaan' => 'string',
        'alamat' => 'string',
        'tertera_di' => 'string',
        'maksud' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required|digits:16',
        'nama' => 'required',
        'nama_kedua' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'jenis_kelamin' => 'required',
        'status_perkawinan' => 'required',
        'agama' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
        'tertera_di' => 'required',
        'maksud' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
