<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AhliWarisKeterenganAhliWaris
 * @package App\Models
 * @version January 29, 2020, 5:58 am UTC
 *
 * @property \App\Models\SuratKeteranganAhliWari ahliWaris
 * @property integer ahli_waris_id
 * @property string nama
 * @property string hubungan
 */
class AhliWarisKeterenganAhliWaris extends Model
{
    use SoftDeletes;

    public $table = 'ahli_waris_keterangan_ahli_waris';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'ahli_waris_id',
        'nama',
        'hubungan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ahli_waris_id' => 'integer',
        'nama' => 'string',
        'hubungan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ahli_waris_id' => 'required',
        'nama' => 'required',
        'hubungan' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ahliWaris()
    {
        return $this->belongsTo(\App\Models\SuratKeteranganAhliWari::class, 'ahli_waris_id');
    }
}
