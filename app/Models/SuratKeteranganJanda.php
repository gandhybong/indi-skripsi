<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganJanda
 * @package App\Models
 * @version April 5, 2020, 5:14 pm +07
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property integer user_id
 * @property string nik
 * @property string nama
 * @property string jenis_kelamin
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string status
 * @property string kewarganegaraan
 * @property string agama
 * @property string pekerjaan
 * @property string alamat
 * @property string rt
 * @property string rw
 * @property string sebab_cerai
 * @property string administrasi
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganJanda extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_janda';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'alamat',
        'rt',
        'rw',
        'sebab_cerai',
        'administrasi',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik' => 'string',
        'nama' => 'string',
        'jenis_kelamin' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'status_perkawinan' => 'string',
        'kewarganegaraan' => 'string',
        'agama' => 'string',
        'pekerjaan' => 'string',
        'alamat' => 'string',
        'rt' => 'string',
        'rw' => 'string',
        'sebab_cerai' => 'string',
        'administrasi' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required',
        'nama' => 'required',
        'jenis_kelamin' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'status_perkawinan' => 'required',
        'kewarganegaraan' => 'required',
        'agama' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
        'rt' => 'required',
        'rw' => 'required',
        'sebab_cerai' => 'required',
        'administrasi' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
