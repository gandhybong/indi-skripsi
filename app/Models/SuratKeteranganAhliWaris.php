<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganAhliWaris
 * @package App\Models
 * @version January 22, 2020, 6:32 am UTC
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection ahliWarisKeteranganAhliWaris
 * @property \Illuminate\Database\Eloquent\Collection saksiKeteranganAhliWaris
 * @property integer user_id
 * @property string nik
 * @property string nama
 * @property string tempat_lahir
 * @property string tanggal_lahir
 * @property string jenis_kelamin
 * @property string kewarganegaraan
 * @property string agama
 * @property string pekerjaan
 * @property string maksud
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganAhliWaris extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_ahli_waris';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'jenis_kelamin',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik' => 'string',
        'nama' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'jenis_kelamin' => 'string',
        'kewarganegaraan' => 'string',
        'agama' => 'string',
        'pekerjaan' => 'string',
        'maksud' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik'                   => 'required|digits:16',
        'nama'                  => 'required',
        'tempat_lahir'          => 'required',
        'tgl_lahir'             => 'required',
        'jenis_kelamin'         => 'required',
        'kewarganegaraan'       => 'required',
        'agama'                 => 'required',
        'pekerjaan'             => 'required',
        'maksud'                => 'required',
        'saksi_pertama'         => 'required',
        'saksi_kedua'           => 'required',
        "ahli_waris"            => "required|array|min:1",
        "ahli_waris.*"          => "required|string|distinct",
        "hubungan_ahli_waris"   => "required|array|min:1",
        "hubungan_ahli_waris.*" => "required|string|distinct",
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ahliWarisKeteranganAhliWaris()
    {
        return $this->hasMany(\App\Models\AhliWarisKeterenganAhliWaris::class, 'ahli_waris_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function saksiKeteranganAhliWaris()
    {
        return $this->hasMany(\App\Models\SaksiKeteranganAhliWaris::class, 'ahli_waris_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
