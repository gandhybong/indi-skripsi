<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKuasaTanah
 * @package App\Models
 * @version January 8, 2020, 4:47 am UTC
 *
 * @property \App\Models\Status status
 * @property string kode
 * @property string nik
 * @property string nama
 * @property string jenis_kelamin
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string status_perkawinan
 * @property string pekerjaan
 * @property string alamat
 * @property string nik_pihak_kedua
 * @property string nama_pihak_kedua
 * @property string jenis_kelamin_pihak_kedua
 * @property string tempat_lahir_pihak_kedua
 * @property string tgl_lahir_pihak_kedua
 * @property string status_perkawinan_pihak_kedua
 * @property string pekerjaan_pihak_kedua
 * @property string alamat_pihak_kedua
 * @property string status_kedua_pihak
 * @property string jenis_tanah
 * @property string nomor_hak_milik
 * @property string atas_nama
 * @property string tahun
 * @property string luas
 * @property string maksud
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKuasaTanah extends Model
{
    use SoftDeletes;

    public $table = 'surat_kuasa_tanah';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'kode',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'nik_pihak_kedua',
        'nama_pihak_kedua',
        'jenis_kelamin_pihak_kedua',
        'tempat_lahir_pihak_kedua',
        'tgl_lahir_pihak_kedua',
        'status_perkawinan_pihak_kedua',
        'pekerjaan_pihak_kedua',
        'alamat_pihak_kedua',
        'status_kedua_pihak',
        'jenis_tanah',
        'nomor_sertifikat_tanah',
        'atas_nama',
        'tahun',
        'luas_tanah',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kode' => 'string',
        'nik' => 'string',
        'nama' => 'string',
        'jenis_kelamin' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'status_perkawinan' => 'string',
        'pekerjaan' => 'string',
        'alamat' => 'string',
        'nik_pihak_kedua' => 'string',
        'nama_pihak_kedua' => 'string',
        'jenis_kelamin_pihak_kedua' => 'string',
        'tempat_lahir_pihak_kedua' => 'string',
        'tgl_lahir_pihak_kedua' => 'date',
        'status_perkawinan_pihak_kedua' => 'string',
        'pekerjaan_pihak_kedua' => 'string',
        'alamat_pihak_kedua' => 'string',
        'status_kedua_pihak' => 'string',
        'jenis_tanah' => 'string',
        'nomor_sertifikat_tanah' => 'string',
        'atas_nama' => 'string',
        'tahun' => 'string',
        'luas_tanah' => 'string',
        'maksud' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required|digits:16',
        'nama' => 'required',
        'jenis_kelamin' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'status_perkawinan' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
        'nik_pihak_kedua' => 'required',
        'nama_pihak_kedua' => 'required',
        'jenis_kelamin_pihak_kedua' => 'required',
        'tempat_lahir_pihak_kedua' => 'required',
        'tgl_lahir_pihak_kedua' => 'required',
        'status_perkawinan_pihak_kedua' => 'required',
        'pekerjaan_pihak_kedua' => 'required',
        'alamat_pihak_kedua' => 'required',
        'status_kedua_pihak' => 'required',
        'jenis_tanah' => 'required',
        'nomor_sertifikat_tanah' => 'required',
        'atas_nama' => 'required',
        'tahun' => 'required',
        'luas_tanah' => 'required',
        'maksud' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }

    public function getTglLahirPihakKeduaAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
