<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratPernyataanHibah
 * @package App\Models
 * @version March 26, 2020, 4:14 pm +07
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection riwayatKepemilikanHibahs
 * @property integer user_id
 * @property string nik
 * @property string nama
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string kewarganegaraan
 * @property string agama
 * @property string pekerjaan
 * @property string alamat
 * @property string letak_tanah
 * @property integer lebar
 * @property integer panjang
 * @property string batas_utara
 * @property string batas_timur
 * @property string batas_selatan
 * @property string batas_barat
 * @property string sejarah_dikuasai_oleh
 * @property string sejarah_tahun_dikuasai
 * @property string dihibahkan_kepada
 * @property string untuk_dijadikan
 * @property string jabatan_saksi_pertama
 * @property string nama_saksi_pertama
 * @property string jabatan_saksi_kedua
 * @property string nama_saksi_kedua
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratPernyataanHibah extends Model
{
    use SoftDeletes;

    public $table = 'surat_pernyataan_hibah';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'alamat',
        'letak_tanah',
        'lebar',
        'panjang',
        'batas_utara',
        'batas_timur',
        'batas_selatan',
        'batas_barat',
        'sejarah_dikuasai_oleh',
        'sejarah_tahun_dikuasai',
        'dihibahkan_kepada',
        'untuk_dijadikan',
        'jabatan_saksi_pertama',
        'nama_saksi_pertama',
        'jabatan_saksi_kedua',
        'nama_saksi_kedua',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik' => 'string',
        'nama' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'kewarganegaraan' => 'string',
        'agama' => 'string',
        'pekerjaan' => 'string',
        'alamat' => 'string',
        'letak_tanah' => 'string',
        'lebar' => 'integer',
        'panjang' => 'integer',
        'batas_utara' => 'string',
        'batas_timur' => 'string',
        'batas_selatan' => 'string',
        'batas_barat' => 'string',
        'sejarah_dikuasai_oleh' => 'string',
        'sejarah_tahun_dikuasai' => 'string',
        'dihibahkan_kepada' => 'string',
        'untuk_dijadikan' => 'string',
        'jabatan_saksi_pertama' => 'string',
        'nama_saksi_pertama' => 'string',
        'jabatan_saksi_kedua' => 'string',
        'nama_saksi_kedua' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required',
        'nama' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'kewarganegaraan' => 'required',
        'agama' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
        'letak_tanah' => 'required',
        'lebar' => 'required',
        'panjang' => 'required',
        'batas_utara' => 'required',
        'batas_timur' => 'required',
        'batas_selatan' => 'required',
        'batas_barat' => 'required',
        'sejarah_dikuasai_oleh' => 'required',
        'sejarah_tahun_dikuasai' => 'required',
        'dihibahkan_kepada' => 'required',
        'untuk_dijadikan' => 'required',
        'jabatan_saksi_pertama' => 'required',
        'nama_saksi_pertama' => 'required',
        'jabatan_saksi_kedua' => 'required',
        'nama_saksi_kedua' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function riwayatKepemilikanHibahs()
    {
        return $this->hasMany(\App\Models\RiwayatKepemilikanHibah::class, 'hibahan_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
