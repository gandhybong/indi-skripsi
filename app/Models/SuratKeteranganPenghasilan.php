<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganPenghasilan
 * @package App\Models
 * @version December 10, 2019, 4:15 am UTC
 *
 * @property \App\Models\Status status
 * @property integer user_id
 * @property string nik
 * @property string nama
 * @property string jenis_kelamin
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string kewarganegaraan
 * @property string pekerjaan
 * @property string agama
 * @property string status_perkawinan
 * @property string alamat
 * @property integer penghasilan
 * @property string maksud
 * @property integer status_id
 */
class SuratKeteranganPenghasilan extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_penghasilan';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'pekerjaan',
        'agama',
        'status_perkawinan',
        'alamat',
        'penghasilan',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik' => 'string',
        'nama' => 'string',
        'jenis_kelamin' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'kewarganegaraan' => 'string',
        'pekerjaan' => 'string',
        'agama' => 'string',
        'status_perkawinan' => 'string',
        'alamat' => 'string',
        'maksud' => 'string',
        'status_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required|digits:16',
        'nama' => 'required',
        'jenis_kelamin' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'kewarganegaraan' => 'required',
        'pekerjaan' => 'required',
        'agama' => 'required',
        'status_perkawinan' => 'required',
        'alamat' => 'required',
        'penghasilan' => 'required',
        'maksud' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo('App\Models\Status','status_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
