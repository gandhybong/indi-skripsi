<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganDomisili
 * @package App\Models
 * @version November 29, 2019, 1:13 pm UTC
 *
 * @property string kode
 * @property string nama
 * @property string nik
 * @property string alamat
 * @property string alamat_domisili
 * @property string Dusun
 * @property string Desa
 * @property string Kecamatan
 * @property string Kabupaten
 * @property string Maksud
 * @property string ketua_rt
 * @property string kepala_dusun
 */
class SuratKeteranganDomisili extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_domisili';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'kode',
        'nama',
        'nik',
        'alamat',
        'alamat_domisili',
        'Dusun',
        'Desa',
        'Kecamatan',
        'Kabupaten',
        'Maksud',
        'ketua_rt',
        'kepala_dusun',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kode' => 'string',
        'nama' => 'string',
        'nik' => 'string',
        'alamat' => 'string',
        'alamat_domisili' => 'string',
        'Dusun' => 'string',
        'Desa' => 'string',
        'Kecamatan' => 'string',
        'Kabupaten' => 'string',
        'Maksud' => 'string',
        'ketua_rt' => 'string',
        'kepala_dusun' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required',
        'nik' => 'required|digits:16',
        'alamat' => 'required',
        'alamat_domisili' => 'required',
        'Dusun' => 'required',
        'Desa' => 'required',
        'Kecamatan' => 'required',
        'Kabupaten' => 'required',
        'Maksud' => 'required',
        'ketua_rt' => 'required',
        'kepala_dusun' => 'required'
    ];

    public function status()
    {
        return $this->belongsTo('App\Models\Status','status_id','id');
    }
}
