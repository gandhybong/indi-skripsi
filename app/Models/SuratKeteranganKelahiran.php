<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganKelahiran
 * @package App\Models
 * @version May 9, 2020, 9:07 pm +07
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property integer user_id
 * @property string nik_pihak_bapak
 * @property string nama_pihak_bapak
 * @property string tempat_lahir_pihak_bapak
 * @property string tgl_lahir_pihak_bapak
 * @property string kewarganegaraan_pihak_bapak
 * @property string agama_pihak_bapak
 * @property string pekerjaan_pihak_bapak
 * @property string alamat_pihak_bapak
 * @property string nik_pihak_ibu
 * @property string nama_pihak_ibu
 * @property string tempat_lahir_pihak_ibu
 * @property string tgl_lahir_pihak_ibu
 * @property string kewarganegaraan_pihak_ibu
 * @property string agama_pihak_ibu
 * @property string pekerjaan_pihak_ibu
 * @property string alamat_pihak_ibu
 * @property string anak_ke
 * @property string nama_anak
 * @property string jenis_kelamin_anak
 * @property string tempat_lahir_anak
 * @property time jam_lahir_anak
 * @property string tgl_lahir_anak
 * @property string kewarganegaraan_anak
 * @property string agama_anak
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganKelahiran extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_kelahiran';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $fillable = [
        'user_id',
        'nik_pihak_bapak',
        'nama_pihak_bapak',
        'tempat_lahir_pihak_bapak',
        'tgl_lahir_pihak_bapak',
        'kewarganegaraan_pihak_bapak',
        'agama_pihak_bapak',
        'pekerjaan_pihak_bapak',
        'alamat_pihak_bapak',
        'nik_pihak_ibu',
        'nama_pihak_ibu',
        'tempat_lahir_pihak_ibu',
        'tgl_lahir_pihak_ibu',
        'kewarganegaraan_pihak_ibu',
        'agama_pihak_ibu',
        'pekerjaan_pihak_ibu',
        'alamat_pihak_ibu',
        'anak_ke',
        'nama_anak',
        'jenis_kelamin_anak',
        'tempat_lahir_anak',
        'jam_lahir_anak',
        'tgl_lahir_anak',
        'kewarganegaraan_anak',
        'agama_anak',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik_pihak_bapak' => 'string',
        'nama_pihak_bapak' => 'string',
        'tempat_lahir_pihak_bapak' => 'string',
        'tgl_lahir_pihak_bapak' => 'date',
        'kewarganegaraan_pihak_bapak' => 'string',
        'agama_pihak_bapak' => 'string',
        'pekerjaan_pihak_bapak' => 'string',
        'alamat_pihak_bapak' => 'string',
        'nik_pihak_ibu' => 'string',
        'nama_pihak_ibu' => 'string',
        'tempat_lahir_pihak_ibu' => 'string',
        'tgl_lahir_pihak_ibu' => 'date',
        'kewarganegaraan_pihak_ibu' => 'string',
        'agama_pihak_ibu' => 'string',
        'pekerjaan_pihak_ibu' => 'string',
        'alamat_pihak_ibu' => 'string',
        'anak_ke' => 'string',
        'nama_anak' => 'string',
        'jenis_kelamin_anak' => 'string',
        'tempat_lahir_anak' => 'string',
        'tgl_lahir_anak' => 'date',
        'kewarganegaraan_anak' => 'string',
        'agama_anak' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik_pihak_bapak' => 'required',
        'nama_pihak_bapak' => 'required',
        'tempat_lahir_pihak_bapak' => 'required',
        'tgl_lahir_pihak_bapak' => 'required',
        'kewarganegaraan_pihak_bapak' => 'required',
        'agama_pihak_bapak' => 'required',
        'pekerjaan_pihak_bapak' => 'required',
        'alamat_pihak_bapak' => 'required',
        'nik_pihak_ibu' => 'required',
        'nama_pihak_ibu' => 'required',
        'tempat_lahir_pihak_ibu' => 'required',
        'tgl_lahir_pihak_ibu' => 'required',
        'kewarganegaraan_pihak_ibu' => 'required',
        'agama_pihak_ibu' => 'required',
        'pekerjaan_pihak_ibu' => 'required',
        'alamat_pihak_ibu' => 'required',
        'anak_ke' => 'required',
        'nama_anak' => 'required',
        'jenis_kelamin_anak' => 'required',
        'tempat_lahir_anak' => 'required',
        'jam_lahir_anak' => 'required',
        'tgl_lahir_anak' => 'required',
        'kewarganegaraan_anak' => 'required',
        'agama_anak' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function getTglLahirPihakBapakAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }

    public function getTglLahirPihakIbuAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
