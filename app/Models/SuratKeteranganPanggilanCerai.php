<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganPanggilanCerai
 * @package App\Models
 * @version April 29, 2020, 3:49 pm +07
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property integer user_id
 * @property string nik_pihak_pertama
 * @property string nama_pihak_pertama
 * @property string umur_pihak_pertama
 * @property string pekerjaan_pihak_pertama
 * @property string alamat_pihak_pertama
 * @property string nik_pihak_kedua
 * @property string nama_pihak_kedua
 * @property string umur_pihak_kedua
 * @property string pekerjaan_pihak_kedua
 * @property string alamat_pihak_kedua
 * @property string yang_ditinggalkan
 * @property string lama_ditinggal
 * @property string nama_pembina_TKI
 * @property string nip_pembina_TKI
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganPanggilanCerai extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_panggilan_cerai';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik_pihak_pertama',
        'nama_pihak_pertama',
        'umur_pihak_pertama',
        'pekerjaan_pihak_pertama',
        'alamat_pihak_pertama',
        'nik_pihak_kedua',
        'nama_pihak_kedua',
        'umur_pihak_kedua',
        'pekerjaan_pihak_kedua',
        'alamat_pihak_kedua',
        'yang_ditinggalkan',
        'lama_ditinggal',
        'nama_pembina_TKI',
        'nip_pembina_TKI',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik_pihak_pertama' => 'string',
        'nama_pihak_pertama' => 'string',
        'umur_pihak_pertama' => 'string',
        'pekerjaan_pihak_pertama' => 'string',
        'alamat_pihak_pertama' => 'string',
        'nik_pihak_kedua' => 'string',
        'nama_pihak_kedua' => 'string',
        'umur_pihak_kedua' => 'string',
        'pekerjaan_pihak_kedua' => 'string',
        'alamat_pihak_kedua' => 'string',
        'yang_ditinggalkan' => 'string',
        'lama_ditinggal' => 'string',
        'nama_pembina_TKI' => 'string',
        'nip_pembina_TKI' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik_pihak_pertama' => 'required',
        'nama_pihak_pertama' => 'required',
        'umur_pihak_pertama' => 'required',
        'pekerjaan_pihak_pertama' => 'required',
        'alamat_pihak_pertama' => 'required',
        'nik_pihak_kedua' => 'required',
        'nama_pihak_kedua' => 'required',
        'umur_pihak_kedua' => 'required',
        'pekerjaan_pihak_kedua' => 'required',
        'alamat_pihak_kedua' => 'required',
        'yang_ditinggalkan' => 'required',
        'lama_ditinggal' => 'required',
        'nama_pembina_TKI' => 'required',
        'nip_pembina_TKI' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
