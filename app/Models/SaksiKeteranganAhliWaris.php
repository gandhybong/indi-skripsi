<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SaksiKeteranganAhliWaris
 * @package App\Models
 * @version January 29, 2020, 6:51 am UTC
 *
 * @property \App\Models\SuratKeteranganAhliWari ahliWaris
 * @property integer ahli_waris_id
 * @property string nama
 */
class SaksiKeteranganAhliWaris extends Model
{
    use SoftDeletes;

    public $table = 'saksi_keterangan_ahli_waris';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'ahli_waris_id',
        'nama'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ahli_waris_id' => 'integer',
        'nama' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ahli_waris_id' => 'required',
        'nama' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ahliWaris()
    {
        return $this->belongsTo(\App\Models\SuratKeteranganAhliWari::class, 'ahli_waris_id');
    }
}
