<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganTentangKematian
 * @package App\Models
 * @version May 6, 2020, 11:07 am +07
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property integer user_id
 * @property string nik
 * @property string nama
 * @property string bin_binti
 * @property string jenis_kelamin
 * @property string agama
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string status_perkawinan
 * @property string pekerjaan
 * @property string alamat
 * @property string tgl_meninggal
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganTentangKematian extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_tentang_kematian';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik',
        'nama',
        'bin_binti',
        'jenis_kelamin',
        'agama',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'tgl_meninggal',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat',
        'tempat_meninggal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik' => 'string',
        'nama' => 'string',
        'bin_binti' => 'string',
        'jenis_kelamin' => 'string',
        'agama' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'status_perkawinan' => 'string',
        'pekerjaan' => 'string',
        'alamat' => 'string',
        'tgl_meninggal' => 'date',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string',
        'tempat_meninggal'=> 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required',
        'nama' => 'required',
        'bin_binti' => 'required',
        'jenis_kelamin' => 'required',
        'agama' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'status_perkawinan' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
        'tgl_meninggal' => 'required',
        'tempat_meninggal' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }

    public function getTglMeninggalAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
