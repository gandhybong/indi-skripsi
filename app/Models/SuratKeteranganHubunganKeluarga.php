<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganHubunganKeluarga
 * @package App\Models
 * @version April 17, 2020, 10:27 am +07
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property integer user_id
 * @property string nik_pihak_pertama
 * @property string nama_pihak_pertama
 * @property string jenis_kelamin_pihak_pertama
 * @property string tempat_lahir_pihak_pertama
 * @property string tgl_lahir_pihak_pertama
 * @property string status_perkawinan_pihak_pertama
 * @property string pekerjaan_pihak_pertama
 * @property string alamat_pihak_pertama
 * @property string nik_pihak_kedua
 * @property string nama_pihak_kedua
 * @property string jenis_kelamin_pihak_kedua
 * @property string tempat_lahir_pihak_kedua
 * @property string tgl_lahir_pihak_kedua
 * @property string status_perkawinan_pihak_kedua
 * @property string pekerjaan_pihak_kedua
 * @property string alamat_pihak_kedua
 * @property string hubungan_antara_kedua_pihak
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganHubunganKeluarga extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_hubungan_keluarga';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik_pihak_pertama',
        'nama_pihak_pertama',
        'jenis_kelamin_pihak_pertama',
        'tempat_lahir_pihak_pertama',
        'tgl_lahir_pihak_pertama',
        'status_perkawinan_pihak_pertama',
        'pekerjaan_pihak_pertama',
        'alamat_pihak_pertama',
        'nik_pihak_kedua',
        'nama_pihak_kedua',
        'jenis_kelamin_pihak_kedua',
        'tempat_lahir_pihak_kedua',
        'tgl_lahir_pihak_kedua',
        'status_perkawinan_pihak_kedua',
        'pekerjaan_pihak_kedua',
        'alamat_pihak_kedua',
        'hubungan_antara_kedua_pihak',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik_pihak_pertama' => 'string',
        'nama_pihak_pertama' => 'string',
        'jenis_kelamin_pihak_pertama' => 'string',
        'tempat_lahir_pihak_pertama' => 'string',
        'tgl_lahir_pihak_pertama' => 'date',
        'status_perkawinan_pihak_pertama' => 'string',
        'pekerjaan_pihak_pertama' => 'string',
        'alamat_pihak_pertama' => 'string',
        'nik_pihak_kedua' => 'string',
        'nama_pihak_kedua' => 'string',
        'jenis_kelamin_pihak_kedua' => 'string',
        'tempat_lahir_pihak_kedua' => 'string',
        'tgl_lahir_pihak_kedua' => 'date',
        'status_perkawinan_pihak_kedua' => 'string',
        'pekerjaan_pihak_kedua' => 'string',
        'alamat_pihak_kedua' => 'string',
        'hubungan_antara_kedua_pihak' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik_pihak_pertama' => 'required',
        'nama_pihak_pertama' => 'required',
        'jenis_kelamin_pihak_pertama' => 'required',
        'tempat_lahir_pihak_pertama' => 'required',
        'tgl_lahir_pihak_pertama' => 'required',
        'status_perkawinan_pihak_pertama' => 'required',
        'pekerjaan_pihak_pertama' => 'required',
        'alamat_pihak_pertama' => 'required',
        'nik_pihak_kedua' => 'required',
        'nama_pihak_kedua' => 'required',
        'jenis_kelamin_pihak_kedua' => 'required',
        'tempat_lahir_pihak_kedua' => 'required',
        'tgl_lahir_pihak_kedua' => 'required',
        'status_perkawinan_pihak_kedua' => 'required',
        'pekerjaan_pihak_kedua' => 'required',
        'alamat_pihak_kedua' => 'required',
        'hubungan_antara_kedua_pihak' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function getTglLahirPihakPertamaAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }

    public function getTglLahirPihakKeduaAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
