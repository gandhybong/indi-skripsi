<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganIzinKeramaian
 * @package App\Models
 * @version November 30, 2019, 2:59 am UTC
 *
 * @property string nama
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string kewarganegaraan
 * @property string agama
 * @property string status_perkawinan
 * @property string alamat
 * @property string acara_kegiatan
 * @property string tgl_mulai
 * @property string tgl_selesai
 * @property string alamat_acara
 */
class SuratKeteranganIzinKeramaian extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_izin_keramaian';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at' ,'tgl_lahir' , 'tgl_mulai', 'tgl_selesai'];

    public $fillable = [
        'user_id',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'status_perkawinan',
        'alamat',
        'acara_kegiatan',
        'tgl_mulai',
        'tgl_selesai',
        'alamat_acara',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'kewarganegaraan' => 'string',
        'agama' => 'string',
        'status_perkawinan' => 'string',
        'alamat' => 'string',
        'acara_kegiatan' => 'string',
        'tgl_mulai' => 'date',
        'tgl_selesai' => 'date',
        'alamat_acara' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'kewarganegaraan' => 'required',
        'agama' => 'required',
        'status_perkawinan' => 'required',
        'alamat' => 'required',
        'acara_kegiatan' => 'required',
        'tgl_mulai' => 'required',
        'tgl_selesai' => 'required',
        'alamat_acara' => 'required'
    ];

    public function status()
    {
        return $this->belongsTo('App\Models\Status','status_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }

    public function getTglMulaiAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }

    public function getTglSelesaiAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
