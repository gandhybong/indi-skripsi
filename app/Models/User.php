<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Eloquent as Model;

/**
 * Class User
 * @package App\Models
 * @version November 28, 2019, 4:07 am UTC
 *
 * @property string name
 * @property string email
 * @property string|\Carbon\Carbon email_verified_at
 * @property string password
 * @property string remember_token
 */
class User extends Authenticatable
{
    use Notifiable;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'username',
        'nik',
        'name',
        'alamat',
        'tgl_lahir',
        'email',
        'email_verified_at',
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'nik'               => 'string',
        'name'              => 'string',
        'alamat'            => 'string',
        'tgl_lahir'         => 'date',
        'email'             => 'string',
        'email_verified_at' => 'datetime',
        'password'          => 'string',
        'remember_token'    => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required',
        // 'password' => 'required'
    ];

    public function scopeExceptAdministrator( $query )
    {
        return $query->where('email', '!=', 'superadmin@indi.id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
