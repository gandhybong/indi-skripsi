<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganPajak
 * @package App\Models
 * @version December 10, 2019, 4:13 am UTC
 *
 * @property \App\Models\Status status
 * @property string kode
 * @property string nik
 * @property string nama
 * @property string jenis_kelamin
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string kewarganegaraan
 * @property string agama
 * @property string status_perkawinan
 * @property string pekerjaan
 * @property string alamat
 * @property integer status_id
 */
class SuratKeteranganPajak extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_pajak';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'kode',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kode' => 'string',
        'nik' => 'string|digits:16',
        'nama' => 'string',
        'jenis_kelamin' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'kewarganegaraan' => 'string',
        'agama' => 'string',
        'status_perkawinan' => 'string',
        'pekerjaan' => 'string',
        'alamat' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required',
        'nama' => 'required',
        'jenis_kelamin' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'kewarganegaraan' => 'required',
        'agama' => 'required',
        'status_perkawinan' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/

    public function status()
    {
        return $this->belongsTo('App\Models\Status','status_id','id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
