<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganBelumNpwp
 * @package App\Models
 * @version February 25, 2020, 9:30 am +07
 *
 * @property \App\Models\Status status
 * @property \App\Models\User user
 * @property integer user_id
 * @property string nik
 * @property string nama
 * @property string jenis_kelamin
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string kewarganegaraan
 * @property string agama
 * @property string pekerjaan
 * @property string status_perkawinan
 * @property string alamat
 * @property string rt
 * @property string rw
 * @property string maksud
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganBelumNpwp extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_belum_npwp';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'status_perkawinan',
        'alamat',
        'rt',
        'rw',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'nik' => 'string',
        'nama' => 'string',
        'jenis_kelamin' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'kewarganegaraan' => 'string',
        'agama' => 'string',
        'pekerjaan' => 'string',
        'status_perkawinan' => 'string',
        'alamat' => 'string',
        'rt' => 'string',
        'rw' => 'string',
        'maksud' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required',
        'nama' => 'required',
        'jenis_kelamin' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'kewarganegaraan' => 'required',
        'agama' => 'required',
        'pekerjaan' => 'required',
        'status_perkawinan' => 'required',
        'alamat' => 'required',
        'rt' => 'required',
        'rw' => 'required',
        'maksud' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
