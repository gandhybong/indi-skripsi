<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Preference
 * @package App\Models
 * @version November 26, 2019, 9:18 am UTC
 *
 * @property string label
 * @property string value
 */
class Preference extends Model
{
    use SoftDeletes;

    public $table = 'preferences';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'label',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'label' => 'string',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'label' => 'required',
        'value' => 'required'
    ];

    public function scopeNamaDesa($query)
    {
        return $query->where('label','Desa')->first();
    }

    public function scopeLogo( $query )
    {
        return $query->where('label', 'Logo');
    }
}
