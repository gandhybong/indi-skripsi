<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SuratKeteranganUsaha
 * @package App\Models
 * @version January 5, 2020, 6:05 am UTC
 *
 * @property \App\Models\Status status
 * @property string kode
 * @property string nik
 * @property string nama
 * @property string jenis_kelamin
 * @property string tempat_lahir
 * @property string tgl_lahir
 * @property string status_perkawinan
 * @property string kewarganegaraan
 * @property string agama
 * @property string pekerjaan
 * @property string alamat
 * @property string nama_usaha
 * @property string tahun_berdiri_usaha
 * @property string alamat_usaha
 * @property integer status_id
 * @property string tanggal_pengajuan
 * @property string tanggal_terbit
 * @property string nomor_surat
 */
class SuratKeteranganUsaha extends Model
{
    use SoftDeletes;

    public $table = 'surat_keterangan_usaha';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'kode',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'alamat',
        'nama_usaha',
        'tahun_berdiri_usaha',
        'alamat_usaha',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kode' => 'string',
        'nik' => 'string|digits:16',
        'nama' => 'string',
        'jenis_kelamin' => 'string',
        'tempat_lahir' => 'string',
        'tgl_lahir' => 'date',
        'status_perkawinan' => 'string',
        'kewarganegaraan' => 'string',
        'agama' => 'string',
        'pekerjaan' => 'string',
        'alamat' => 'string',
        'nama_usaha' => 'string',
        'tahun_berdiri_usaha' => 'string',
        'alamat_usaha' => 'string',
        'status_id' => 'integer',
        'tanggal_pengajuan' => 'date',
        'tanggal_terbit' => 'date',
        'nomor_surat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nik' => 'required',
        'nama' => 'required',
        'jenis_kelamin' => 'required',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required',
        'status_perkawinan' => 'required',
        'kewarganegaraan' => 'required',
        'agama' => 'required',
        'pekerjaan' => 'required',
        'alamat' => 'required',
        'nama_usaha' => 'required',
        'tahun_berdiri_usaha' => 'required',
        'alamat_usaha' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function getTglLahirAttribute( $date )
    {
        return date( 'Y-m-d', strtotime( $date ) );
    }
}
