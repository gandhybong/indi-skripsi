<?php

namespace App\Repositories;

use App\Models\SuratKuasaTanah;
use App\Repositories\BaseRepository;

/**
 * Class SuratKuasaTanahRepository
 * @package App\Repositories
 * @version January 8, 2020, 4:47 am UTC
*/

class SuratKuasaTanahRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kode',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'nik_pihak_kedua',
        'nama_pihak_kedua',
        'jenis_kelamin_pihak_kedua',
        'tempat_lahir_pihak_kedua',
        'tgl_lahir_pihak_kedua',
        'status_perkawinan_pihak_kedua',
        'pekerjaan_pihak_kedua',
        'alamat_pihak_kedua',
        'status_kedua_pihak',
        'jenis_tanah',
        'nomor_sertifikat_tanah',
        'atas_nama',
        'tahun',
        'luas_tanah',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKuasaTanah::class;
    }
}
