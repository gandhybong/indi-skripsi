<?php

namespace App\Repositories;

use App\Models\SuratKeteranganHubunganKeluarga;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganHubunganKeluargaRepository
 * @package App\Repositories
 * @version April 17, 2020, 10:27 am +07
*/

class SuratKeteranganHubunganKeluargaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik_pihak_pertama',
        'nama_pihak_pertama',
        'jenis_kelamin_pihak_pertama',
        'tempat_lahir_pihak_pertama',
        'tgl_lahir_pihak_pertama',
        'status_perkawinan_pihak_pertama',
        'pekerjaan_pihak_pertama',
        'alamat_pihak_pertama',
        'nik_pihak_kedua',
        'nama_pihak_kedua',
        'jenis_kelamin_pihak_kedua',
        'tempat_lahir_pihak_kedua',
        'tgl_lahir_pihak_kedua',
        'status_perkawinan_pihak_kedua',
        'pekerjaan_pihak_kedua',
        'alamat_pihak_kedua',
        'hubungan_antara_kedua_pihak',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganHubunganKeluarga::class;
    }
}
