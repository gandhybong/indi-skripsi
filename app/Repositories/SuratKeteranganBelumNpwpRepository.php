<?php

namespace App\Repositories;

use App\Models\SuratKeteranganBelumNpwp;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganBelumNpwpRepository
 * @package App\Repositories
 * @version February 25, 2020, 9:30 am +07
*/

class SuratKeteranganBelumNpwpRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'status_perkawinan',
        'alamat',
        'rt',
        'rw',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganBelumNpwp::class;
    }
}
