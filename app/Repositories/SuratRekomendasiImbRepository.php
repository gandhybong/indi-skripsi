<?php

namespace App\Repositories;

use App\Models\SuratRekomendasiImb;
use App\Repositories\BaseRepository;

/**
 * Class SuratRekomendasiImbRepository
 * @package App\Repositories
 * @version December 16, 2019, 7:36 am UTC
*/

class SuratRekomendasiImbRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kode',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'maksud',
        'keperluan',
        'alamat_bangunan',
        'luas_bangunan',
        'luas_tanah',
        'nama_pemilik_tanah',
        'nomor_sertifikat_tanah',
        'bangunan_terbuat_dari',
        'status_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratRekomendasiImb::class;
    }
}
