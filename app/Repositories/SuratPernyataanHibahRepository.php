<?php

namespace App\Repositories;

use App\Models\SuratPernyataanHibah;
use App\Repositories\BaseRepository;

/**
 * Class SuratPernyataanHibahRepository
 * @package App\Repositories
 * @version March 26, 2020, 4:14 pm +07
*/

class SuratPernyataanHibahRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik',
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'alamat',
        'letak_tanah',
        'lebar',
        'panjang',
        'batas_utara',
        'batas_timur',
        'batas_selatan',
        'batas_barat',
        'sejarah_dikuasai_oleh',
        'sejarah_tahun_dikuasai',
        'sejarah_dihibahkan_kepada',
        'sejarah_tahun_dihibahkan',
        'dihibahkan_kepada',
        'untuk_dijadikan',
        'jabatan_saksi_pertama',
        'nama_saksi_pertama',
        'jabatan_saksi_kedua',
        'nama_saksi_kedua',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratPernyataanHibah::class;
    }
}
