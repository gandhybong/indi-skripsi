<?php

namespace App\Repositories;

use App\Models\SuratKeteranganUsaha;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganUsahaRepository
 * @package App\Repositories
 * @version January 5, 2020, 6:05 am UTC
*/

class SuratKeteranganUsahaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kode',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'alamat',
        'nama_usaha',
        'tahun_berdiri_usaha',
        'alamat_usaha',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganUsaha::class;
    }
}
