<?php

namespace App\Repositories;

use App\Models\SuratKeteranganPanggilanCerai;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganPanggilanCeraiRepository
 * @package App\Repositories
 * @version April 29, 2020, 3:49 pm +07
*/

class SuratKeteranganPanggilanCeraiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik_pihak_pertama',
        'nama_pihak_pertama',
        'umur_pihak_pertama',
        'pekerjaan_pihak_pertama',
        'alamat_pihak_pertama',
        'nik_pihak_kedua',
        'nama_pihak_kedua',
        'umur_pihak_kedua',
        'pekerjaan_pihak_kedua',
        'alamat_pihak_kedua',
        'yang_ditinggalkan',
        'lama_ditinggal',
        'nama_pembina_TKI',
        'nip_pembina_TKI',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganPanggilanCerai::class;
    }
}
