<?php

namespace App\Repositories;

use App\Models\SuratKeteranganTentangKematian;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganTentangKematianRepository
 * @package App\Repositories
 * @version May 6, 2020, 11:07 am +07
*/

class SuratKeteranganTentangKematianRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik',
        'nama',
        'bin_binti',
        'jenis_kelamin',
        'agama',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'tgl_meninggal',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganTentangKematian::class;
    }
}
