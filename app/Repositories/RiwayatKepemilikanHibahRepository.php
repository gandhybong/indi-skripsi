<?php

namespace App\Repositories;

use App\Models\RiwayatKepemilikanHibah;
use App\Repositories\BaseRepository;

/**
 * Class RiwayatKepemilikanHibahRepository
 * @package App\Repositories
 * @version March 30, 2020, 1:40 pm +07
*/

class RiwayatKepemilikanHibahRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hibahan_id',
        'alasan',
        'tahun',
        'nama'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RiwayatKepemilikanHibah::class;
    }
}
