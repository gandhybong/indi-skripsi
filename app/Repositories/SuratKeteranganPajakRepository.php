<?php

namespace App\Repositories;

use App\Models\SuratKeteranganPajak;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganPajakRepository
 * @package App\Repositories
 * @version December 10, 2019, 4:13 am UTC
*/

class SuratKeteranganPajakRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kode',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'status_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganPajak::class;
    }
}
