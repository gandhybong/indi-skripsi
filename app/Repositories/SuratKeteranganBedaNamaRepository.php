<?php

namespace App\Repositories;

use App\Models\SuratKeteranganBedaNama;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganBedaNamaRepository
 * @package App\Repositories
 * @version January 31, 2020, 5:24 am UTC
*/

class SuratKeteranganBedaNamaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik',
        'nama',
        'nama_kedua',
        'tempat_lahir',
        'tgl_lahir',
        'jenis_kelamin',
        'status_perkawinan',
        'agama',
        'pekerjaan',
        'alamat',
        'tertera_di',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganBedaNama::class;
    }
}
