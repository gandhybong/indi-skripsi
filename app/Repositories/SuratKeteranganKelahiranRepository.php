<?php

namespace App\Repositories;

use App\Models\SuratKeteranganKelahiran;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganKelahiranRepository
 * @package App\Repositories
 * @version May 9, 2020, 9:07 pm +07
*/

class SuratKeteranganKelahiranRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik_pihak_bapak',
        'nama_pihak_bapak',
        'tempat_lahir_pihak_bapak',
        'tgl_lahir_pihak_bapak',
        'kewarganegaraan_pihak_bapak',
        'agama_pihak_bapak',
        'pekerjaan_pihak_bapak',
        'alamat_pihak_bapak',
        'nik_pihak_ibu',
        'nama_pihak_ibu',
        'tempat_lahir_pihak_ibu',
        'tgl_lahir_pihak_ibu',
        'kewarganegaraan_pihak_ibu',
        'agama_pihak_ibu',
        'pekerjaan_pihak_ibu',
        'alamat_pihak_ibu',
        'anak_ke',
        'nama_anak',
        'jenis_kelamin_anak',
        'tempat_lahir_anak',
        'jam_lahir_anak',
        'tgl_lahir_anak',
        'kewarganegaraan_anak',
        'agama_anak',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganKelahiran::class;
    }
}
