<?php

namespace App\Repositories;

use App\Models\SuratKeteranganIzinKeramaian;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganIzinKeramaianRepository
 * @package App\Repositories
 * @version November 30, 2019, 2:59 am UTC
*/

class SuratKeteranganIzinKeramaianRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'agama',
        'status_perkawinan',
        'alamat',
        'acara_kegiatan',
        'tgl_mulai',
        'tgl_selesai',
        'alamat_acara'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganIzinKeramaian::class;
    }
}
