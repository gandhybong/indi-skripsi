<?php

namespace App\Repositories;

use App\Models\SuratKeteranganAhliWaris;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganAhliWarisRepository
 * @package App\Repositories
 * @version January 22, 2020, 6:32 am UTC
*/

class SuratKeteranganAhliWarisRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik',
        'nama',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganAhliWaris::class;
    }
}
