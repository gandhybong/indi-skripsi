<?php

namespace App\Repositories;

use App\Models\SuratKeteranganTidakMampu;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganTidakMampuRepository
 * @package App\Repositories
 * @version January 15, 2020, 3:29 am UTC
*/

class SuratKeteranganTidakMampuRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nomor_kartu_keluarga',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'kewarganegaraan',
        'agama',
        'status_perkawinan',
        'pekerjaan',
        'alamat',
        'penghasilan',
        'maksud',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganTidakMampu::class;
    }
}
