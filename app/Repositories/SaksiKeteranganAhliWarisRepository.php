<?php

namespace App\Repositories;

use App\Models\SaksiKeteranganAhliWaris;
use App\Repositories\BaseRepository;

/**
 * Class SaksiKeteranganAhliWarisRepository
 * @package App\Repositories
 * @version January 29, 2020, 6:51 am UTC
*/

class SaksiKeteranganAhliWarisRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ahli_waris_id',
        'nama'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SaksiKeteranganAhliWaris::class;
    }
}
