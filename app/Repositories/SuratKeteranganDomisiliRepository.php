<?php

namespace App\Repositories;

use App\Models\SuratKeteranganDomisili;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganDomisiliRepository
 * @package App\Repositories
 * @version November 29, 2019, 1:13 pm UTC
*/

class SuratKeteranganDomisiliRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kode',
        'nama',
        'nik',
        'alamat',
        'alamat_domisili',
        'Dusun',
        'Desa',
        'Kecamatan',
        'Kabupaten',
        'Maksud',
        'ketua_rt',
        'kepala_dusun'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganDomisili::class;
    }
}
