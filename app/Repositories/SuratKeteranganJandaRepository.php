<?php

namespace App\Repositories;

use App\Models\SuratKeteranganJanda;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganJandaRepository
 * @package App\Repositories
 * @version April 5, 2020, 5:14 pm +07
*/

class SuratKeteranganJandaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'status',
        'kewarganegaraan',
        'agama',
        'pekerjaan',
        'alamat',
        'rt',
        'rw',
        'sebab_cerai',
        'administrasi',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganJanda::class;
    }
}
