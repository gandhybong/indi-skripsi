<?php

namespace App\Repositories;

use App\Models\SuratKeteranganPenghasilan;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganPenghasilanRepository
 * @package App\Repositories
 * @version December 10, 2019, 4:15 am UTC
*/

class SuratKeteranganPenghasilanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'Pekerjaan',
        'agama',
        'status_perkawinan',
        'alamat',
        'penghasilan',
        'maksud',
        'status_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganPenghasilan::class;
    }
}
