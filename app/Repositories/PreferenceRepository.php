<?php

namespace App\Repositories;

use App\Models\Preference;
use App\Repositories\BaseRepository;

/**
 * Class PreferenceRepository
 * @package App\Repositories
 * @version November 26, 2019, 9:18 am UTC
*/

class PreferenceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'label',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Preference::class;
    }
}
