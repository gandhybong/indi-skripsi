<?php

namespace App\Repositories;

use App\Models\SuratKeteranganBelumPernahMenikah;
use App\Repositories\BaseRepository;

/**
 * Class SuratKeteranganBelumPernahMenikahRepository
 * @package App\Repositories
 * @version April 16, 2020, 10:35 am +07
*/

class SuratKeteranganBelumPernahMenikahRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'nik',
        'nama',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'kewarganegaraan',
        'pekerjaan',
        'agama',
        'status_perkawinan',
        'alamat',
        'administrasi',
        'status_id',
        'tanggal_pengajuan',
        'tanggal_terbit',
        'nomor_surat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SuratKeteranganBelumPernahMenikah::class;
    }
}
