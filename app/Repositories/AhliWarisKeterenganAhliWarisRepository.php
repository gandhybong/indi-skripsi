<?php

namespace App\Repositories;

use App\Models\AhliWarisKeterenganAhliWaris;
use App\Repositories\BaseRepository;

/**
 * Class AhliWarisKeterenganAhliWarisRepository
 * @package App\Repositories
 * @version January 29, 2020, 5:58 am UTC
*/

class AhliWarisKeterenganAhliWarisRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ahli_waris_id',
        'nama',
        'hubungan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AhliWarisKeterenganAhliWaris::class;
    }
}
