<?php
namespace App\Http\Services;

use App\Models\SuratKeteranganAhliWaris;
use App\Models\SuratKeteranganBedaNama;
use App\Models\SuratKeteranganDomisili;
use App\Models\SuratKeteranganIzinKeramaian;
use App\Models\SuratKeteranganPenghasilan;
use App\Models\SuratKeteranganPajak;
use App\Models\SuratKeteranganTidakMampu;
use App\Models\SuratKuasaTanah;
use App\Models\SuratRekomendasiImb;
use App\Models\SuratKeteranganUsaha;
use App\Models\Status;
use Carbon\Carbon;

class DeleteIjinExpired
{
    public function __invoke()
    {
        $expiredStatus  = Status::where('status','Ditolak - Expired')->first();
        $draftStatus    = Status::where('status','Draf')->first();
        $expirationDate = Carbon::now();
        
        SuratKeteranganAhliWaris::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratKeteranganBedaNama::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratKeteranganDomisili::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratKeteranganIzinKeramaian::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratKeteranganPenghasilan::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratKeteranganPajak::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratKeteranganTidakMampu::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratKuasaTanah::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratRekomendasiImb::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
        SuratKeteranganUsaha::where('tanggal_pengajuan', '<' , $expirationDate->subDays(2))->where('status_id',$draftStatus->id)->whereNull('tanggal_terbit')->update(['status_id' => $expiredStatus->id]);
    }
}