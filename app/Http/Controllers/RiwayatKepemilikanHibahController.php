<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRiwayatKepemilikanHibahRequest;
use App\Http\Requests\UpdateRiwayatKepemilikanHibahRequest;
use App\Repositories\RiwayatKepemilikanHibahRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RiwayatKepemilikanHibahController extends AppBaseController
{
    /** @var  RiwayatKepemilikanHibahRepository */
    private $riwayatKepemilikanHibahRepository;

    public function __construct(RiwayatKepemilikanHibahRepository $riwayatKepemilikanHibahRepo)
    {
        $this->riwayatKepemilikanHibahRepository = $riwayatKepemilikanHibahRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the RiwayatKepemilikanHibah.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $riwayatKepemilikanHibahs = $this->riwayatKepemilikanHibahRepository->all();

        return view('riwayat_kepemilikan_hibahs.index')
            ->with('riwayatKepemilikanHibahs', $riwayatKepemilikanHibahs);
    }

    /**
     * Show the form for creating a new RiwayatKepemilikanHibah.
     *
     * @return Response
     */
    public function create()
    {
        return view('riwayat_kepemilikan_hibahs.create');
    }

    /**
     * Store a newly created RiwayatKepemilikanHibah in storage.
     *
     * @param CreateRiwayatKepemilikanHibahRequest $request
     *
     * @return Response
     */
    public function store(CreateRiwayatKepemilikanHibahRequest $request)
    {
        $input = $request->all();

        $riwayatKepemilikanHibah = $this->riwayatKepemilikanHibahRepository->create($input);

        Flash::success('Riwayat Kepemilikan Hibah saved successfully.');

        return redirect(route('riwayatKepemilikanHibahs.index'));
    }

    /**
     * Display the specified RiwayatKepemilikanHibah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $riwayatKepemilikanHibah = $this->riwayatKepemilikanHibahRepository->find($id);

        if (empty($riwayatKepemilikanHibah)) {
            Flash::error('Riwayat Kepemilikan Hibah not found');

            return redirect(route('riwayatKepemilikanHibahs.index'));
        }

        return view('riwayat_kepemilikan_hibahs.show')->with('riwayatKepemilikanHibah', $riwayatKepemilikanHibah);
    }

    /**
     * Show the form for editing the specified RiwayatKepemilikanHibah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $riwayatKepemilikanHibah = $this->riwayatKepemilikanHibahRepository->find($id);

        if (empty($riwayatKepemilikanHibah)) {
            Flash::error('Riwayat Kepemilikan Hibah not found');

            return redirect(route('riwayatKepemilikanHibahs.index'));
        }

        return view('riwayat_kepemilikan_hibahs.edit')->with('riwayatKepemilikanHibah', $riwayatKepemilikanHibah);
    }

    /**
     * Update the specified RiwayatKepemilikanHibah in storage.
     *
     * @param int $id
     * @param UpdateRiwayatKepemilikanHibahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRiwayatKepemilikanHibahRequest $request)
    {
        $riwayatKepemilikanHibah = $this->riwayatKepemilikanHibahRepository->find($id);

        if (empty($riwayatKepemilikanHibah)) {
            Flash::error('Riwayat Kepemilikan Hibah not found');

            return redirect(route('riwayatKepemilikanHibahs.index'));
        }

        $riwayatKepemilikanHibah = $this->riwayatKepemilikanHibahRepository->update($request->all(), $id);

        Flash::success('Riwayat Kepemilikan Hibah updated successfully.');

        return redirect(route('riwayatKepemilikanHibahs.index'));
    }

    /**
     * Remove the specified RiwayatKepemilikanHibah from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $riwayatKepemilikanHibah = $this->riwayatKepemilikanHibahRepository->find($id);

        if (empty($riwayatKepemilikanHibah)) {
            Flash::error('Riwayat Kepemilikan Hibah not found');

            return redirect(route('riwayatKepemilikanHibahs.index'));
        }

        $this->riwayatKepemilikanHibahRepository->delete($id);

        Flash::success('Riwayat Kepemilikan Hibah deleted successfully.');

        return redirect(route('riwayatKepemilikanHibahs.index'));
    }
}
