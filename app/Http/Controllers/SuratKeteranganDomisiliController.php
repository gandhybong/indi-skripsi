<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganDomisiliRequest;
use App\Http\Requests\UpdateSuratKeteranganDomisiliRequest;
use App\Http\Requests\SuratKeteranganDomisiliRequest;
use App\Repositories\SuratKeteranganDomisiliRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganDomisili;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;

class SuratKeteranganDomisiliController extends AppBaseController
{
    /** @var  SuratKeteranganDomisiliRepository */
    private $suratKeteranganDomisiliRepository;

    public function __construct(SuratKeteranganDomisiliRepository $suratKeteranganDomisiliRepo)
    {
        $this->suratKeteranganDomisiliRepository = $suratKeteranganDomisiliRepo;
        $this->middleware('auth')->except('indexGuest','insertGuest');
    }

    /**
     * Display a listing of the SuratKeteranganDomisili.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganDomisilis = SuratKeteranganDomisili::with('status')->get();

        return view('surat_keterangan_domisilis.index')
            ->with('suratKeteranganDomisilis', $suratKeteranganDomisilis);
    }

    /**
     * Show the form for creating a new SuratKeteranganDomisili.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_domisilis.create');
    }

    /**
     * Store a newly created SuratKeteranganDomisili in storage.
     *
     * @param CreateSuratKeteranganDomisiliRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganDomisiliRequest $request)
    {
        $input        = $request->all();
        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now()->format('Y-m-d');

        $input['status_id']         = $status->id;
        $input['kode']              = $randomNumber;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganDomisili = $this->suratKeteranganDomisiliRepository->create($input);

        Flash::success('Surat Keterangan Domisili saved successfully.');

        return redirect(route('suratKeteranganDomisilis.index'));
    }

    /**
     * Display the specified SuratKeteranganDomisili.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganDomisili = $this->suratKeteranganDomisiliRepository->find($id);

        if (empty($suratKeteranganDomisili)) {
            Flash::error('Surat Keterangan Domisili not found');

            return redirect(route('suratKeteranganDomisilis.index'));
        }

        return view('surat_keterangan_domisilis.show')->with('suratKeteranganDomisili', $suratKeteranganDomisili);
    }

    /**
     * Show the form for editing the specified SuratKeteranganDomisili.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganDomisili = SuratKeteranganDomisili::with('status')->whereId($id)->first();

        $status = Status::all();
        if (empty($suratKeteranganDomisili)) {
            Flash::error('Surat Keterangan Domisili not found');

            return redirect(route('suratKeteranganDomisilis.index'));
        }

        return view('surat_keterangan_domisilis.edit')->with(['suratKeteranganDomisili' => $suratKeteranganDomisili , 'statuses' => $status]);
    }

    /**
     * Update the specified SuratKeteranganDomisili in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganDomisiliRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganDomisiliRequest $request)
    {
        $suratKeteranganDomisili = $this->suratKeteranganDomisiliRepository->find($id);

        if (empty($suratKeteranganDomisili)) {
            Flash::error('Surat Keterangan Domisili not found');

            return redirect(route('suratKeteranganDomisilis.index'));
        }
        $allRequestData          = $request->all();
        
        if($request->terima == 'Terima'){
            $status     = Status::where('status','Diterima')->first();
            $dateNow    = Carbon::now()->format('Y-m-d');
            $countSurat = SuratKeteranganDomisili::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT); 
            $allRequestData['status_id']      = $status->id;
            if($suratKeteranganDomisili->tanggal_terbit == NULL && $suratKeteranganDomisili->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "470.2/ $nomorSurat / I / PEM";
            }
            
            $suratKeteranganDomisili = $this->suratKeteranganDomisiliRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;
            $suratKeteranganDomisili = $this->suratKeteranganDomisiliRepository->update($allRequestData, $id);
            Flash::success('Surat Keterangan Domisili berhasil diperbaharui.');

            return redirect(route('suratKeteranganDomisilis.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganDomisili from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganDomisili = $this->suratKeteranganDomisiliRepository->find($id);

        if (empty($suratKeteranganDomisili)) {
            Flash::error('Surat Keterangan Domisili not found');

            return redirect(route('suratKeteranganDomisilis.index'));
        }

        $this->suratKeteranganDomisiliRepository->delete($id);

        Flash::success('Surat Keterangan Domisili deleted successfully.');

        return redirect(route('suratKeteranganDomisilis.index'));
    }

    public function print($id)
    {
        $SuratKeteranganDomisili = SuratKeteranganDomisili::find($id);
        $preferences             = Preference::get()->keyBy('label')->toArray();
        $dateNow                 = Carbon::now()->format('d-m-Y');
        $pdf                     = \PDF::loadView('umum.suratKeteranganDomisili.surat', ['SuratKeteranganDomisili' => $SuratKeteranganDomisili,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->setPaper('a4', 'portrait')->stream('surat.pdf');
    }

    public function indexGuest()
    {
        return view('umum.suratKeteranganDomisili.index');
    }

    public function insertGuest(SuratKeteranganDomisiliRequest $request)
    {
        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now();

        $suratKeteranganDomisili                    = new SuratKeteranganDomisili;
        $suratKeteranganDomisili->kode              = $randomNumber;
        $suratKeteranganDomisili->nik               = $request->nik;
        $suratKeteranganDomisili->nama              = $request->nama;
        $suratKeteranganDomisili->alamat            = $request->alamat;
        $suratKeteranganDomisili->alamat_domisili   = $request->alamat_domisili;
        $suratKeteranganDomisili->dusun             = $request->dusun;
        $suratKeteranganDomisili->desa              = $request->desa;
        $suratKeteranganDomisili->kecamatan         = $request->kecamatan;
        $suratKeteranganDomisili->kabupaten         = $request->kabupaten;
        $suratKeteranganDomisili->maksud            = $request->maksud;
        $suratKeteranganDomisili->ketua_rt          = $request->ketua_rt;
        $suratKeteranganDomisili->kepala_dusun      = $request->kepala_dusun;
        $suratKeteranganDomisili->kepala_dusun      = $request->kepala_dusun;
        $suratKeteranganDomisili->status_id         = $status->id;
        $suratKeteranganDomisili->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganDomisili->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('umum.suratKeteranganDomisili.kodeSurat')->with([
            'kode'                    => $randomNumber,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
