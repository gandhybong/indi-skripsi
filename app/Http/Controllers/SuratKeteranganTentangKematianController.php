<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganTentangKematianRequest;
use App\Http\Requests\UpdateSuratKeteranganTentangKematianRequest;
use App\Repositories\SuratKeteranganTentangKematianRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganTentangKematian;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganTentangKematianRequest;

class SuratKeteranganTentangKematianController extends AppBaseController
{
    /** @var  SuratKeteranganTentangKematianRepository */
    private $suratKeteranganTentangKematianRepository;

    public function __construct(SuratKeteranganTentangKematianRepository $suratKeteranganTentangKematianRepo)
    {
        $this->suratKeteranganTentangKematianRepository = $suratKeteranganTentangKematianRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganTentangKematian.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganTentangKematians = $this->suratKeteranganTentangKematianRepository->all();

        return view('surat_keterangan_tentang_kematians.index')
            ->with('suratKeteranganTentangKematians', $suratKeteranganTentangKematians);
    }

    /**
     * Show the form for creating a new SuratKeteranganTentangKematian.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_tentang_kematians.create');
    }

    /**
     * Store a newly created SuratKeteranganTentangKematian in storage.
     *
     * @param CreateSuratKeteranganTentangKematianRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganTentangKematianRequest $request)
    {
        $input = $request->all();

        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganTentangKematian = $this->suratKeteranganTentangKematianRepository->create($input);

        Flash::success('Surat Keterangan Tentang Kematian saved successfully.');

        return redirect(route('suratKeteranganTentangKematians.index'));
    }

    /**
     * Display the specified SuratKeteranganTentangKematian.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganTentangKematian = $this->suratKeteranganTentangKematianRepository->find($id);

        if (empty($suratKeteranganTentangKematian)) {
            Flash::error('Surat Keterangan Tentang Kematian not found');

            return redirect(route('suratKeteranganTentangKematians.index'));
        }

        return view('surat_keterangan_tentang_kematians.show')->with('suratKeteranganTentangKematian', $suratKeteranganTentangKematian);
    }

    /**
     * Show the form for editing the specified SuratKeteranganTentangKematian.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganTentangKematian = $this->suratKeteranganTentangKematianRepository->find($id);

        if (empty($suratKeteranganTentangKematian)) {
            Flash::error('Surat Keterangan Tentang Kematian not found');

            return redirect(route('suratKeteranganTentangKematians.index'));
        }

        return view('surat_keterangan_tentang_kematians.edit')->with('suratKeteranganTentangKematian', $suratKeteranganTentangKematian);
    }

    /**
     * Update the specified SuratKeteranganTentangKematian in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganTentangKematianRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganTentangKematianRequest $request)
    {
        $suratKeteranganTentangKematian = $this->suratKeteranganTentangKematianRepository->find($id);

        if (empty($suratKeteranganTentangKematian)) {
            Flash::error('Surat Keterangan Tentang Kematian not found');

            return redirect(route('suratKeteranganTentangKematians.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganTentangKematian::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganTentangKematian->tanggal_terbit == NULL && $suratKeteranganTentangKematian->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "883 / $nomorSurat /I/ PEM";
            }
            
            $suratKeteranganTentangKematian = $this->suratKeteranganTentangKematianRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganTentangKematian = $this->suratKeteranganTentangKematianRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Tentang Kematian updated successfully.');
    
            return redirect(route('suratKeteranganTentangKematians.index'));
        }

       
    }

    /**
     * Remove the specified SuratKeteranganTentangKematian from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganTentangKematian = $this->suratKeteranganTentangKematianRepository->find($id);

        if (empty($suratKeteranganTentangKematian)) {
            Flash::error('Surat Keterangan Tentang Kematian not found');

            return redirect(route('suratKeteranganTentangKematians.index'));
        }

        $this->suratKeteranganTentangKematianRepository->delete($id);

        Flash::success('Surat Keterangan Tentang Kematian deleted successfully.');

        return redirect(route('suratKeteranganTentangKematians.index'));
    }

    public function print($id)
    {
        $suratKeteranganTentangKematian = SuratKeteranganTentangKematian::find($id);
        $preferences                    = Preference::get()->keyBy('label')->toArray();
        $dateNow                        = Carbon::now()->format('d-m-Y');
        $pdf                            = \PDF::loadView('khusus.suratKeteranganTentangKematian.surat', ['suratKeteranganTentangKematian' => $suratKeteranganTentangKematian,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganTentangKematian.index');
    }

    public function insertGuest(SuratKeteranganTentangKematianRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $suratKeteranganTentangKematian                    = new SuratKeteranganTentangKematian;
        $suratKeteranganTentangKematian->user_id           = $user->id;
        $suratKeteranganTentangKematian->nik               = $request->nik;
        $suratKeteranganTentangKematian->nama              = $request->nama;
        $suratKeteranganTentangKematian->bin_binti         = $request->bin_binti;
        $suratKeteranganTentangKematian->jenis_kelamin     = $request->jenis_kelamin;
        $suratKeteranganTentangKematian->tempat_lahir      = $request->tempat_lahir;
        $suratKeteranganTentangKematian->tgl_lahir         = $request->tgl_lahir;
        $suratKeteranganTentangKematian->status_perkawinan = $request->status_perkawinan;
        $suratKeteranganTentangKematian->agama             = $request->agama;
        $suratKeteranganTentangKematian->pekerjaan         = $request->pekerjaan;
        $suratKeteranganTentangKematian->alamat            = $request->alamat;
        $suratKeteranganTentangKematian->tgl_meninggal     = $request->tgl_meninggal;
        $suratKeteranganTentangKematian->tempat_meninggal  = $request->tempat_meninggal;
        $suratKeteranganTentangKematian->status_id         = $status->id;
        $suratKeteranganTentangKematian->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganTentangKematian->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganTentangKematian.kodeSurat')->with([
            'suratKeteranganTentangKematian' => $suratKeteranganTentangKematian,
            'hariKadarluasaPengajuan'        => $hariKadarluasaPengajuan
            ]);
    }
}
