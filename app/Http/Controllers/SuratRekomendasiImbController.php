<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratRekomendasiImbRequest;
use App\Http\Requests\UpdateSuratRekomendasiImbRequest;
use App\Repositories\SuratRekomendasiImbRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratRekomendasiImb;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratRekomendasiImbRequest;

class SuratRekomendasiImbController extends AppBaseController
{
    /** @var  SuratRekomendasiImbRepository */
    private $suratRekomendasiImbRepository;

    public function __construct(SuratRekomendasiImbRepository $suratRekomendasiImbRepo)
    {
        $this->suratRekomendasiImbRepository = $suratRekomendasiImbRepo;
        $this->middleware('auth')->except('indexGuest','insertGuest');
    }

    /**
     * Display a listing of the SuratRekomendasiImb.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratRekomendasiImbs = $this->suratRekomendasiImbRepository->all();

        return view('surat_rekomendasi_imbs.index')
            ->with('suratRekomendasiImbs', $suratRekomendasiImbs);
    }

    /**
     * Show the form for creating a new SuratRekomendasiImb.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_rekomendasi_imbs.create');
    }

    /**
     * Store a newly created SuratRekomendasiImb in storage.
     *
     * @param CreateSuratRekomendasiImbRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratRekomendasiImbRequest $request)
    {
        $input = $request->all();

        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now()->format('Y-m-d');

        $input['kode']              = $randomNumber;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratRekomendasiImb = $this->suratRekomendasiImbRepository->create($input);

        Flash::success('Surat Rekomendasi Imb saved successfully.');

        return redirect(route('suratRekomendasiImbs.index'));
    }

    /**
     * Display the specified SuratRekomendasiImb.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratRekomendasiImb = $this->suratRekomendasiImbRepository->find($id);

        if (empty($suratRekomendasiImb)) {
            Flash::error('Surat Rekomendasi Imb not found');

            return redirect(route('suratRekomendasiImbs.index'));
        }

        return view('surat_rekomendasi_imbs.show')->with('suratRekomendasiImb', $suratRekomendasiImb);
    }

    /**
     * Show the form for editing the specified SuratRekomendasiImb.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratRekomendasiImb = $this->suratRekomendasiImbRepository->find($id);

        if (empty($suratRekomendasiImb)) {
            Flash::error('Surat Rekomendasi Imb not found');

            return redirect(route('suratRekomendasiImbs.index'));
        }

        return view('surat_rekomendasi_imbs.edit')->with('suratRekomendasiImb', $suratRekomendasiImb);
    }

    /**
     * Update the specified SuratRekomendasiImb in storage.
     *
     * @param int $id
     * @param UpdateSuratRekomendasiImbRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratRekomendasiImbRequest $request)
    {
        $suratRekomendasiImb = $this->suratRekomendasiImbRepository->find($id);

        if (empty($suratRekomendasiImb)) {
            Flash::error('Surat Rekomendasi Imb not found');

            return redirect(route('suratRekomendasiImbs.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratRekomendasiImb::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratRekomendasiImb->tanggal_terbit == NULL && $suratRekomendasiImb->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "503/ $nomorSurat /I/BANG";
            }
            
            $suratRekomendasiImb = $this->suratRekomendasiImbRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratRekomendasiImb = $this->suratRekomendasiImbRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Penghasilan updated successfully.');
    
            return redirect(route('suratRekomendasiImbs.index'));
        }
    }

    /**
     * Remove the specified SuratRekomendasiImb from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratRekomendasiImb = $this->suratRekomendasiImbRepository->find($id);

        if (empty($suratRekomendasiImb)) {
            Flash::error('Surat Rekomendasi Imb not found');

            return redirect(route('suratRekomendasiImbs.index'));
        }

        $this->suratRekomendasiImbRepository->delete($id);

        Flash::success('Surat Rekomendasi Imb deleted successfully.');

        return redirect(route('suratRekomendasiImbs.index'));
    }

    public function print($id)
    {
        $suratRekomendasiImb              = SuratRekomendasiImb::find($id);
        $suratRekomendasiImb->penghasilan = substr(strrev(chunk_split(strrev($suratRekomendasiImb->penghasilan), 3, '.')),1);
        $preferences                      = Preference::get()->keyBy('label')->toArray();
        $dateNow                          = Carbon::now()->format('d-m-Y');
        $pdf                              = \PDF::loadView('umum.suratRekomendasiImb.surat', ['suratRekomendasiImb' => $suratRekomendasiImb,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('umum.suratRekomendasiImb.index');
    }

    public function insertGuest(SuratRekomendasiImbRequest $request)
    {
        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now();
        
        $suratRekomendasiImb                         = new SuratRekomendasiImb;
        $suratRekomendasiImb->kode                   = $randomNumber;
        $suratRekomendasiImb->nik                    = $request->nik;
        $suratRekomendasiImb->nama                   = $request->nama;
        $suratRekomendasiImb->jenis_kelamin          = $request->jenis_kelamin;
        $suratRekomendasiImb->tempat_lahir           = $request->tempat_lahir;
        $suratRekomendasiImb->tgl_lahir              = $request->tgl_lahir;
        $suratRekomendasiImb->kewarganegaraan        = $request->kewarganegaraan;
        $suratRekomendasiImb->agama                  = $request->agama;
        $suratRekomendasiImb->status_perkawinan      = $request->status_perkawinan;
        $suratRekomendasiImb->pekerjaan              = $request->pekerjaan;
        $suratRekomendasiImb->alamat                 = $request->alamat;
        $suratRekomendasiImb->maksud                 = $request->maksud;
        $suratRekomendasiImb->keperluan              = $request->keperluan;
        $suratRekomendasiImb->alamat_bangunan        = $request->alamat_bangunan;
        $suratRekomendasiImb->luas_bangunan          = $request->luas_bangunan;
        $suratRekomendasiImb->luas_tanah             = $request->luas_tanah;
        $suratRekomendasiImb->nama_pemilik_tanah     = $request->nama_pemilik_tanah;
        $suratRekomendasiImb->nomor_sertifikat_tanah = $request->nomor_sertifikat_tanah;
        $suratRekomendasiImb->bangunan_terbuat_dari  = $request->bangunan_terbuat_dari;
        $suratRekomendasiImb->status_id              = $status->id;
        $suratRekomendasiImb->tanggal_pengajuan      = $dateNow->format('Y-m-d');
        $suratRekomendasiImb->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('umum.suratRekomendasiImb.kodeSurat')->with([
            'kode'                    => $randomNumber,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
