<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratPernyataanHibahRequest;
use App\Http\Requests\UpdateSuratPernyataanHibahRequest;
use App\Repositories\SuratPernyataanHibahRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratPernyataanHibah;
use App\Models\RiwayatKepemilikanHibah;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratPernyataanHibahRequest;

class SuratPernyataanHibahController extends AppBaseController
{
    /** @var  SuratPernyataanHibahRepository */
    private $suratPernyataanHibahRepository;

    public function __construct(SuratPernyataanHibahRepository $suratPernyataanHibahRepo)
    {
        $this->suratPernyataanHibahRepository = $suratPernyataanHibahRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratPernyataanHibah.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratPernyataanHibahs = $this->suratPernyataanHibahRepository->all();

        return view('surat_pernyataan_hibahs.index')
            ->with('suratPernyataanHibahs', $suratPernyataanHibahs);
    }

    /**
     * Show the form for creating a new SuratPernyataanHibah.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_pernyataan_hibahs.create');
    }

    /**
     * Store a newly created SuratPernyataanHibah in storage.
     *
     * @param CreateSuratPernyataanHibahRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratPernyataanHibahRequest $request)
    {
        $input = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();
        
        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;
        
        $suratPernyataanHibah = $this->suratPernyataanHibahRepository->create($input);
        
        if( !in_array(NULL,$input['alasan'],true) ){
            foreach($input['alasan'] as $key=>$value){
                $ahliWarisKeterenganAhliWaris         = new RiwayatKepemilikanHibah;
                $ahliWarisKeterenganAhliWaris->alasan = $value;
                $ahliWarisKeterenganAhliWaris->tahun  = $input['tahun_hibah'][$key];
                $ahliWarisKeterenganAhliWaris->nama   = $input['kepada_hibah'][$key];
                $suratPernyataanHibah->riwayatKepemilikanHibahs()->save($ahliWarisKeterenganAhliWaris);
            }
        }

        Flash::success('Surat Pernyataan Hibah saved successfully.');

        return redirect(route('suratPernyataanHibahs.index'));
    }

    /**
     * Display the specified SuratPernyataanHibah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratPernyataanHibah = $this->suratPernyataanHibahRepository->find($id);

        if (empty($suratPernyataanHibah)) {
            Flash::error('Surat Pernyataan Hibah not found');

            return redirect(route('suratPernyataanHibahs.index'));
        }

        return view('surat_pernyataan_hibahs.show')->with('suratPernyataanHibah', $suratPernyataanHibah);
    }

    /**
     * Show the form for editing the specified SuratPernyataanHibah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratPernyataanHibah = $this->suratPernyataanHibahRepository->find($id);
        if (empty($suratPernyataanHibah)) {
            Flash::error('Surat Pernyataan Hibah not found');

            return redirect(route('suratPernyataanHibahs.index'));
        }

        return view('surat_pernyataan_hibahs.edit')->with('suratPernyataanHibah', $suratPernyataanHibah);
    }

    /**
     * Update the specified SuratPernyataanHibah in storage.
     *
     * @param int $id
     * @param UpdateSuratPernyataanHibahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratPernyataanHibahRequest $request)
    {
        $suratPernyataanHibah = $this->suratPernyataanHibahRepository->find($id);

        if (empty($suratPernyataanHibah)) {
            Flash::error('Surat Pernyataan Hibah not found');

            return redirect(route('suratPernyataanHibahs.index'));
        }

        $allRequestData = $request->all();
        if($request->terima == 'Terima'){
            $status     = Status::where('status','Diterima')->first();
            $dateNow    = Carbon::now()->format('Y-m-d');
            $countSurat = SuratPernyataanHibah::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT); 
            $allRequestData['status_id']      = $status->id;
            if($suratPernyataanHibah->tanggal_terbit == NULL && $suratPernyataanHibah->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "593/ $nomorSurat / I /SED-E/2019";
            }
            
            $suratPernyataanHibah = $this->suratPernyataanHibahRepository->update($allRequestData, $id);
            $suratPernyataanHibah = SuratPernyataanHibah::find($id);
            $preferences          = Preference::get()->keyBy('label')->toArray();
            $dateNow              = Carbon::now()->format('d-m-Y');
            
            $riwayatKepemilikanHibahId = $suratPernyataanHibah->riwayatKepemilikanHibahs->pluck('id')->toArray();
            foreach($allRequestData['alasan'] as $key => $value){
                $carisuratPernyataanHibah = $suratPernyataanHibah->riwayatKepemilikanHibahs->find($key);
                if($carisuratPernyataanHibah){
                    $carisuratPernyataanHibah->update([
                        'alasan' => $value,
                        'tahun'  => $allRequestData['tahun_hibah'][$key],
                        'nama'   => $allRequestData['kepada_hibah'][$key]
                    ]);
                    $riwayatKepemilikanHibahId = array_diff($riwayatKepemilikanHibahId, [$key]);
                }else{
                    $ahliWarisKeterenganAhliWaris         = new RiwayatKepemilikanHibah;
                    $ahliWarisKeterenganAhliWaris->alasan = $value;
                    $ahliWarisKeterenganAhliWaris->tahun  = $input['tahun_hibah'][$key];
                    $ahliWarisKeterenganAhliWaris->nama   = $input['kepada_hibah'][$key];
                    $suratPernyataanHibah->riwayatKepemilikanHibahs()->save($ahliWarisKeterenganAhliWaris);
                }
            }
            foreach($riwayatKepemilikanHibahId as $key => $value){
                $carisuratPernyataanHibah = $suratPernyataanHibah->ahliWarisKeteranganAhliWaris->find($value)->delete();
            }

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratPernyataanHibah = $this->suratPernyataanHibahRepository->update($allRequestData, $id);

            Flash::success('Surat Pernyataan Hibah updated successfully.');

            return redirect(route('suratPernyataanHibahs.index'));
        }

        
    }

    /**
     * Remove the specified SuratPernyataanHibah from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratPernyataanHibah = $this->suratPernyataanHibahRepository->find($id);

        if (empty($suratPernyataanHibah)) {
            Flash::error('Surat Pernyataan Hibah not found');

            return redirect(route('suratPernyataanHibahs.index'));
        }

        $this->suratPernyataanHibahRepository->delete($id);

        Flash::success('Surat Pernyataan Hibah deleted successfully.');

        return redirect(route('suratPernyataanHibahs.index'));
    }

    public function print($id)
    {
        $suratPernyataanHibah = SuratPernyataanHibah::find($id);
        $preferences          = Preference::get()->keyBy('label')->toArray();
        $dateNow              = Carbon::now()->format('d-m-Y');

        $pdf = \PDF::loadView('khusus.suratPernyataanHibah.surat', ['suratPernyataanHibah' => $suratPernyataanHibah,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratPernyataanHibah.index');
    }

    public function insertGuest(SuratPernyataanHibahRequest $request)
    {
        $input   = $request->all();
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();
        
        $suratPernyataanHibah                         = new SuratPernyataanHibah;
        $suratPernyataanHibah->user_id                = $user->id;
        $suratPernyataanHibah->nik                    = $request->nik;
        $suratPernyataanHibah->nama                   = $request->nama;
        $suratPernyataanHibah->tempat_lahir           = $request->tempat_lahir;
        $suratPernyataanHibah->tgl_lahir              = $request->tgl_lahir;
        $suratPernyataanHibah->kewarganegaraan        = $request->kewarganegaraan;
        $suratPernyataanHibah->agama                  = $request->agama;
        $suratPernyataanHibah->pekerjaan              = $request->pekerjaan;
        $suratPernyataanHibah->alamat                 = $request->alamat;
        $suratPernyataanHibah->letak_tanah            = $request->letak_tanah;
        $suratPernyataanHibah->lebar                  = $request->lebar;
        $suratPernyataanHibah->panjang                = $request->panjang;
        $suratPernyataanHibah->batas_utara            = $request->batas_utara;
        $suratPernyataanHibah->batas_timur            = $request->batas_timur;
        $suratPernyataanHibah->batas_selatan          = $request->batas_selatan;
        $suratPernyataanHibah->batas_barat            = $request->batas_barat;
        $suratPernyataanHibah->sejarah_dikuasai_oleh  = $request->sejarah_dikuasai_oleh;
        $suratPernyataanHibah->sejarah_tahun_dikuasai = $request->sejarah_tahun_dikuasai;
        $suratPernyataanHibah->dihibahkan_kepada      = $request->dihibahkan_kepada;
        $suratPernyataanHibah->untuk_dijadikan        = $request->untuk_dijadikan;
        $suratPernyataanHibah->jabatan_saksi_pertama  = $request->jabatan_saksi_pertama;
        $suratPernyataanHibah->nama_saksi_pertama     = $request->nama_saksi_pertama;
        $suratPernyataanHibah->jabatan_saksi_kedua    = $request->jabatan_saksi_kedua;
        $suratPernyataanHibah->nama_saksi_kedua       = $request->nama_saksi_kedua;
        $suratPernyataanHibah->status_id              = $status->id;
        $suratPernyataanHibah->tanggal_pengajuan      = $dateNow->format('Y-m-d');
        $suratPernyataanHibah->save();

        if( !in_array(NULL,$input['alasan'],true) ){
            foreach($input['alasan'] as $key=>$value){
                $ahliWarisKeterenganAhliWaris         = new RiwayatKepemilikanHibah;
                $ahliWarisKeterenganAhliWaris->alasan = $value;
                $ahliWarisKeterenganAhliWaris->tahun  = $input['tahun_hibah'][$key];
                $ahliWarisKeterenganAhliWaris->nama   = $input['kepada_hibah'][$key];
                $suratPernyataanHibah->riwayatKepemilikanHibahs()->save($ahliWarisKeterenganAhliWaris);
            }
        }

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratPernyataanHibah.kodeSurat')->with([
            'suratPernyataanHibah' => $suratPernyataanHibah,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
