<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganKelahiranRequest;
use App\Http\Requests\UpdateSuratKeteranganKelahiranRequest;
use App\Repositories\SuratKeteranganKelahiranRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganKelahiran;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganKelahiranRequest;


class SuratKeteranganKelahiranController extends AppBaseController
{
    /** @var  SuratKeteranganKelahiranRepository */
    private $suratKeteranganKelahiranRepository;

    public function __construct(SuratKeteranganKelahiranRepository $suratKeteranganKelahiranRepo)
    {
        $this->suratKeteranganKelahiranRepository = $suratKeteranganKelahiranRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganKelahiran.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganKelahirans = $this->suratKeteranganKelahiranRepository->all();

        return view('surat_keterangan_kelahirans.index')
            ->with('suratKeteranganKelahirans', $suratKeteranganKelahirans);
    }

    /**
     * Show the form for creating a new SuratKeteranganKelahiran.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_kelahirans.create');
    }

    /**
     * Store a newly created SuratKeteranganKelahiran in storage.
     *
     * @param CreateSuratKeteranganKelahiranRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganKelahiranRequest $request)
    {
        $input = $request->all();

        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganKelahiran = $this->suratKeteranganKelahiranRepository->create($input);

        Flash::success('Surat Keterangan Kelahiran saved successfully.');

        return redirect(route('suratKeteranganKelahirans.index'));
    }

    /**
     * Display the specified SuratKeteranganKelahiran.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganKelahiran = $this->suratKeteranganKelahiranRepository->find($id);

        if (empty($suratKeteranganKelahiran)) {
            Flash::error('Surat Keterangan Kelahiran not found');

            return redirect(route('suratKeteranganKelahirans.index'));
        }

        return view('surat_keterangan_kelahirans.show')->with('suratKeteranganKelahiran', $suratKeteranganKelahiran);
    }

    /**
     * Show the form for editing the specified SuratKeteranganKelahiran.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganKelahiran = $this->suratKeteranganKelahiranRepository->find($id);

        if (empty($suratKeteranganKelahiran)) {
            Flash::error('Surat Keterangan Kelahiran not found');

            return redirect(route('suratKeteranganKelahirans.index'));
        }

        return view('surat_keterangan_kelahirans.edit')->with('suratKeteranganKelahiran', $suratKeteranganKelahiran);
    }

    /**
     * Update the specified SuratKeteranganKelahiran in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganKelahiranRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganKelahiranRequest $request)
    {
        $suratKeteranganKelahiran = $this->suratKeteranganKelahiranRepository->find($id);

        if (empty($suratKeteranganKelahiran)) {
            Flash::error('Surat Keterangan Kelahiran not found');

            return redirect(route('suratKeteranganKelahirans.index'));
        }

        
        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganKelahiran::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganKelahiran->tanggal_terbit == NULL && $suratKeteranganKelahiran->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "474.2/ $nomorSurat / I / PEM";
            }
            
            $suratKeteranganKelahiran = $this->suratKeteranganKelahiranRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganKelahiran = $this->suratKeteranganKelahiranRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Kelahiran updated successfully.');
    
            return redirect(route('suratKeteranganKelahirans.index'));
        }

       
    }

    /**
     * Remove the specified SuratKeteranganKelahiran from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganKelahiran = $this->suratKeteranganKelahiranRepository->find($id);

        if (empty($suratKeteranganKelahiran)) {
            Flash::error('Surat Keterangan Kelahiran not found');

            return redirect(route('suratKeteranganKelahirans.index'));
        }

        $this->suratKeteranganKelahiranRepository->delete($id);

        Flash::success('Surat Keterangan Kelahiran deleted successfully.');

        return redirect(route('suratKeteranganKelahirans.index'));
    }

    public function print($id)
    {
        $SuratKeteranganKelahiran = SuratKeteranganKelahiran::find($id);
        $preferences              = Preference::get()->keyBy('label')->toArray();
        $dateNow                  = Carbon::now()->format('d-m-Y');
        $pdf                      = \PDF::loadView('khusus.suratKeteranganKelahiran.surat', ['SuratKeteranganKelahiran' => $SuratKeteranganKelahiran,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganKelahiran.index');
    }

    public function insertGuest(SuratKeteranganPajakRequest $request)
    {
        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now();
        
        $suratKeteranganPajak                    = new SuratKeteranganPajak;
        $suratKeteranganPajak->kode              = $randomNumber;
        $suratKeteranganPajak->nik               = $request->nik;
        $suratKeteranganPajak->nama              = $request->nama;
        $suratKeteranganPajak->alamat            = $request->alamat;
        $suratKeteranganPajak->jenis_kelamin     = $request->jenis_kelamin;
        $suratKeteranganPajak->tempat_lahir      = $request->tempat_lahir;
        $suratKeteranganPajak->tgl_lahir         = $request->tgl_lahir;
        $suratKeteranganPajak->kewarganegaraan   = $request->kewarganegaraan;
        $suratKeteranganPajak->agama             = $request->agama;
        $suratKeteranganPajak->status_perkawinan = $request->status_perkawinan;
        $suratKeteranganPajak->pekerjaan         = $request->pekerjaan;
        $suratKeteranganPajak->status_id         = $status->id;
        $suratKeteranganPajak->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganPajak->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('umum.suratKeteranganPajak.kodeSurat')->with([
            'kode'                    => $randomNumber,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
