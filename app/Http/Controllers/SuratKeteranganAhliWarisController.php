<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganAhliWarisRequest;
use App\Http\Requests\UpdateSuratKeteranganAhliWarisRequest;
use App\Repositories\SuratKeteranganAhliWarisRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganAhliWaris;
use App\Models\AhliWarisKeterenganAhliWaris;
use App\Models\SaksiKeteranganAhliWaris;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganAhliWarisRequest;

class SuratKeteranganAhliWarisController extends AppBaseController
{
    /** @var  SuratKeteranganAhliWarisRepository */
    private $suratKeteranganAhliWarisRepository;

    public function __construct(SuratKeteranganAhliWarisRepository $suratKeteranganAhliWarisRepo)
    {
        $this->suratKeteranganAhliWarisRepository = $suratKeteranganAhliWarisRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganAhliWaris.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganAhliWaris = $this->suratKeteranganAhliWarisRepository->all();

        return view('surat_keterangan_ahli_waris.index')
            ->with('suratKeteranganAhliWaris', $suratKeteranganAhliWaris);
    }

    /**
     * Show the form for creating a new SuratKeteranganAhliWaris.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_ahli_waris.create');
    }

    /**
     * Store a newly created SuratKeteranganAhliWaris in storage.
     *
     * @param CreateSuratKeteranganAhliWarisRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganAhliWarisRequest $request)
    {
        $input   = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();
        
        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;
        
        $suratKeteranganAhliWaris = $this->suratKeteranganAhliWarisRepository->create($input);
        
        $saksiKeteranganAhliWaris       = new SaksiKeteranganAhliWaris;
        $saksiKeteranganAhliWaris->nama = $input['saksi_pertama'];
        $suratKeteranganAhliWaris->saksiKeteranganAhliWaris()->save($saksiKeteranganAhliWaris);
        
        $saksiKeteranganAhliWaris       = new SaksiKeteranganAhliWaris;
        $saksiKeteranganAhliWaris->nama = $input['saksi_kedua'];
        $suratKeteranganAhliWaris->saksiKeteranganAhliWaris()->save($saksiKeteranganAhliWaris);

        foreach($input['ahli_waris'] as $key=>$value){
            $ahliWarisKeterenganAhliWaris           = new AhliWarisKeterenganAhliWaris;
            $ahliWarisKeterenganAhliWaris->nama     = $value;
            $ahliWarisKeterenganAhliWaris->hubungan = $input['hubungan_ahli_waris'][$key];
            $suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris()->save($ahliWarisKeterenganAhliWaris);
        }

        Flash::success('Surat Keterangan Ahli Waris saved successfully.');

        return redirect(route('suratKeteranganAhliWaris.index'));
    }

    /**
     * Display the specified SuratKeteranganAhliWaris.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganAhliWaris = $this->suratKeteranganAhliWarisRepository->find($id);

        if (empty($suratKeteranganAhliWaris)) {
            Flash::error('Surat Keterangan Ahli Waris not found');

            return redirect(route('suratKeteranganAhliWaris.index'));
        }

        return view('surat_keterangan_ahli_waris.show')->with('suratKeteranganAhliWaris', $suratKeteranganAhliWaris);
    }

    /**
     * Show the form for editing the specified SuratKeteranganAhliWaris.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganAhliWaris = $this->suratKeteranganAhliWarisRepository->find($id);

        if (empty($suratKeteranganAhliWaris)) {
            Flash::error('Surat Keterangan Ahli Waris not found');

            return redirect(route('suratKeteranganAhliWaris.index'));
        }

        return view('surat_keterangan_ahli_waris.edit')->with('suratKeteranganAhliWaris', $suratKeteranganAhliWaris);
    }

    /**
     * Update the specified SuratKeteranganAhliWaris in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganAhliWarisRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganAhliWarisRequest $request)
    {
        $suratKeteranganAhliWaris = $this->suratKeteranganAhliWarisRepository->find($id);

        if (empty($suratKeteranganAhliWaris)) {
            Flash::error('Surat Keterangan Ahli Waris not found');

            return redirect(route('suratKeteranganAhliWaris.index'));
        }

        $allRequestData          = $request->all();
        
        if($request->terima == 'Terima'){
            $status     = Status::where('status','Diterima')->first();
            $dateNow    = Carbon::now()->format('Y-m-d');
            $countSurat = SuratKeteranganAhliWaris::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT); 
            $allRequestData['status_id']      = $status->id;
            if($suratKeteranganAhliWaris->tanggal_terbit == NULL && $suratKeteranganAhliWaris->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "593/ $nomorSurat / I /SED-E/2019";
            }
            
            $suratKeteranganAhliWaris = $this->suratKeteranganAhliWarisRepository->update($allRequestData, $id);
            $suratKeteranganAhliWaris = SuratKeteranganAhliWaris::find($id);
            $preferences             = Preference::get()->keyBy('label')->toArray();
            $dateNow                 = Carbon::now()->format('d-m-Y');
            
            $suratKeteranganAhliWaris->saksiKeteranganAhliWaris->find($allRequestData['saksi_pertama_id'])->update([
                'nama' => $allRequestData['saksi_pertama'],
            ]);

            $suratKeteranganAhliWaris->saksiKeteranganAhliWaris->find($allRequestData['saksi_kedua_id'])->update([
                'nama' => $allRequestData['saksi_kedua'],
            ]);
            
            $ahliWarisId = $suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris->pluck('id')->toArray();
            foreach($allRequestData['ahli_waris'] as $key => $value){
                $cariSuratKeteranganAhliWaris = $suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris->find($key);
                if($cariSuratKeteranganAhliWaris){
                    $cariSuratKeteranganAhliWaris->update([
                        'nama'     => $value,
                        'hubungan' => $allRequestData['hubungan_ahli_waris'][$key]
                    ]);
                    $ahliWarisId = array_diff($ahliWarisId, [$key]);
                }else{
                    $ahliWarisKeterenganAhliWaris           = new AhliWarisKeterenganAhliWaris;
                    $ahliWarisKeterenganAhliWaris->nama     = $value;
                    $ahliWarisKeterenganAhliWaris->hubungan = $allRequestData['hubungan_ahli_waris'][$key];
                    $suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris()->save($ahliWarisKeterenganAhliWaris);
                }
            }
            foreach($ahliWarisId as $key => $value){
                $cariSuratKeteranganAhliWaris = $suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris->find($value)->delete();
            }

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;
            $suratKeteranganAhliWaris = $this->suratKeteranganAhliWarisRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Ahli Waris updated successfully.');

            return redirect(route('suratKeteranganAhliWaris.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganAhliWaris from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganAhliWaris = $this->suratKeteranganAhliWarisRepository->find($id);

        if (empty($suratKeteranganAhliWaris)) {
            Flash::error('Surat Keterangan Ahli Waris not found');

            return redirect(route('suratKeteranganAhliWaris.index'));
        }

        $this->suratKeteranganAhliWarisRepository->delete($id);

        Flash::success('Surat Keterangan Ahli Waris deleted successfully.');

        return redirect(route('suratKeteranganAhliWaris.index'));
    }

    public function print($id)
    {
        $suratKeteranganAhliWaris = SuratKeteranganAhliWaris::find($id);
        $preferences              = Preference::get()->keyBy('label')->toArray();
        $dateNow                  = Carbon::now()->format('d-m-Y');
        $pdf                      = \PDF::loadView('khusus.suratKeteranganAhliWaris.surat', ['suratKeteranganAhliWaris' => $suratKeteranganAhliWaris,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganAhliWaris.index');
    }

    public function insertGuest(SuratKeteranganAhliWarisRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();
        
        $suratKeteranganAhliWaris                    = new SuratKeteranganAhliWaris;
        $suratKeteranganAhliWaris->user_id           = $user->id;
        $suratKeteranganAhliWaris->nik               = $request->nik;
        $suratKeteranganAhliWaris->nama              = $request->nama;
        $suratKeteranganAhliWaris->tempat_lahir      = $request->tempat_lahir;
        $suratKeteranganAhliWaris->tgl_lahir         = $request->tgl_lahir;
        $suratKeteranganAhliWaris->jenis_kelamin     = $request->jenis_kelamin;
        $suratKeteranganAhliWaris->kewarganegaraan   = $request->kewarganegaraan;
        $suratKeteranganAhliWaris->agama             = $request->agama;
        $suratKeteranganAhliWaris->pekerjaan         = $request->pekerjaan;
        $suratKeteranganAhliWaris->maksud            = $request->maksud;
        $suratKeteranganAhliWaris->status_id         = $status->id;
        $suratKeteranganAhliWaris->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganAhliWaris->save();

        $saksiKeteranganAhliWaris       = new SaksiKeteranganAhliWaris;
        $saksiKeteranganAhliWaris->nama = $request->saksi_pertama;
        $suratKeteranganAhliWaris->saksiKeteranganAhliWaris()->save($saksiKeteranganAhliWaris);
        
        $saksiKeteranganAhliWaris       = new SaksiKeteranganAhliWaris;
        $saksiKeteranganAhliWaris->nama = $request->saksi_kedua;
        $suratKeteranganAhliWaris->saksiKeteranganAhliWaris()->save($saksiKeteranganAhliWaris);

        foreach($request->ahli_waris as $key=>$value){
            $ahliWarisKeterenganAhliWaris           = new AhliWarisKeterenganAhliWaris;
            $ahliWarisKeterenganAhliWaris->nama     = $value;
            $ahliWarisKeterenganAhliWaris->hubungan = $request->hubungan_ahli_waris[$key];
            $suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris()->save($ahliWarisKeterenganAhliWaris);
        }
        
        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganAhliWaris.kodeSurat')->with([
            'suratKeteranganAhliWaris' => $suratKeteranganAhliWaris,
            'hariKadarluasaPengajuan'  => $hariKadarluasaPengajuan
            ]);
    }
}
