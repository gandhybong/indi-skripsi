<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePreferenceRequest;
use App\Http\Requests\UpdatePreferenceRequest;
use App\Http\Requests\UpdateLogoRequest;
use App\Repositories\PreferenceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Preference;

class PreferenceController extends AppBaseController
{
    /** @var  PreferenceRepository */
    private $preferenceRepository;

    public function __construct(PreferenceRepository $preferenceRepo)
    {
        $this->preferenceRepository = $preferenceRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Preference.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $preferences = Preference::get()->keyBy('label')->toArray();

        return view('preferences.index')
            ->with('preferences', $preferences);
    }

    public function store(Request $request)
    {
        $input = $request->except(['_token','submit']);
        foreach($input as $label => $value){
            $preference = Preference::where('label',str_replace("_"," ", $label))->first();

            $preference->value = $value;

            $preference->save();
        }

        $preferences = Preference::get()->keyBy('label')->toArray();
        Flash::success('Preference saved successfully.');
        return view('preferences.index')
            ->with('preferences', $preferences);
    }

    public function updateLogo(UpdateLogoRequest $request)
    {
        $publicPath = public_path('images/assets');
        $imageName = md5(date('Y-m-d H:i:s')) . '.' . $request->logo->getClientOriginalExtension();

        $request->logo->move($publicPath, $imageName);
        
        Preference::logo()->update([
            'value' => 'images/assets/' . $imageName,
        ]);

        Flash::success('Logo berhasil diperbaharui.');

        return redirect()->route('preferences.index');
    }

    public function destroyLogo()
    {
        Preference::logo()->update([
            'value' => '',
        ]);

        Flash::success('Logo berhasil dihapus.');

        return redirect()->route('preferences.index');
    }
}
