<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganPenghasilanRequest;
use App\Http\Requests\UpdateSuratKeteranganPenghasilanRequest;
use App\Repositories\SuratKeteranganPenghasilanRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganPenghasilan;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganPenghasilanRequest;


class SuratKeteranganPenghasilanController extends AppBaseController
{
    /** @var  SuratKeteranganPenghasilanRepository */
    private $suratKeteranganPenghasilanRepository;

    public function __construct(SuratKeteranganPenghasilanRepository $suratKeteranganPenghasilanRepo)
    {
        $this->suratKeteranganPenghasilanRepository = $suratKeteranganPenghasilanRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganPenghasilan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganPenghasilans = $this->suratKeteranganPenghasilanRepository->all();

        return view('surat_keterangan_penghasilans.index')
            ->with('suratKeteranganPenghasilans', $suratKeteranganPenghasilans);
    }

    /**
     * Show the form for creating a new SuratKeteranganPenghasilan.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_penghasilans.create');
    }

    /**
     * Store a newly created SuratKeteranganPenghasilan in storage.
     *
     * @param CreateSuratKeteranganPenghasilanRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganPenghasilanRequest $request)
    {
        $input   = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;
        
        $suratKeteranganPenghasilan = $this->suratKeteranganPenghasilanRepository->create($input);

        Flash::success('Surat Keterangan Penghasilan saved successfully.');

        return redirect(route('suratKeteranganPenghasilans.index'));
    }

    /**
     * Display the specified SuratKeteranganPenghasilan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganPenghasilan = $this->suratKeteranganPenghasilanRepository->find($id);

        if (empty($suratKeteranganPenghasilan)) {
            Flash::error('Surat Keterangan Penghasilan not found');

            return redirect(route('suratKeteranganPenghasilans.index'));
        }

        return view('surat_keterangan_penghasilans.show')->with('suratKeteranganPenghasilan', $suratKeteranganPenghasilan);
    }

    /**
     * Show the form for editing the specified SuratKeteranganPenghasilan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganPenghasilan = $this->suratKeteranganPenghasilanRepository->find($id);

        if (empty($suratKeteranganPenghasilan)) {
            Flash::error('Surat Keterangan Penghasilan not found');

            return redirect(route('suratKeteranganPenghasilans.index'));
        }

        return view('surat_keterangan_penghasilans.edit')->with('suratKeteranganPenghasilan', $suratKeteranganPenghasilan);
    }

    /**
     * Update the specified SuratKeteranganPenghasilan in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganPenghasilanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganPenghasilanRequest $request)
    {
        $suratKeteranganPenghasilan = $this->suratKeteranganPenghasilanRepository->find($id);

        if (empty($suratKeteranganPenghasilan)) {
            Flash::error('Surat Keterangan Penghasilan not found');

            return redirect(route('suratKeteranganPenghasilans.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganPenghasilan::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganPenghasilan->tanggal_terbit == NULL && $suratKeteranganPenghasilan->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "474.3/ $nomorSurat /I/PEM";
            }
            
            $suratKeteranganPenghasilan = $this->suratKeteranganPenghasilanRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganPenghasilan = $this->suratKeteranganPenghasilanRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Penghasilan updated successfully.');
    
            return redirect(route('suratKeteranganPenghasilans.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganPenghasilan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganPenghasilan = $this->suratKeteranganPenghasilanRepository->find($id);

        if (empty($suratKeteranganPenghasilan)) {
            Flash::error('Surat Keterangan Penghasilan not found');

            return redirect(route('suratKeteranganPenghasilans.index'));
        }

        $this->suratKeteranganPenghasilanRepository->delete($id);

        Flash::success('Surat Keterangan Penghasilan deleted successfully.');

        return redirect(route('suratKeteranganPenghasilans.index'));
    }

    public function print($id)
    {
        $suratKeteranganPenghasilan              = SuratKeteranganPenghasilan::find($id);
        $suratKeteranganPenghasilan->penghasilan = substr(strrev(chunk_split(strrev($suratKeteranganPenghasilan->penghasilan), 3, '.')),1);
        
        $preferences                             = Preference::get()->keyBy('label')->toArray();
        $dateNow                                 = Carbon::now()->format('d-m-Y');
        $pdf                                     = \PDF::loadView('khusus.suratKeteranganPenghasilan.surat', ['suratKeteranganPenghasilan' => $suratKeteranganPenghasilan,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganPenghasilan.index');
    }

    public function insertGuest(SuratKeteranganPenghasilanRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();
        
        $suratKeteranganPenghasilan                    = new SuratKeteranganPenghasilan;
        $suratKeteranganPenghasilan->user_id           = $user->id;
        $suratKeteranganPenghasilan->nik               = $request->nik;
        $suratKeteranganPenghasilan->nama              = $request->nama;
        $suratKeteranganPenghasilan->jenis_kelamin     = $request->jenis_kelamin;
        $suratKeteranganPenghasilan->tempat_lahir      = $request->tempat_lahir;
        $suratKeteranganPenghasilan->tgl_lahir         = $request->tgl_lahir;
        $suratKeteranganPenghasilan->kewarganegaraan   = $request->kewarganegaraan;
        $suratKeteranganPenghasilan->pekerjaan         = $request->pekerjaan;
        $suratKeteranganPenghasilan->agama             = $request->agama;
        $suratKeteranganPenghasilan->status_perkawinan = $request->status_perkawinan;
        $suratKeteranganPenghasilan->alamat            = $request->alamat;
        $suratKeteranganPenghasilan->penghasilan       = $request->penghasilan;
        $suratKeteranganPenghasilan->maksud            = $request->maksud;
        $suratKeteranganPenghasilan->status_id         = $status->id;
        $suratKeteranganPenghasilan->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganPenghasilan->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganPenghasilan.kodeSurat')->with([
            'suratKeteranganPenghasilan' => $suratKeteranganPenghasilan,
            'hariKadarluasaPengajuan'    => $hariKadarluasaPengajuan
            ]);
    }
}
