<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganBedaNamaRequest;
use App\Http\Requests\UpdateSuratKeteranganBedaNamaRequest;
use App\Repositories\SuratKeteranganBedaNamaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganBedaNama;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganBedaNamaRequest;

class SuratKeteranganBedaNamaController extends AppBaseController
{
    /** @var  SuratKeteranganBedaNamaRepository */
    private $suratKeteranganBedaNamaRepository;

    public function __construct(SuratKeteranganBedaNamaRepository $suratKeteranganBedaNamaRepo)
    {
        $this->suratKeteranganBedaNamaRepository = $suratKeteranganBedaNamaRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganBedaNama.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganBedaNamas = $this->suratKeteranganBedaNamaRepository->all();

        return view('surat_keterangan_beda_namas.index')
            ->with('suratKeteranganBedaNamas', $suratKeteranganBedaNamas);
    }

    /**
     * Show the form for creating a new SuratKeteranganBedaNama.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_beda_namas.create');
    }

    /**
     * Store a newly created SuratKeteranganBedaNama in storage.
     *
     * @param CreateSuratKeteranganBedaNamaRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganBedaNamaRequest $request)
    {
        $input   = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();
        
        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganBedaNama = $this->suratKeteranganBedaNamaRepository->create($input);

        Flash::success('Surat Keterangan Beda Nama saved successfully.');

        return redirect(route('suratKeteranganBedaNamas.index'));
    }

    /**
     * Display the specified SuratKeteranganBedaNama.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganBedaNama = $this->suratKeteranganBedaNamaRepository->find($id);

        if (empty($suratKeteranganBedaNama)) {
            Flash::error('Surat Keterangan Beda Nama not found');

            return redirect(route('suratKeteranganBedaNamas.index'));
        }

        return view('surat_keterangan_beda_namas.show')->with('suratKeteranganBedaNama', $suratKeteranganBedaNama);
    }

    /**
     * Show the form for editing the specified SuratKeteranganBedaNama.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganBedaNama = $this->suratKeteranganBedaNamaRepository->find($id);

        if (empty($suratKeteranganBedaNama)) {
            Flash::error('Surat Keterangan Beda Nama not found');

            return redirect(route('suratKeteranganBedaNamas.index'));
        }

        return view('surat_keterangan_beda_namas.edit')->with('suratKeteranganBedaNama', $suratKeteranganBedaNama);
    }

    /**
     * Update the specified SuratKeteranganBedaNama in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganBedaNamaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganBedaNamaRequest $request)
    {
        $suratKeteranganBedaNama = $this->suratKeteranganBedaNamaRepository->find($id);

        if (empty($suratKeteranganBedaNama)) {
            Flash::error('Surat Keterangan Beda Nama not found');

            return redirect(route('suratKeteranganBedaNamas.index'));
        }

        $allRequestData          = $request->all();
        
        if($request->terima == 'Terima'){
            $status     = Status::where('status','Diterima')->first();
            $dateNow    = Carbon::now()->format('Y-m-d');
            $countSurat = SuratKeteranganBedaNama::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT); 
            $allRequestData['status_id']      = $status->id;
            if($suratKeteranganBedaNama->tanggal_terbit == NULL && $suratKeteranganBedaNama->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "593/ $nomorSurat / I /SED-E/2019";
            }
            
            $suratKeteranganBedaNama = $this->suratKeteranganBedaNamaRepository->update($allRequestData, $id);
            
            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;
    
            $suratKeteranganBedaNama = $this->suratKeteranganBedaNamaRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Beda Nama updated successfully.');

            return redirect(route('suratKeteranganBedaNamas.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganBedaNama from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganBedaNama = $this->suratKeteranganBedaNamaRepository->find($id);

        if (empty($suratKeteranganBedaNama)) {
            Flash::error('Surat Keterangan Beda Nama not found');

            return redirect(route('suratKeteranganBedaNamas.index'));
        }

        $this->suratKeteranganBedaNamaRepository->delete($id);

        Flash::success('Surat Keterangan Beda Nama deleted successfully.');

        return redirect(route('suratKeteranganBedaNamas.index'));
    }

    public function print($id)
    {
        $suratKeteranganBedaNama = SuratKeteranganBedaNama::find($id);
        $preferences             = Preference::get()->keyBy('label')->toArray();
        $dateNow                 = Carbon::now()->format('d-m-Y');

        $pdf = \PDF::loadView('khusus.suratKeteranganBedaNama.surat', ['suratKeteranganBedaNama' => $suratKeteranganBedaNama,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganBedaNama.index');
    }

    public function insertGuest(SuratKeteranganBedaNamaRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $suratKeteranganBedaNama                    = new SuratKeteranganBedaNama;
        $suratKeteranganBedaNama->user_id           = $user->id;
        $suratKeteranganBedaNama->nik               = $request->nik;
        $suratKeteranganBedaNama->nama              = $request->nama;
        $suratKeteranganBedaNama->nama_kedua        = $request->nama_kedua;
        $suratKeteranganBedaNama->tempat_lahir      = $request->tempat_lahir;
        $suratKeteranganBedaNama->tgl_lahir         = $request->tgl_lahir;
        $suratKeteranganBedaNama->jenis_kelamin     = $request->jenis_kelamin;
        $suratKeteranganBedaNama->status_perkawinan = $request->status_perkawinan;
        $suratKeteranganBedaNama->agama             = $request->agama;
        $suratKeteranganBedaNama->pekerjaan         = $request->pekerjaan;
        $suratKeteranganBedaNama->alamat            = $request->alamat;
        $suratKeteranganBedaNama->tertera_di        = $request->tertera_di;
        $suratKeteranganBedaNama->maksud            = $request->maksud;
        $suratKeteranganBedaNama->status_id         = $status->id;
        $suratKeteranganBedaNama->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganBedaNama->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganBedaNama.kodeSurat')->with([
            'suratKeteranganBedaNama' => $suratKeteranganBedaNama,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
