<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganPajakRequest;
use App\Http\Requests\UpdateSuratKeteranganPajakRequest;
use App\Repositories\SuratKeteranganPajakRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganPajak;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganPajakRequest;

class SuratKeteranganPajakController extends AppBaseController
{
    /** @var  SuratKeteranganPajakRepository */
    private $suratKeteranganPajakRepository;

    public function __construct(SuratKeteranganPajakRepository $suratKeteranganPajakRepo)
    {
        $this->suratKeteranganPajakRepository = $suratKeteranganPajakRepo;
        $this->middleware('auth')->except('indexGuest','insertGuest');
    }

    /**
     * Display a listing of the SuratKeteranganPajak.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganPajaks = $this->suratKeteranganPajakRepository->all();

        return view('surat_keterangan_pajaks.index')
            ->with('suratKeteranganPajaks', $suratKeteranganPajaks);
    }

    /**
     * Show the form for creating a new SuratKeteranganPajak.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_pajaks.create');
    }

    /**
     * Store a newly created SuratKeteranganPajak in storage.
     *
     * @param CreateSuratKeteranganPajakRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganPajakRequest $request)
    {
        $input = $request->all();

        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now()->format('Y-m-d');

        $input['status_id']         = $status->id;
        $input['kode']              = $randomNumber;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganPajak = $this->suratKeteranganPajakRepository->create($input);

        Flash::success('Surat Keterangan Pajak saved successfully.');

        return redirect(route('suratKeteranganPajaks.index'));
    }

    /**
     * Display the specified SuratKeteranganPajak.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganPajak = $this->suratKeteranganPajakRepository->find($id);

        if (empty($suratKeteranganPajak)) {
            Flash::error('Surat Keterangan Pajak not found');

            return redirect(route('suratKeteranganPajaks.index'));
        }

        return view('surat_keterangan_pajaks.show')->with('suratKeteranganPajak', $suratKeteranganPajak);
    }

    /**
     * Show the form for editing the specified SuratKeteranganPajak.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganPajak = $this->suratKeteranganPajakRepository->find($id);

        if (empty($suratKeteranganPajak)) {
            Flash::error('Surat Keterangan Pajak not found');

            return redirect(route('suratKeteranganPajaks.index'));
        }
        return view('surat_keterangan_pajaks.edit')->with('suratKeteranganPajak', $suratKeteranganPajak);
    }

    /**
     * Update the specified SuratKeteranganPajak in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganPajakRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganPajakRequest $request)
    {
        $suratKeteranganPajak = $this->suratKeteranganPajakRepository->find($id);

        if (empty($suratKeteranganPajak)) {
            Flash::error('Surat Keterangan Pajak not found');

            return redirect(route('suratKeteranganPajaks.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganPajak::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            if($suratKeteranganPajak->tanggal_terbit == NULL && $suratKeteranganPajak->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "593. 1/ $nomorSurat / I / PEM";
            }
            $allRequestData['status_id'] = $status->id;
            $suratKeteranganDomisili = $this->suratKeteranganPajakRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;
            $suratKeteranganPajak = $this->suratKeteranganPajakRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Pajak updated successfully.');

            return redirect(route('suratKeteranganPajaks.index'));
        }
        
    }

    /**
     * Remove the specified SuratKeteranganPajak from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganPajak = $this->suratKeteranganPajakRepository->find($id);

        if (empty($suratKeteranganPajak)) {
            Flash::error('Surat Keterangan Pajak not found');

            return redirect(route('suratKeteranganPajaks.index'));
        }

        $this->suratKeteranganPajakRepository->delete($id);

        Flash::success('Surat Keterangan Pajak deleted successfully.');

        return redirect(route('suratKeteranganPajaks.index'));
    }

    public function print($id)
    {
        $SuratKeteranganPajak = SuratKeteranganPajak::find($id);
        $preferences          = Preference::get()->keyBy('label')->toArray();
        $dateNow              = Carbon::now()->format('d-m-Y');
        $pdf                  = \PDF::loadView('umum.suratKeteranganPajak.surat', ['SuratKeteranganPajak' => $SuratKeteranganPajak,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('umum.suratKeteranganPajak.index');
    }

    public function insertGuest(SuratKeteranganPajakRequest $request)
    {
        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now();
        
        $suratKeteranganPajak                    = new SuratKeteranganPajak;
        $suratKeteranganPajak->kode              = $randomNumber;
        $suratKeteranganPajak->nik               = $request->nik;
        $suratKeteranganPajak->nama              = $request->nama;
        $suratKeteranganPajak->alamat            = $request->alamat;
        $suratKeteranganPajak->jenis_kelamin     = $request->jenis_kelamin;
        $suratKeteranganPajak->tempat_lahir      = $request->tempat_lahir;
        $suratKeteranganPajak->tgl_lahir         = $request->tgl_lahir;
        $suratKeteranganPajak->kewarganegaraan   = $request->kewarganegaraan;
        $suratKeteranganPajak->agama             = $request->agama;
        $suratKeteranganPajak->status_perkawinan = $request->status_perkawinan;
        $suratKeteranganPajak->pekerjaan         = $request->pekerjaan;
        $suratKeteranganPajak->status_id         = $status->id;
        $suratKeteranganPajak->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganPajak->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('umum.suratKeteranganPajak.kodeSurat')->with([
            'kode'                    => $randomNumber,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
