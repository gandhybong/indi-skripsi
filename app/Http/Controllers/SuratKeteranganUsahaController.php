<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganUsahaRequest;
use App\Http\Requests\UpdateSuratKeteranganUsahaRequest;
use App\Repositories\SuratKeteranganUsahaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganUsaha;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganUsahaRequest;

class SuratKeteranganUsahaController extends AppBaseController
{
    /** @var  SuratKeteranganUsahaRepository */
    private $suratKeteranganUsahaRepository;

    public function __construct(SuratKeteranganUsahaRepository $suratKeteranganUsahaRepo)
    {
        $this->suratKeteranganUsahaRepository = $suratKeteranganUsahaRepo;
        $this->middleware('auth')->except('indexGuest','insertGuest');
    }

    /**
     * Display a listing of the SuratKeteranganUsaha.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganUsahas = $this->suratKeteranganUsahaRepository->all();

        return view('surat_keterangan_usahas.index')
            ->with('suratKeteranganUsahas', $suratKeteranganUsahas);
    }

    /**
     * Show the form for creating a new SuratKeteranganUsaha.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_usahas.create');
    }

    /**
     * Store a newly created SuratKeteranganUsaha in storage.
     *
     * @param CreateSuratKeteranganUsahaRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganUsahaRequest $request)
    {
        $input = $request->all();

        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now()->format('Y-m-d');
        
        $input['tanggal_pengajuan'] = $dateNow;
        $input['kode']              = $randomNumber;
        $input['status_id']         = $status->id;

        $suratKeteranganUsaha = $this->suratKeteranganUsahaRepository->create($input);

        Flash::success('Surat Keterangan Usaha saved successfully.');

        return redirect(route('suratKeteranganUsahas.index'));
    }

    /**
     * Display the specified SuratKeteranganUsaha.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganUsaha = $this->suratKeteranganUsahaRepository->find($id);

        if (empty($suratKeteranganUsaha)) {
            Flash::error('Surat Keterangan Usaha not found');

            return redirect(route('suratKeteranganUsahas.index'));
        }

        return view('surat_keterangan_usahas.show')->with('suratKeteranganUsaha', $suratKeteranganUsaha);
    }

    /**
     * Show the form for editing the specified SuratKeteranganUsaha.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganUsaha = $this->suratKeteranganUsahaRepository->find($id);

        if (empty($suratKeteranganUsaha)) {
            Flash::error('Surat Keterangan Usaha not found');

            return redirect(route('suratKeteranganUsahas.index'));
        }

        return view('surat_keterangan_usahas.edit')->with('suratKeteranganUsaha', $suratKeteranganUsaha);
    }

    /**
     * Update the specified SuratKeteranganUsaha in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganUsahaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganUsahaRequest $request)
    {
        $suratKeteranganUsaha = $this->suratKeteranganUsahaRepository->find($id);

        if (empty($suratKeteranganUsaha)) {
            Flash::error('Surat Keterangan Usaha not found');

            return redirect(route('suratKeteranganUsahas.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganUsaha::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganUsaha->tanggal_terbit == NULL && $suratKeteranganUsaha->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "581/ $nomorSurat /I/BANG";
            }

            $suratKeteranganUsaha = $this->suratKeteranganUsahaRepository->update($allRequestData, $id);
            
            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganUsaha = $this->suratKeteranganUsahaRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Usaha updated successfully.');

            return redirect(route('suratKeteranganUsahas.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganUsaha from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganUsaha = $this->suratKeteranganUsahaRepository->find($id);

        if (empty($suratKeteranganUsaha)) {
            Flash::error('Surat Keterangan Usaha not found');

            return redirect(route('suratKeteranganUsahas.index'));
        }

        $this->suratKeteranganUsahaRepository->delete($id);

        Flash::success('Surat Keterangan Usaha deleted successfully.');

        return redirect(route('suratKeteranganUsahas.index'));
    }

    public function print($id)
    {
        $suratKeteranganUsaha = suratKeteranganUsaha::find($id);
        $preferences          = Preference::get()->keyBy('label')->toArray();
        $dateNow              = Carbon::now()->format('d-m-Y');
        $pdf                  = \PDF::loadView('umum.suratKeteranganUsaha.surat', ['suratKeteranganUsaha' => $suratKeteranganUsaha,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('umum.suratKeteranganUsaha.index');
    }

    public function insertGuest(SuratKeteranganUsahaRequest $request)
    {
        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now();
        
        $suratKeteranganUsaha                      = new SuratKeteranganUsaha;
        $suratKeteranganUsaha->kode                = $randomNumber;
        $suratKeteranganUsaha->nik                 = $request->nik;
        $suratKeteranganUsaha->nama                = $request->nama;
        $suratKeteranganUsaha->jenis_kelamin       = $request->jenis_kelamin;
        $suratKeteranganUsaha->tempat_lahir        = $request->tempat_lahir;
        $suratKeteranganUsaha->tgl_lahir           = $request->tgl_lahir;
        $suratKeteranganUsaha->status_perkawinan   = $request->status_perkawinan;
        $suratKeteranganUsaha->kewarganegaraan     = $request->kewarganegaraan;
        $suratKeteranganUsaha->agama               = $request->agama;
        $suratKeteranganUsaha->pekerjaan           = $request->pekerjaan;
        $suratKeteranganUsaha->alamat              = $request->alamat;
        $suratKeteranganUsaha->nama_usaha          = $request->nama_usaha;
        $suratKeteranganUsaha->tahun_berdiri_usaha = $request->tahun_berdiri_usaha;
        $suratKeteranganUsaha->alamat_usaha        = $request->alamat_usaha;
        $suratKeteranganUsaha->status_id           = $status->id;
        $suratKeteranganUsaha->tanggal_pengajuan   = $dateNow->format('Y-m-d');
        $suratKeteranganUsaha->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('umum.suratKeteranganUsaha.kodeSurat')->with([
            'kode'                    => $randomNumber,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
