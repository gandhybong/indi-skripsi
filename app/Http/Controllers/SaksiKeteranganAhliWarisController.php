<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSaksiKeteranganAhliWarisRequest;
use App\Http\Requests\UpdateSaksiKeteranganAhliWarisRequest;
use App\Repositories\SaksiKeteranganAhliWarisRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SaksiKeteranganAhliWarisController extends AppBaseController
{
    /** @var  SaksiKeteranganAhliWarisRepository */
    private $saksiKeteranganAhliWarisRepository;

    public function __construct(SaksiKeteranganAhliWarisRepository $saksiKeteranganAhliWarisRepo)
    {
        $this->saksiKeteranganAhliWarisRepository = $saksiKeteranganAhliWarisRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SaksiKeteranganAhliWaris.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $saksiKeteranganAhliWaris = $this->saksiKeteranganAhliWarisRepository->all();

        return view('saksi_keterangan_ahli_waris.index')
            ->with('saksiKeteranganAhliWaris', $saksiKeteranganAhliWaris);
    }

    /**
     * Show the form for creating a new SaksiKeteranganAhliWaris.
     *
     * @return Response
     */
    public function create()
    {
        return view('saksi_keterangan_ahli_waris.create');
    }

    /**
     * Store a newly created SaksiKeteranganAhliWaris in storage.
     *
     * @param CreateSaksiKeteranganAhliWarisRequest $request
     *
     * @return Response
     */
    public function store(CreateSaksiKeteranganAhliWarisRequest $request)
    {
        $input = $request->all();

        $saksiKeteranganAhliWaris = $this->saksiKeteranganAhliWarisRepository->create($input);

        Flash::success('Saksi Keterangan Ahli Waris saved successfully.');

        return redirect(route('saksiKeteranganAhliWaris.index'));
    }

    /**
     * Display the specified SaksiKeteranganAhliWaris.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $saksiKeteranganAhliWaris = $this->saksiKeteranganAhliWarisRepository->find($id);

        if (empty($saksiKeteranganAhliWaris)) {
            Flash::error('Saksi Keterangan Ahli Waris not found');

            return redirect(route('saksiKeteranganAhliWaris.index'));
        }

        return view('saksi_keterangan_ahli_waris.show')->with('saksiKeteranganAhliWaris', $saksiKeteranganAhliWaris);
    }

    /**
     * Show the form for editing the specified SaksiKeteranganAhliWaris.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $saksiKeteranganAhliWaris = $this->saksiKeteranganAhliWarisRepository->find($id);

        if (empty($saksiKeteranganAhliWaris)) {
            Flash::error('Saksi Keterangan Ahli Waris not found');

            return redirect(route('saksiKeteranganAhliWaris.index'));
        }

        return view('saksi_keterangan_ahli_waris.edit')->with('saksiKeteranganAhliWaris', $saksiKeteranganAhliWaris);
    }

    /**
     * Update the specified SaksiKeteranganAhliWaris in storage.
     *
     * @param int $id
     * @param UpdateSaksiKeteranganAhliWarisRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSaksiKeteranganAhliWarisRequest $request)
    {
        $saksiKeteranganAhliWaris = $this->saksiKeteranganAhliWarisRepository->find($id);

        if (empty($saksiKeteranganAhliWaris)) {
            Flash::error('Saksi Keterangan Ahli Waris not found');

            return redirect(route('saksiKeteranganAhliWaris.index'));
        }

        $saksiKeteranganAhliWaris = $this->saksiKeteranganAhliWarisRepository->update($request->all(), $id);

        Flash::success('Saksi Keterangan Ahli Waris updated successfully.');

        return redirect(route('saksiKeteranganAhliWaris.index'));
    }

    /**
     * Remove the specified SaksiKeteranganAhliWaris from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $saksiKeteranganAhliWaris = $this->saksiKeteranganAhliWarisRepository->find($id);

        if (empty($saksiKeteranganAhliWaris)) {
            Flash::error('Saksi Keterangan Ahli Waris not found');

            return redirect(route('saksiKeteranganAhliWaris.index'));
        }

        $this->saksiKeteranganAhliWarisRepository->delete($id);

        Flash::success('Saksi Keterangan Ahli Waris deleted successfully.');

        return redirect(route('saksiKeteranganAhliWaris.index'));
    }
}
