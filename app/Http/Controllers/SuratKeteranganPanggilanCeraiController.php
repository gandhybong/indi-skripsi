<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganPanggilanCeraiRequest;
use App\Http\Requests\UpdateSuratKeteranganPanggilanCeraiRequest;
use App\Repositories\SuratKeteranganPanggilanCeraiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganPanggilanCerai;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganPanggilanCeraiRequest;


class SuratKeteranganPanggilanCeraiController extends AppBaseController
{
    /** @var  SuratKeteranganPanggilanCeraiRepository */
    private $suratKeteranganPanggilanCeraiRepository;

    public function __construct(SuratKeteranganPanggilanCeraiRepository $suratKeteranganPanggilanCeraiRepo)
    {
        $this->suratKeteranganPanggilanCeraiRepository = $suratKeteranganPanggilanCeraiRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganPanggilanCerai.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganPanggilanCerais = $this->suratKeteranganPanggilanCeraiRepository->all();

        return view('surat_keterangan_panggilan_cerais.index')
            ->with('suratKeteranganPanggilanCerais', $suratKeteranganPanggilanCerais);
    }

    /**
     * Show the form for creating a new SuratKeteranganPanggilanCerai.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_panggilan_cerais.create');
    }

    /**
     * Store a newly created SuratKeteranganPanggilanCerai in storage.
     *
     * @param CreateSuratKeteranganPanggilanCeraiRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganPanggilanCeraiRequest $request)
    {
        $input = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganPanggilanCerai = $this->suratKeteranganPanggilanCeraiRepository->create($input);

        Flash::success('Surat Keterangan Panggilan Cerai saved successfully.');

        return redirect(route('suratKeteranganPanggilanCerais.index'));
    }

    /**
     * Display the specified SuratKeteranganPanggilanCerai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganPanggilanCerai = $this->suratKeteranganPanggilanCeraiRepository->find($id);

        if (empty($suratKeteranganPanggilanCerai)) {
            Flash::error('Surat Keterangan Panggilan Cerai not found');

            return redirect(route('suratKeteranganPanggilanCerais.index'));
        }

        return view('surat_keterangan_panggilan_cerais.show')->with('suratKeteranganPanggilanCerai', $suratKeteranganPanggilanCerai);
    }

    /**
     * Show the form for editing the specified SuratKeteranganPanggilanCerai.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganPanggilanCerai = $this->suratKeteranganPanggilanCeraiRepository->find($id);

        if (empty($suratKeteranganPanggilanCerai)) {
            Flash::error('Surat Keterangan Panggilan Cerai not found');

            return redirect(route('suratKeteranganPanggilanCerais.index'));
        }

        return view('surat_keterangan_panggilan_cerais.edit')->with('suratKeteranganPanggilanCerai', $suratKeteranganPanggilanCerai);
    }

    /**
     * Update the specified SuratKeteranganPanggilanCerai in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganPanggilanCeraiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganPanggilanCeraiRequest $request)
    {
        $suratKeteranganPanggilanCerai = $this->suratKeteranganPanggilanCeraiRepository->find($id);

        if (empty($suratKeteranganPanggilanCerai)) {
            Flash::error('Surat Keterangan Panggilan Cerai not found');

            return redirect(route('suratKeteranganPanggilanCerais.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganPanggilanCerai::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganPanggilanCerai->tanggal_terbit == NULL && $suratKeteranganPanggilanCerai->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "470/ $nomorSurat / 1 /PEM";
            }
            
            $suratKeteranganPanggilanCerai = $this->suratKeteranganPanggilanCeraiRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganPanggilanCerai = $this->suratKeteranganPanggilanCeraiRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Panggilan Cerai updated successfully.');

            return redirect(route('suratKeteranganPanggilanCerais.index'));
        } 
    }

    /**
     * Remove the specified SuratKeteranganPanggilanCerai from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganPanggilanCerai = $this->suratKeteranganPanggilanCeraiRepository->find($id);

        if (empty($suratKeteranganPanggilanCerai)) {
            Flash::error('Surat Keterangan Panggilan Cerai not found');

            return redirect(route('suratKeteranganPanggilanCerais.index'));
        }

        $this->suratKeteranganPanggilanCeraiRepository->delete($id);

        Flash::success('Surat Keterangan Panggilan Cerai deleted successfully.');

        return redirect(route('suratKeteranganPanggilanCerais.index'));
    }

    public function print($id)
    {
        $suratKeteranganPanggilanCerai = SuratKeteranganPanggilanCerai::find($id);
        $preferences          = Preference::get()->keyBy('label')->toArray();
        $dateNow              = Carbon::now()->format('d-m-Y');
        $pdf                  = \PDF::loadView('khusus.suratKeteranganPanggilanCerai.surat', ['suratKeteranganPanggilanCerai' => $suratKeteranganPanggilanCerai,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganPanggilanCerai.index');
    }

    public function insertGuest(SuratKeteranganPanggilanCeraiRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $suratKeteranganPanggilanCerai                          = new SuratKeteranganPanggilanCerai;
        $suratKeteranganPanggilanCerai->user_id                 = $user->id;
        $suratKeteranganPanggilanCerai->nik_pihak_pertama       = $request->nik_pihak_pertama;
        $suratKeteranganPanggilanCerai->nama_pihak_pertama      = $request->nama_pihak_pertama;
        $suratKeteranganPanggilanCerai->umur_pihak_pertama      = $request->umur_pihak_pertama;
        $suratKeteranganPanggilanCerai->pekerjaan_pihak_pertama = $request->pekerjaan_pihak_pertama;
        $suratKeteranganPanggilanCerai->alamat_pihak_pertama    = $request->alamat_pihak_pertama;
        $suratKeteranganPanggilanCerai->nik_pihak_kedua         = $request->nik_pihak_kedua;
        $suratKeteranganPanggilanCerai->nama_pihak_kedua        = $request->nama_pihak_kedua;
        $suratKeteranganPanggilanCerai->umur_pihak_kedua        = $request->umur_pihak_kedua;
        $suratKeteranganPanggilanCerai->pekerjaan_pihak_kedua   = $request->pekerjaan_pihak_kedua;
        $suratKeteranganPanggilanCerai->alamat_pihak_kedua      = $request->alamat_pihak_kedua;
        $suratKeteranganPanggilanCerai->yang_ditinggalkan       = $request->yang_ditinggalkan;
        $suratKeteranganPanggilanCerai->lama_ditinggal          = $request->lama_ditinggal;
        $suratKeteranganPanggilanCerai->status_id               = $status->id;
        $suratKeteranganPanggilanCerai->tanggal_pengajuan       = $dateNow->format('Y-m-d');
        $suratKeteranganPanggilanCerai->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganPanggilanCerai.kodeSurat')->with([
            'suratKeteranganPanggilanCerai' => $suratKeteranganPanggilanCerai,
            'hariKadarluasaPengajuan'         => $hariKadarluasaPengajuan
            ]);
    }
}
