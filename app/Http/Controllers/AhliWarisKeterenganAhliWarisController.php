<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAhliWarisKeterenganAhliWarisRequest;
use App\Http\Requests\UpdateAhliWarisKeterenganAhliWarisRequest;
use App\Repositories\AhliWarisKeterenganAhliWarisRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class AhliWarisKeterenganAhliWarisController extends AppBaseController
{
    /** @var  AhliWarisKeterenganAhliWarisRepository */
    private $ahliWarisKeterenganAhliWarisRepository;

    public function __construct(AhliWarisKeterenganAhliWarisRepository $ahliWarisKeterenganAhliWarisRepo)
    {
        $this->ahliWarisKeterenganAhliWarisRepository = $ahliWarisKeterenganAhliWarisRepo;
    }

    /**
     * Display a listing of the AhliWarisKeterenganAhliWaris.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $ahliWarisKeterenganAhliWaris = $this->ahliWarisKeterenganAhliWarisRepository->all();
        //test
        return view('ahli_waris_keterengan_ahli_waris.index')
            ->with('ahliWarisKeterenganAhliWaris', $ahliWarisKeterenganAhliWaris);
    }

    /**
     * Show the form for creating a new AhliWarisKeterenganAhliWaris.
     *
     * @return Response
     */
    public function create()
    {
        return view('ahli_waris_keterengan_ahli_waris.create');
    }

    /**
     * Store a newly created AhliWarisKeterenganAhliWaris in storage.
     *
     * @param CreateAhliWarisKeterenganAhliWarisRequest $request
     *
     * @return Response
     */
    public function store(CreateAhliWarisKeterenganAhliWarisRequest $request)
    {
        $input = $request->all();

        $ahliWarisKeterenganAhliWaris = $this->ahliWarisKeterenganAhliWarisRepository->create($input);

        Flash::success('Ahli Waris Keterengan Ahli Waris saved successfully.');

        return redirect(route('ahliWarisKeterenganAhliWaris.index'));
    }

    /**
     * Display the specified AhliWarisKeterenganAhliWaris.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ahliWarisKeterenganAhliWaris = $this->ahliWarisKeterenganAhliWarisRepository->find($id);

        if (empty($ahliWarisKeterenganAhliWaris)) {
            Flash::error('Ahli Waris Keterengan Ahli Waris not found');

            return redirect(route('ahliWarisKeterenganAhliWaris.index'));
        }

        return view('ahli_waris_keterengan_ahli_waris.show')->with('ahliWarisKeterenganAhliWaris', $ahliWarisKeterenganAhliWaris);
    }

    /**
     * Show the form for editing the specified AhliWarisKeterenganAhliWaris.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ahliWarisKeterenganAhliWaris = $this->ahliWarisKeterenganAhliWarisRepository->find($id);

        if (empty($ahliWarisKeterenganAhliWaris)) {
            Flash::error('Ahli Waris Keterengan Ahli Waris not found');

            return redirect(route('ahliWarisKeterenganAhliWaris.index'));
        }

        return view('ahli_waris_keterengan_ahli_waris.edit')->with('ahliWarisKeterenganAhliWaris', $ahliWarisKeterenganAhliWaris);
    }

    /**
     * Update the specified AhliWarisKeterenganAhliWaris in storage.
     *
     * @param int $id
     * @param UpdateAhliWarisKeterenganAhliWarisRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAhliWarisKeterenganAhliWarisRequest $request)
    {
        $ahliWarisKeterenganAhliWaris = $this->ahliWarisKeterenganAhliWarisRepository->find($id);

        if (empty($ahliWarisKeterenganAhliWaris)) {
            Flash::error('Ahli Waris Keterengan Ahli Waris not found');

            return redirect(route('ahliWarisKeterenganAhliWaris.index'));
        }

        $ahliWarisKeterenganAhliWaris = $this->ahliWarisKeterenganAhliWarisRepository->update($request->all(), $id);

        Flash::success('Ahli Waris Keterengan Ahli Waris updated successfully.');

        return redirect(route('ahliWarisKeterenganAhliWaris.index'));
    }

    /**
     * Remove the specified AhliWarisKeterenganAhliWaris from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ahliWarisKeterenganAhliWaris = $this->ahliWarisKeterenganAhliWarisRepository->find($id);

        if (empty($ahliWarisKeterenganAhliWaris)) {
            Flash::error('Ahli Waris Keterengan Ahli Waris not found');

            return redirect(route('ahliWarisKeterenganAhliWaris.index'));
        }

        $this->ahliWarisKeterenganAhliWarisRepository->delete($id);

        Flash::success('Ahli Waris Keterengan Ahli Waris deleted successfully.');

        return redirect(route('ahliWarisKeterenganAhliWaris.index'));
    }
}
