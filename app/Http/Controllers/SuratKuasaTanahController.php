<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKuasaTanahRequest;
use App\Http\Requests\UpdateSuratKuasaTanahRequest;
use App\Repositories\SuratKuasaTanahRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKuasaTanah;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKuasaTanahRequest;

class SuratKuasaTanahController extends AppBaseController
{
    /** @var  SuratKuasaTanahRepository */
    private $suratKuasaTanahRepository;

    public function __construct(SuratKuasaTanahRepository $suratKuasaTanahRepo)
    {
        $this->suratKuasaTanahRepository = $suratKuasaTanahRepo;
        $this->middleware('auth')->except('indexGuest','insertGuest');
    }

    /**
     * Display a listing of the SuratKuasaTanah.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKuasaTanahs = $this->suratKuasaTanahRepository->all();

        return view('surat_kuasa_tanahs.index')
            ->with('suratKuasaTanahs', $suratKuasaTanahs);
    }

    /**
     * Show the form for creating a new SuratKuasaTanah.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_kuasa_tanahs.create');
    }

    /**
     * Store a newly created SuratKuasaTanah in storage.
     *
     * @param CreateSuratKuasaTanahRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKuasaTanahRequest $request)
    {
        $input = $request->all();

        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now()->format('Y-m-d');

        $input['kode']              = $randomNumber;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKuasaTanah = $this->suratKuasaTanahRepository->create($input);

        Flash::success('Surat Kuasa Tanah saved successfully.');

        return redirect(route('suratKuasaTanahs.index'));
    }

    /**
     * Display the specified SuratKuasaTanah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKuasaTanah = $this->suratKuasaTanahRepository->find($id);

        if (empty($suratKuasaTanah)) {
            Flash::error('Surat Kuasa Tanah not found');

            return redirect(route('suratKuasaTanahs.index'));
        }

        return view('surat_kuasa_tanahs.show')->with('suratKuasaTanah', $suratKuasaTanah);
    }

    /**
     * Show the form for editing the specified SuratKuasaTanah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKuasaTanah = $this->suratKuasaTanahRepository->find($id);

        if (empty($suratKuasaTanah)) {
            Flash::error('Surat Kuasa Tanah not found');

            return redirect(route('suratKuasaTanahs.index'));
        }

        return view('surat_kuasa_tanahs.edit')->with('suratKuasaTanah', $suratKuasaTanah);
    }

    /**
     * Update the specified SuratKuasaTanah in storage.
     *
     * @param int $id
     * @param UpdateSuratKuasaTanahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKuasaTanahRequest $request)
    {
        $suratKuasaTanah = $this->suratKuasaTanahRepository->find($id);

        if (empty($suratKuasaTanah)) {
            Flash::error('Surat Kuasa Tanah not found');

            return redirect(route('suratKuasaTanahs.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKuasaTanah::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKuasaTanah->tanggal_terbit == NULL && $suratKuasaTanah->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "$nomorSurat";
            }

            $suratKuasaTanah = $this->suratKuasaTanahRepository->update($allRequestData, $id);
            
            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKuasaTanah = $this->suratKuasaTanahRepository->update($allRequestData, $id);

            Flash::success('Surat Kuasa Tanah updated successfully.');
    
            return redirect(route('suratKuasaTanahs.index'));
        }

       
    }

    /**
     * Remove the specified SuratKuasaTanah from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKuasaTanah = $this->suratKuasaTanahRepository->find($id);

        if (empty($suratKuasaTanah)) {
            Flash::error('Surat Kuasa Tanah not found');

            return redirect(route('suratKuasaTanahs.index'));
        }

        $this->suratKuasaTanahRepository->delete($id);

        Flash::success('Surat Kuasa Tanah deleted successfully.');

        return redirect(route('suratKuasaTanahs.index'));
    }

    public function print($id)
    {
        $suratKuasaTanah = SuratKuasaTanah::find($id);
        $preferences     = Preference::get()->keyBy('label')->toArray();
        $dateNow         = Carbon::now()->format('d-m-Y');
        $pdf             = \PDF::loadView('umum.suratKuasaTanah.surat', ['suratKuasaTanah' => $suratKuasaTanah,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('umum.suratKuasaTanah.index');
    }

    public function insertGuest(SuratKuasaTanahRequest $request)
    {
        $status       = Status::where('status','draf')->first();
        $randomNumber = mt_rand(111111, 999999);
        $dateNow      = Carbon::now();

        $suratKuasaTanah                                = new SuratKuasaTanah;
        $suratKuasaTanah->kode                          = $randomNumber;
        $suratKuasaTanah->nik                           = $request->nik;
        $suratKuasaTanah->nama                          = $request->nama;
        $suratKuasaTanah->jenis_kelamin                 = $request->jenis_kelamin;
        $suratKuasaTanah->tempat_lahir                  = $request->tempat_lahir;
        $suratKuasaTanah->tgl_lahir                     = $request->tgl_lahir;
        $suratKuasaTanah->status_perkawinan             = $request->status_perkawinan;
        $suratKuasaTanah->pekerjaan                     = $request->pekerjaan;
        $suratKuasaTanah->alamat                        = $request->alamat;
        $suratKuasaTanah->nik_pihak_kedua               = $request->nik_pihak_kedua;
        $suratKuasaTanah->nama_pihak_kedua              = $request->nama_pihak_kedua;
        $suratKuasaTanah->jenis_kelamin_pihak_kedua     = $request->jenis_kelamin_pihak_kedua;
        $suratKuasaTanah->tempat_lahir_pihak_kedua      = $request->tempat_lahir_pihak_kedua;
        $suratKuasaTanah->tgl_lahir_pihak_kedua         = $request->tgl_lahir_pihak_kedua;
        $suratKuasaTanah->status_perkawinan_pihak_kedua = $request->status_perkawinan_pihak_kedua;
        $suratKuasaTanah->pekerjaan_pihak_kedua         = $request->pekerjaan_pihak_kedua;
        $suratKuasaTanah->alamat_pihak_kedua            = $request->alamat_pihak_kedua;
        $suratKuasaTanah->status_kedua_pihak            = $request->status_kedua_pihak;
        $suratKuasaTanah->jenis_tanah                   = $request->jenis_tanah;
        $suratKuasaTanah->nomor_sertifikat_tanah        = $request->nomor_sertifikat_tanah;
        $suratKuasaTanah->atas_nama                     = $request->atas_nama;
        $suratKuasaTanah->tahun                         = $request->tahun;
        $suratKuasaTanah->luas_tanah                    = $request->luas_tanah;
        $suratKuasaTanah->maksud                        = $request->maksud;
        $suratKuasaTanah->status_id                     = $status->id;
        $suratKuasaTanah->tanggal_pengajuan             = $dateNow->format('Y-m-d');
        $suratKuasaTanah->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('umum.suratKuasaTanah.kodeSurat')->with([
            'kode'                    => $randomNumber,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
