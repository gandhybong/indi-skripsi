<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganBelumPernahMenikahRequest;
use App\Http\Requests\UpdateSuratKeteranganBelumPernahMenikahRequest;
use App\Repositories\SuratKeteranganBelumPernahMenikahRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganBelumPernahMenikah;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganBelumPernahMenikahRequest;

class SuratBelumPernahMenikahController extends AppBaseController
{
    /** @var  SuratKeteranganBelumPernahMenikahRepository */
    private $suratKeteranganBelumPernahMenikahRepository;

    public function __construct(SuratKeteranganBelumPernahMenikahRepository $suratKeteranganBelumPernahMenikahRepo)
    {
        $this->suratKeteranganBelumPernahMenikahRepository = $suratKeteranganBelumPernahMenikahRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganBelumPernahMenikah.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganBelumPernahMenikahs = $this->suratKeteranganBelumPernahMenikahRepository->all();

        return view('surat_keterangan_belum_pernah_menikahs.index')
            ->with('suratKeteranganBelumPernahMenikahs', $suratKeteranganBelumPernahMenikahs);
    }

    /**
     * Show the form for creating a new SuratKeteranganBelumPernahMenikah.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_belum_pernah_menikahs.create');
    }

    /**
     * Store a newly created SuratKeteranganBelumPernahMenikah in storage.
     *
     * @param CreateSuratKeteranganBelumPernahMenikahRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganBelumPernahMenikahRequest $request)
    {
        $input   = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganBelumPernahMenikah = $this->suratKeteranganBelumPernahMenikahRepository->create($input);

        Flash::success('Surat Keterangan Belum Pernah Menikah saved successfully.');

        return redirect(route('suratBelumPernahMenikahs.index'));
    }

    /**
     * Display the specified SuratKeteranganBelumPernahMenikah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganBelumPernahMenikah = $this->suratKeteranganBelumPernahMenikahRepository->find($id);

        if (empty($suratKeteranganBelumPernahMenikah)) {
            Flash::error('Surat Keterangan Belum Pernah Menikah not found');

            return redirect(route('suratBelumPernahMenikahs.index'));
        }

        return view('surat_keterangan_belum_pernah_menikahs.show')->with('suratKeteranganBelumPernahMenikah', $suratKeteranganBelumPernahMenikah);
    }

    /**
     * Show the form for editing the specified SuratKeteranganBelumPernahMenikah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganBelumPernahMenikah = $this->suratKeteranganBelumPernahMenikahRepository->find($id);

        if (empty($suratKeteranganBelumPernahMenikah)) {
            Flash::error('Surat Keterangan Belum Pernah Menikah not found');

            return redirect(route('suratBelumPernahMenikahs.index'));
        }

        return view('surat_keterangan_belum_pernah_menikahs.edit')->with('suratKeteranganBelumPernahMenikah', $suratKeteranganBelumPernahMenikah);
    }

    /**
     * Update the specified SuratKeteranganBelumPernahMenikah in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganBelumPernahMenikahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganBelumPernahMenikahRequest $request)
    {
        $suratKeteranganBelumPernahMenikah = $this->suratKeteranganBelumPernahMenikahRepository->find($id);

        if (empty($suratKeteranganBelumPernahMenikah)) {
            Flash::error('Surat Keterangan Belum Pernah Menikah not found');

            return redirect(route('suratBelumPernahMenikahs.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganBelumPernahMenikah::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganBelumPernahMenikah->tanggal_terbit == NULL && $suratKeteranganBelumPernahMenikah->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "474.2 / $nomorSurat /I/ PEM";
            }
            
            $suratKeteranganBelumPernahMenikah = $this->suratKeteranganBelumPernahMenikahRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganBelumPernahMenikah = $this->suratKeteranganBelumPernahMenikahRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Belum Pernah Menikah updated successfully.');

            return redirect(route('suratBelumPernahMenikahs.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganBelumPernahMenikah from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganBelumPernahMenikah = $this->suratKeteranganBelumPernahMenikahRepository->find($id);

        if (empty($suratKeteranganBelumPernahMenikah)) {
            Flash::error('Surat Keterangan Belum Pernah Menikah not found');

            return redirect(route('suratBelumPernahMenikahs.index'));
        }

        $this->suratKeteranganBelumPernahMenikahRepository->delete($id);

        Flash::success('Surat Keterangan Belum Pernah Menikah deleted successfully.');

        return redirect(route('suratBelumPernahMenikahs.index'));
    }

    public function print($id)
    {
        $suratBelumPernahMenikah = SuratKeteranganBelumPernahMenikah::find($id);
        $preferences          = Preference::get()->keyBy('label')->toArray();
        $dateNow              = Carbon::now()->format('d-m-Y');
        $pdf                  = \PDF::loadView('khusus.suratKeteranganBelumPernahMenikah.surat', ['suratBelumPernahMenikah' => $suratBelumPernahMenikah,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganBelumPernahMenikah.index');
    }

    public function insertGuest(SuratKeteranganBelumPernahMenikahRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $SuratBelumPernahMenikah                    = new SuratKeteranganBelumPernahMenikah;
        $SuratBelumPernahMenikah->user_id           = $user->id;
        $SuratBelumPernahMenikah->nik               = $request->nik;
        $SuratBelumPernahMenikah->nama              = $request->nama;
        $SuratBelumPernahMenikah->jenis_kelamin     = $request->jenis_kelamin;
        $SuratBelumPernahMenikah->tempat_lahir      = $request->tempat_lahir;
        $SuratBelumPernahMenikah->tgl_lahir         = $request->tgl_lahir;
        $SuratBelumPernahMenikah->status_perkawinan = $request->status_perkawinan;
        $SuratBelumPernahMenikah->kewarganegaraan   = $request->kewarganegaraan;
        $SuratBelumPernahMenikah->agama             = $request->agama;
        $SuratBelumPernahMenikah->pekerjaan         = $request->pekerjaan;
        $SuratBelumPernahMenikah->alamat            = $request->alamat;
        $SuratBelumPernahMenikah->administrasi      = $request->administrasi;
        $SuratBelumPernahMenikah->status_id         = $status->id;
        $SuratBelumPernahMenikah->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $SuratBelumPernahMenikah->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganBelumPernahMenikah.kodeSurat')->with([
            'suratBelumPernahMenikah' => $SuratBelumPernahMenikah,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
