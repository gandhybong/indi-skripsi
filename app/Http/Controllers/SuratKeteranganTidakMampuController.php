<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganTidakMampuRequest;
use App\Http\Requests\UpdateSuratKeteranganTidakMampuRequest;
use App\Repositories\SuratKeteranganTidakMampuRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganTidakMampu;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganTidakMampuRequest;

class SuratKeteranganTidakMampuController extends AppBaseController
{
    /** @var  SuratKeteranganTidakMampuRepository */
    private $suratKeteranganTidakMampuRepository;

    public function __construct(SuratKeteranganTidakMampuRepository $suratKeteranganTidakMampuRepo)
    {
        $this->suratKeteranganTidakMampuRepository = $suratKeteranganTidakMampuRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganTidakMampu.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganTidakMampus = $this->suratKeteranganTidakMampuRepository->all();

        return view('surat_keterangan_tidak_mampus.index')
            ->with('suratKeteranganTidakMampus', $suratKeteranganTidakMampus);
    }

    /**
     * Show the form for creating a new SuratKeteranganTidakMampu.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_tidak_mampus.create');
    }

    /**
     * Store a newly created SuratKeteranganTidakMampu in storage.
     *
     * @param CreateSuratKeteranganTidakMampuRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganTidakMampuRequest $request)
    {
        $input = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganTidakMampu = $this->suratKeteranganTidakMampuRepository->create($input);

        Flash::success('Surat Keterangan Tidak Mampu saved successfully.');

        return redirect(route('suratKeteranganTidakMampus.index'));
    }

    /**
     * Display the specified SuratKeteranganTidakMampu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganTidakMampu = $this->suratKeteranganTidakMampuRepository->find($id);

        if (empty($suratKeteranganTidakMampu)) {
            Flash::error('Surat Keterangan Tidak Mampu not found');

            return redirect(route('suratKeteranganTidakMampus.index'));
        }

        return view('surat_keterangan_tidak_mampus.show')->with('suratKeteranganTidakMampu', $suratKeteranganTidakMampu);
    }

    /**
     * Show the form for editing the specified SuratKeteranganTidakMampu.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganTidakMampu = $this->suratKeteranganTidakMampuRepository->find($id);

        if (empty($suratKeteranganTidakMampu)) {
            Flash::error('Surat Keterangan Tidak Mampu not found');

            return redirect(route('suratKeteranganTidakMampus.index'));
        }

        return view('surat_keterangan_tidak_mampus.edit')->with('suratKeteranganTidakMampu', $suratKeteranganTidakMampu);
    }

    /**
     * Update the specified SuratKeteranganTidakMampu in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganTidakMampuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganTidakMampuRequest $request)
    {
        $suratKeteranganTidakMampu = $this->suratKeteranganTidakMampuRepository->find($id);

        if (empty($suratKeteranganTidakMampu)) {
            Flash::error('Surat Keterangan Tidak Mampu not found');

            return redirect(route('suratKeteranganTidakMampus.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganTidakMampu::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganTidakMampu->tanggal_terbit == NULL && $suratKeteranganTidakMampu->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "474.3/ $nomorSurat /I/PEM";
            }
            
            $suratKeteranganTidakMampu = $this->suratKeteranganTidakMampuRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganTidakMampu = $this->suratKeteranganTidakMampuRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Tidak Mampu updated successfully.');

            return redirect(route('suratKeteranganTidakMampus.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganTidakMampu from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganTidakMampu = $this->suratKeteranganTidakMampuRepository->find($id);

        if (empty($suratKeteranganTidakMampu)) {
            Flash::error('Surat Keterangan Tidak Mampu not found');

            return redirect(route('suratKeteranganTidakMampus.index'));
        }

        $this->suratKeteranganTidakMampuRepository->delete($id);

        Flash::success('Surat Keterangan Tidak Mampu deleted successfully.');

        return redirect(route('suratKeteranganTidakMampus.index'));
    }

    public function print($id)
    {
        $suratKeteranganTidakMampu              = SuratKeteranganTidakMampu::find($id);
        $suratKeteranganTidakMampu->penghasilan = substr(strrev(chunk_split(strrev($suratKeteranganTidakMampu->penghasilan), 3, '.')),1);
        
        $preferences                             = Preference::get()->keyBy('label')->toArray();
        $dateNow                                 = Carbon::now()->format('d-m-Y');
        $pdf                                     = \PDF::loadView('khusus.suratKeteranganTidakMampu.surat', ['suratKeteranganTidakMampu' => $suratKeteranganTidakMampu,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganTidakMampu.index');
    }

    public function insertGuest(SuratKeteranganTidakMampuRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now()->format('Y-m-d');
        
        $suratKeteranganTidakMampu                       = new SuratKeteranganTidakMampu;
        $suratKeteranganTidakMampu->user_id              = $user->id;
        $suratKeteranganTidakMampu->nomor_kartu_keluarga = $request->nomor_kartu_keluarga;
        $suratKeteranganTidakMampu->nik                  = $request->nik;
        $suratKeteranganTidakMampu->nama                 = $request->nama;
        $suratKeteranganTidakMampu->jenis_kelamin        = $request->jenis_kelamin;
        $suratKeteranganTidakMampu->tempat_lahir         = $request->tempat_lahir;
        $suratKeteranganTidakMampu->tgl_lahir            = $request->tgl_lahir;
        $suratKeteranganTidakMampu->kewarganegaraan      = $request->kewarganegaraan;
        $suratKeteranganTidakMampu->agama                = $request->agama;
        $suratKeteranganTidakMampu->status_perkawinan    = $request->status_perkawinan;
        $suratKeteranganTidakMampu->pekerjaan            = $request->pekerjaan;
        $suratKeteranganTidakMampu->alamat               = $request->alamat;
        $suratKeteranganTidakMampu->penghasilan          = $request->penghasilan;
        $suratKeteranganTidakMampu->maksud               = $request->maksud;
        $suratKeteranganTidakMampu->status_id            = $status->id;
        $suratKeteranganTidakMampu->tanggal_pengajuan    = $dateNow;
        $suratKeteranganTidakMampu->save();
        return view('khusus.suratKeteranganTidakMampu.kodeSurat')->with('suratKeteranganTidakMampu',$suratKeteranganTidakMampu);
    }
}
