<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganBelumNpwpRequest;
use App\Http\Requests\UpdateSuratKeteranganBelumNpwpRequest;
use App\Repositories\SuratKeteranganBelumNpwpRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganBelumNpwp;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\suratKeteranganBelumNpwpRequest;

class SuratKeteranganBelumNpwpController extends AppBaseController
{
    /** @var  SuratKeteranganBelumNpwpRepository */
    private $suratKeteranganBelumNpwpRepository;

    public function __construct(SuratKeteranganBelumNpwpRepository $suratKeteranganBelumNpwpRepo)
    {
        $this->suratKeteranganBelumNpwpRepository = $suratKeteranganBelumNpwpRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganBelumNpwp.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganBelumNpwps = $this->suratKeteranganBelumNpwpRepository->all();

        return view('surat_keterangan_belum_npwps.index')
            ->with('suratKeteranganBelumNpwps', $suratKeteranganBelumNpwps);
    }

    /**
     * Show the form for creating a new SuratKeteranganBelumNpwp.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_belum_npwps.create');
    }

    /**
     * Store a newly created SuratKeteranganBelumNpwp in storage.
     *
     * @param CreateSuratKeteranganBelumNpwpRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganBelumNpwpRequest $request)
    {
        $input   = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();
        
        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganBelumNpwp = $this->suratKeteranganBelumNpwpRepository->create($input);

        Flash::success('Surat Keterangan Belum Npwp saved successfully.');

        return redirect(route('suratKeteranganBelumNpwps.index'));
    }

    /**
     * Display the specified SuratKeteranganBelumNpwp.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganBelumNpwp = $this->suratKeteranganBelumNpwpRepository->find($id);

        if (empty($suratKeteranganBelumNpwp)) {
            Flash::error('Surat Keterangan Belum Npwp not found');

            return redirect(route('suratKeteranganBelumNpwps.index'));
        }

        return view('surat_keterangan_belum_npwps.show')->with('suratKeteranganBelumNpwp', $suratKeteranganBelumNpwp);
    }

    /**
     * Show the form for editing the specified SuratKeteranganBelumNpwp.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganBelumNpwp = $this->suratKeteranganBelumNpwpRepository->find($id);

        if (empty($suratKeteranganBelumNpwp)) {
            Flash::error('Surat Keterangan Belum Npwp not found');

            return redirect(route('suratKeteranganBelumNpwps.index'));
        }

        return view('surat_keterangan_belum_npwps.edit')->with('suratKeteranganBelumNpwp', $suratKeteranganBelumNpwp);
    }

    /**
     * Update the specified SuratKeteranganBelumNpwp in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganBelumNpwpRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganBelumNpwpRequest $request)
    {
        $suratKeteranganBelumNpwp = $this->suratKeteranganBelumNpwpRepository->find($id);

        if (empty($suratKeteranganBelumNpwp)) {
            Flash::error('Surat Keterangan Belum Npwp not found');

            return redirect(route('suratKeteranganBelumNpwps.index'));
        }

        $allRequestData          = $request->all();
        
        if($request->terima == 'Terima'){
            $status     = Status::where('status','Diterima')->first();
            $dateNow    = Carbon::now()->format('Y-m-d');
            $countSurat = SuratKeteranganBelumNpwp::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT); 
            $allRequestData['status_id']      = $status->id;
            if($suratKeteranganBelumNpwp->tanggal_terbit == NULL && $suratKeteranganBelumNpwp->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "593/ $nomorSurat / I / PEM";
            }
            
            $suratKeteranganBelumNpwp = $this->suratKeteranganBelumNpwpRepository->update($allRequestData, $id);
            
            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();
            
            $allRequestData['status_id'] = $status->id;
    
            $suratKeteranganBelumNpwp = $this->suratKeteranganBelumNpwpRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Belum Npwp updated successfully.');

            return redirect(route('suratKeteranganBelumNpwps.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganBelumNpwp from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganBelumNpwp = $this->suratKeteranganBelumNpwpRepository->find($id);

        if (empty($suratKeteranganBelumNpwp)) {
            Flash::error('Surat Keterangan Belum Npwp not found');

            return redirect(route('suratKeteranganBelumNpwps.index'));
        }

        $this->suratKeteranganBelumNpwpRepository->delete($id);

        Flash::success('Surat Keterangan Belum Npwp deleted successfully.');

        return redirect(route('suratKeteranganBelumNpwps.index'));
    }

    public function print($id)
    {
        $suratKeteranganBelumNpwp = SuratKeteranganBelumNpwp::find($id);
        $preferences              = Preference::get()->keyBy('label')->toArray();
        $dateNow                  = Carbon::now()->format('d-m-Y');

        $pdf = \PDF::loadView('khusus.suratKeteranganBelumNpwp.surat', ['suratKeteranganBelumNpwp' => $suratKeteranganBelumNpwp,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganBelumNpwp.index');
    }

    public function insertGuest(SuratKeteranganBelumNpwpRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $SuratKeteranganBelumNpwp                    = new SuratKeteranganBelumNpwp;
        $SuratKeteranganBelumNpwp->user_id           = $user->id;
        $SuratKeteranganBelumNpwp->nik               = $request->nik;
        $SuratKeteranganBelumNpwp->nama              = $request->nama;
        $SuratKeteranganBelumNpwp->jenis_kelamin     = $request->jenis_kelamin;
        $SuratKeteranganBelumNpwp->tempat_lahir      = $request->tempat_lahir;
        $SuratKeteranganBelumNpwp->tgl_lahir         = $request->tgl_lahir;
        $SuratKeteranganBelumNpwp->kewarganegaraan   = $request->kewarganegaraan;
        $SuratKeteranganBelumNpwp->agama             = $request->agama;
        $SuratKeteranganBelumNpwp->pekerjaan         = $request->pekerjaan;
        $SuratKeteranganBelumNpwp->status_perkawinan = $request->status_perkawinan;
        $SuratKeteranganBelumNpwp->alamat            = $request->alamat;
        $SuratKeteranganBelumNpwp->rt                = $request->rt;
        $SuratKeteranganBelumNpwp->rw                = $request->rw;
        $SuratKeteranganBelumNpwp->maksud            = $request->maksud;
        $SuratKeteranganBelumNpwp->status_id         = $status->id;
        $SuratKeteranganBelumNpwp->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $SuratKeteranganBelumNpwp->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.SuratKeteranganBelumNpwp.kodeSurat')->with([
            'SuratKeteranganBelumNpwp' => $SuratKeteranganBelumNpwp,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
