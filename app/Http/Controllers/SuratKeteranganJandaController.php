<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganJandaRequest;
use App\Http\Requests\UpdateSuratKeteranganJandaRequest;
use App\Repositories\SuratKeteranganJandaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganJanda;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganJandaRequest;

class SuratKeteranganJandaController extends AppBaseController
{
    /** @var  SuratKeteranganJandaRepository */
    private $suratKeteranganJandaRepository;

    public function __construct(SuratKeteranganJandaRepository $suratKeteranganJandaRepo)
    {
        $this->suratKeteranganJandaRepository = $suratKeteranganJandaRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganJanda.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganJandas = $this->suratKeteranganJandaRepository->all();

        return view('surat_keterangan_jandas.index')
            ->with('suratKeteranganJandas', $suratKeteranganJandas);
    }

    /**
     * Show the form for creating a new SuratKeteranganJanda.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_jandas.create');
    }

    /**
     * Store a newly created SuratKeteranganJanda in storage.
     *
     * @param CreateSuratKeteranganJandaRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganJandaRequest $request)
    {
        $input   = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganJanda = $this->suratKeteranganJandaRepository->create($input);

        Flash::success('Surat Keterangan Janda saved successfully.');

        return redirect(route('suratKeteranganJandas.index'));
    }

    /**
     * Display the specified SuratKeteranganJanda.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganJanda = $this->suratKeteranganJandaRepository->find($id);

        if (empty($suratKeteranganJanda)) {
            Flash::error('Surat Keterangan Janda not found');

            return redirect(route('suratKeteranganJandas.index'));
        }

        return view('surat_keterangan_jandas.show')->with('suratKeteranganJanda', $suratKeteranganJanda);
    }

    /**
     * Show the form for editing the specified SuratKeteranganJanda.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganJanda = $this->suratKeteranganJandaRepository->find($id);

        if (empty($suratKeteranganJanda)) {
            Flash::error('Surat Keterangan Janda not found');

            return redirect(route('suratKeteranganJandas.index'));
        }

        return view('surat_keterangan_jandas.edit')->with('suratKeteranganJanda', $suratKeteranganJanda);
    }

    /**
     * Update the specified SuratKeteranganJanda in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganJandaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganJandaRequest $request)
    {
        $suratKeteranganJanda = $this->suratKeteranganJandaRepository->find($id);

        if (empty($suratKeteranganJanda)) {
            Flash::error('Surat Keterangan Janda not found');

            return redirect(route('suratKeteranganJandas.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganJanda::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganJanda->tanggal_terbit == NULL && $suratKeteranganJanda->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "882/ $nomorSurat /I/ PEM";
            }
            
            $suratKeteranganJanda = $this->suratKeteranganJandaRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganJanda = $this->suratKeteranganJandaRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Janda updated successfully.');

            return redirect(route('suratKeteranganJandas.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganJanda from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganJanda = $this->suratKeteranganJandaRepository->find($id);

        if (empty($suratKeteranganJanda)) {
            Flash::error('Surat Keterangan Janda not found');

            return redirect(route('suratKeteranganJandas.index'));
        }

        $this->suratKeteranganJandaRepository->delete($id);

        Flash::success('Surat Keterangan Janda deleted successfully.');

        return redirect(route('suratKeteranganJandas.index'));
    }

    public function print($id)
    {
        $suratKeteranganJanda = SuratKeteranganJanda::find($id);
        $preferences          = Preference::get()->keyBy('label')->toArray();
        $dateNow              = Carbon::now()->format('d-m-Y');
        $pdf                  = \PDF::loadView('khusus.suratKeteranganJanda.surat', ['suratKeteranganJanda' => $suratKeteranganJanda,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganJanda.index');
    }

    public function insertGuest(SuratKeteranganJandaRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $suratKeteranganJanda                    = new SuratKeteranganJanda;
        $suratKeteranganJanda->user_id           = $user->id;
        $suratKeteranganJanda->nik               = $request->nik;
        $suratKeteranganJanda->nama              = $request->nama;
        $suratKeteranganJanda->jenis_kelamin     = $request->jenis_kelamin;
        $suratKeteranganJanda->tempat_lahir      = $request->tempat_lahir;
        $suratKeteranganJanda->tgl_lahir         = $request->tgl_lahir;
        $suratKeteranganJanda->status_perkawinan = $request->status_perkawinan;
        $suratKeteranganJanda->kewarganegaraan   = $request->kewarganegaraan;
        $suratKeteranganJanda->agama             = $request->agama;
        $suratKeteranganJanda->pekerjaan         = $request->pekerjaan;
        $suratKeteranganJanda->alamat            = $request->alamat;
        $suratKeteranganJanda->rt                = $request->rt;
        $suratKeteranganJanda->rw                = $request->rw;
        $suratKeteranganJanda->sebab_cerai       = $request->sebab_cerai;
        $suratKeteranganJanda->administrasi      = $request->administrasi;
        $suratKeteranganJanda->status_id         = $status->id;
        $suratKeteranganJanda->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganJanda->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganJanda.kodeSurat')->with([
            'suratKeteranganJanda'    => $suratKeteranganJanda,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
