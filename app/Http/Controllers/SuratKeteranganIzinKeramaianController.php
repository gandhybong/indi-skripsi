<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganIzinKeramaianRequest;
use App\Http\Requests\UpdateSuratKeteranganIzinKeramaianRequest;
use App\Repositories\SuratKeteranganIzinKeramaianRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganIzinKeramaian;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganIzinKeramaianRequest;

class SuratKeteranganIzinKeramaianController extends AppBaseController
{
    /** @var  SuratKeteranganIzinKeramaianRepository */
    private $suratKeteranganIzinKeramaianRepository;

    public function __construct(SuratKeteranganIzinKeramaianRepository $suratKeteranganIzinKeramaianRepo)
    {
        $this->suratKeteranganIzinKeramaianRepository = $suratKeteranganIzinKeramaianRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganIzinKeramaian.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganIzinKeramaians = $this->suratKeteranganIzinKeramaianRepository->all();

        return view('surat_keterangan_izin_keramaians.index')
            ->with('suratKeteranganIzinKeramaians', $suratKeteranganIzinKeramaians);
    }

    /**
     * Show the form for creating a new SuratKeteranganIzinKeramaian.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_izin_keramaians.create');
    }

    /**
     * Store a newly created SuratKeteranganIzinKeramaian in storage.
     *
     * @param CreateSuratKeteranganIzinKeramaianRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganIzinKeramaianRequest $request)
    {
        $input   = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();
        
        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;
        
        $suratKeteranganIzinKeramaian = $this->suratKeteranganIzinKeramaianRepository->create($input);

        Flash::success('Surat Keterangan Izin Keramaian saved successfully.');

        return redirect(route('suratKeteranganIzinKeramaians.index'));
    }

    /**
     * Display the specified SuratKeteranganIzinKeramaian.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganIzinKeramaian = $this->suratKeteranganIzinKeramaianRepository->find($id);

        if (empty($suratKeteranganIzinKeramaian)) {
            Flash::error('Surat Keterangan Izin Keramaian not found');

            return redirect(route('suratKeteranganIzinKeramaians.index'));
        }

        return view('surat_keterangan_izin_keramaians.show')->with('suratKeteranganIzinKeramaian', $suratKeteranganIzinKeramaian);
    }

    /**
     * Show the form for editing the specified SuratKeteranganIzinKeramaian.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganIzinKeramaian = $this->suratKeteranganIzinKeramaianRepository->find($id);
        $status = Status::all();

        if (empty($suratKeteranganIzinKeramaian)) {
            Flash::error('Surat Keterangan Izin Keramaian not found');

            return redirect(route('suratKeteranganIzinKeramaians.index'));
        }

        return view('surat_keterangan_izin_keramaians.edit')->with(['suratKeteranganIzinKeramaian' => $suratKeteranganIzinKeramaian , 'statuses' => $status]);
    }

    /**
     * Update the specified SuratKeteranganIzinKeramaian in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganIzinKeramaianRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganIzinKeramaianRequest $request)
    {
        $suratKeteranganIzinKeramaian = $this->suratKeteranganIzinKeramaianRepository->find($id);

        if (empty($suratKeteranganIzinKeramaian)) {
            Flash::error('Surat Keterangan Izin Keramaian not found');

            return redirect(route('suratKeteranganIzinKeramaians.index'));
        }

        $allRequestData          = $request->all();
        
        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganIzinKeramaian::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganIzinKeramaian->tanggal_terbit == NULL && $suratKeteranganIzinKeramaian->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "470.1/ $nomorSurat / I/ SED-D";
            }
            $suratKeteranganIzinKeramaian = $this->suratKeteranganIzinKeramaianRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;
            $suratKeteranganIzinKeramaian = $this->suratKeteranganIzinKeramaianRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Izin Keramaian updated successfully.');

            return redirect(route('suratKeteranganIzinKeramaians.index'));
        }
    }

    /**
     * Remove the specified SuratKeteranganIzinKeramaian from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganIzinKeramaian = $this->suratKeteranganIzinKeramaianRepository->find($id);

        if (empty($suratKeteranganIzinKeramaian)) {
            Flash::error('Surat Keterangan Izin Keramaian not found');

            return redirect(route('suratKeteranganIzinKeramaians.index'));
        }

        $this->suratKeteranganIzinKeramaianRepository->delete($id);

        Flash::success('Surat Keterangan Izin Keramaian deleted successfully.');

        return redirect(route('suratKeteranganIzinKeramaians.index'));
    }

    public function print($id)
    {
        $SuratKeteranganIzinKeramaian = SuratKeteranganIzinKeramaian::find($id);
        $preferences                  = Preference::get()->keyBy('label')->toArray();
        $dateNow                      = Carbon::now()->format('d-m-Y');
        $pdf                          = \PDF::loadView('khusus.suratKeteranganIzinKeramaian.surat', ['SuratKeteranganIzinKeramaian' => $SuratKeteranganIzinKeramaian,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->setPaper('a4', 'portrait')->download('surat.pdf');
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganIzinKeramaian.index');
    }

    public function insertGuest(SuratKeteranganIzinKeramaianRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $suratKeteranganIzinKeramaian                    = new SuratKeteranganIzinKeramaian;
        $suratKeteranganIzinKeramaian->user_id           = $user->id;
        $suratKeteranganIzinKeramaian->nama              = $request->nama;
        $suratKeteranganIzinKeramaian->tempat_lahir      = $request->tempat_lahir;
        $suratKeteranganIzinKeramaian->tgl_lahir         = $request->tgl_lahir;
        $suratKeteranganIzinKeramaian->kewarganegaraan   = $request->kewarganegaraan;
        $suratKeteranganIzinKeramaian->agama             = $request->agama;
        $suratKeteranganIzinKeramaian->status_perkawinan = $request->status_perkawinan;
        $suratKeteranganIzinKeramaian->alamat            = $request->alamat;
        $suratKeteranganIzinKeramaian->acara_kegiatan    = $request->acara_kegiatan;
        $suratKeteranganIzinKeramaian->tgl_mulai         = $request->tgl_mulai;
        $suratKeteranganIzinKeramaian->tgl_selesai       = $request->tgl_selesai;
        $suratKeteranganIzinKeramaian->alamat_acara      = $request->alamat_acara;
        $suratKeteranganIzinKeramaian->status_id         = $status->id;
        $suratKeteranganIzinKeramaian->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeteranganIzinKeramaian->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganIzinKeramaian.kodeSurat')->with([
            'suratKeteranganIzinKeramaian' => $suratKeteranganIzinKeramaian,
            'hariKadarluasaPengajuan'      => $hariKadarluasaPengajuan
        ]);
    }
}
