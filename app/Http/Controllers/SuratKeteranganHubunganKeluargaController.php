<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganHubunganKeluargaRequest;
use App\Http\Requests\UpdateSuratKeteranganHubunganKeluargaRequest;
use App\Repositories\SuratKeteranganHubunganKeluargaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeteranganHubunganKeluarga;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganHubunganKeluargaRequest;

class SuratKeteranganHubunganKeluargaController extends AppBaseController
{
    /** @var  SuratKeteranganHubunganKeluargaRepository */
    private $suratKeteranganHubunganKeluargaRepository;

    public function __construct(SuratKeteranganHubunganKeluargaRepository $suratKeteranganHubunganKeluargaRepo)
    {
        $this->suratKeteranganHubunganKeluargaRepository = $suratKeteranganHubunganKeluargaRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeteranganHubunganKeluarga.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeteranganHubunganKeluargas = $this->suratKeteranganHubunganKeluargaRepository->all();

        return view('surat_keterangan_hubungan_keluargas.index')
            ->with('suratKeteranganHubunganKeluargas', $suratKeteranganHubunganKeluargas);
    }

    /**
     * Show the form for creating a new SuratKeteranganHubunganKeluarga.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangan_hubungan_keluargas.create');
    }

    /**
     * Store a newly created SuratKeteranganHubunganKeluarga in storage.
     *
     * @param CreateSuratKeteranganHubunganKeluargaRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganHubunganKeluargaRequest $request)
    {
        $input   = $request->all();
        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeteranganHubunganKeluarga = $this->suratKeteranganHubunganKeluargaRepository->create($input);

        Flash::success('Surat Keterangan Hubungan Keluarga saved successfully.');

        return redirect(route('suratKeteranganHubunganKeluargas.index'));
    }

    /**
     * Display the specified SuratKeteranganHubunganKeluarga.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeteranganHubunganKeluarga = $this->suratKeteranganHubunganKeluargaRepository->find($id);

        if (empty($suratKeteranganHubunganKeluarga)) {
            Flash::error('Surat Keterangan Hubungan Keluarga not found');

            return redirect(route('suratKeteranganHubunganKeluargas.index'));
        }

        return view('surat_keterangan_hubungan_keluargas.show')->with('suratKeteranganHubunganKeluarga', $suratKeteranganHubunganKeluarga);
    }

    /**
     * Show the form for editing the specified SuratKeteranganHubunganKeluarga.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeteranganHubunganKeluarga = $this->suratKeteranganHubunganKeluargaRepository->find($id);

        if (empty($suratKeteranganHubunganKeluarga)) {
            Flash::error('Surat Keterangan Hubungan Keluarga not found');

            return redirect(route('suratKeteranganHubunganKeluargas.index'));
        }

        return view('surat_keterangan_hubungan_keluargas.edit')->with('suratKeteranganHubunganKeluarga', $suratKeteranganHubunganKeluarga);
    }

    /**
     * Update the specified SuratKeteranganHubunganKeluarga in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganHubunganKeluargaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganHubunganKeluargaRequest $request)
    {
        
        $suratKeteranganHubunganKeluarga = $this->suratKeteranganHubunganKeluargaRepository->find($id);

        if (empty($suratKeteranganHubunganKeluarga)) {
            Flash::error('Surat Keterangan Hubungan Keluarga not found');

            return redirect(route('suratKeteranganHubunganKeluargas.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeteranganHubunganKeluarga::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeteranganHubunganKeluarga->tanggal_terbit == NULL && $suratKeteranganHubunganKeluarga->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "581.1// $nomorSurat /PEM";
            }
            
            $suratKeteranganHubunganKeluarga = $this->suratKeteranganHubunganKeluargaRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeteranganHubunganKeluarga = $this->suratKeteranganHubunganKeluargaRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan Hubungan Keluarga updated successfully.');

            return redirect(route('suratKeteranganHubunganKeluargas.index'));
        } 
    }

    /**
     * Remove the specified SuratKeteranganHubunganKeluarga from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeteranganHubunganKeluarga = $this->suratKeteranganHubunganKeluargaRepository->find($id);

        if (empty($suratKeteranganHubunganKeluarga)) {
            Flash::error('Surat Keterangan Hubungan Keluarga not found');

            return redirect(route('suratKeteranganHubunganKeluargas.index'));
        }

        $this->suratKeteranganHubunganKeluargaRepository->delete($id);

        Flash::success('Surat Keterangan Hubungan Keluarga deleted successfully.');

        return redirect(route('suratKeteranganHubunganKeluargas.index'));
    }

    public function print($id)
    {
        $suratKeteranganHubunganKeluarga = SuratKeteranganHubunganKeluarga::find($id);
        $preferences          = Preference::get()->keyBy('label')->toArray();
        $dateNow              = Carbon::now()->format('d-m-Y');
        $pdf                  = \PDF::loadView('khusus.suratKeteranganHubunganKeluarga.surat', ['suratKeteranganHubunganKeluarga' => $suratKeteranganHubunganKeluarga,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeteranganHubunganKeluarga.index');
    }

    public function insertGuest(SuratKeteranganHubunganKeluargaRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $suratKeteranganHubunganKeluarga                                  = new SuratKeteranganHubunganKeluarga;
        $suratKeteranganHubunganKeluarga->user_id                         = $user->id;
        $suratKeteranganHubunganKeluarga->nik_pihak_pertama               = $request->nik_pihak_pertama;
        $suratKeteranganHubunganKeluarga->nama_pihak_pertama              = $request->nama_pihak_pertama;
        $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_pertama     = $request->jenis_kelamin_pihak_pertama;
        $suratKeteranganHubunganKeluarga->tempat_lahir_pihak_pertama      = $request->tempat_lahir_pihak_pertama;
        $suratKeteranganHubunganKeluarga->tgl_lahir_pihak_pertama         = $request->tgl_lahir_pihak_pertama;
        $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_pertama = $request->status_perkawinan_pihak_pertama;
        $suratKeteranganHubunganKeluarga->pekerjaan_pihak_pertama         = $request->pekerjaan_pihak_pertama;
        $suratKeteranganHubunganKeluarga->alamat_pihak_pertama            = $request->alamat_pihak_pertama;
        $suratKeteranganHubunganKeluarga->nik_pihak_kedua                 = $request->nik_pihak_kedua;
        $suratKeteranganHubunganKeluarga->nama_pihak_kedua                = $request->nama_pihak_kedua;
        $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_kedua       = $request->jenis_kelamin_pihak_kedua;
        $suratKeteranganHubunganKeluarga->tempat_lahir_pihak_kedua        = $request->tempat_lahir_pihak_kedua;
        $suratKeteranganHubunganKeluarga->tgl_lahir_pihak_kedua           = $request->tgl_lahir_pihak_kedua;
        $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_kedua   = $request->status_perkawinan_pihak_kedua;
        $suratKeteranganHubunganKeluarga->pekerjaan_pihak_kedua           = $request->pekerjaan_pihak_kedua;
        $suratKeteranganHubunganKeluarga->alamat_pihak_kedua              = $request->alamat_pihak_kedua;
        $suratKeteranganHubunganKeluarga->hubungan_antara_kedua_pihak     = $request->hubungan_antara_kedua_pihak;
        $suratKeteranganHubunganKeluarga->status_id                       = $status->id;
        $suratKeteranganHubunganKeluarga->tanggal_pengajuan               = $dateNow->format('Y-m-d');
        $suratKeteranganHubunganKeluarga->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeteranganHubunganKeluarga.kodeSurat')->with([
            'suratKeteranganHubunganKeluarga' => $suratKeteranganHubunganKeluarga,
            'hariKadarluasaPengajuan'         => $hariKadarluasaPengajuan
            ]);
    }
}
