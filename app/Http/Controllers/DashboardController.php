<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SuratKeteranganDomisili;
use App\Models\SuratKeteranganPajak;
use App\Models\SuratKeteranganIzinKeramaian;
use App\Models\SuratKeteranganPenghasilan;

use Carbon\Carbon;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $name = null )
    {
        $suratKeteranganDomisili = SuratKeteranganDomisili::whereDate('tanggal_pengajuan', Carbon::today())->count();
        $suratKeteranganPajak = SuratKeteranganPajak::whereDate('tanggal_pengajuan', Carbon::today())->count();
        $suratKeteranganIzinKeramaian = SuratKeteranganIzinKeramaian::whereDate('tanggal_pengajuan', Carbon::today())->count();
        $suratKeteranganPenghasilan = SuratKeteranganPenghasilan::whereDate('tanggal_pengajuan', Carbon::today())->count();

        if($name == 'minggu'){
            $suratKeteranganDomisili = SuratKeteranganDomisili::whereBetween('tanggal_pengajuan', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
            $suratKeteranganPajak = SuratKeteranganPajak::whereBetween('tanggal_pengajuan', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
            $suratKeteranganIzinKeramaian = SuratKeteranganIzinKeramaian::whereBetween('tanggal_pengajuan', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
            $suratKeteranganPenghasilan = SuratKeteranganPenghasilan::whereBetween('tanggal_pengajuan', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        }else if($name == 'bulan') {
            $suratKeteranganDomisili = SuratKeteranganDomisili::whereMonth('tanggal_pengajuan', '=', Carbon::now()->month)->count();
            $suratKeteranganPajak = SuratKeteranganPajak::whereMonth('tanggal_pengajuan', '=', Carbon::now()->month)->count();
            $suratKeteranganIzinKeramaian = SuratKeteranganIzinKeramaian::whereMonth('tanggal_pengajuan', '=', Carbon::now()->month)->count();
            $suratKeteranganPenghasilan = SuratKeteranganPenghasilan::whereMonth('tanggal_pengajuan', '=', Carbon::now()->month)->count();
        }else if($name == 'tahun'){
            $suratKeteranganDomisili = SuratKeteranganDomisili::whereYear('tanggal_pengajuan', '=', Carbon::now()->year)->count();
            $suratKeteranganPajak = SuratKeteranganPajak::whereYear('tanggal_pengajuan', '=', Carbon::now()->year)->count();
            $suratKeteranganIzinKeramaian = SuratKeteranganIzinKeramaian::whereYear('tanggal_pengajuan', '=', Carbon::now()->year)->count();
            $suratKeteranganPenghasilan = SuratKeteranganPenghasilan::whereYear('tanggal_pengajuan', '=', Carbon::now()->year)->count();
        }

        $labelsSurat = [
            'Surat Keterangan Domisili',
            'Surat Keterangan Pajak',
            'Surat Keterangan Izin Keramaian',
            'Surat Keterangan Penghasilan'
        ];

        $dataSurat = [
            $suratKeteranganDomisili,
            $suratKeteranganPajak,
            $suratKeteranganIzinKeramaian,
            $suratKeteranganPenghasilan
        ];

        $data = [
            'labelsSurat' => $labelsSurat,
            'dataSurat'   => $dataSurat,
            'labelGrafik' => 'Total Surat Yang Pernah Diajukan'
        ];

        // return $data;
        return view('dashboard.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
