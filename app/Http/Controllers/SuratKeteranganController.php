<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSuratKeteranganRequest;
use App\Http\Requests\UpdateSuratKeteranganRequest;
use App\Repositories\SuratKeteranganRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\SuratKeterangan;
use App\Models\Status;
use App\Models\Preference;
use Carbon\Carbon;
use App\Http\Requests\SuratKeteranganRequest;

class SuratKeteranganController extends AppBaseController
{
    /** @var  SuratKeteranganRepository */
    private $suratKeteranganRepository;

    public function __construct(SuratKeteranganRepository $suratKeteranganRepo)
    {
        $this->suratKeteranganRepository = $suratKeteranganRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the SuratKeterangan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $suratKeterangans = $this->suratKeteranganRepository->all();

        return view('surat_keterangans.index')
            ->with('suratKeterangans', $suratKeterangans);
    }

    /**
     * Show the form for creating a new SuratKeterangan.
     *
     * @return Response
     */
    public function create()
    {
        return view('surat_keterangans.create');
    }

    /**
     * Store a newly created SuratKeterangan in storage.
     *
     * @param CreateSuratKeteranganRequest $request
     *
     * @return Response
     */
    public function store(CreateSuratKeteranganRequest $request)
    {
        $input = $request->all();

        $dateNow = Carbon::now()->format('Y-m-d');
        $status  = Status::where('status','draf')->first();

        $input['user_id']           = auth()->user()->id;
        $input['status_id']         = $status->id;
        $input['tanggal_pengajuan'] = $dateNow;

        $suratKeterangan = $this->suratKeteranganRepository->create($input);

        Flash::success('Surat Keterangan saved successfully.');

        return redirect(route('suratKeterangans.index'));
    }

    /**
     * Display the specified SuratKeterangan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $suratKeterangan = $this->suratKeteranganRepository->find($id);

        if (empty($suratKeterangan)) {
            Flash::error('Surat Keterangan not found');

            return redirect(route('suratKeterangans.index'));
        }

        return view('surat_keterangans.show')->with('suratKeterangan', $suratKeterangan);
    }

    /**
     * Show the form for editing the specified SuratKeterangan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $suratKeterangan = $this->suratKeteranganRepository->find($id);

        if (empty($suratKeterangan)) {
            Flash::error('Surat Keterangan not found');

            return redirect(route('suratKeterangans.index'));
        }

        return view('surat_keterangans.edit')->with('suratKeterangan', $suratKeterangan);
    }

    /**
     * Update the specified SuratKeterangan in storage.
     *
     * @param int $id
     * @param UpdateSuratKeteranganRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSuratKeteranganRequest $request)
    {
        $suratKeterangan = $this->suratKeteranganRepository->find($id);

        if (empty($suratKeterangan)) {
            Flash::error('Surat Keterangan not found');

            return redirect(route('suratKeterangans.index'));
        }

        $allRequestData          = $request->all();

        if($request->terima == 'Terima'){
            $status  = Status::where('status','Diterima')->first();
            $dateNow = Carbon::now()->format('Y-m-d');

            $countSurat = SuratKeterangan::whereNotNull('nomor_surat')->count();
            $nomorSurat = str_pad(($countSurat+1), 4, 0,STR_PAD_LEFT);

            $allRequestData['status_id'] = $status->id;
            if($suratKeterangan->tanggal_terbit == NULL && $suratKeterangan->nomor_surat == NULL){
                $allRequestData['tanggal_terbit'] = $dateNow;
                $allRequestData['nomor_surat']    = "470 / $nomorSurat /I/ PEM";
            }
            
            $suratKeterangan = $this->suratKeteranganRepository->update($allRequestData, $id);

            return redirect()->back()->with('status','print_surat');
        }else{
            $status       = Status::where('status','Ditolak')->first();

            $allRequestData['status_id'] = $status->id;

            $suratKeterangan = $this->suratKeteranganRepository->update($allRequestData, $id);

            Flash::success('Surat Keterangan updated successfully.');

            return redirect(route('suratKeterangans.index'));
        }
    }

    /**
     * Remove the specified SuratKeterangan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $suratKeterangan = $this->suratKeteranganRepository->find($id);

        if (empty($suratKeterangan)) {
            Flash::error('Surat Keterangan not found');

            return redirect(route('suratKeterangans.index'));
        }

        $this->suratKeteranganRepository->delete($id);

        Flash::success('Surat Keterangan deleted successfully.');

        return redirect(route('suratKeterangans.index'));
    }

    public function print($id)
    {
        $suratKeterangan = SuratKeterangan::find($id);
        $preferences     = Preference::get()->keyBy('label')->toArray();
        $dateNow         = Carbon::now()->format('d-m-Y');
        $pdf             = \PDF::loadView('khusus.suratKeterangan.surat', ['suratKeterangan' => $suratKeterangan,'preferences' => $preferences, 'dateNow' => $dateNow]);
        return $pdf->stream('surat.pdf',array("Attachment" => 0));
    }

    public function indexGuest()
    {
        return view('khusus.suratKeterangan.index');
    }

    public function insertGuest(SuratKeteranganRequest $request)
    {
        $status  = Status::where('status','draf')->first();
        $user    = auth()->user();
        $dateNow = Carbon::now();

        $suratKeterangan                    = new SuratKeterangan;
        $suratKeterangan->user_id           = $user->id;
        $suratKeterangan->nik               = $request->nik;
        $suratKeterangan->nama              = $request->nama;
        $suratKeterangan->jenis_kelamin     = $request->jenis_kelamin;
        $suratKeterangan->tempat_lahir      = $request->tempat_lahir;
        $suratKeterangan->tgl_lahir         = $request->tgl_lahir;
        $suratKeterangan->status_perkawinan = $request->status_perkawinan;
        $suratKeterangan->kewarganegaraan   = $request->kewarganegaraan;
        $suratKeterangan->agama             = $request->agama;
        $suratKeterangan->pekerjaan         = $request->pekerjaan;
        $suratKeterangan->alamat            = $request->alamat;
        $suratKeterangan->administrasi      = $request->administrasi;
        $suratKeterangan->status_id         = $status->id;
        $suratKeterangan->tanggal_pengajuan = $dateNow->format('Y-m-d');
        $suratKeterangan->save();

        $hariKadarluasaPengajuan = $dateNow->addDays(2)->format('Y-m-d');
        return view('khusus.suratKeterangan.kodeSurat')->with([
            'suratKeterangan' => $suratKeterangan,
            'hariKadarluasaPengajuan' => $hariKadarluasaPengajuan
            ]);
    }
}
