<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganDomisiliRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'             => 'required|digits:16',
            'nama'            => 'required',
            'alamat'          => 'required',
            'alamat_domisili' => 'required',
            'dusun'           => 'required',
            'desa'            => 'required',
            'kecamatan'       => 'required',
            'kabupaten'       => 'required',
            'maksud'          => 'required',
            'ketua_rt'        => 'required',
            'kepala_dusun'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nik.required'             => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'               => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'            => 'Nama Harus Diisi',
            'alamat.required'          => 'Alamat Harus Diisi',
            'alamat_domisili.required' => 'Alamat Domisili Harus Diisi',
            'dusun.required'           => 'Nama Dusun Harus Diisi',
            'desa.required'            => 'Nama Desa Harus Diisi',
            'kecamatan.required'       => 'Nama Kecamatan Harus Diisi',
            'kabupaten.required'       => 'Nama Kabupaten Harus Diisi',
            'maksud.required'          => 'Tujuan Pembuatan Surat Harus Diisi',
            'ketua_rt.required'        => 'Nama Ketua RT Harus Diisi',
            'kepala_dusun.required'    => 'Nama Kepala Dusun Harus Diisi'
        ];
    }
}
