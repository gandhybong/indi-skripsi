<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganPenghasilanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'               => 'required|digits:16',
            'nama'              => 'required',
            'jenis_kelamin'     => 'required',
            'tempat_lahir'      => 'required',
            'tgl_lahir'         => 'required',
            'kewarganegaraan'   => 'required',
            'agama'             => 'required',
            'status_perkawinan' => 'required',
            'alamat'            => 'required',
            'pekerjaan'         => 'required',
            'penghasilan'       => 'required',
            'maksud'            => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nik.required'               => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'                 => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'              => 'Nama Harus Diisi',
            'jenis_kelamin.required'     => 'Jenis Kelamin Harus Dipilih',
            'tempat_lahir.required'      => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'         => 'Tanggal Lahir Harus Diisi',
            'kewarganegaraan.required'   => 'Kewarganegaraan Harus Diisi',
            'agama.required'             => 'Agama Harus Dipilih',
            'status_perkawinan.required' => 'Status Perkawinan Harus Dipilih',
            'alamat.required'            => 'Alamat Harus Diisi',
            'pekerjaan.required'         => 'Pekerjaan Harus Diisi',
            'penghasilan.required'       => 'Penghasilan Harus diisi',
            'maksud.required'            => 'Tujuan Pembuatan Surat Harus Diisi'
        ];
    }
}
