<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganPanggilanCeraiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik_pihak_pertama'       => 'required|digits:16',
            'nama_pihak_pertama'      => 'required',
            'umur_pihak_pertama'      => 'required',
            'pekerjaan_pihak_pertama' => 'required',
            'alamat_pihak_pertama'    => 'required',
            'nik_pihak_kedua'         => 'required|digits:16',
            'nama_pihak_kedua'        => 'required',
            'umur_pihak_kedua'        => 'required',
            'pekerjaan_pihak_kedua'   => 'required',
            'alamat_pihak_kedua'      => 'required',
            'yang_ditinggalkan'       => 'required',
            'lama_ditinggal'          => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nik_pihak_pertama.required'       => 'Nomor Induk Kependudukan (NIK) Pihak Pertama Harus Diisi.',
            'nik_pihak_pertama.digits:16'      => 'Nomor Induk Kependudukan (NIK) Pihak Pertama Harus Terdiri Dari 16 Karakter.',
            'nama_pihak_pertama.required'      => 'Nama Pihak Pertama Harus Diisi',
            'umur_pihak_pertama.required'      => 'Umur Pihak Pertama Harus Diisi',
            'pekerjaan_pihak_pertama.required' => 'Pekerjaan Pihak Pertama Harus Diisi',
            'alamat_pihak_pertama.required'    => 'Alamat Pihak Pertama Harus Diisi',
            'nik_pihak_kedua.required'         => 'Nomor Induk Kependudukan (NIK) Pihak Kedua Harus Diisi.',
            'nik_pihak_kedua.digits:16'        => 'Nomor Induk Kependudukan (NIK) Pihak Kedua Harus Terdiri Dari 16 Karakter.',
            'nama_pihak_kedua.required'        => 'Nama Pihak Kedua Harus Diisi',
            'umur_pihak_kedua.required'        => 'Umur Pihak Kedua Harus Diisi',
            'pekerjaan_pihak_kedua.required'   => 'Pekerjaan Pihak Kedua Harus Diisi',
            'alamat_pihak_kedua.required'      => 'Alamat Pihak Kedua Harus Diisi',
            'yang_ditinggalkan.required'       => 'Yang Ditinggalkan Harus Diisi',
            'lama_ditinggal.required'          => 'Lama Ditinggal Harus Diisi',
        ];
    }
}
