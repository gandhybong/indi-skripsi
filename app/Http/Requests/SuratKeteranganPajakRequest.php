<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganPajakRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'               => 'required|digits:16',
            'nama'              => 'required',
            'alamat'            => 'required',
            'jenis_kelamin'     => 'required',
            'tempat_lahir'      => 'required',
            'tgl_lahir'         => 'required',
            'kewarganegaraan'   => 'required',
            'agama'             => 'required',
            'status_perkawinan' => 'required',
            'pekerjaan'         => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nik.required'               => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'                 => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'              => 'Nama Harus Diisi',
            'alamat.required'            => 'Alamat Harus Diisi',
            'jenis_kelamin.required'     => 'Jenis Kelamin Harus Diisi',
            'tempat_lahir.required'      => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'         => 'Tanggal Lahir Harus Diisi',
            'kewarganegaraan.required'   => 'Kewarganegaraan Harus Diisi',
            'agama.required'             => 'Agama Harus Diisi',
            'status_perkawinan.required' => 'Status Perkawinan Harus Diisi',
            'pekerjaan.required'         => 'Pekerjaan Harus Diisi',
        ];
    }
}
