<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratRekomendasiImbRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'                    => 'required|digits:16',
            'nama'                   => 'required',
            'jenis_kelamin'          => 'required',
            'tempat_lahir'           => 'required',
            'tgl_lahir'              => 'required',
            'kewarganegaraan'        => 'required',
            'agama'                  => 'required',
            'status_perkawinan'      => 'required',
            'pekerjaan'              => 'required',
            'alamat'                 => 'required',
            'maksud'                 => 'required',
            'keperluan'              => 'required',
            'alamat_bangunan'        => 'required',
            'luas_bangunan'          => 'required',
            'luas_tanah'             => 'required',
            'nama_pemilik_tanah'     => 'required',
            'nomor_sertifikat_tanah' => 'required',
            'bangunan_terbuat_dari'  => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nik.required'                    => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'                      => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'                   => 'Nama Harus Diisi',
            'jenis_kelamin.required'          => 'Jenis Kelamin Harus Dipilih',
            'tempat_lahir.required'           => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'              => 'Tanggal Lahir Harus Diisi',
            'kewarganegaraan.required'        => 'Kewarganegaraan Harus Diisi',
            'agama.required'                  => 'Agama Harus Dipilih',
            'status_perkawinan.required'      => 'Status Perkawinan Harus Dipilih',
            'pekerjaan.required'              => 'Pekerjaan Harus Diisi',
            'alamat.required'                 => 'Alamat Harus Diisi',
            'maksud.required'                 => 'Tujuan Pembuatan Surat Harus Diisi',
            'keperluan.required'              => 'Keperluan Pembuatan Surat Harus Diisi',
            'alamat_bangunan.required'        => 'Alamat Bangunan Harus Diisi',
            'luas_bangunan.required'          => 'Luas Bangunan Harus Diisi',
            'luas_tanah.required'             => 'Luas Tanah Harus Diisi',
            'nama_pemilik_tanah.required'     => 'Nama Pemilik Tanah Harus Diisi',
            'nomor_sertifikat_tanah.required' => 'Nomor Sertifikat Tanah Harus Diisi',
            'bangunan_terbuat_dari.required'  => 'Bahan Bangunan Harus Diisi',
        ];
    }
}
