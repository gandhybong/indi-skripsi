<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganTentangKematianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'               => 'required|digits:16',
            'nama'              => 'required',
            'bin_binti'         => 'required',
            'jenis_kelamin'     => 'required',
            'tempat_lahir'      => 'required',
            'tgl_lahir'         => 'required',
            'pekerjaan'         => 'required',
            'agama'             => 'required',
            'status_perkawinan' => 'required',
            'alamat'            => 'required',
            'tgl_meninggal'     => 'required',
            'tempat_meninggal'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nik.required'               => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'                 => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'              => 'Nama Harus Diisi',
            'bin_binti.required'         => 'Bin Atau Binti Harus Diisi',
            'jenis_kelamin.required'     => 'Jenis Kelamin Harus Diisi',
            'tempat_lahir.required'      => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'         => 'Tanggal Lahir Harus Diisi',
            'pekerjaan.required'         => 'Pekerjaan Harus Diisi',
            'agama.required'             => 'Agama Harus Diisi',
            'status_perkawinan.required' => 'Status Perkawinan Harus Diisi',
            'alamat.required'            => 'Alamat Harus Diisi',
            'tgl_meninggal.required'     => 'Tanggal Meninggal Harus Diisi',
            'tempat_meninggal.required'  => 'Tempat Meninggal Harus Diisi',
        ];
    }
}
