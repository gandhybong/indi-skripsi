<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganBedaNamaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'               => 'required|digits:16',
            'nama'              => 'required',
            'nama_kedua'        => 'required',
            'tempat_lahir'      => 'required',
            'tgl_lahir'         => 'required',
            'jenis_kelamin'     => 'required',
            'status_perkawinan' => 'required',
            'agama'             => 'required',
            'pekerjaan'         => 'required',
            'alamat'            => 'required',
            'tertera_di'        => 'required',
            'maksud'            => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nik.required'               => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'                 => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'              => 'Nama Harus Diisi',
            'nama_kedua.required'        => 'Nama Kedua Harus Diisi',
            'tempat_lahir.required'      => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'         => 'Tanggal Lahir Harus Diisi',
            'jenis_kelamin.required'     => 'Jenis Kelamin Harus Dipilih',
            'status_perkawinan.required' => 'Status Perkawinan Harus Dipilih',
            'agama.required'             => 'Agama Harus Dipilih',
            'pekerjaan.required'         => 'Pekerjaan Harus Diisi',
            'alamat.required'            => 'Alamat Harus Diisi',
            'tertera_di'                 => 'Nama Tertera Di Harus Diisi',
            'maksud.required'            => 'Tujuan Pembuatan Surat Harus Diisi'
        ];
    }
}
