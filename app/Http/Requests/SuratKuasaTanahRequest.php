<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKuasaTanahRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'                           => 'required|digits:16',
            'nama'                          => 'required',
            'jenis_kelamin'                 => 'required',
            'tempat_lahir'                  => 'required',
            'tgl_lahir'                     => 'required',
            'status_perkawinan'             => 'required',
            'pekerjaan'                     => 'required',
            'alamat'                        => 'required',
            'nik_pihak_kedua'               => 'required|digits:16',
            'nama_pihak_kedua'              => 'required',
            'jenis_kelamin_pihak_kedua'     => 'required',
            'tempat_lahir_pihak_kedua'      => 'required',
            'tgl_lahir_pihak_kedua'         => 'required',
            'status_perkawinan_pihak_kedua' => 'required',
            'pekerjaan_pihak_kedua'         => 'required',
            'alamat_pihak_kedua'            => 'required',
            'status_kedua_pihak'            => 'required',
            'jenis_tanah'                   => 'required',
            'nomor_sertifikat_tanah'        => 'required',
            'atas_nama'                     => 'required',
            'tahun'                         => 'required|digits:4',
            'luas_tanah'                    => 'required',
            'maksud'                        => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nik.required'                           => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'                             => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'                          => 'Nama Harus Diisi',
            'jenis_kelamin.required'                 => 'Jenis Kelamin Harus Dipilih',
            'tempat_lahir.required'                  => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'                     => 'Tanggal Lahir Harus Diisi',
            'status_perkawinan.required'             => 'Status Perkawinan Harus Dipilih',
            'pekerjaan.required'                     => 'Pekerjaan Harus Diisi',
            'alamat.required'                        => 'Alamat Harus Diisi',
            'nik_pihak_kedua.required'               => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik_pihak_kedua.digits'                 => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama_pihak_kedua.required'              => 'Nama Harus Diisi',
            'jenis_kelamin_pihak_kedua.required'     => 'Jenis Kelamin Harus Dipilih',
            'tempat_lahir_pihak_kedua.required'      => 'Tempat Lahir Harus Diisi',
            'tgl_lahir_pihak_kedua.required'         => 'Tanggal Lahir Harus Diisi',
            'status_perkawinan_pihak_kedua.required' => 'Status Perkawinan Harus Dipilih',
            'pekerjaan_pihak_kedua.required'         => 'Pekerjaan Harus Diisi',
            'alamat_pihak_kedua.required'            => 'Alamat Harus Diisi',
            'status_kedua_pihak.required'            => 'Status Kedua Pihak Harus Diisi',
            'jenis_tanah.required'                   => 'jenis Tanah Harus Diisi',
            'nomor_sertifikat_tanah.required'        => 'Nomor Sertifikat Tanah Harus Diisi',
            'atas_nama.required'                     => 'Atas Nama Harus Diisi',
            'tahun.required'                         => 'Tahun Harus Diisi',
            'tahun.digits'                           => 'Tahun Harus Memliki Format Yang Salah',
            'luas_tanah.required'                    => 'Luas Harus Diisi',
            'maksud.required'                        => 'Maksud Harus Diisi',
        ];
    }
}
