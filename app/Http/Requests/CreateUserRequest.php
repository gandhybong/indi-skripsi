<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class CreateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'       => 'required|size:16',
            'name'      => 'required',
            'tgl_lahir' => 'required|date',
            'username'  => 'required|unique:users,username',
            'password'  => 'required|min:3',
            'email'     => 'required|unique:users,email',
        ];
    }

    public function messages()
    {
        return [
            'nik.required' => 'Nomor Induk Kependudukan (NIK) harus diisi.',
            'nik.size' => 'Nomor Induk Kependudukan (NIK) harus terdiri dari 16 karakter.',
            'name.required' => 'Nama pemohon harus diisi.',
            'tgl_lahir.required' => 'Tanggal lahir harus diisi.',
            'tgl_lahir.date' => 'Tanggal lahir harus berupa tanggal.',
            'username.required' => 'Username harus diisi.',
            'username.unique' => 'Username telah digunakan sebelumnya. Gunakan username lain.',
            'password.required' => 'Password harus diisi.',
            'password.min' => 'Password terdiri dari minimal tiga karaketer.',
            'email.required' => 'Email harus diisi.',
            'email.unique' => 'Email sudah didaftarkan sebelumnya. Gunakan email lain.'
        ];
    }
}
