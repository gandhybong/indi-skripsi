<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratPernyataanHibahRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'                    => 'required|digits:16',
            'nama'                   => 'required',
            'tempat_lahir'           => 'required',
            'tgl_lahir'              => 'required',
            'kewarganegaraan'        => 'required',
            'agama'                  => 'required',
            'pekerjaan'              => 'required',
            'alamat'                 => 'required',
            'letak_tanah'            => 'required',
            'lebar'                  => 'required',
            'panjang'                => 'required',
            'dihibahkan_kepada'      => 'required',
            'untuk_dijadikan'        => 'required',
            'batas_utara'            => 'required',
            'batas_timur'            => 'required',
            'batas_selatan'          => 'required',
            'batas_barat'            => 'required',
            'sejarah_dikuasai_oleh'  => 'required',
            'sejarah_tahun_dikuasai' => 'required',
            'jabatan_saksi_pertama'  => 'required',
            'nama_saksi_pertama'     => 'required',
            'jabatan_saksi_kedua'    => 'required',
            'nama_saksi_kedua'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nik.required'                    => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'                      => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'                   => 'Nama Harus Diisi',
            'tempat_lahir.required'           => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'              => 'Tanggal Lahir Harus Diisi',
            'kewarganegaraan.required'        => 'Kewarganegaraan Harus Diisi',
            'agama.required'                  => 'Agama Harus Dipilih',
            'pekerjaan.required'              => 'Pekerjaan Harus Diisi',
            'alamat.required'                 => 'Alamat Harus Diisi',
            'letak_tanah.required'            => 'Letak Tanah Harus Diisi',
            'lebar.required'                  => 'Lebar Tanah Harus Diisi',
            'panjang.required'                => 'Panjang Tanah Harus Diisi',
            'dihibahkan_kepada.required'      => 'Dihibahkan Harus Diisi',
            'untuk_dijadikan.required'        => 'Untuk Dijadikan Harus Diisi',
            'batas_utara.required'            => 'Batas Harus Diisi',
            'batas_timur.required'            => 'Batas Harus Diisi',
            'batas_selatan.required'          => 'Batas Harus Diisi',
            'batas_barat.required'            => 'Batas Harus Diisi',
            'sejarah_dikuasai_oleh.required'  => 'Sejarah Dikuasai Oleh Harus Diisi',
            'sejarah_tahun_dikuasai.required' => 'Sejarah Tahun Dikuasai Harus Diisi',
            'jabatan_saksi_pertama.required'  => 'Jabatan Saksi Pertama Harus Diisi',
            'nama_saksi_pertama.required'     => 'Nama Saksi Pertama Harus Diisi',
            'jabatan_saksi_kedua.required'    => 'Jabatan Saksi Kedua Harus Diisi',
            'nama_saksi_kedua.required'       => 'Nama Saksi Kedua Harus Diisi',
        ];
    }
}
