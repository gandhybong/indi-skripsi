<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganHubunganKeluargaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik_pihak_pertama' => 'required|digits:16',
            'nama_pihak_pertama' => 'required',
            'jenis_kelamin_pihak_pertama' => 'required',
            'tempat_lahir_pihak_pertama' => 'required',
            'tgl_lahir_pihak_pertama' => 'required',
            'status_perkawinan_pihak_pertama' => 'required',
            'pekerjaan_pihak_pertama' => 'required',
            'alamat_pihak_pertama' => 'required',
            'nik_pihak_kedua' => 'required|digits:16',
            'nama_pihak_kedua' => 'required',
            'jenis_kelamin_pihak_kedua' => 'required',
            'tempat_lahir_pihak_kedua' => 'required',
            'tgl_lahir_pihak_kedua' => 'required',
            'status_perkawinan_pihak_kedua' => 'required',
            'pekerjaan_pihak_kedua' => 'required',
            'alamat_pihak_kedua' => 'required',
            'hubungan_antara_kedua_pihak' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nik_pihak_pertama.required'  => 'Nomor Induk Kependudukan (NIK) Pihak Pertama Harus Diisi.',
            'nik_pihak_pertama.digits:16' => 'Nomor Induk Kependudukan (NIK) Pihak Pertama Harus Terdiri Dari 16 Karakter.',
            'nama_pihak_pertama.required' => 'Nama Pihak Pertama Harus Diisi',
            'jenis_kelamin_pihak_pertama.required' => 'Jenis Kelamin Pihak Pertama Harus Diisi',
            'tempat_lahir_pihak_pertama.required' => 'Tempat Lahir Pihak Pertama Harus Diisi',
            'tgl_lahir_pihak_pertama.required' => 'Tanggal Lahir Pihak Pertama Harus Diisi',
            'status_perkawinan_pihak_pertama.required' => 'Status Perkawinan Pihak Pertama Harus Diisi',
            'pekerjaan_pihak_pertama.required' => 'Pekerjaan Pihak Pertama Harus Diisi',
            'alamat_pihak_pertama.required' => 'Alamat Pihak Pertama Harus Diisi',
            'nik_pihak_kedua.required' => 'Nomor Induk Kependudukan (NIK) Pihak Kedua Harus Diisi.',
            'nik_pihak_kedua.digits:16' => 'Nomor Induk Kependudukan (NIK) Pihak Kedua Harus Terdiri Dari 16 Karakter.',
            'nama_pihak_kedua.required' => 'Nama Pihak Kedua Harus Diisi',
            'jenis_kelamin_pihak_kedua.required' => 'Jenis Kelamin Pihak Kedua Harus Diisi',
            'tempat_lahir_pihak_kedua.required' => 'Tempat Lahir Pihak Kedua Harus Diisi',
            'tgl_lahir_pihak_kedua.required' => 'Tanggal Lahir Pihak Kedua Harus Diisi',
            'status_perkawinan_pihak_kedua.required' => 'Status Perkawinan Pihak Kedua Harus Diisi',
            'pekerjaan_pihak_kedua.required' => 'Pekerjaan Pihak Kedua Harus Diisi',
            'alamat_pihak_kedua.required' => 'Alamat Pihak Kedua Harus Diisi',
            'hubungan_antara_kedua_pihak.required' => 'Hubungan Antara Kedua Pihak Harus Diisi',
        ];
    }
}
