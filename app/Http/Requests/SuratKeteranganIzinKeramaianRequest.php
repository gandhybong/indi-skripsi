<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganIzinKeramaianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'              => 'required',
            'tempat_lahir'      => 'required',
            'tgl_lahir'         => 'required',
            'kewarganegaraan'   => 'required',
            'agama'             => 'required',
            'status_perkawinan' => 'required',
            'alamat'            => 'required',
            'alamat_acara'      => 'required',
            'acara_kegiatan'    => 'required',
            'tgl_mulai'         => 'required',
            'tgl_selesai'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nama.required'              => 'Nama Harus Diisi',
            'tempat_lahir.required'      => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'         => 'Tanggal Lahir Harus Diisi',
            'kewarganegaraan.required'   => 'Kewarganegaraan Harus Diisi',
            'agama.required'             => 'Agama Harus Dipilih',
            'status_perkawinan.required' => 'Status Perkawinan Harus Dipilih',
            'alamat.required'            => 'Alamat Harus Diisi',
            'alamat_acara.required'      => 'Alamat Acara Harus Diisi',
            'acara_kegiatan.required'    => 'Nama Acara Harus Diisi',
            'tgl_mulai.required'         => 'Tanggal Mulai Harus Diisi',
            'tgl_selesai.required'       => 'Tanggal Selesai Harus Diisi',
        ];
    }
}
