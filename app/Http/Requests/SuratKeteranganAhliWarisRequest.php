<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratKeteranganAhliWarisRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik'                   => 'required|digits:16',
            'nama'                  => 'required',
            'tempat_lahir'          => 'required',
            'tgl_lahir'             => 'required',
            'jenis_kelamin'         => 'required',
            'kewarganegaraan'       => 'required',
            'agama'                 => 'required',
            'pekerjaan'             => 'required',
            'maksud'                => 'required',
            'saksi_pertama'         => 'required',
            'saksi_kedua'           => 'required',
            'ahli_waris'            => 'required|array|min:1',
            'ahli_waris.*'          => 'required|string|distinct',
            'hubungan_ahli_waris'   => 'required|array|min:1',
            'hubungan_ahli_waris.*' => 'required|string|distinct',
        ];
    }

    public function messages()
    {
        return [
            'nik.required'                   => 'Nomor Induk Kependudukan (NIK) Harus Diisi.',
            'nik.digits'                     => 'Nomor Induk Kependudukan (NIK) Harus Terdiri Dari 16 Karakter.',
            'nama.required'                  => 'Nama Harus Diisi',
            'tempat_lahir.required'          => 'Tempat Lahir Harus Diisi',
            'tgl_lahir.required'             => 'Tanggal Lahir Harus Diisi',
            'jenis_kelamin.required'         => 'Jenis Kelamin Harus Dipilih',
            'kewarganegaraan.required'       => 'Kewarganegaraan Harus Diisi',
            'agama.required'                 => 'Agama Harus Dipilih',
            'pekerjaan.required'             => 'Pekerjaan Harus Diisi',
            'maksud.required'                => 'Tujuan Pembuatan Surat Harus Diisi',
            'saksi_pertama.required'         => 'Saksi Pertama Harus Diisi',
            'saksi_kedua.required'           => 'Saksi Kedua Harus Diisi',
            'ahli_waris.required'            => 'Ahli Waris Harus Diiisi',
            'ahli_waris.array'               => 'Ahli Waris Harus Berupa Array',
            'ahli_waris.min'                 => 'Ahli Waris Minimal 1',
            'ahli_waris.*.required'          => 'Ahli Waris Harus Diisi',
            'ahli_waris.*.string'            => 'Ahli Waris Harus String',
            'ahli_waris.*.distinct'          => 'Ahli Waris Harus Berbeda-Beda',
            'hubungan_ahli_waris.required'   => 'Hubungan Ahli Waris Harus Diisi',
            'hubungan_ahli_waris.array'      => 'Ahli Waris Harus Berupa Array',
            'hubungan_ahli_waris.min'        => 'Ahli Waris Minimal 1',
            'hubungan_ahli_waris.*.required' => 'Hubungan Ahli Waris Harus Diisi',
            'hubungan_ahli_waris.*.string'   => 'Hubungan Ahli Waris Harus String',
            'hubungan_ahli_waris.*.distinct' => 'Hubungan Ahli Waris Harus Berbeda-beda',
        ];
    }
}
