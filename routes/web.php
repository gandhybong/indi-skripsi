<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if ( !auth()->user() )
        return view('general.index');
    else
        return redirect()->to( 'home' );
});


Route::group(['prefix' => '/umum'], function() {
    Route::get('/', 'SuratUmumController@index')->name('umum.index');

    Route::get('/domisili', 'SuratKeteranganDomisiliController@indexGuest')->name('domisili.index');
    Route::post('/domisili', 'SuratKeteranganDomisiliController@insertGuest')->name('domisili.insert');
    Route::get('/domisili/print/{id}', 'SuratKeteranganDomisiliController@print')->name('domisili.print');

    Route::get('/pajak', 'SuratKeteranganPajakController@indexGuest')->name('pajak.index');
    Route::post('/pajak', 'SuratKeteranganPajakController@insertGuest')->name('pajak.insert');
    Route::get('/pajak/print/{id}', 'SuratKeteranganPajakController@print')->name('pajak.print');

    Route::get('/kuasa', 'SuratKuasaTanahController@indexGuest')->name('kuasa.index');
    Route::post('/kuasa', 'SuratKuasaTanahController@insertGuest')->name('kuasa.insert');
    Route::get('/kuasa/print/{id}', 'SuratKuasaTanahController@print')->name('kuasa.print');
    
    Route::get('/imb', 'SuratRekomendasiImbController@indexGuest')->name('imb.index');
    Route::post('/imb', 'SuratRekomendasiImbController@insertGuest')->name('imb.insert');
    Route::get('/imb/print/{id}', 'SuratRekomendasiImbController@print')->name('imb.print');

    Route::get('/usaha', 'SuratKeteranganUsahaController@indexGuest')->name('usaha.index');
    Route::post('/usaha', 'SuratKeteranganUsahaController@insertGuest')->name('usaha.insert');
    Route::get('/usaha/print/{id}', 'SuratKeteranganUsahaController@print')->name('usaha.print');

});

Route::group(['prefix' => '/khusus'], function() {
    Route::get('/', 'SuratKhususController@index')->name('khusus.index');

    Route::get('/keramaian', 'SuratKeteranganIzinKeramaianController@indexGuest')->name('keramaian.index');
    Route::post('/keramaian', 'SuratKeteranganIzinKeramaianController@insertGuest')->name('keramaian.insert');
    Route::get('/keramaian/print/{id}', 'SuratKeteranganIzinKeramaianController@print')->name('keramaian.print');

    Route::get('/penghasilan', 'SuratKeteranganPenghasilanController@indexGuest')->name('penghasilan.index');
    Route::post('/penghasilan', 'SuratKeteranganPenghasilanController@insertGuest')->name('penghasilan.insert');
    Route::get('/penghasilan/print/{id}', 'SuratKeteranganPenghasilanController@print')->name('penghasilan.print');

    Route::get('/tidakMampu', 'SuratKeteranganTidakMampuController@indexGuest')->name('tidakMampu.index');
    Route::post('/tidakMampu', 'SuratKeteranganTidakMampuController@insertGuest')->name('tidakMampu.insert');
    Route::get('/tidakMampu/print/{id}', 'SuratKeteranganTidakMampuController@print')->name('tidakMampu.print');

    Route::get('/ahliWaris', 'SuratKeteranganAhliWarisController@indexGuest')->name('ahliWaris.index');
    Route::post('/ahliWaris', 'SuratKeteranganAhliWarisController@insertGuest')->name('ahliWaris.insert');
    Route::get('/ahliWaris/print/{id}', 'SuratKeteranganAhliWarisController@print')->name('ahliWaris.print');

    Route::get('/bedaNama', 'SuratKeteranganBedaNamaController@indexGuest')->name('bedaNama.index');
    Route::post('/bedaNama', 'SuratKeteranganBedaNamaController@insertGuest')->name('bedaNama.insert');
    Route::get('/bedaNama/print/{id}', 'SuratKeteranganBedaNamaController@print')->name('bedaNama.print');

    Route::get('/belumNpwp', 'SuratKeteranganBelumNpwpController@indexGuest')->name('belumNpwp.index');
    Route::post('/belumNpwp', 'SuratKeteranganBelumNpwpController@insertGuest')->name('belumNpwp.insert');
    Route::get('/belumNpwp/print/{id}', 'SuratKeteranganBelumNpwpController@print')->name('belumNpwp.print');

    Route::get('/hibah', 'SuratPernyataanHibahController@indexGuest')->name('hibah.index');
    Route::post('/hibah', 'SuratPernyataanHibahController@insertGuest')->name('hibah.insert');
    Route::get('/hibah/print/{id}', 'SuratPernyataanHibahController@print')->name('hibah.print');

    Route::get('/janda', 'SuratKeteranganJandaController@indexGuest')->name('janda.index');
    Route::post('/janda', 'SuratKeteranganJandaController@insertGuest')->name('janda.insert');
    Route::get('/janda/print/{id}', 'SuratKeteranganJandaController@print')->name('janda.print');

    Route::get('/belumMenikah', 'SuratBelumPernahMenikahController@indexGuest')->name('belumMenikah.index');
    Route::post('/belumMenikah', 'SuratBelumPernahMenikahController@insertGuest')->name('belumMenikah.insert');
    Route::get('/belumMenikah/print/{id}', 'SuratBelumPernahMenikahController@print')->name('belumMenikah.print');

    Route::get('/hubunganKeluarga', 'SuratKeteranganHubunganKeluargaController@indexGuest')->name('hubunganKeluarga.index');
    Route::post('/hubunganKeluarga', 'SuratKeteranganHubunganKeluargaController@insertGuest')->name('hubunganKeluarga.insert');
    Route::get('/hubunganKeluarga/print/{id}', 'SuratKeteranganHubunganKeluargaController@print')->name('hubunganKeluarga.print');

    Route::get('/PanggilanCerai', 'SuratKeteranganPanggilanCeraiController@indexGuest')->name('PanggilanCerai.index');
    Route::post('/PanggilanCerai', 'SuratKeteranganPanggilanCeraiController@insertGuest')->name('PanggilanCerai.insert');
    Route::get('/PanggilanCerai/print/{id}', 'SuratKeteranganPanggilanCeraiController@print')->name('PanggilanCerai.print');

    Route::get('/keterangan', 'SuratKeteranganController@indexGuest')->name('keterangan.index');
    Route::post('/keterangan', 'SuratKeteranganController@insertGuest')->name('keterangan.insert');
    Route::get('/keterangan/print/{id}', 'SuratKeteranganController@print')->name('keterangan.print');

    Route::get('/keteranganKematian', 'SuratKeteranganTentangKematianController@indexGuest')->name('keteranganKematian.index');
    Route::post('/keteranganKematian', 'SuratKeteranganTentangKematianController@insertGuest')->name('keteranganKematian.insert');
    Route::get('/keteranganKematian/print/{id}', 'SuratKeteranganTentangKematianController@print')->name('keteranganKematian.print');

    Route::get('/keteranganKelahiran', 'SuratKeteranganKelahiranController@indexGuest')->name('keteranganKelahiran.index');
    Route::post('/keteranganKelahiran', 'SuratKeteranganKelahiranController@insertGuest')->name('keteranganKelahiran.insert');
    Route::get('/keteranganKelahiran/print/{id}', 'SuratKeteranganKelahiranController@print')->name('keteranganKelahiran.print');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home.index');
Route::get('/about', 'AboutController@index')->name('about.index');
Route::get('/dashboard/{name?}', 'DashboardController@index' )->name('dashboard.index');


Route::resource('preferences', 'PreferenceController')->only(['index', 'store']);
Route::patch('preferences/logo/update', 'PreferenceController@updateLogo')->name('preferences.update.logo');
Route::delete('preferences/logo/destroy', 'PreferenceController@destroyLogo')->name('preferences.destroy.logo');

Route::resource('users', 'UserController');
Route::patch('users/{user}/password/update', 'UserController@updatePassword')->name('users.update.password');

Route::resource('suratKeteranganDomisilis', 'SuratKeteranganDomisiliController');

Route::resource('suratKeteranganIzinKeramaians', 'SuratKeteranganIzinKeramaianController');

Route::resource('suratKeteranganPajaks', 'SuratKeteranganPajakController');

Route::resource('suratKeteranganPenghasilans', 'SuratKeteranganPenghasilanController');

Route::get('documents', 'DocumentController@index')->name('documents.index');


Route::resource('suratRekomendasiImbs', 'SuratRekomendasiImbController');

Route::resource('suratKeteranganUsahas', 'SuratKeteranganUsahaController');

Route::resource('suratKuasaTanahs', 'SuratKuasaTanahController');

Route::resource('suratKeteranganTidakMampus', 'SuratKeteranganTidakMampuController');

Route::resource('suratKeteranganAhliWaris', 'SuratKeteranganAhliWarisController');

Route::resource('ahliWarisKeterenganAhliWaris', 'AhliWarisKeterenganAhliWarisController');

Route::resource('saksiKeteranganAhliWaris', 'SaksiKeteranganAhliWarisController');

Route::resource('suratKeteranganBedaNamas', 'SuratKeteranganBedaNamaController');

Route::resource('suratKeteranganBelumNpwps', 'SuratKeteranganBelumNpwpController');

Route::resource('suratPernyataanHibahs', 'SuratPernyataanHibahController');

Route::resource('riwayatKepemilikanHibahs', 'RiwayatKepemilikanHibahController');

Route::resource('suratKeteranganJandas', 'SuratKeteranganJandaController');

Route::resource('suratBelumPernahMenikahs', 'SuratBelumPernahMenikahController');

Route::resource('suratKeteranganHubunganKeluargas', 'SuratKeteranganHubunganKeluargaController');

Route::resource('suratKeteranganPanggilanCerais', 'SuratKeteranganPanggilanCeraiController');

Route::resource('suratKeterangans', 'SuratKeteranganController');

Route::resource('suratKeteranganTentangKematians', 'SuratKeteranganTentangKematianController');

Route::resource('suratKeteranganKelahirans', 'SuratKeteranganKelahiranController');