<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSuratPernyataanHibah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_pernyataan_hibah', function (Blueprint $table) {
            $table->dropColumn('sejarah_dihibahkan_kepada');
            $table->dropColumn('sejarah_tahun_dihibahkan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_pernyataan_hibah', function (Blueprint $table) {
            $table->string('sejarah_dihibahkan_kepada')->after('sejarah_tahun_dikuasai');
            $table->string('sejarah_tahun_dihibahkan')->after('sejarah_dihibahkan_kepada');
        });
    }
}
