<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratKeteranganPanggilanCeraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_keterangan_panggilan_cerai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('nik_pihak_pertama',16);
            $table->string('nama_pihak_pertama');
            $table->string('umur_pihak_pertama');
            $table->string('pekerjaan_pihak_pertama');
            $table->string('alamat_pihak_pertama');
            $table->string('nik_pihak_kedua',16);
            $table->string('nama_pihak_kedua');
            $table->string('umur_pihak_kedua');
            $table->string('pekerjaan_pihak_kedua');
            $table->string('alamat_pihak_kedua');
            $table->string('yang_ditinggalkan');
            $table->string('lama_ditinggal');
            $table->string('nama_pembina_TKI')->nullable();
            $table->string('nip_pembina_TKI')->nullable();
            $table->bigInteger('status_id')->unsigned();
            $table->date('tanggal_pengajuan')->nullable();
            $table->date('tanggal_terbit')->nullable();
            $table->string('nomor_surat')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_keterangan_panggilan_cerai');
    }
}
