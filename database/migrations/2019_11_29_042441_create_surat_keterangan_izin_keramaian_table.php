<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratKeteranganIzinKeramaianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_keterangan_izin_keramaian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->string('kewarganegaraan');
            $table->string('agama');
            $table->string('status_perkawinan');
            $table->string('alamat');
            $table->string('acara_kegiatan');
            $table->date('tgl_mulai');
            $table->date('tgl_selesai');
            $table->string('alamat_acara');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_keterangan_izin_keramaian');
    }
}
