<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnNameStatusSuratKeteranganPajak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keterangan_pajak', function (Blueprint $table) {
            $table->renameColumn('status','status_perkawinan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keterangan_pajak', function (Blueprint $table) {
            $table->renameColumn('status_perkawinan','status');
        });
    }
}
