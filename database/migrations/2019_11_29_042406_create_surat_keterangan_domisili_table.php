<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratKeteranganDomisiliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_keterangan_domisili', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode')->unique();
            $table->string('nama');
            $table->string('nik',16);
            $table->string('alamat');
            $table->string('alamat_domisili');
            $table->string('Dusun');
            $table->string('Desa');
            $table->string('Kecamatan');
            $table->string('Kabupaten');
            $table->string('Maksud');
            $table->string('ketua_rt');
            $table->string('kepala_dusun');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_keterangan_domisili');
    }
}
