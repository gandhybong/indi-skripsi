<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAhliWarisSuratKeteranganAhliWarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ahli_waris_keterangan_ahli_waris', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ahli_waris_id')->unsigned();
            $table->string('nama');
            $table->string('hubungan');
            $table->foreign('ahli_waris_id')->references('id')->on('surat_keterangan_ahli_waris');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ahli_waris_surat_keterangan_ahli_waris');
    }
}
