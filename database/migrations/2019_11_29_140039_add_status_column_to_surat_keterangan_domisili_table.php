<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumnToSuratKeteranganDomisiliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keterangan_domisili', function (Blueprint $table) {
            $table->bigInteger('status_id')->unsigned()->after('kepala_dusun');
            $table->foreign('status_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keterangan_domisili', function (Blueprint $table) {
            $table->dropForeign('surat_keterangan_domisili_status_id_foreign');
            $table->dropColumn('status_id');
        });
    }
}
