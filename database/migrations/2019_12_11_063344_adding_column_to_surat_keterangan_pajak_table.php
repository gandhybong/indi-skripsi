<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingColumnToSuratKeteranganPajakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keterangan_pajak', function (Blueprint $table) {
            $table->date('tanggal_pengajuan')->nullable()->after('status_id');
            $table->date('tanggal_terbit')->nullable()->after('tanggal_pengajuan');
            $table->string('nomor_surat')->nullable()->after('tanggal_terbit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keterangan_pajak', function (Blueprint $table) {
            $table->dropColumn('tanggal_pengajuan');
            $table->dropColumn('tanggal_terbit');
            $table->dropColumn('nomor_surat');
        });
    }
}
