<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratPernyataanHibah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_pernyataan_hibah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('nik',16);
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->string('kewarganegaraan');
            $table->string('agama');
            $table->string('pekerjaan');
            $table->string('alamat');
            $table->string('letak_tanah');
            $table->bigInteger('lebar');
            $table->bigInteger('panjang');
            $table->string('batas_utara');
            $table->string('batas_timur');
            $table->string('batas_selatan');
            $table->string('batas_barat');
            $table->string('sejarah_dikuasai_oleh');
            $table->string('sejarah_tahun_dikuasai');
            $table->string('sejarah_dihibahkan_kepada');
            $table->string('sejarah_tahun_dihibahkan');
            $table->string('dihibahkan_kepada');
            $table->string('untuk_dijadikan');
            $table->string('jabatan_saksi_pertama');
            $table->string('nama_saksi_pertama');
            $table->string('jabatan_saksi_kedua');
            $table->string('nama_saksi_kedua');
            $table->bigInteger('status_id')->unsigned();
            $table->date('tanggal_pengajuan')->nullable();
            $table->date('tanggal_terbit')->nullable();
            $table->string('nomor_surat')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_pernyataan_hibah');
    }
}
