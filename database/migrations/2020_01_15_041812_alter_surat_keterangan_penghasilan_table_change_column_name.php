<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSuratKeteranganPenghasilanTableChangeColumnName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keterangan_penghasilan', function (Blueprint $table) {
            $table->renameColumn('Pekerjaan', 'pekerjaan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keterangan_penghasilan', function (Blueprint $table) {
            $table->renameColumn('pekerjaan', 'Pekerjaan');
        });
    }
}
