<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratKuasaTanahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_kuasa_tanah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode')->unique();
            $table->string('nik',16);
            $table->string('nama');
            $table->string('jenis_kelamin');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->string('status_perkawinan');
            $table->string('pekerjaan');
            $table->string('alamat');
            $table->string('nik_pihak_kedua',16);
            $table->string('nama_pihak_kedua');
            $table->string('jenis_kelamin_pihak_kedua');
            $table->string('tempat_lahir_pihak_kedua');
            $table->date('tgl_lahir_pihak_kedua');
            $table->string('status_perkawinan_pihak_kedua');
            $table->string('pekerjaan_pihak_kedua');
            $table->string('alamat_pihak_kedua');
            $table->string('status_kedua_pihak');
            $table->string('jenis_tanah');
            $table->string('nomor_sertifikat_tanah');
            $table->string('atas_nama');
            $table->string('tahun');
            $table->string('luas_tanah');
            $table->string('maksud');
            $table->bigInteger('status_id')->unsigned();
            $table->date('tanggal_pengajuan')->nullable();
            $table->date('tanggal_terbit')->nullable();
            $table->string('nomor_surat')->nullable();
            $table->foreign('status_id')->references('id')->on('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_kuasa_tanah');
    }
}
