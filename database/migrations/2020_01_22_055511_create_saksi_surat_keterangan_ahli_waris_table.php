<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaksiSuratKeteranganAhliWarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saksi_keterangan_ahli_waris', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ahli_waris_id')->unsigned();
            $table->string('nama');
            $table->foreign('ahli_waris_id')->references('id')->on('surat_keterangan_ahli_waris');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saksi_surat_keterangan_ahli_waris');
    }
}
