<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSuratKeteranganTentangKematianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keterangan_tentang_kematian', function (Blueprint $table) {
            $table->string('tempat_meninggal')->after('tgl_meninggal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keterangan_tentang_kematian', function (Blueprint $table) {
            $table->dropColumn('tempat_meninggal');
        });
    }
}
