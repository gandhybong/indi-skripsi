<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSuratKeteranganTidakMampuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keterangan_tidak_mampu', function (Blueprint $table) {
            $table->renameColumn('tanggal_lahir', 'tgl_lahir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        chema::table('surat_keterangan_tidak_mampu', function (Blueprint $table) {
            $table->renameColumn('tanggal_lahir', 'tgl_lahir');
        });
    }
}
