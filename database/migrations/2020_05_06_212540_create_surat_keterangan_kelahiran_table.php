<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratKeteranganKelahiranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_keterangan_kelahiran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('nik_pihak_bapak',16);
            $table->string('nama_pihak_bapak');
            $table->string('tempat_lahir_pihak_bapak');
            $table->date('tgl_lahir_pihak_bapak');
            $table->string('kewarganegaraan_pihak_bapak');
            $table->string('agama_pihak_bapak');
            $table->string('pekerjaan_pihak_bapak');
            $table->string('alamat_pihak_bapak');
            $table->string('nik_pihak_ibu',16);
            $table->string('nama_pihak_ibu');
            $table->string('tempat_lahir_pihak_ibu');
            $table->date('tgl_lahir_pihak_ibu');
            $table->string('kewarganegaraan_pihak_ibu');
            $table->string('agama_pihak_ibu');
            $table->string('pekerjaan_pihak_ibu');
            $table->string('alamat_pihak_ibu');
            $table->string('anak_ke');
            $table->string('nama_anak');
            $table->string('jenis_kelamin_anak');
            $table->string('tempat_lahir_anak');
            $table->time('jam_lahir_anak');
            $table->date('tgl_lahir_anak');
            $table->string('kewarganegaraan_anak');
            $table->string('agama_anak');
            $table->bigInteger('status_id')->unsigned();
            $table->date('tanggal_pengajuan')->nullable();
            $table->date('tanggal_terbit')->nullable();
            $table->string('nomor_surat')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_keterangan_kelahiran');
    }
}
