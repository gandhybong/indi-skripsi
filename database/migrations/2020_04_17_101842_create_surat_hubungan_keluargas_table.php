<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratHubunganKeluargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_keterangan_hubungan_keluarga', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->string('nik_pihak_pertama',16);
            $table->string('nama_pihak_pertama');
            $table->string('jenis_kelamin_pihak_pertama');
            $table->string('tempat_lahir_pihak_pertama');
            $table->date('tgl_lahir_pihak_pertama');
            $table->string('status_perkawinan_pihak_pertama');
            $table->string('pekerjaan_pihak_pertama');
            $table->string('alamat_pihak_pertama');
            $table->string('nik_pihak_kedua',16);
            $table->string('nama_pihak_kedua');
            $table->string('jenis_kelamin_pihak_kedua');
            $table->string('tempat_lahir_pihak_kedua');
            $table->date('tgl_lahir_pihak_kedua');
            $table->string('status_perkawinan_pihak_kedua');
            $table->string('pekerjaan_pihak_kedua');
            $table->string('alamat_pihak_kedua');
            $table->string('hubungan_antara_kedua_pihak');
            $table->bigInteger('status_id')->unsigned();
            $table->date('tanggal_pengajuan')->nullable();
            $table->date('tanggal_terbit')->nullable();
            $table->string('nomor_surat')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_keterangan_hubungan_keluarga');
    }
}
