<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAhliWarisSuratKeteranganAhliWarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keterangan_ahli_waris', function (Blueprint $table) {
            $table->renameColumn('tanggal_lahir', 'tgl_lahir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keterangan_ahli_waris', function (Blueprint $table) {
            $table->renameColumn('tgl_lahir', 'tanggal_lahir');
        });
    }
}
