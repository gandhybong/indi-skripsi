<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiwayatKepemilikanHibah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_kepemilikan_hibah', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('hibahan_id')->unsigned();
            $table->string('alasan');
            $table->string('tahun');
            $table->string('nama');
            $table->foreign('hibahan_id')->references('id')->on('surat_pernyataan_hibah');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_kepemilikan_hibah');
    }
}
