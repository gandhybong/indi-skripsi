<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumnToSuratKeteranganIzinKeramaian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keterangan_izin_keramaian', function (Blueprint $table) {
            $table->bigInteger('status_id')->unsigned()->after('alamat_acara');
            $table->foreign('status_id')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keterangan_izin_keramaian', function (Blueprint $table) {
            $table->dropForeign('surat_keterangan_izin_keramaian_status_id_foreign');
            $table->dropColumn('status_id');
        });
    }
}
