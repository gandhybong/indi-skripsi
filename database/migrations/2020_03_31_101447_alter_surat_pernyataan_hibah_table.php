<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSuratPernyataanHibahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_pernyataan_hibah', function (Blueprint $table) {
            $table->renameColumn('Letak_tanah','letak_tanah');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_pernyataan_hibah', function (Blueprint $table) {
            $table->renameColumn('letak_tanah','Letak_tanah');
        });
    }
}
