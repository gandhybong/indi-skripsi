<?php

use Illuminate\Database\Seeder;
use App\Models\Preference;


class PreferencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $preferences = [
            ['label' => 'Provinsi','value' => ''],
            ['label' => 'Kabupaten','value' => ''],
            ['label' => 'Kecamatan','value' => ''],
            ['label' => 'Desa','value' => ''],
            ['label' => 'Alamat','value' => ''],
            ['label' => 'Kepala Desa','value' => ''],
            ['label' => 'Logo','value' => ''],
        ];

        foreach($preferences as $preference){
            $preferenceData = Preference::firstOrNew([
                'label' => $preference['label']
            ]);

            if( !$preferenceData->exists )
                Preference::create($preference);
        }
    }
}
