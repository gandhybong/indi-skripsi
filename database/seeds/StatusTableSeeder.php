<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            ['status' => 'Draf'],
            ['status' => 'Diterima'],
            ['status' => 'Ditolak'],
            ['status' => 'Ditolak - Expired']
        ];
        foreach($statuses as $status){
            $statusData = status::firstOrNew( $status );
            if(!$statusData->exists){
                status::create($status);
            }
        }
    }
}
