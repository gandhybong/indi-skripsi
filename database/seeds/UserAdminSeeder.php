<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userAdmin = [
            'name'      => 'Super Administrator',
            'email'     => 'superadmin@indi.id',
            'username'  => 'superadmin',
            'nik'       => '6112090205930002',
            'alamat'    => 'Jalan Nusantara Indonesia No 99',
            'tgl_lahir' => '2019-11-28',
        ];

        $user = User::firstOrNew( $userAdmin );

        if ( !$user->exists )
        {
            $user->password = bcrypt('indi2019');
            $user->save();
        }
    }
}
