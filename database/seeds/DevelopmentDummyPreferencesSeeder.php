<?php

use Illuminate\Database\Seeder;

use App\Models\Preference;

class DevelopmentDummyPreferencesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $preferences = [
            ['label' => 'Provinsi','value' => 'Kalimantan Barat'],
            ['label' => 'Kabupaten','value' => 'Kayong Utara'],
            ['label' => 'Kecamatan','value' => 'Sukadana'],
            ['label' => 'Desa','value' => 'Sutera'],
            ['label' => 'Alamat','value' => 'Jalan Akcaya No 04 Sukadana (Kode Pos 78852)'],
            ['label' => 'Kepala Desa','value' => 'Ripa\'i'],
            ['label' => 'Logo','value' => ''],
        ];

        foreach( $preferences as $preference )
        {
            Preference::whereLabel($preference['label'])->update([
                'value' => $preference['value']
            ]);
        }
    }
}
