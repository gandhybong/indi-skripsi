<?php

use Illuminate\Database\Seeder;

class DevelopmentDummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PreferencesTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(UserAdminSeeder::class);
        $this->call(DevelopmentDummyPreferencesSeeder::class);
    }
}
