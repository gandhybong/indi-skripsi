<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganKelahiran;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganKelahiran::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nik_pihak_bapak' => $faker->word,
        'nama_pihak_bapak' => $faker->word,
        'tempat_lahir_pihak_bapak' => $faker->word,
        'tgl_lahir_pihak_bapak' => $faker->word,
        'kewarganegaraan_pihak_bapak' => $faker->word,
        'agama_pihak_bapak' => $faker->word,
        'pekerjaan_pihak_bapak' => $faker->word,
        'alamat_pihak_bapak' => $faker->word,
        'nik_pihak_ibu' => $faker->word,
        'nama_pihak_ibu' => $faker->word,
        'tempat_lahir_pihak_ibu' => $faker->word,
        'tgl_lahir_pihak_ibu' => $faker->word,
        'kewarganegaraan_pihak_ibu' => $faker->word,
        'agama_pihak_ibu' => $faker->word,
        'pekerjaan_pihak_ibu' => $faker->word,
        'alamat_pihak_ibu' => $faker->word,
        'anak_ke' => $faker->word,
        'nama_anak' => $faker->word,
        'jenis_kelamin_anak' => $faker->word,
        'tempat_lahir_anak' => $faker->word,
        'jam_lahir_anak' => $faker->word,
        'tgl_lahir_anak' => $faker->word,
        'kewarganegaraan_anak' => $faker->word,
        'agama_anak' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
