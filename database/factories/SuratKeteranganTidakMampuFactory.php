<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganTidakMampu;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganTidakMampu::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nomor_kartu_keluarga' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tanggal_lahir' => $faker->word,
        'kewarganegaraan' => $faker->word,
        'agama' => $faker->word,
        'status_perkawinan' => $faker->word,
        'pekerjaan' => $faker->word,
        'alamat' => $faker->word,
        'penghasilan' => $faker->randomDigitNotNull,
        'maksud' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
