<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganTentangKematian;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganTentangKematian::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'bin_binti' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'agama' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'status_perkawinan' => $faker->word,
        'pekerjaan' => $faker->word,
        'alamat' => $faker->word,
        'tgl_meninggal' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
