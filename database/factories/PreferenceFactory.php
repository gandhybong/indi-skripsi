<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Preference;
use Faker\Generator as Faker;

$factory->define(Preference::class, function (Faker $faker) {

    return [
        'label' => $faker->word,
        'value' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
