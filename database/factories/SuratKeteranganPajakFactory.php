<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganPajak;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganPajak::class, function (Faker $faker) {

    return [
        'kode' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'kewarganegaraan' => $faker->word,
        'agama' => $faker->word,
        'status' => $faker->word,
        'pekerjaan' => $faker->word,
        'alamat' => $faker->word,
        'status_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
