<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganPenghasilan;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganPenghasilan::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'kewarganegaraan' => $faker->word,
        'Pekerjaan' => $faker->word,
        'agama' => $faker->word,
        'status_perkawinan' => $faker->word,
        'alamat' => $faker->word,
        'penghasilan' => $faker->randomDigitNotNull,
        'maksud' => $faker->word,
        'status_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
