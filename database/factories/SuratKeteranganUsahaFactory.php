<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganUsaha;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganUsaha::class, function (Faker $faker) {

    return [
        'kode' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'status_perkawinan' => $faker->word,
        'kewarganegaraan' => $faker->word,
        'agama' => $faker->word,
        'pekerjaan' => $faker->word,
        'alamat' => $faker->word,
        'nama_usaha' => $faker->word,
        'tahun_berdiri_usaha' => $faker->word,
        'alamat_usaha' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
