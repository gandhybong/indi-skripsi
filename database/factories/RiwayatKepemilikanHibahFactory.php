<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RiwayatKepemilikanHibah;
use Faker\Generator as Faker;

$factory->define(RiwayatKepemilikanHibah::class, function (Faker $faker) {

    return [
        'hibahan_id' => $faker->word,
        'alasan' => $faker->word,
        'tahun' => $faker->word,
        'nama' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
