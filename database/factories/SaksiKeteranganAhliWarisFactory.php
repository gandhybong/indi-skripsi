<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SaksiKeteranganAhliWaris;
use Faker\Generator as Faker;

$factory->define(SaksiKeteranganAhliWaris::class, function (Faker $faker) {

    return [
        'ahli_waris_id' => $faker->word,
        'nama' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
