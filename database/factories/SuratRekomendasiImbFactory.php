<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratRekomendasiImb;
use Faker\Generator as Faker;

$factory->define(SuratRekomendasiImb::class, function (Faker $faker) {

    return [
        'kode' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'kewarganegaraan' => $faker->word,
        'agama' => $faker->word,
        'status_perkawinan' => $faker->word,
        'pekerjaan' => $faker->word,
        'alamat' => $faker->word,
        'maksud' => $faker->word,
        'keperluan' => $faker->word,
        'alamat_bangunan' => $faker->word,
        'luas_bangunan' => $faker->word,
        'luas_tanah' => $faker->word,
        'nama_pemilik_tanah' => $faker->word,
        'nomor_sertifikat_tanah' => $faker->word,
        'bangunan_terbuat_dari' => $faker->word,
        'status_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
