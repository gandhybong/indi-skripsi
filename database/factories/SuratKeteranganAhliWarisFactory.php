<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganAhliWaris;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganAhliWaris::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tanggal_lahir' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'kewarganegaraan' => $faker->word,
        'agama' => $faker->word,
        'pekerjaan' => $faker->word,
        'maksud' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
