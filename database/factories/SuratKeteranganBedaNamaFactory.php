<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganBedaNama;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganBedaNama::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'nama_kedua' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'status_perkawinan' => $faker->word,
        'agama' => $faker->word,
        'pekerjaan' => $faker->word,
        'alamat' => $faker->word,
        'tertera_di' => $faker->word,
        'maksud' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
