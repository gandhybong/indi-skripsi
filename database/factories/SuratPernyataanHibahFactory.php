<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratPernyataanHibah;
use Faker\Generator as Faker;

$factory->define(SuratPernyataanHibah::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'kewarganegaraan' => $faker->word,
        'agama' => $faker->word,
        'pekerjaan' => $faker->word,
        'alamat' => $faker->word,
        'letak_tanah' => $faker->word,
        'lebar' => $faker->word,
        'panjang' => $faker->word,
        'batas_utara' => $faker->word,
        'batas_timur' => $faker->word,
        'batas_selatan' => $faker->word,
        'batas_barat' => $faker->word,
        'sejarah_dikuasai_oleh' => $faker->word,
        'sejarah_tahun_dikuasai' => $faker->word,
        'sejarah_dihibahkan_kepada' => $faker->word,
        'sejarah_tahun_dihibahkan' => $faker->word,
        'dihibahkan_kepada' => $faker->word,
        'untuk_dijadikan' => $faker->word,
        'jabatan_saksi_pertama' => $faker->word,
        'nama_saksi_pertama' => $faker->word,
        'jabatan_saksi_kedua' => $faker->word,
        'nama_saksi_kedua' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
