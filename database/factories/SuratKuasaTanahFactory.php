<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKuasaTanah;
use Faker\Generator as Faker;

$factory->define(SuratKuasaTanah::class, function (Faker $faker) {

    return [
        'kode' => $faker->word,
        'nik' => $faker->word,
        'nama' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'status_perkawinan' => $faker->word,
        'pekerjaan' => $faker->word,
        'alamat' => $faker->word,
        'nik_pihak_kedua' => $faker->word,
        'nama_pihak_kedua' => $faker->word,
        'jenis_kelamin_pihak_kedua' => $faker->word,
        'tempat_lahir_pihak_kedua' => $faker->word,
        'tgl_lahir_pihak_kedua' => $faker->word,
        'status_perkawinan_pihak_kedua' => $faker->word,
        'pekerjaan_pihak_kedua' => $faker->word,
        'alamat_pihak_kedua' => $faker->word,
        'status_kedua_pihak' => $faker->word,
        'jenis_tanah' => $faker->word,
        'nomor_hak_milik' => $faker->word,
        'atas_nama' => $faker->word,
        'tahun' => $faker->word,
        'luas' => $faker->word,
        'maksud' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
