<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganIzinKeramaian;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganIzinKeramaian::class, function (Faker $faker) {

    return [
        'nama' => $faker->word,
        'tempat_lahir' => $faker->word,
        'tgl_lahir' => $faker->word,
        'kewarganegaraan' => $faker->word,
        'agama' => $faker->word,
        'status_perkawinan' => $faker->word,
        'alamat' => $faker->word,
        'acara_kegiatan' => $faker->word,
        'tgl_mulai' => $faker->word,
        'tgl_selesai' => $faker->word,
        'alamat_acara' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
