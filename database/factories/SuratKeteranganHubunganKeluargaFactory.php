<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganHubunganKeluarga;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganHubunganKeluarga::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nik_pihak_pertama' => $faker->word,
        'nama_pihak_pertama' => $faker->word,
        'jenis_kelamin_pihak_pertama' => $faker->word,
        'tempat_lahir_pihak_pertama' => $faker->word,
        'tgl_lahir_pihak_pertama' => $faker->word,
        'status_perkawinan_pihak_pertama' => $faker->word,
        'pekerjaan_pihak_pertama' => $faker->word,
        'alamat_pihak_pertama' => $faker->word,
        'nik_pihak_kedua' => $faker->word,
        'nama_pihak_kedua' => $faker->word,
        'jenis_kelamin_pihak_kedua' => $faker->word,
        'tempat_lahir_pihak_kedua' => $faker->word,
        'tgl_lahir_pihak_kedua' => $faker->word,
        'status_perkawinan_pihak_kedua' => $faker->word,
        'pekerjaan_pihak_kedua' => $faker->word,
        'alamat_pihak_kedua' => $faker->word,
        'hubungan_antara_kedua_pihak' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
