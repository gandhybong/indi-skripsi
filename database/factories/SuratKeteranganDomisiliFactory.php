<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganDomisili;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganDomisili::class, function (Faker $faker) {

    return [
        'kode' => $faker->word,
        'nama' => $faker->word,
        'nik' => $faker->word,
        'alamat' => $faker->word,
        'alamat_domisili' => $faker->word,
        'Dusun' => $faker->word,
        'Desa' => $faker->word,
        'Kecamatan' => $faker->word,
        'Kabupaten' => $faker->word,
        'Maksud' => $faker->word,
        'ketua_rt' => $faker->word,
        'kepala_dusun' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
