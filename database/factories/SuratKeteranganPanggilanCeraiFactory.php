<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SuratKeteranganPanggilanCerai;
use Faker\Generator as Faker;

$factory->define(SuratKeteranganPanggilanCerai::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'nik_pihak_pertama' => $faker->word,
        'nama_pihak_pertama' => $faker->word,
        'umur_pihak_pertama' => $faker->word,
        'pekerjaan_pihak_pertama' => $faker->word,
        'alamat_pihak_pertama' => $faker->word,
        'nik_pihak_kedua' => $faker->word,
        'nama_pihak_kedua' => $faker->word,
        'umur_pihak_kedua' => $faker->word,
        'pekerjaan_pihak_kedua' => $faker->word,
        'alamat_pihak_kedua' => $faker->word,
        'yang_ditinggalkan' => $faker->word,
        'lama_ditinggal' => $faker->word,
        'nama_pembina_TKI' => $faker->word,
        'nip_pembina_TKI' => $faker->word,
        'status_id' => $faker->word,
        'tanggal_pengajuan' => $faker->word,
        'tanggal_terbit' => $faker->word,
        'nomor_surat' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
