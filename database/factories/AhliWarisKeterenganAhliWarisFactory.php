<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AhliWarisKeterenganAhliWaris;
use Faker\Generator as Faker;

$factory->define(AhliWarisKeterenganAhliWaris::class, function (Faker $faker) {

    return [
        'ahli_waris_id' => $faker->word,
        'nama' => $faker->word,
        'hubungan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
