@section( 'css' )
    @include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratKeteranganBedaNamas-table' ).DataTable({

            });
        } );
    </script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKeteranganBedaNamas-table">
        <thead>
            <tr>
                <th>Nama Pengguna</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>Nama Kedua</th>
                <th>Tempat Lahir</th>
                <th>Tgl Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Status Perkawinan</th>
                <th>Pekerjaan</th>
                <th>Alamat</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganBedaNamas as $suratKeteranganBedaNama)
            <tr>
                <td>{!! $suratKeteranganBedaNama->user->name !!}</td>
                <td>{{ $suratKeteranganBedaNama->nik }}</td>
                <td>{{ $suratKeteranganBedaNama->nama }}</td>
                <td>{{ $suratKeteranganBedaNama->nama_kedua }}</td>
                <td>{{ $suratKeteranganBedaNama->tempat_lahir }}</td>
                <td>{{ $suratKeteranganBedaNama->tgl_lahir }}</td>
                <td>{{ $suratKeteranganBedaNama->jenis_kelamin }}</td>
                <td>{{ $suratKeteranganBedaNama->status_perkawinan }}</td>
                <td>{{ $suratKeteranganBedaNama->pekerjaan }}</td>
                <td>{{ $suratKeteranganBedaNama->alamat }}</td>
                <td>{!! $suratKeteranganBedaNama->status->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganBedaNamas.destroy', $suratKeteranganBedaNama->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganBedaNamas.edit', [$suratKeteranganBedaNama->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
