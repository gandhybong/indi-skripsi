@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Belum Pernah Menikah
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('surat_keterangan_belum_pernah_menikahs.show_fields')
                    <a href="{{ route('suratKeteranganBelumPernahMenikahs.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
