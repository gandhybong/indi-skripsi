<div class="col-sm-6">
    <!-- Nik Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik', 'Nik:') !!}
        {!! Form::text('nik', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama', 'Nama:') !!}
        {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Jenis Kelamin Field -->
    <div class="form-group col-sm-12">
        <label for="jenis_kelamin">
            Jenis Kelamin
        </label>
        @isset($suratKeteranganBelumPernahMenikah)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki' || $suratKeteranganBelumPernahMenikah->jenis_kelamin == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan' || $suratKeteranganBelumPernahMenikah->jenis_kelamin == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endisset
    
        @empty($suratKeteranganBelumPernahMenikah)
        <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endempty
    </div>
    
    <!-- Tempat Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
        {!! Form::text('tempat_lahir', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tgl Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
        {!! Form::date('tgl_lahir', null, ['class' => 'form-control','id'=>'tgl_lahir']) !!}
    </div>
    
    <!-- Kewarganegaraan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
        {!! Form::text('kewarganegaraan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Pekerjaan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
        {!! Form::text('pekerjaan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Agama Field -->
    <div class="form-group col-sm-12">
        <label for="agama">Agama</label>
        <select class="form-control" id="agama" name="agama" >
            @isset($suratKeteranganBelumPernahMenikah)
                <option value="Islam" {{ (old('agama') == 'Islam') || $suratKeteranganBelumPernahMenikah->agama == 'Islam' ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') || $suratKeteranganBelumPernahMenikah->agama == 'Protestan' ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') || $suratKeteranganBelumPernahMenikah->agama == 'Katolik' ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') || $suratKeteranganBelumPernahMenikah->agama == 'Hindu' ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') || $suratKeteranganBelumPernahMenikah->agama == 'Buddha' ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') || $suratKeteranganBelumPernahMenikah->agama == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
            @endisset
    
            @empty($suratKeteranganBelumPernahMenikah)
                <option value="Islam" {{ (old('agama') == 'Islam') ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
            @endempty
        </select>
    </div>
    
    <!-- Status Perkawinan Field -->
    <div class="form-group col-sm-12">
        <label for="status_perkawinan">
            Status Perkawinan 
        </label>
        @isset($suratKeteranganBelumPernahMenikah)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') || $suratKeteranganBelumPernahMenikah->status_perkawinan == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') || $suratKeteranganBelumPernahMenikah->status_perkawinan == 'Kawin' ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') || $suratKeteranganBelumPernahMenikah->status_perkawinan == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') || $suratKeteranganBelumPernahMenikah->status_perkawinan == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endisset
    
        @empty($suratKeteranganBelumPernahMenikah)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endempty
    </div>
    
    <!-- Alamat Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat', 'Alamat:') !!}
        {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Administrasi Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('administrasi', 'Administrasi:') !!}
        {!! Form::text('administrasi', null, ['class' => 'form-control']) !!}
    </div>
    
    @if(Route::current()->getName() == 'suratBelumPernahMenikahs.edit')
        <!-- Status Id Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status_id', 'Status Id') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganBelumPernahMenikah->status->status}}" readonly>
        </div>
    @endif
    
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        @if(Route::current()->getName() == 'suratBelumPernahMenikahs.edit')
            @if ( $suratKeteranganBelumPernahMenikah->status->status == "Draf" )
                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
                    {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
                    <a href="{!! route('suratBelumPernahMenikahs.index') !!}" class="btn btn-default">Kembali</a>
                </div>
            @elseif ( $suratKeteranganBelumPernahMenikah->status->status == "Diterima" )
                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    <a href="{{ route('belumMenikah.print', ['id' => $suratKeteranganBelumPernahMenikah->id]) }}" class="btn btn-success" target="_blank" id="print">
                        Print
                    </a>
                    <a href="{!! route('suratBelumPernahMenikahs.index') !!}" class="btn btn-default">Kembali</a>
                </div>
            @else
                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    <a href="{!! route('suratBelumPernahMenikahs.index') !!}" class="btn btn-default">Kembali</a>
                </div>
            @endif
        @else
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('suratBelumPernahMenikahs.index') }}" class="btn btn-default">Cancel</a>
        @endif
    </div>
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection