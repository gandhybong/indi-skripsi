@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Belum Pernah Menikah
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganBelumPernahMenikah, ['route' => ['suratBelumPernahMenikahs.update', $suratKeteranganBelumPernahMenikah->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_belum_pernah_menikahs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection