@section( 'css' )
    @include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganBelumPernahMenikahs-table' ).DataTable({

        });
    } );
</script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKeteranganBelumPernahMenikahs-table">
        <thead>
            <tr>
                <th>Nik</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Tgl Lahir</th>
                <th>Kewarganegaraan</th>
                <th>Pekerjaan</th>
                <th>Agama</th>
                <th>Status Perkawinan</th>
                <th>Alamat</th>
                <th>Administrasi</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganBelumPernahMenikahs as $suratKeteranganBelumPernahMenikah)
            <tr>
                <td>{{ $suratKeteranganBelumPernahMenikah->nik }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->nama }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->jenis_kelamin }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->tempat_lahir }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->tgl_lahir }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->kewarganegaraan }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->pekerjaan }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->agama }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->status_perkawinan }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->alamat }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->administrasi }}</td>
                <td>{{ $suratKeteranganBelumPernahMenikah->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratBelumPernahMenikahs.destroy', $suratKeteranganBelumPernahMenikah->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratBelumPernahMenikahs.edit', [$suratKeteranganBelumPernahMenikah->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
