<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section class="title">
            <h1 class="underlined">
                SURAT KETERANGAN TIDAK MAMPU
            </h1>
            <h1>
                Nomor : {{$suratKeteranganTidakMampu->nomor_surat}}
            </h1>
        </section>
        <p>
            Yang bertanda tangan dibawah ini Kepala Desa {{ $preferences['Desa']['value'] }} Kecamatan {{ $preferences['Kecamatan']['value'] }} Kabupaten {{ $preferences['Kabupaten']['value'] }} menerangkan bahwa : --------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>Nomor KK</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->nomor_kartu_keluarga }}</td>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->nik }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->nama }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->jenis_kelamin }}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->tempat_lahir }}, {{ $suratKeteranganTidakMampu->tgl_lahir }}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->kewarganegaraan }}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->agama }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->status_perkawinan }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->pekerjaan }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratKeteranganTidakMampu->alamat }}</td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td>Nama tersebut di atas benar penduduk  Desa {{$preferences['Desa']['value']}} 
                  Kec.{{$preferences['Kecamatan']['value']}} Kab.{{$preferences['Kabupaten']['value']}}. Dan termasuk warga prasejahtera (miskin). Dan benar warga dengan penghasilan Rp.{{ $suratKeteranganTidakMampu->penghasilan }} perbulan. Keterangan ini diberikan dengan maksudnya
                  untuk persyaratan administrasi {{ $suratKeteranganTidakMampu->maksud }}</td>
            </tr>
        </table>
        <p>
            Demikian keterangan ini diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>