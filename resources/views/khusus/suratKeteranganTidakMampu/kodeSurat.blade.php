@extends('layouts.app')

@section('content')
    <div>
        Surat Keterangan Tidak Mampu
    </div>  
    <div>
        ID Surat : {{ $suratKeteranganTidakMampu->id }}
    </div>
    <div>
       Silahkan Pergi Ke Kepala Desa untuk Mencetak Surat Anda
    </div>
@endsection