<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section>
            <h2 class="underlined">
                SURAT KETERANGAN HUBUNGAN KELUARGA
            </h2>
            <h2>
                Nomor : {{ $suratKeteranganHubunganKeluarga->nomor_surat }}
            </h2>
        </section>
        <p>
            Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong Utara dengan ini menerangkan bahwa hubungan keluarga antara :-----------------------------------------------------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->nik_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->nama_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->tempat_lahir_pihak_pertama }}, {{ $suratKeteranganHubunganKeluarga->tgl_lahir_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->pekerjaan_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->alamat_pihak_pertama }}</td>
            </tr>
        </table>
        <p>
            Dengan Saudara/Saudari : -------------------------------------------------------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->nik_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->nama_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->tempat_lahir_pihak_kedua }}, {{ $suratKeteranganHubunganKeluarga->tgl_lahir_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->pekerjaan_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratKeteranganHubunganKeluarga->alamat_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td>Adalah antara kedua belah pihak tersebut diatas adalah  benar {{ $suratKeteranganHubunganKeluarga->hubungan_antara_kedua_pihak }} .</td>
            </tr>
        </table>
        <br>
        <p>
            Demikianlah surat keterangan ini dibuat dengan sebenar- benarnya dan diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagai mana mestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>