@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Hubungan Keluarga
        </h1>
        <br>
        <form action="{{ route('hubunganKeluarga.insert') }}" method="POST">
            @csrf
            @include('forms.inputNikPihakPertama')

            @include('forms.inputNamaPihakPertama')

            @include('forms.inputJenisKelaminPihakPertama')

            @include('forms.inputTempatLahirPihakPertama')
    
            @include('forms.inputTanggalLahirPihakPertama')

            @include('forms.inputStatusPerkawinanPihakPertama')

            @include('forms.inputPekerjaanPihakPertama')

            @include('forms.inputAlamatPihakPertama')

            @include('forms.inputNikPihakKedua')

            @include('forms.inputNamaPihakKedua')

            @include('forms.inputJenisKelaminPihakKedua')

            @include('forms.inputTempatLahirPihakKedua')
    
            @include('forms.inputTanggalLahirPihakKedua')

            @include('forms.inputStatusPerkawinanPihakKedua')

            @include('forms.inputPekerjaanPihakKedua')

            @include('forms.inputAlamatPihakKedua')
            
            @include('forms.inputHubunganAntarKeduaPihak')

            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection