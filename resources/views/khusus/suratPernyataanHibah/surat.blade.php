<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section class="title">
            <h2 class="underlined">
                SURAT PERNYATAAN HIBAH
            </h2>
            <h2>
                Nomor : {{ $suratPernyataanHibah->nomor_surat }}
            </h2>
        </section>
        <p>
            Yang bertanda tangan di bawah : --------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td>{{ $suratPernyataanHibah->nama }}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $suratPernyataanHibah->tempat_lahir}}, {{ $suratPernyataanHibah->tgl_lahir }}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{ $suratPernyataanHibah->kewarganegaraan}}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{ $suratPernyataanHibah->agama}}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratPernyataanHibah->pekerjaan }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratPernyataanHibah->alamat }}, Rt.{{ $suratPernyataanHibah->rt }} /Rw.{{ $suratPernyataanHibah->rw }}</td>
            </tr>
        </table>
        <p>
            Dengan ini menyatakan bahwa saya benar ada menguasai/memiliki sebidang tanah pertanian/perumahan dengan keterangan sebagai berikut:
        </p>
        <br>
        <p>
            Demikian keterangan ini diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>