@extends('layouts.app')

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var max_fields = 20; //maximum input boxes allowed
            var wrapper = $("#items"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID
            
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    console.log(x);
                    $(wrapper).append(
                        "<div class='form-group col-sm-12'>"+
                            "<div class='form-group col-sm-12'>"+
                                "<label>Dijual atau Dihibahkan :</label>"+
                                "<input id='alasan' class='form-control' name='alasan[]' required='required' type='text' />"+
                            "</div>"+
                            "<div class='form-group col-sm-12'>"+
                                "<label>Kepada:</label>"+
                                "<input id='kepada_hibah' class='form-control' name='kepada_hibah[]' required='required' type='text' />"+
                            "</div>"+
                            "<div class='form-group col-sm-12'>"+
                                "<label>Tahun:</label>"+
                                "<input id='tahun_hibah' class='form-control' name='tahun_hibah[]' required='required' type='text' />"+
                            "</div>"+
                            "<a href='#' class='remove_field'>"+
                                "<i class='fa fa-times'></i>"+
                            "</a>"+
                        "</div>"
                    ); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
                e.preventDefault(); $(this).parent('div').remove(); x--;
            });
        });
    </script>
@endsection

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Pernyataan Hibah
        </h1>
        <br>
        <form action="{{ route('hibah.insert') }}" method="POST">
            @csrf
            @include('forms.inputNik')

            @include('forms.inputNama')
    
            @include('forms.inputTempatLahir')
    
            @include('forms.inputTanggalLahir')

            @include('forms.inputKewarganegaraan')

            @include('forms.inputAgama')

            @include('forms.inputPekerjaan')
            
            @include('forms.inputAlamat')
            
            @include('forms.inputLetakTanah')

            @include('forms.inputLebarTanah')

            @include('forms.inputPanjangTanah')

            @include('forms.inputDihibahkanKepada')

            @include('forms.inputUntukDijadikan')

            @include('forms.inputBatasTanahUtara')

            @include('forms.inputBatasTanahTimur')
            
            @include('forms.inputBatasTanahSelatan')
            
            @include('forms.inputBatasTanahBarat')

            @include('forms.inputSejarahSingkat')

            @include('forms.inputJabatanSaksiPertama')

            @include('forms.inputNamaSaksiPertama')

            @include('forms.inputJabatanSaksiKedua')

            @include('forms.inputNamaSaksiKedua')

            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection