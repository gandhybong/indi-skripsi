@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>
    <div class="col-md-8">
        <h1>
            DAFTAR SURAT KHUSUS
        </h1>
        <ul class="list-group list-group-flush list-items">
            <li class="">
                <a href="{{ route('keramaian.index') }}">
                    Surat Keterangan Izin Keramaian
                </a>
            </li>
            <li class="">
                <a href="{{ route('penghasilan.index') }}">
                    Surat Keterangan Izin Penghasilan
                </a>
            </li>
            <li class="">
                <a href="{{ route('tidakMampu.index') }}">
                    Surat Keterangan Tidak Mampu
                </a>
            </li>
            <li class="">
                <a href="{{ route('ahliWaris.index') }}">
                    Surat Keterangan Ahli Waris
                </a>
            </li>
            <li class="">
                <a href="{{ route('bedaNama.index') }}">
                    Surat Keterangan Beda Nama
                </a>
            </li>
            <li class="">
                <a href="{{ route('belumNpwp.index') }}">
                    Surat Keterangan Belum Npwp
                </a>
            </li>
            <li class="">
                <a href="{{ route('hibah.index') }}">
                    Surat Pernyataan Hibah
                </a>
            </li>
            <li class="">
                <a href="{{ route('janda.index') }}">
                    Surat Keterangan Janda
                </a>
            </li>
            <li class="">
                <a href="{{ route('belumMenikah.index') }}">
                    Surat Belum Pernah Menikah
                </a>
            </li>
            <li class="">
                <a href="{{ route('hubunganKeluarga.index') }}">
                    Surat Keterangan Hubungan Keluarga
                </a>
            </li>
            <li class="">
                <a href="{{ route('PanggilanCerai.index') }}">
                    Surat Keterangan Panggilan Cerai
                </a>
            </li>
            <li class="">
                <a href="{{ route('keterangan.index') }}">
                    Surat Keterangan
                </a>
            </li>
            <li class="">
                <a href="{{ route('keteranganKematian.index') }}">
                    Surat Keterangan Tentang Kematian
                </a>
            </li>
            <li class="">
                <a href="{{ route('keteranganKelahiran.index') }}">
                    Surat Keterangan Tentang Kelahiran
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-2">

    </div>
@endsection