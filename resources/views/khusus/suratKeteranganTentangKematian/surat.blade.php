<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section>
            <h2 class="underlined">
                SURAT KETERANGAN TENTANG KEMATIAN
            </h2>
            <h2>
                Nomor : {{ $suratKeteranganTentangKematian->nomor_surat }}
            </h2>
        </section>
        <p>
            Yang bertanda tangan di bawah ini Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong Utara menerangkan bahwa : --------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->nik }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->nama }}</td>
            </tr>
            <tr>
                <td>Bin/Binti</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->bin_binti }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->jenis_kelamin }}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->agama}}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->tempat_lahir}}, {{ $suratKeteranganTentangKematian->tgl_lahir }}</td>
            </tr>
            <tr>
                <td>Status Perkawinan</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->status_perkawinan }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->pekerjaan }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratKeteranganTentangKematian->alamat }}</td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td>
                    Nama tersebut diatas adalah benar penduduk Desa Sutera Sukadana Kab.Kayong Utara dan benar telah meninggal dunia {{ $suratKeteranganTentangKematian->tgl_meninggal }} Di {{ $suratKeteranganTentangKematian->tempat_meninggal }}.
                </td>
            </tr>
        </table>
        <br>
        <p>
            Demikian surat keterangan ini dibuat dan diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimanamestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>