<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section class="title">
            <h2 class="underlined">
                SURAT KETERANGAN IZIN KERAMAIAN
            </h2>
            <h2>
                Nomor : {{ $SuratKeteranganIzinKeramaian->nomor_surat }}
            </h2>
        </section>
        <p>
            Yang bertanda tangan dibawah ini Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong Utara menerangkan bahwa : --------------------------------------------------------------------------------
        </p>
        
        <table class="document-data">
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{$SuratKeteranganIzinKeramaian->nama}}</td>
            </tr>
            <tr>
                <td>Tempat tgl lahir</td>
                <td>:</td>
                <td>{{$SuratKeteranganIzinKeramaian->tempat_lahir}}, {{$SuratKeteranganIzinKeramaian->tgl_lahir}}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{$SuratKeteranganIzinKeramaian->kewarganegaraan}}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{$SuratKeteranganIzinKeramaian->agama}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{$SuratKeteranganIzinKeramaian->status_perkawinan}}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{$SuratKeteranganIzinKeramaian->alamat}}</td>
            </tr>
            <tr>
                <td>Acara Kegiatan</td>
                <td>:</td>
                <td>{{$SuratKeteranganIzinKeramaian->acara_kegiatan}}</td>
            </tr>
            <tr>
                <td>Lamanya</td>
                <td>:</td>
                <td>Terhitung dari tanggal {{$SuratKeteranganIzinKeramaian->tgl_mulai}} - {{$SuratKeteranganIzinKeramaian->tgl_selesai}}</td>
            </tr>
            <tr>
                <td>Tempat</td>
                <td>:</td>
                <td>{{$SuratKeteranganIzinKeramaian->alamat_acara}}</td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td>Dipihak kami tidak merasa keberatan sepanjang yang bersangkutan mematuhi tata tertib menjaga keamanan sesuai peraturan yang berlaku.</td>
            </tr>
        </table>

        <p>
            Demikian keterangan ini diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.
        </p>
        <br>
    </div>
    <footer>
        <div id="tandaTangan">
            <div>
                Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
            </div>
            <div>
                {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
            </div>
            <div>
                Kepala Desa {{$preferences['Desa']['value']}}
            </div>

            <div id="kepalaDesa">
                {{$preferences['Kepala Desa']['value']}}
            </div>
        </div>
    </footer>
</body>
</html>