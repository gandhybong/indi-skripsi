@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Izin Keramaian
        </h1>
        <br>
        <form action="{{ route('keramaian.insert') }}" method="POST">
            @csrf
            @include('forms.inputNama')
    
            @include('forms.inputTempatLahir')
    
            @include('forms.inputTanggalLahir')
            
            @include('forms.inputKewarganegaraan')
            
            @include('forms.inputAgama')
            
            @include('forms.inputStatusPerkawinan')
            
            @include('forms.inputAlamat')

            @include('forms.inputAlamatAcara')

            @include('forms.inputAcaraKegiatan')

            @include('forms.inputTanggalMulai')
            
            @include('forms.inputTanggalSelesai')

            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection