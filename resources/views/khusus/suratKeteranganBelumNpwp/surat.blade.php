<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section class="title">
            <h2 class="underlined">
                SURAT KETERANGAN BELUM MEMILIKI NPWP
            </h2>
            <h2>
                Nomor : {{ $suratKeteranganBelumNpwp->nomor_surat }}
            </h2>
        </center>
        <p>
            Yang bertanda tangan dibawah ini Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong Utara menerangkan bahwa : --------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->nik }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->nama }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->jenis_kelamin }}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->tempat_lahir}}, {{ $suratKeteranganBelumNpwp->tgl_lahir }}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->kewarganegaraan}}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->agama}}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->pekerjaan }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->status_perkawinan }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratKeteranganBelumNpwp->alamat }}, Rt.{{ $suratKeteranganBelumNpwp->rt }} /Rw.{{ $suratKeteranganBelumNpwp->rw }}</td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td>Nama yang bersangkutan di atas benar penduduk Desa {{ $preferences['Desa']['value'] }} Kecamatan {{ $preferences['Kecamatan']['value']}} Kabupaten {{ $preferences['Kabupaten']['value'] }}. Keterangan ini diberikan sehubungan dengan maksudnya untuk melengkapi persyaratan administrasi Pembuatan NPWP .</td>
            </tr>
        </table>
        <br>
        <p>
            Demikian keterangan ini diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>