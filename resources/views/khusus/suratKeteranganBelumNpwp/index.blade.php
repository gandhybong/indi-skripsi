@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Belum Npwp
        </h1>
        <br>
        <form action="{{ route('belumNpwp.insert') }}" method="POST">
            @csrf
            @include('forms.inputNik')

            @include('forms.inputNama')
    
            @include('forms.inputJenisKelamin')

            @include('forms.inputTempatLahir')
    
            @include('forms.inputTanggalLahir')

            @include('forms.inputKewarganegaraan')

            @include('forms.inputAgama')

            @include('forms.inputPekerjaan')

            @include('forms.inputStatusPerkawinan')
            
            @include('forms.inputAlamat')
            
            @include('forms.inputRt')

            @include('forms.inputRw')

            @include('forms.inputTujuanPembuatanSurat')

            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection