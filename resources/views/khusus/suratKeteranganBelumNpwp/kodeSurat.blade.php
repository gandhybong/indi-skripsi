@extends('layouts.app')

@section('content')
    <div>
        Surat Keterangan Izin Keramaian
    </div>
    <div>
        Nomor Id Surat : {{ $SuratKeteranganBelumNpwp->id }}
    </div>
    <div>
        Pengajuan Berlaku Sampai dengan : {{ $hariKadarluasaPengajuan }}
    </div>
    <div>
        Harap Bawa :
        <ul>
            <li>
                Kode Unik
            </li>
            <li>
                KTP
            </li>
        </ul>
        Ke Kepala Desa untuk Ditukarkan dengan Surat Anda
    </div>
@endsection