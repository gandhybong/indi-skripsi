@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Tentang Kematian
        </h1>
        <br>
        <form action="{{ route('keteranganKematian.insert') }}" method="POST">
            @csrf
            @include('forms.inputNik')

            @include('forms.inputNama')

            @include('forms.inputBinBinti')
    
            @include('forms.inputJenisKelamin')

            @include('forms.inputAgama')

            @include('forms.inputTempatLahir')
    
            @include('forms.inputTanggalLahir')

            @include('forms.inputStatusPerkawinan')

            @include('forms.inputPekerjaan')

            @include('forms.inputAlamat')

            @include('forms.inputTanggalMeninggal')

            @include('forms.inputTempatMeninggal')

            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection