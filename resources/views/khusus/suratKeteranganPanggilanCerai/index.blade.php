@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Panggilan Cerai
        </h1>
        <br>
        <form action="{{ route('PanggilanCerai.insert') }}" method="POST">
            @csrf
            @include('forms.inputNikPihakPertama')

            @include('forms.inputNamaPihakPertama')
            
            @include('forms.inputUmurPihakPertama')

            @include('forms.inputPekerjaanPihakPertama')

            @include('forms.inputAlamatPihakPertama')

            @include('forms.inputNikPihakKedua')

            @include('forms.inputNamaPihakKedua')

            @include('forms.inputUmurPihakKedua')

            @include('forms.inputPekerjaanPihakKedua')

            @include('forms.inputAlamatPihakKedua')
            
            @include('forms.inputYangDitinggal')
            
            @include('forms.inputLamaDitinggal')
            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection