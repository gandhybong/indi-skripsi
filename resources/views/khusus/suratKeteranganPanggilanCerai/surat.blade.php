<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section>
            <h2 class="underlined">
                SURAT KETERANGAN PANGGILAN CERAI
            </h2>
            <h2>
                Nomor : {{ $suratKeteranganPanggilanCerai->nomor_surat }}
            </h2>
        </section>
        <p>
            Yang bertanda tangan di bawah ini Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong Utara menerangkan dengan sesungguhnya bahwa:-----------------------------------------------------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->nik_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->nama_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Umur</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->umur_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->pekerjaan_pihak_pertama }}</td>
            </tr>
            <tr>
                <td>Tempat tinggal</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->alamat_pihak_pertama }}</td>
            </tr>
        </table>
        <p>
            Orang tersebut di atas, benar-benar telah meninggalkan Desa Kami, dengan meninggalkan {{ $suratKeteranganPanggilanCerai->yang_ditinggalkan }} : : -------------------------------------------------------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->nik_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->nama_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Umur</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->umur_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->pekerjaan_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Tempat tinggal</td>
                <td>:</td>
                <td>{{ $suratKeteranganPanggilanCerai->alamat_pihak_kedua }}</td>
            </tr>
        </table>
        <br>
        <p>
            Hingga saat ini selama 6 tahun dengan tidak diketahui tempat tinggal (Alamat) terakhirnya secara pasti, dan kepergiannya tersebut tanpa sepengetahuan kami maupun aparat desa yang lain.
        </p>
        <br>
        <p>
            Demikianlah surat keterangan ini dibuat dengan sebenar-benarnya dan diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagai mana mestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di: Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di: Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$suratKeteranganPanggilanCerai->nama_pembina_TKI}}
                </div>

                <div id="kepalaDesa">
                    {{$suratKeteranganPanggilanCerai->nip_pembina_TKI}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>