@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Beda Nama
        </h1>
        <br>
        <form action="{{ route('bedaNama.insert') }}" method="POST">
            @csrf
            @include('forms.inputNik')

            @include('forms.inputNama')
    
            @include('forms.inputTempatLahir')
    
            @include('forms.inputTanggalLahir')

            @include('forms.inputJenisKelamin')
            
            @include('forms.inputStatusPerkawinan')
            
            @include('forms.inputAgama')
            
            @include('forms.inputPekerjaan')

            @include('forms.inputAlamat')

            @include('forms.inputNamaKedua')

            @include('forms.inputTerteraDi')

            @include('forms.inputTujuanPembuatanSurat')

            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection