@extends('layouts.app')

@section('content')
    <div>
        Surat Keterangan Penghasilan
    </div>
    <div>
        Nomor Id Surat : {{ $suratKeteranganPenghasilan->id }}
    </div>
    <div>
        Pengajuan Berlaku Sampai dengan : {{ $hariKadarluasaPengajuan }}
    </div>
    <div>
        Harap Bawa :
        <ul>
            <li>
                Kode Unik
            </li>
            <li>
                Fotokopi Kartu Keluarga
            </li>
        </ul>
        Ke Kepala Desa untuk Ditukarkan dengan Surat Anda
    </div>
@endsection