@extends('layouts.app')

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields = 20; //maximum input boxes allowed
        var wrapper = $("#items"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                console.log(x);
                $(wrapper).append(
                    "<div class='form-group col-sm-12'>"+
                        "<div class='form-group col-sm-6'>"+
                            "<label>Ahli Waris :</label>"+
                            "<input id='ahli_waris' class='form-control' name='ahli_waris[]' required='required' type='text' />"+
                        "</div>"+
                        "<div class='form-group col-sm-6'>"+
                            "<label>Hubungan Waris:</label>"+
                            "<input id='hubungan_ahli_waris' class='form-control' name='hubungan_ahli_waris[]' required='required' type='text' />"+
                        "</div>"+
                        "<a href='#' class='remove_field'>"+
                            "<i class='fa fa-times'></i>"+
                        "</a>"+
                    "</div>"
                ); //add input box
            }
        });
     
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>
@endsection

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Ahli Waris
        </h1>
        <br>
        <form action="{{ route('ahliWaris.insert') }}" method="POST">
            @csrf
            
            @include('forms.inputNik')

            @include('forms.inputNama')

            @include('forms.inputTempatLahir')
            
            @include('forms.inputTanggalLahir')

            @include('forms.inputJenisKelamin')
    
            @include('forms.inputKewarganegaraan')
    
            @include('forms.inputAgama')
    
            @include('forms.inputPekerjaan')

            @include('forms.inputTujuanPembuatanSurat')

            @include('forms.inputAhliWaris')

            @include('forms.inputSaksiAhliWaris')
            
            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection