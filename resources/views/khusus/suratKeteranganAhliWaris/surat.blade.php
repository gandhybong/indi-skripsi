<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section class="title">
            <h2 class="underlined">
                SURAT KETERANGAN AHLI WARIS
            </h2>
            <h2>
                Nomor : {{$suratKeteranganAhliWaris->nomor_surat}}
            </h2>
        </section>
        <p>
            Yang bertanda tangan di bawah ini Kepala Desa {{ $preferences['Desa']['value'] }} Kecamatan {{ $preferences['Kecamatan']['value'] }} Kabupaten {{ $preferences['Kabupaten']['value'] }} menerangkan bahwa : --------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKeteranganAhliWaris->nama }}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $suratKeteranganAhliWaris->tempat_lahir }}, {{ $suratKeteranganAhliWaris->tgl_lahir }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratKeteranganAhliWaris->jenis_kelamin }}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{ $suratKeteranganAhliWaris->kewarganegaraan }}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{ $suratKeteranganAhliWaris->agama }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratKeteranganAhliWaris->pekerjaan }}</td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td>Dan benar bahwa sdr.{{ $suratKeteranganAhliWaris->nama }} Telah Meninggal Dunia dan bahwa ahli waris almarhum {{ $suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris[0]->nama }} {{ $suratKeteranganAhliWaris->maksud }}. Adapun ahli waris dari {{ $suratKeteranganAhliWaris->nama }} sbb:</td>
            </tr>
            @if ($counter = 0)
            @endif
            @foreach ($suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris as $ahliWaris)
                @if ($counter == 0) 
                    @if ($counter++)
                    @endif
                    @continue
                @endif
                <tr>
                    <td>- {{ $ahliWaris['nama'] }}</td>
                    <td>:</td>
                    <td>{{ $ahliWaris['hubungan'] }}</td>
                </tr>
            @endforeach
        </table>
        <p>
            Berikut Saksi-Saksi:
        <p>
        <table class="document-data">
            @foreach ($suratKeteranganAhliWaris->saksiKeteranganAhliWaris as $saksiAhliWaris)
            <tr>
                <td>- {{ $saksiAhliWaris['nama'] }}</td>
                <td>(.....................)</td>
            </tr>
            @endforeach
        </table>
        <p>
            Dan orang-orang di atas adalah ahli waris dari sdr. {{ $suratKeteranganAhliWaris->nama }}
        </p>
        <br>
        <p>
            Demikian keterangan ini diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>