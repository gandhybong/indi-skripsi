@extends('layouts.app')

@section('content')
    <div>
        Surat Keterangan Ahli Waris
    </div>  
    <div>
        Nomor Id Surat : {{ $suratKeteranganAhliWaris->id }}
    </div>
    <div>
        Pengajuan Berlaku Sampai dengan : {{ $hariKadarluasaPengajuan }}
    </div>
    <div>
        Harap Bawa :
        <ul>
            <li>
                Kode Unik
            </li>
            <li>
                KTP Ahli Waris
            </li>
            <li>
                KTP Saksi
            </li>
        </ul>
        Ke Kepala Desa untuk Ditukarkan dengan Surat Anda
    </div>
@endsection