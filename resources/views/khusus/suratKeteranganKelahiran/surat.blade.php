<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section>
            <h2 class="underlined">
                SURAT KETERANGAN KELAHIRAN
            </h2>
            <h2>
                Nomor : {{ $SuratKeteranganKelahiran->nomor_surat }}
            </h2>
        </section>
        <p>
            Yang bertanda tangan di bawah ini Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong Utara menerangkan bahwa : --------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <th>
                    <b>
                        I. BAPAK
                    </b>
                </th>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->nik_pihak_bapak }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->nama_pihak_bapak }}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->tempat_lahir_pihak_bapak }}, {{ $SuratKeteranganKelahiran->tgl_lahir_pihak_bapak }}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->kewarganegaraan_pihak_bapak }}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->agama_pihak_bapak }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->pekerjaan_pihak_bapak }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->alamat_pihak_bapak }}</td>
            </tr>
        </table>
        <br>
        <br>
        <table>
            <tr>
                <th>
                    <b>
                        II. IBU
                    </b>
                </th>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->nik_pihak_ibu }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->nama_pihak_ibu }}</td>
            </tr>
            <tr>
                <td>Tempat, tanggal lahir</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->tempat_lahir_pihak_ibu }}, {{ $SuratKeteranganKelahiran->tgl_lahir_pihak_ibu }}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->kewarganegaraan_pihak_ibu }}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->agama_pihak_ibu }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->pekerjaan_pihak_ibu }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $SuratKeteranganKelahiran->alamat_pihak_ibu }}</td>
            </tr>
        </table>
        <br>
        <p>
            Dari Perkawinan / Pernikahan kedua orang tersebut diatas benar telah melahirkan seorang anak yang ke   ({{ $SuratKeteranganKelahiran->anak_ke }}) yaitu : --------------------------------------------------------------------------------------------------
        </p>
        <br>
        <table>
            <tr>
                <tr>
                    <td>Nama Lengkap</td>
                    <td>:</td>
                    <td>{{ $SuratKeteranganKelahiran->nama_anak }}</td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>{{ $SuratKeteranganKelahiran->jenis_kelamin_anak }}</td>
                </tr>
                <tr>
                    <td>Tempat, tanggal lahir</td>
                    <td>:</td>
                    <td>{{ $SuratKeteranganKelahiran->tempat_lahir_anak }}, {{ $SuratKeteranganKelahiran->tgl_lahir_anak->format('Y-m-d') }}</td>
                </tr>
                <tr>
                    <td>Hari / Jam</td>
                    <td>:</td>
                    <td>{{ $SuratKeteranganKelahiran->tgl_lahir_anak->locale('id')->dayName }} / {{ trim($SuratKeteranganKelahiran->jam_lahir_anak, ":00") }} WiB</td>
                </tr>
                <tr>
                    <td>Kewarganegaraan</td>
                    <td>:</td>
                    <td>{{ $SuratKeteranganKelahiran->kewarganegaraan_pihak_ibu }}</td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td>:</td>
                    <td>{{ $SuratKeteranganKelahiran->agama_pihak_ibu }}</td>
                </tr>
            </tr>
        </table>
        <br>
        <p>
            Demikianlah surat keterangan lahir ini dibuat dan diberikan kepada yang bersangkutan untuk dapat digunakan sebagai mana mestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>