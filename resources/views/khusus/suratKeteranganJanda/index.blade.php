@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>

    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Janda
        </h1>
        <br>
        <form action="{{ route('janda.insert') }}" method="POST">
            @csrf
            @include('forms.inputNik')

            @include('forms.inputNama')
    
            @include('forms.inputJenisKelamin')

            @include('forms.inputTempatLahir')
    
            @include('forms.inputTanggalLahir')

            @include('forms.inputKewarganegaraan')

            @include('forms.inputAgama')
            
            @include('forms.inputPekerjaan')
            
            @include('forms.inputAlamat')

            @include('forms.inputStatusPerkawinan')
            
            @include('forms.inputRt')

            @include('forms.inputRw')

            @include('forms.inputSebabCerai')

            @include('forms.inputAdministrasi')

            <input class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">
        </form>
    </div>

    <div class="col-md-2">

    </div>
@endsection