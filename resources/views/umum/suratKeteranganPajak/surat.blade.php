<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section class="title">
            <h2 class="underlined">
                SURAT KETERANGAN PAJAK
            </h2>
            <h2>
                Nomor : {{$SuratKeteranganPajak->nomor_surat}}
            </h2>
        </section class="title">
        <p>
            Yang bertanda tangan dibawah ini Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong Utara menerangkan bahwa : --------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->nik}}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->nama}}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->jenis_kelamin}}</td>
            </tr>
            <tr>
                <td>Tempat Tgl Lahir</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->tempat_lahir}} {{$SuratKeteranganPajak->tgl_lahir}}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->kewarganegaraan}}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->agama}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->status_perkawinan}}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->pekerjaan}}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{$SuratKeteranganPajak->alamat}}</td>
            </tr>
        </table>
        <h3>
            <u>KETERANGAN</u>
        </h3>
        <p>
            Dengan ini menerangkan bahwa nama yang bersangkutan di atas adalah benar pada saat ini belum mempunyai pajak PBB dan benar belum terdaftar di dalam DHKP
        </p>
        <p>
            Demikian keterangan ini diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.
        </p>
        <br>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>
                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>