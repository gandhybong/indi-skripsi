@extends('layouts.app')

@section('content')
    <div>
        Surat Keterangan Pajak
    </div>
    <div>
        Kode Unik : {{$kode}}
    </div>
    <div>
        Pengajuan Berlaku Sampai dengan : {{ $hariKadarluasaPengajuan }}
    </div>
    <div>
        Harap Bawa :
        <ul>
            <li>
                Kode Unik
            </li>
            <li>
                KTP
            </li>
        </ul>
        Ke Kepala Desa untuk Ditukarkan dengan Surat Anda
    </div>
@endsection