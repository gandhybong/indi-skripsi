@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>
    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Pajak
        </h1>
        <br>
        <form action="{{ route('pajak.insert') }}" method="POST">
            @csrf
            
            @include('forms.inputNik')
    
            @include('forms.inputNama')
    
            @include('forms.inputAlamat')
    
            @include('forms.inputJenisKelamin')
    
            @include('forms.inputTempatLahir')
    
            @include('forms.inputTanggalLahir')

            @include('forms.inputKewarganegaraan')

            @include('forms.inputAgama')
    
            @include('forms.inputStatusPerkawinan')

            @include('forms.inputPekerjaan')

            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="submit">
        </form>
    </div>
    
    <div class="col-md-2">

    </div>
@endsection