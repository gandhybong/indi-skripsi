@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>
    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Usaha
        </h1>
        <br>
        <form action="{{ route('usaha.insert') }}" method="POST">
            @csrf

            @include('forms.inputNik')
    
            @include('forms.inputNama')

            @include('forms.inputJenisKelamin')
    
            @include('forms.inputTempatLahir')

            @include('forms.inputTanggalLahir')

            @include('forms.inputStatusPerkawinan')

            @include('forms.inputKewarganegaraan')

            @include('forms.inputAgama')

            @include('forms.inputPekerjaan')

            @include('forms.inputAlamat')

            @include('forms.inputNamaUsaha')
            
            @include('forms.inputTahunBerdiriUsaha')

            @include('forms.inputAlamatUsaha')

            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="submit">
        </form>
    </div>
    
    <div class="col-md-2">

    </div>
@endsection