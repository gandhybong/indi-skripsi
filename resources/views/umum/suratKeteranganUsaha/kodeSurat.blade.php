@extends('layouts.app')

@section('content')
    <div>
        Surat Keterangan Usaha
    </div>
    <div>
        Kode Unik : {{$kode}}
    </div>
    <div>
        Pengajuan Berlaku Sampai dengan : {{ $hariKadarluasaPengajuan }}
    </div>
    <div>
        Harap Bawa :
        <ul>
            <li>
                Kode Unik
            </li>
            <li>
                KTP
            </li>
            <li>
                Nama Usaha
            </li>
            <li>
                Tahun Berdirinya Usaha
            </li>
        </ul>
        Ke Kepala Desa untuk Ditukarkan dengan Surat Anda
    </div>
@endsection