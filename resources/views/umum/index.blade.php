@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>
    <div class="col-md-8">
        <h1>
            DAFTAR SURAT UMUM
        </h1>
        <ul class="list-group list-group-flush list-items">
            <li class="">
                <a href="{{ route('domisili.index') }}">
                    Surat Keterangan Domisili
                </a>
            </li>
            <li class="">
                <a href="{{ route('pajak.index') }}">
                    Surat Keterangan Pajak
                </a>
            </li>
            <li class="">
                <a href="{{ route('imb.index') }}">
                    Surat Rekomendasi IMB
                </a>
            </li>
            <li class="">
                <a href="{{ route('usaha.index') }}">
                    Surat Keterangan Usaha
                </a>
            </li>
            <li class="">
                <a href="{{ route('kuasa.index') }}">
                    Surat Kuasa Tanah
                </a>
            </li>
        </ul>    
    </div>
    <div class="col-md-2">
    </div>
@endsection