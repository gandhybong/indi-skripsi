@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>
    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Domisili
        </h1>
        <br>
        <form action="{{ route('domisili.insert') }}" method="POST">
            @csrf

            @include('forms.inputNik')
    
            @include('forms.inputNama')
    
            @include('forms.inputAlamat')
    
            @include('forms.inputAlamatDomisili')
    
            @include('forms.inputNamaDusun')
    
            @include('forms.inputNamaDesa')
    
            @include('forms.inputKecamatan')
    
            @include('forms.inputKabupaten')
    
            @include('forms.inputTujuanPembuatanSurat')
    
            @include('forms.inputNamaKetuaRt')
    
            @include('forms.inputNamaKepalaDusun')

            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="submit">
        </form>
    </div>
    
    <div class="col-md-2">

    </div>
@endsection