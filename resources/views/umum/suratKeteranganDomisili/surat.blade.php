<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
    </header>
    <hr>
    <div class="main">
        <section class="title">
            <h2 class="underlined">
                SURAT KETERANGAN BERDOMISILI
            </h2>
            <h2>
                Nomor : {{$SuratKeteranganDomisili->nomor_surat}}
            </h2>
        </section>
        <p>
            Yang bertanda tangan dibawah ini Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong  menerangkan bahwa : --------------------------------------------------------------------------------
        </p>

        <table class="document-data">
            <tr>
                <td>Nama Orang/Instansi/Sekretariat</td>
                <td>:</td>
                <td>{{$SuratKeteranganDomisili->nama}}</td>
            </tr>
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{$SuratKeteranganDomisili->nik}}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{$SuratKeteranganDomisili->alamat}}</td>
            </tr>
            <tr>
                <td>-Dusun</td>
                <td>:</td>
                <td>{{$SuratKeteranganDomisili->Dusun}}</td>
            </tr>
            <tr>
                <td>-Desa</td>
                <td>:</td>
                <td>{{$SuratKeteranganDomisili->Desa}}</td>
            </tr>
            <tr>
                <td>-Kecamatan</td>
                <td>:</td>
                <td>{{$SuratKeteranganDomisili->Kecamatan}}</td>
            </tr>
            <tr>
                <td>-Kabupaten</td>
                <td>:</td>
                <td>{{$SuratKeteranganDomisili->Kabupaten}}</td>
            </tr>
        </table>
        <h3>
            <u>KETERANGAN</u>
        </h3>
        <p>
            Bahwa benar “<u>{{$SuratKeteranganDomisili->nama}}</u>” tersebut Di atas Berdomisili di <u>Jl.{{$SuratKeteranganDomisili->alamat_domisili}}</u>, Desa Sutera Kec.Sukadana  Kab.Kayong Utara. 
            Keterangan ini di berikan  sehubungan  dengan maksudnya untuk <u>{{$SuratKeteranganDomisili->Maksud}}</u>.
        </p>
        <p>
            Demikian keterangan ini diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.
        </p>
        <br>
    </div>
    <footer>
        <div id="tandaTangan">
            <div>
                Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
            </div>
            <div>
                {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
            </div>
            <div>
                Kepala Desa {{$preferences['Desa']['value']}}
            </div>

            <div id="kepalaDesa">
                {{$preferences['Kepala Desa']['value']}}
            </div>
        </div>
    </footer>
    <small id="tembusan">
        Tembusan :
        1. Kepala Dusun <u>{{ $SuratKeteranganDomisili->kepala_dusun }}</u>
        2. Ketua Rt <u>{{ $SuratKeteranganDomisili->ketua_rt }}</u>
    </small>
</body>
</html>