<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
        footer {
            display: block;
            margin-top: 50px;
        }
        .tanda-tangan {
            width: 33.33%;
            display: inline-block;
            margin: 0;
            float:left;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section class="title">
            <h2 class="underlined">
                SURAT KUASA TANAH
            </h2>
        </section>
        <p>
            Yang bertanda tangan dibawah ini : -----------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->nik }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->nama }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->jenis_kelamin }}</td>
            </tr>
            <tr>
                <td>Tempat, Tgl Lahir</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->tempat_lahir }}, {{ $suratKuasaTanah->tgl_lahir }}</td>
            </tr>
            <tr>
                <td>Status Perkawinan</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->status_perkawinan }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->alamat }}</td>
            </tr>
        </table>
        <p>
            Dengan ini meberikan kuasa kepada Saudara/Saudari : ------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->nik_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->nama_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->jenis_kelamin_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Tempat, Tgl Lahir</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->tempat_lahir_pihak_kedua }}, {{ $suratKuasaTanah->tgl_lahir_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Status Perkawinan</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->status_perkawinan_pihak_kedua }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->alamat_pihak_kedua }}</td>
            </tr>
        </table>
        <p>
            Adalah antara kedua belah pihak tersebut diatas adalah {{ $suratKuasaTanah->status_kedua_pihak }}.
        </p>
        <p>
            Atas harta benda dibawah ini : ---------------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>Sebidang tanah</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->jenis_tanah }}</td>
            </tr>
            <tr>
                <td>Nomor Hak Milik</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->nomor_sertifikat_tanah }}</td>
            </tr>
            <tr>
                <td>Atas Nama</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->atas_nama }}</td>
            </tr>
            <tr>
                <td>Tahun</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->tahun }}</td>
            </tr>
            <tr>
                <td>Luas</td>
                <td>:</td>
                <td>{{ $suratKuasaTanah->luas_tanah }}</td>
            </tr>
        </table>
        <br>
        <p>
            Beserta segala sesuatu yang telah dan akan ada dan atau ditanam/didirikan diatas tanah tersebut diatas.
        </p>
        <p>
            {{ $suratKuasaTanah->maksud }}
        </p>
        <p>
            Untuk mewakili dan bertindak untuk dan atas nama pemberi Kuasa,dalam hal menerima menentukan dan memutuskan penjualan bangunan yang dikuasakan.
        </p>
        <p>
            Demikian surat kuasa ini dibuat dan ditandatangani sebagai mana mestinya
        </p>
    </div>
            
    <footer>
        <div id="tandaTanganPihakPertama" class="tanda-tangan">
            <div>
                Dikeluarkan di : Desa {{ $preferences['Desa']['value'] }}
            </div>
            <div>
                {{ $preferences['Kecamatan']['value'] }}, {{ $dateNow }}
            </div>
            <div>
                Kepala Desa {{ $preferences['Desa']['value'] }}
            </div>
            <div id="kepalaDesa">
                <!-- {{ $preferences['Kepala Desa']['value'] }} -->
                ________________
            </div>
        </div>
        <div id="tandaTangan" class="tanda-tangan">
            <div>
                Dikeluarkan di : Desa {{ $preferences['Desa']['value'] }}
            </div>
            <div>
                {{ $preferences['Kecamatan']['value'] }}, {{ $dateNow }}
            </div>
            <div>
                Kepala Desa {{ $preferences['Desa']['value'] }}
            </div>
            <div id="kepalaDesa">
                <!-- {{ $preferences['Kepala Desa']['value'] }} -->
                ________________
            </div>
        </div>
        <div id="tandaTanganPihakKedua" class="tanda-tangan">
            <div>
                Dikeluarkan di : Desa {{ $preferences['Desa']['value'] }}
            </div>
            <div>
                {{ $preferences['Kecamatan']['value'] }}, {{ $dateNow }}
            </div>
            <div>
                Kepala Desa {{ $preferences['Desa']['value'] }}
            </div>
            <div id="kepalaDesa">
                <!-- {{ $preferences['Kepala Desa']['value'] }} -->
                _______________
            </div>
        </div>
    </footer>
</body>
</html>