@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>
    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Kuasa Tanah
        </h1>
        <br>
        <form action="{{ route('kuasa.insert') }}" method="POST">
            @csrf

            @include('forms.inputNik')
    
            @include('forms.inputNama')

            @include('forms.inputJenisKelamin')
    
            @include('forms.inputTempatLahir')

            @include('forms.inputTanggalLahir')

            @include('forms.inputStatusPerkawinan')
            
            @include('forms.inputPekerjaan')

            @include('forms.inputAlamat')

            @include('forms.inputNikPihakKedua')
    
            @include('forms.inputNamaPihakKedua')

            @include('forms.inputJenisKelaminPihakKedua')
    
            @include('forms.inputTempatLahirPihakKedua')

            @include('forms.inputTanggalLahirPihakKedua')

            @include('forms.inputStatusPerkawinanPihakKedua')
            
            @include('forms.inputPekerjaanPihakKedua')

            @include('forms.inputAlamatPihakKedua')

            @include('forms.inputStatusKeduaPihak')

            @include('forms.inputJenisTanah')
            
            @include('forms.inputNomorSertifikatTanah')

            @include('forms.inputAtasNama')

            @include('forms.inputTahun')
            
            @include('forms.inputLuasTanah')
            
            @include('forms.inputTujuanPembuatanSurat')

            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="submit">
        </form>
    </div>
    
    <div class="col-md-2">

    </div>
@endsection