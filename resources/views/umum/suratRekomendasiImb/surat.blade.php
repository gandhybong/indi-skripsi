<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cetak Surat</title>
    <style type="text/css" media="all">
        header {
            text-align: center;
        }
        img {
            width: 136px;
            float: left;
        }
        section.title {
            text-align: center;
            display: block
        }
        #tembusan{
            float: left;
        }
        #tandaTangan{
            text-align: right; 
            display: block;
            margin-top: 32px;
        }
        #kepalaDesa{
            padding-top : 80px;
        }
        table tr td, table.document-data tr td {
            vertical-align: top;
        }
        .underlined {
            text-decoration: underline;
        }
        .main p {
            text-align: justify;
        }
    </style>
</head>
<body>
    <header>
        <img src="{{public_path('/logo/Lambang_Kabupaten_Kayong_Utara.jpg')}}" alt="Image"/>
        <h1>
            PEMERINTAH KABUPATEN KAYONG UTARA
            KECAMATAN SUKADANA 
            DESA SUTERA
        </h1>
        Alamat : Jl. Akcaya, No. 04. Sukadana (KodePos 78852)
        <hr>
    </header>
    <div class="main">
        <section class="title">
            <h2 class="underlined">
                REKOMENDASI IMB
            </h2>
            <h2>
                Nomor : {{ $suratRekomendasiImb->nomor_surat }}
            </h2>
        </section>
        <p>
            Yang bertanda tangan dibawah ini Kepala Desa Sutera Kecamatan Sukadana Kabupaten Kayong Utara menerangkan bahwa : --------------------------------------------------------------------------------
        </p>
        <table class="document-data">
            <tr>
                <td>NIK</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->nik }}</td>
            </tr>
            <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->nama }}</td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->jenis_kelamin }}</td>
            </tr>
            <tr>
                <td>Kewarganegaraan</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->kewarganegaraan }}</td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->agama }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->status_perkawinan }}</td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->pekerjaan }}</td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->alamat }}</td>
            </tr>
        </table>

        <p>
            Sehubungan yang bersangkutan diatas berkeperluan untuk mendapatkan Izin {{ $suratRekomendasiImb->maksud }} yang diperuntukan untuk : 
        </p>
        <p>    
            {{ $suratRekomendasiImb->keperluan }}
        </p>

        <table class="document-data">
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->alamat_bangunan }}</td>
            </tr>
            <tr>
                <td>Luas Bangunan</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->luas_bangunan }}</td>
            </tr>
            <tr>
                <td>Luas Tanah</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->luas_tanah }}</td>
            </tr>
            <tr>
                <td>Dibangun di atas tanah</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->nama_pemilik_tanah }}</td>
            </tr>
            <tr>
                <td>Dengan Sertifikat Hak Milik nomor</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->nomor_sertifikat_tanah }}</td>
            </tr>
            <tr>
                <td>Bangunan terbuat dari</td>
                <td>:</td>
                <td>{{ $suratRekomendasiImb->bangunan_terbuat_dari }}</td>
            </tr>
        </table>            
            <p>
                Pada prinsipnya kami tidak berkeberatan atas maksud rencana yang bersangkutan,selama yang bersangkutan dimaksud dapat mematuhi ketentuan sebagai berikut :
            </p>
            <p>
                1. Peraturan perundang-undangan yang berlaku
            </p>
            <p>
                2.Wajib menjaga kebersihan, keamanan, dan ketertiban dalam membangun
            </p>
            <p>
                3.Rekomendasi ini diberikan bukan merupakan izin selamanya, tetapi hanya sebagai kelengkapan Dokumen untuk mengurus ke Instansi terkait.
            </p>
            <p>
                4.Apabila salah satu tidak terpenuhi / dilanggar, maka Rekomendasi ini akan dicabut dan akan diadakan perubahan kembali.
            </p>
            <p>
                5.Rekomendasi ini tidak dibenarkan untuk dirubah/disalahgunakan dan harus sesuai dengan peruntukan tertulis.
            </p>
            <p>
                6. Dan rekomendasi ini hanya berlaku sejak tanggal dikeluarkan sampai dengan {{ $suratRekomendasiImb->tanggal_terbit->addYears(5)->format('d-m-Y') }}.
            </p>
            <p>
                Demikian rekomendasi ini dibuat dan diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagai mana mestinya.
            </p>
        </div>
        <footer>
            <div id="tandaTangan">
                <div>
                    Dikeluarkan di : Desa {{$preferences['Desa']['value']}}
                </div>
                <div>
                    {{$preferences['Kecamatan']['value']}}, {{ $dateNow }}
                </div>
                <div>
                    Kepala Desa {{$preferences['Desa']['value']}}
                </div>

                <div id="kepalaDesa">
                    {{$preferences['Kepala Desa']['value']}}
                </div>
            </div>
        </footer>
    </div>
</body>
</html>