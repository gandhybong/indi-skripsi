@extends('layouts.app')

@section('content')
    <div class="col-md-2">

    </div>
    <div class="col-md-8 justify-content-center">
        <h1>
            Surat Keterangan Domisili
        </h1>
        <br>
        <form action="{{ route('imb.insert') }}" method="POST">
            @csrf

            @include('forms.inputNik')
    
            @include('forms.inputNama')

            @include('forms.inputJenisKelamin')
    
            @include('forms.inputTempatLahir')

            @include('forms.inputTanggalLahir')

            @include('forms.inputKewarganegaraan')

            @include('forms.inputAgama')

            @include('forms.inputStatusPerkawinan')

            @include('forms.inputPekerjaan')

            @include('forms.inputAlamat')
    
            @include('forms.inputTujuanPembuatanSurat')

            @include('forms.inputKeperluan')

            @include('forms.inputAlamatBangunan')

            @include('forms.inputLuasBangunan')

            @include('forms.inputLuasTanah')

            @include('forms.inputNamaPemilikTanah')

            @include('forms.inputNomorSertifikatTanah')

            @include('forms.inputBangunanTerbuatDari')

            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="submit">
        </form>
    </div>
    
    <div class="col-md-2">

    </div>
@endsection