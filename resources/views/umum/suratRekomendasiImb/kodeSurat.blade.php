@extends('layouts.app')

@section('content')
    <div>
        Surat Rekomendasi IMB
    </div>
    <div>
        Kode Unik : {{$kode}}
    </div>
    <div>
        Pengajuan Berlaku Sampai dengan : {{ $hariKadarluasaPengajuan }}
    </div>
    <div>
        Harap Bawa :
        <ul>
            <li>
                Kode Unik
            </li>
            <li>
                KTP
            </li>
            <li>
                Sertifikat Tanah
            </li>
        </ul>
        Ke Kepala Desa untuk Ditukarkan dengan Surat Anda
    </div>
@endsection