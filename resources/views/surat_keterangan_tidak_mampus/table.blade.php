@section( 'css' )
@include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganTidakMampus-table' ).DataTable({
            responsive: true
        });
    } );
</script>
@endsection

<div class="table-responsive">
    <table class="table" id="suratKeteranganTidakMampus-table">
        <thead>
            <tr>
                <th>User Id</th>
                <th>Nomor Kartu Keluarga</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Status Perkawinan</th>
                <th>Pekerjaan</th>
                <th>Alamat</th>
                <th>Penghasilan</th>
                <th>Maksud</th>
                <th>Status Id</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganTidakMampus as $suratKeteranganTidakMampu)
            <tr>
                <td>{{ $suratKeteranganTidakMampu->user_id }}</td>
                <td>{{ $suratKeteranganTidakMampu->nomor_kartu_keluarga }}</td>
                <td>{{ $suratKeteranganTidakMampu->nik }}</td>
                <td>{{ $suratKeteranganTidakMampu->nama }}</td>
                <td>{{ $suratKeteranganTidakMampu->jenis_kelamin }}</td>
                <td>{{ $suratKeteranganTidakMampu->status_perkawinan }}</td>
                <td>{{ $suratKeteranganTidakMampu->pekerjaan }}</td>
                <td>{{ $suratKeteranganTidakMampu->alamat }}</td>
                <td>{{ $suratKeteranganTidakMampu->penghasilan }}</td>
                <td>{{ $suratKeteranganTidakMampu->maksud }}</td>
                <td>{{ $suratKeteranganTidakMampu->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganTidakMampus.destroy', $suratKeteranganTidakMampu->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganTidakMampus.edit', [$suratKeteranganTidakMampu->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
