<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $suratKeteranganTidakMampu->user_id }}</p>
</div>

<!-- Nomor Kartu Keluarga Field -->
<div class="form-group">
    {!! Form::label('nomor_kartu_keluarga', 'Nomor Kartu Keluarga:') !!}
    <p>{{ $suratKeteranganTidakMampu->nomor_kartu_keluarga }}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{{ $suratKeteranganTidakMampu->nik }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $suratKeteranganTidakMampu->nama }}</p>
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{{ $suratKeteranganTidakMampu->jenis_kelamin }}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{{ $suratKeteranganTidakMampu->tempat_lahir }}</p>
</div>

<!-- Tanggal Lahir Field -->
<div class="form-group">
    {!! Form::label('tanggal_lahir', 'Tanggal Lahir:') !!}
    <p>{{ $suratKeteranganTidakMampu->tanggal_lahir }}</p>
</div>

<!-- Kewarganegaraan Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
    <p>{{ $suratKeteranganTidakMampu->kewarganegaraan }}</p>
</div>

<!-- Agama Field -->
<div class="form-group">
    {!! Form::label('agama', 'Agama:') !!}
    <p>{{ $suratKeteranganTidakMampu->agama }}</p>
</div>

<!-- Status Perkawinan Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan', 'Status Perkawinan:') !!}
    <p>{{ $suratKeteranganTidakMampu->status_perkawinan }}</p>
</div>

<!-- Pekerjaan Field -->
<div class="form-group">
    {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
    <p>{{ $suratKeteranganTidakMampu->pekerjaan }}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $suratKeteranganTidakMampu->alamat }}</p>
</div>

<!-- Penghasilan Field -->
<div class="form-group">
    {!! Form::label('penghasilan', 'Penghasilan:') !!}
    <p>{{ $suratKeteranganTidakMampu->penghasilan }}</p>
</div>

<!-- Maksud Field -->
<div class="form-group">
    {!! Form::label('maksud', 'Maksud:') !!}
    <p>{{ $suratKeteranganTidakMampu->maksud }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratKeteranganTidakMampu->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratKeteranganTidakMampu->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratKeteranganTidakMampu->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratKeteranganTidakMampu->nomor_surat }}</p>
</div>

