@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Tidak Mampu
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganTidakMampu, ['route' => ['suratKeteranganTidakMampus.update', $suratKeteranganTidakMampu->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_tidak_mampus.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection