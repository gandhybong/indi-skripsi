@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Tidak Mampu
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('surat_keterangan_tidak_mampus.show_fields')
                    <a href="{{ route('suratKeteranganTidakMampus.index') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
