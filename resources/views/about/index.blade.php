@extends('layouts.app')

@section('content')
    <div class="jumbotron" style="background-color: transparent;">
        <div class="col-sm-6 col-md-4">
            <img src="{{ asset('images/icons/icon-512x512.png') }}" width="100%">
        </div>
        <div class="col-sm-6 col-md-8">
            <h1>Desa Sutera</h1>
            <p>Sutera adalah desa di kecamatan Sukadana, Kayong Utara, Kalimantan Barat, Indonesia. Desa Sutera Pernah Menerima Penghargaan Dari Kementerian Desa (Kemendes) Sebagai Desa Terbaik Kategori Indeks Desa Membangun (IDM)</p>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12">
        <div class="antara">
            <img src="https://kalbar.antaranews.com/img/logo_ant_kalbar.png" alt="">
            <span>kolaborasi Antara Kalbar & Turbin Inovasi Indonesia</span>
        </div>
    </div>
@endsection