<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $suratKeteranganAhliWaris->user_id }}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{{ $suratKeteranganAhliWaris->nik }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $suratKeteranganAhliWaris->nama }}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{{ $suratKeteranganAhliWaris->tempat_lahir }}</p>
</div>

<!-- Tanggal Lahir Field -->
<div class="form-group">
    {!! Form::label('tanggal_lahir', 'Tanggal Lahir:') !!}
    <p>{{ $suratKeteranganAhliWaris->tanggal_lahir }}</p>
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{{ $suratKeteranganAhliWaris->jenis_kelamin }}</p>
</div>

<!-- Kewarganegaraan Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
    <p>{{ $suratKeteranganAhliWaris->kewarganegaraan }}</p>
</div>

<!-- Agama Field -->
<div class="form-group">
    {!! Form::label('agama', 'Agama:') !!}
    <p>{{ $suratKeteranganAhliWaris->agama }}</p>
</div>

<!-- Pekerjaan Field -->
<div class="form-group">
    {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
    <p>{{ $suratKeteranganAhliWaris->pekerjaan }}</p>
</div>

<!-- Maksud Field -->
<div class="form-group">
    {!! Form::label('maksud', 'Maksud:') !!}
    <p>{{ $suratKeteranganAhliWaris->maksud }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratKeteranganAhliWaris->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratKeteranganAhliWaris->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratKeteranganAhliWaris->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratKeteranganAhliWaris->nomor_surat }}</p>
</div>

