@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Ahli Waris
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganAhliWaris, ['route' => ['suratKeteranganAhliWaris.update', $suratKeteranganAhliWaris->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_ahli_waris.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection