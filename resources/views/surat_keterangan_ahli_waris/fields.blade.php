
@section('scripts')
<script type="text/javascript">
    $('#tgl_lahir').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false
    });

    $(document).ready(function() {
        var max_fields = 20; //maximum input boxes allowed
        var wrapper = $("#items"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                console.log(x);
                $(wrapper).append(
                    "<div class='form-group col-sm-12'>"+
                        "<div class='form-group col-sm-6'>"+
                            "<label>Ahli Waris :</label>"+
                            "<input id='ahli_waris' class='form-control' name='ahli_waris[]' required='required' type='text' />"+
                        "</div>"+
                        "<div class='form-group col-sm-6'>"+
                            "<label>Hubungan Waris:</label>"+
                            "<input id='hubungan_ahli_waris' class='form-control' name='hubungan_ahli_waris[]' required='required' type='text' />"+
                        "</div>"+
                        "<a href='#' class='remove_field'>"+
                            "<i class='fa fa-times'></i>"+
                        "</a>"+
                    "</div>"
                ); //add input box
            }
        });
     
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

    @if(session('status'))
        document.getElementById("print").click();
    @endif
</script>
@endsection

<div class="col-sm-6">
    <!-- Nik Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik', 'NIK:') !!}
        {!! Form::text('nik', null, ['class' => 'form-control', 'maxlength' => '16']) !!}
    </div>

    <!-- Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama', 'Nama:') !!}
        {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Tempat Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
        {!! Form::text('tempat_lahir', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Tanggal Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('Tanggal Lahir', 'Tanggal Lahir:') !!}
        {!! Form::date('tgl_lahir', null, ['class' => 'form-control','id'=>'tgl_lahir']) !!}
    </div>

    <!-- Jenis Kelamin Field -->
    <div class="form-group col-sm-12">
        <label for="jenis_kelamin">
            Jenis Kelamin
        </label>
        @isset($suratKeteranganAhliWaris)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki' || $suratKeteranganAhliWaris->jenis_kelamin == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan' || $suratKeteranganAhliWaris->jenis_kelamin == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endisset

        @empty($suratKeteranganAhliWaris)
        <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endempty
    </div>

    <!-- Kewarganegaraan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
        {!! Form::text('kewarganegaraan', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Agama Field -->
    <div class="form-group col-sm-12">
        <label for="agama">Agama</label>
        <select class="form-control" id="agama" name="agama" >
            @isset($suratKeteranganAhliWaris)
                <option value="Islam" {{ (old('agama') == 'Islam') || $suratKeteranganAhliWaris->agama == 'Islam' ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') || $suratKeteranganAhliWaris->agama == 'Protestan' ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') || $suratKeteranganAhliWaris->agama == 'Katolik' ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') || $suratKeteranganAhliWaris->agama == 'Hindu' ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') || $suratKeteranganAhliWaris->agama == 'Buddha' ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') || $suratKeteranganAhliWaris->agama == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
            @endisset
    
            @empty($suratKeteranganAhliWaris)
                <option value="Islam" {{ (old('agama') == 'Islam') ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
            @endempty
        </select>
    </div>

    <!-- Pekerjaan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
        {!! Form::text('pekerjaan', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Maksud Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('maksud', 'Maksud:') !!}
        {!! Form::text('maksud', null, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group col-sm-12">
        <div id="items">
        @isset($suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris)
            @if ($x = 1) @endif
            @foreach ($suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris as $item)
                <div class="form-group col-sm-12">
                    <div class="form-group col-sm-6">
                        <label>Ahli Waris:</label>
                        <input id="ahli_waris" class="form-control" name="ahli_waris[{{$item->id}}]" required="required" type="text" value="{{ $item->nama }}" />
                    </div>
                    <div class="form-group col-sm-6">
                        <label>Hubungan Waris:</label>
                        <input id="hubungan_ahli_waris" class="form-control" name="hubungan_ahli_waris[{{$item->id}}]" required="required" type="text" value="{{ $item->hubungan }}" />
                    </div>
                    <a href='#' class='remove_field'>
                        <i class='fa fa-times'></i>
                    </a>
                </div>
                @if ($x++) @endif
            @endforeach
        @endisset

        @empty($suratKeteranganAhliWaris->ahliWarisKeteranganAhliWaris)
        <div class="row">
            <div class="row">
                <div class="form-group col-sm-12">
                    <div class="form-group col-sm-6">
                        <label>Ahli Waris 1:</label>
                        <input id="ahli_waris" class="form-control" name="ahli_waris[]" required="required" type="text" />
                    </div>
                    <div class="form-group col-sm-6">
                        <label>Hubungan Waris 1:</label>
                        <input id="hubungan_ahli_waris" class="form-control" name="hubungan_ahli_waris[]" required="required" type="text" />
                    </div>
                </div>
            </div>
        </div>
        @endempty
        </div>
        <button type="button" class="add_field_button">Tambahkan Ahli Waris</button>
    </div>

    <div class="form-group col-sm-12">
        <label>
            Saksi Pertama
        </label>

        @isset($suratKeteranganAhliWaris->saksiKeteranganAhliWaris[0])
            <input type="hidden" class="form-control" id="saksi_pertama_id" name="saksi_pertama_id" 
                value="{{ $suratKeteranganAhliWaris->saksiKeteranganAhliWaris[0]->id }}"
            required>
        @endisset

        <input type="text" class="form-control" id="saksi_pertama" name="saksi_pertama" 
            @isset($suratKeteranganAhliWaris->saksiKeteranganAhliWaris[0])
                value="{{ $suratKeteranganAhliWaris->saksiKeteranganAhliWaris[0]->nama }}"
            @endisset    
        required>
    </div>

    <div class="form-group col-sm-12">
        <label>
            Saksi Kedua
        </label>
        @isset($suratKeteranganAhliWaris->saksiKeteranganAhliWaris[1])
            <input type="hidden" class="form-control" id="saksi_pertama_id" name="saksi_kedua_id" 
                value="{{ $suratKeteranganAhliWaris->saksiKeteranganAhliWaris[1]->id }}"
            required>
        @endisset 

        <input type="text" class="form-control" id="saksi_kedua" name="saksi_kedua" 
            @isset($suratKeteranganAhliWaris->saksiKeteranganAhliWaris[1])
                value="{{ $suratKeteranganAhliWaris->saksiKeteranganAhliWaris[1]->nama }}"
            @endisset 
        required>
    </div>

    @if(Route::current()->getName() == 'suratKeteranganAhliWaris.edit')
        <!-- Status Id Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status_id', 'Status Id') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganAhliWaris->status->status}}" readonly>
        </div>
    @endif
</div>

<div class="col-sm-12">
@if(Route::current()->getName() == 'suratKeteranganAhliWaris.edit')
    @if ( $suratKeteranganAhliWaris->status->status == "Draf" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
            {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
            <a href="{!! route('suratKeteranganAhliWaris.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @elseif ( $suratKeteranganAhliWaris->status->status == "Diterima" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{{ route('ahliWaris.print', ['id' => $suratKeteranganAhliWaris->id]) }}" class="btn btn-success" target="_blank" id="print">
                Print
            </a>
            <a href="{!! route('suratKeteranganAhliWaris.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('suratKeteranganAhliWaris.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @endif
@else
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Buat Surat Keterangan Ahli Waris', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('suratKeteranganAhliWaris.index') !!}" class="btn btn-default">kembali</a>
    </div>
@endif
</div>
