@section( 'css' )
    @include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratKeteranganAhliWaris-table' ).DataTable({

            });
        } );
    </script>
@endsection

<div class="table-responsive">
    <table class="table" id="suratKeteranganAhliWaris-table">
        <thead>
            <tr>
                <th>Nik</th>
                <th>Nama</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Kewarganegaraan</th>
                <th>Agama</th>
                <th>Pekerjaan</th>
                <th>Maksud</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganAhliWaris as $suratKeteranganAhliWaris)
            <tr>
                <td>{{ $suratKeteranganAhliWaris->nik }}</td>
                <td>{{ $suratKeteranganAhliWaris->nama }}</td>
                <td>{{ $suratKeteranganAhliWaris->tempat_lahir }}</td>
                <td>{{ $suratKeteranganAhliWaris->tanggal_lahir }}</td>
                <td>{{ $suratKeteranganAhliWaris->jenis_kelamin }}</td>
                <td>{{ $suratKeteranganAhliWaris->kewarganegaraan }}</td>
                <td>{{ $suratKeteranganAhliWaris->agama }}</td>
                <td>{{ $suratKeteranganAhliWaris->pekerjaan }}</td>
                <td>{{ $suratKeteranganAhliWaris->maksud }}</td>
                <td>{{ $suratKeteranganAhliWaris->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganAhliWaris.destroy', $suratKeteranganAhliWaris->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganAhliWaris.edit', [$suratKeteranganAhliWaris->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
