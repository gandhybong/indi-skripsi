@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Pengajuan Surat Umum dan Khusus</h1>
    </section>
    <br>
    <div class="content">
        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="col-sm-12">
                    <h3 class="box-title">Surat Umum</h3>
                </div>
                <div class="col-sm-6 list-items">
                    <a href="{{ route('suratKeteranganDomisilis.index') }}" class="">
                        Surat Keterangan Berdomisili
                    </a>
                    <a href="{{ route('suratKeteranganPajaks.index') }}" class="">
                        Surat Keterangan Pajak
                    </a>
                    <a href="{{ route('suratKuasaTanahs.index') }}" class="">
                        Surat Kuasa Tanah
                    </a>
                </div>
                <div class="col-sm-6 list-items">
                    <a href="{{ route('suratRekomendasiImbs.index') }}" class="">
                        Rekomendasi IMB
                    </a>
                    <a href="{{ route('suratKeteranganUsahas.index') }}" class="">
                        Surat Keterangan Usaha
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="col-sm-12">
                        <h3 class="box-title">Surat Khusus</h3>
                </div>
                <div class="col-sm-6 list-items">
                    <a href="{{ route('suratKeteranganIzinKeramaians.index') }}" class="">
                        Surat Keterangan Izin Keramaian
                    </a>
                    <a href="{{ route('suratKeteranganPenghasilans.index') }}"class="">
                        Surat Keterangan Penghasilan
                    </a>
                    <a href="{{ route('suratKeteranganTidakMampus.index') }}" class="">
                        Surat Keterangan Tidak Mampu
                    </a>
                    <a href="{{ route('suratKeteranganAhliWaris.index') }}" class="">
                        Surat Keterangan Ahli Waris
                    </a>
                    <a href="{{ route('suratKeteranganBedaNamas.index') }}" class="">
                        Surat Keterangan Beda Nama
                    </a>
                    <a href="{{ route('suratKeteranganBelumNpwps.index') }}" class="">
                        Surat Keterangan Belum NPWP
                    </a>
                    <a href="{{ route('suratPernyataanHibahs.index') }}" class="">
                        Surat Pernyataan Hibah
                    </a>
                    <a href="{{ route('suratKeteranganJandas.index') }}" class="">
                        Surat Keterangan Janda
                    </a>
                    <a href="{{ route('suratBelumPernahMenikahs.index') }}" class="">
                        Keterangan Belum Pernah Menikah
                    </a>
                </div>
                <div class="col-sm-6 list-items">
                    <a href="{{ route('suratKeteranganHubunganKeluargas.index') }}" class="">
                        Surat Keterangan Hubungan Keluarga
                    </a>
                    <a href="{{ route('suratKeteranganPanggilanCerais.index') }}" class="">
                        Surat Keterangan Panggilan Cerai
                    </a>
                    <a href="{{ route('suratKeterangans.index') }}" class="">
                        Surat Keterangan
                    </a>
                    <a href="{{ route('suratKeteranganTentangKematians.index') }}" class="">
                        Surat Keterangan Tentang Kematian
                    </a>
                    <a href="{{ route('suratKeteranganKelahirans.index') }}" class="">
                        Surat Keterangan Tentang Kelahiran
                    </a>
                    <a href="" class="" disabled="disabled">
                        Surat Keterangan Untuk Menikah
                    </a>
                    <a href="" class="" disabled="disabled">
                        Surat Keterangan Asal Usul
                    </a>
                    <a href="" class="" disabled="disabled">
                        Surat Keterangan Pernah Menikah
                    </a>
                    <a href="" class="" disabled="disabled">
                        Surat Pernyataan
                    </a>
                </div>
            </div>
        </div>
    </div>
    

@endsection

