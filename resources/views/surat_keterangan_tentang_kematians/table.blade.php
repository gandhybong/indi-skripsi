@section( 'css' )
@include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratKeteranganTentangKematians-table' ).DataTable({
                responsive: true
            });
        } );
    </script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKeteranganTentangKematians-table">
        <thead>
            <tr>
                <th>Nik</th>
                <th>Nama</th>
                <th>Bin Binti</th>
                <th>Jenis Kelamin</th>
                <th>Agama</th>
                <th>Tempat Lahir</th>
                <th>Tgl Lahir</th>
                <th>Status Perkawinan</th>
                <th>Pekerjaan</th>
                <th>Alamat</th>
                <th>Tgl Meninggal</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganTentangKematians as $suratKeteranganTentangKematian)
            <tr>
                <td>{{ $suratKeteranganTentangKematian->nik }}</td>
                <td>{{ $suratKeteranganTentangKematian->nama }}</td>
                <td>{{ $suratKeteranganTentangKematian->bin_binti }}</td>
                <td>{{ $suratKeteranganTentangKematian->jenis_kelamin }}</td>
                <td>{{ $suratKeteranganTentangKematian->agama }}</td>
                <td>{{ $suratKeteranganTentangKematian->tempat_lahir }}</td>
                <td>{{ $suratKeteranganTentangKematian->tgl_lahir }}</td>
                <td>{{ $suratKeteranganTentangKematian->status_perkawinan }}</td>
                <td>{{ $suratKeteranganTentangKematian->pekerjaan }}</td>
                <td>{{ $suratKeteranganTentangKematian->alamat }}</td>
                <td>{{ $suratKeteranganTentangKematian->tgl_meninggal }}</td>
                <td>{{ $suratKeteranganTentangKematian->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganTentangKematians.destroy', $suratKeteranganTentangKematian->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganTentangKematians.edit', [$suratKeteranganTentangKematian->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
