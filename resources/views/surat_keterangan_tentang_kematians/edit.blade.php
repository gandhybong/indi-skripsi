@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Tentang Kematian
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganTentangKematian, ['route' => ['suratKeteranganTentangKematians.update', $suratKeteranganTentangKematian->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_tentang_kematians.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection