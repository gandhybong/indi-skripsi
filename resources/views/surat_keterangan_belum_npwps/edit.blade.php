@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Belum Npwp
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganBelumNpwp, ['route' => ['suratKeteranganBelumNpwps.update', $suratKeteranganBelumNpwp->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_belum_npwps.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection