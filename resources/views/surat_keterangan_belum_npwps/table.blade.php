@section( 'css' )
@include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganBelumNpwps-table' ).DataTable({

        });
    } );
</script>
@endsection<div class="table-responsive">
    <table class="table" id="suratKeteranganBelumNpwps-table">
        <thead>
            <tr>
                <th>Nik</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Tgl Lahir</th>
                <th>Pekerjaan</th>
                <th>Status Perkawinan</th>
                <th>Alamat</th>
                <th>Maksud</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganBelumNpwps as $suratKeteranganBelumNpwp)
            <tr>
                <td>{{ $suratKeteranganBelumNpwp->nik }}</td>
                <td>{{ $suratKeteranganBelumNpwp->nama }}</td>
                <td>{{ $suratKeteranganBelumNpwp->jenis_kelamin }}</td>
                <td>{{ $suratKeteranganBelumNpwp->tempat_lahir }}</td>
                <td>{{ $suratKeteranganBelumNpwp->tgl_lahir }}</td>
                <td>{{ $suratKeteranganBelumNpwp->pekerjaan }}</td>
                <td>{{ $suratKeteranganBelumNpwp->status_perkawinan }}</td>
                <td>{{ $suratKeteranganBelumNpwp->alamat }}</td>
                <td>{{ $suratKeteranganBelumNpwp->maksud }}</td>
                <td>{{ $suratKeteranganBelumNpwp->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganBelumNpwps.destroy', $suratKeteranganBelumNpwp->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganBelumNpwps.edit', [$suratKeteranganBelumNpwp->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
