@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Penghasilan
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganPenghasilan, ['route' => ['suratKeteranganPenghasilans.update', $suratKeteranganPenghasilan->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_penghasilans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection