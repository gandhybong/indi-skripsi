@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Penghasilan
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'suratKeteranganPenghasilans.store']) !!}

                        @include('surat_keterangan_penghasilans.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
