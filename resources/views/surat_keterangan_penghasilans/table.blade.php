@section( 'css' )
@include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganPenghasilans-table' ).DataTable({
            responsive: true
        });
    } );
</script>
@endsection

<div class="table-responsive">
    <table class="table" id="suratKeteranganPenghasilans-table">
        <thead>
            <tr>
                <th>Nama Pengguna</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Pekerjaan</th>
                <th>Alamat</th>
                <th>Penghasilan</th>
                <th>Maksud</th>
                <th>Status Id</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganPenghasilans as $suratKeteranganPenghasilan)
            <tr>
                <td>{!! $suratKeteranganPenghasilan->user->name !!}</td>
                <td>{!! $suratKeteranganPenghasilan->nik !!}</td>
                <td>{!! $suratKeteranganPenghasilan->nama !!}</td>
                <td>{!! $suratKeteranganPenghasilan->jenis_kelamin !!}</td>
                <td>{!! $suratKeteranganPenghasilan->tempat_lahir !!}</td>
                <td>{!! $suratKeteranganPenghasilan->Pekerjaan !!}</td>
                <td>{!! $suratKeteranganPenghasilan->alamat !!}</td>
                <td>{!! $suratKeteranganPenghasilan->penghasilan !!}</td>
                <td>{!! $suratKeteranganPenghasilan->maksud !!}</td>
                <td>{!! $suratKeteranganPenghasilan->status->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganPenghasilans.destroy', $suratKeteranganPenghasilan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('suratKeteranganPenghasilans.edit', [$suratKeteranganPenghasilan->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
