<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $suratKeteranganPenghasilan->user_id !!}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{!! $suratKeteranganPenghasilan->nik !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $suratKeteranganPenghasilan->nama !!}</p>
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{!! $suratKeteranganPenghasilan->jenis_kelamin !!}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{!! $suratKeteranganPenghasilan->tempat_lahir !!}</p>
</div>

<!-- Tgl Lahir Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
    <p>{!! $suratKeteranganPenghasilan->tgl_lahir !!}</p>
</div>

<!-- Kewarganegaraan Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
    <p>{!! $suratKeteranganPenghasilan->kewarganegaraan !!}</p>
</div>

<!-- Pekerjaan Field -->
<div class="form-group">
    {!! Form::label('Pekerjaan', 'Pekerjaan:') !!}
    <p>{!! $suratKeteranganPenghasilan->Pekerjaan !!}</p>
</div>

<!-- Agama Field -->
<div class="form-group">
    {!! Form::label('agama', 'Agama:') !!}
    <p>{!! $suratKeteranganPenghasilan->agama !!}</p>
</div>

<!-- Status Perkawinan Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan', 'Status Perkawinan:') !!}
    <p>{!! $suratKeteranganPenghasilan->status_perkawinan !!}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $suratKeteranganPenghasilan->alamat !!}</p>
</div>

<!-- Penghasilan Field -->
<div class="form-group">
    {!! Form::label('penghasilan', 'Penghasilan:') !!}
    <p>{!! $suratKeteranganPenghasilan->penghasilan !!}</p>
</div>

<!-- Maksud Field -->
<div class="form-group">
    {!! Form::label('maksud', 'Maksud:') !!}
    <p>{!! $suratKeteranganPenghasilan->maksud !!}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{!! $suratKeteranganPenghasilan->status_id !!}</p>
</div>

