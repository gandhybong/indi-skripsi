{{--
<li class="{{ Request::is('suratKeteranganDomisilis*')||Request::is('suratKeteranganPajaks*') ? 'active' : '' }}">
    <a href="#suratUmum" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Surat Umum</a>
    <ul class="sidebar-menu" data-widget="tree" id="suratUmum">
        <li class="{{ Request::is('suratKeteranganDomisilis*') ? 'active' : '' }}">
            <a href="{!! route('suratKeteranganDomisilis.index') !!}"><i class="fa fa-edit"></i><span>Ket. Domisili</span></a>
        </li>
        
        <li class="{{ Request::is('suratKeteranganPajaks*') ? 'active' : '' }}">
            <a href="{!! route('suratKeteranganPajaks.index') !!}"><i class="fa fa-edit"></i><span>Ket. Pajak</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('suratKeteranganIzinKeramaians*')||Request::is('suratKeteranganPenghasilans*') ? 'active' : '' }}">
    <a href="#suratKhusus" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Surat Khusus</a>
    <ul class="sidebar-menu" data-widget="tree" id="suratKhusus">
        <li class="{{ Request::is('suratKeteranganIzinKeramaians*') ? 'active' : '' }}">
            <a href="{!! route('suratKeteranganIzinKeramaians.index') !!}"><i class="fa fa-edit"></i><span>Ket. Izin Keramaian</span></a>
        </li>
        
        <li class="{{ Request::is('suratKeteranganPenghasilans*') ? 'active' : '' }}">
            <a href="{!! route('suratKeteranganPenghasilans.index') !!}"><i class="fa fa-edit"></i><span>Ket. Penghasilan</span></a>
        </li>
    </ul>
</li>
--}}


<li class="{{ Request::is('dashboard*') ? 'active' : '' }}">
    <a href="{!! route('dashboard.index') !!}"><i class="fa fa-tachometer" aria-hidden="true"></i><span>Dashboard</span></a>
</li>

<li class="{{ Request::is('documents*') ? 'active' : '' }}">
    <a href="{!! route('documents.index') !!}"><i class="fa fa-envelope-o"></i><span>Layanan Surat</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-users"></i><span>Pemohon</span></a>
</li>

<li class="{{ Request::is('preferences*') ? 'active' : '' }}">
    <a href="{!! route('preferences.index') !!}"><i class="fa fa-gears"></i><span>Preferensi</span></a>
</li>

{{-- 
<li class="{{ Request::is('suratRekomendasiImbs*') ? 'active' : '' }}">
    <a href="{{ route('suratRekomendasiImbs.index') }}"><i class="fa fa-edit"></i><span>Surat Rekomendasi Imbs</span></a>
</li> --}}

{{--  <li class="{{ Request::is('suratKeteranganUsahas*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganUsahas.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Usahas</span></a>
</li>  --}}

{{--  <li class="{{ Request::is('suratKuasaTanahs*') ? 'active' : '' }}">
    <a href="{{ route('suratKuasaTanahs.index') }}"><i class="fa fa-edit"></i><span>Surat Kuasa Tanahs</span></a>
</li>  --}}

{{--  <li class="{{ Request::is('suratKeteranganTidakMampus*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganTidakMampus.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Tidak Mampus</span></a>
</li>  --}}

{{--  <li class="{{ Request::is('suratKeteranganAhliWaris*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganAhliWaris.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Ahli Waris</span></a>
</li>  --}}

{{--  <li class="{{ Request::is('ahliWarisKeterenganAhliWaris*') ? 'active' : '' }}">
    <a href="{{ route('ahliWarisKeterenganAhliWaris.index') }}"><i class="fa fa-edit"></i><span>Ahli Waris Keterengan Ahli Waris</span></a>
</li>  --}}

{{--  <li class="{{ Request::is('saksiKeteranganAhliWaris*') ? 'active' : '' }}">
    <a href="{{ route('saksiKeteranganAhliWaris.index') }}"><i class="fa fa-edit"></i><span>Saksi Keterangan Ahli Waris</span></a>
</li>  --}}

{{-- <li class="{{ Request::is('suratKeteranganBedaNamas*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganBedaNamas.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Beda Namas</span></a>
</li> --}}

{{-- <li class="{{ Request::is('suratKeteranganBelumNpwps*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganBelumNpwps.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Belum Npwps</span></a>
</li> --}}

{{--  <li class="{{ Request::is('suratPernyataanHibahs*') ? 'active' : '' }}">
    <a href="{{ route('suratPernyataanHibahs.index') }}"><i class="fa fa-edit"></i><span>Surat Pernyataan Hibahs</span></a>
</li>  --}}

{{--  <li class="{{ Request::is('riwayatKepemilikanHibahs*') ? 'active' : '' }}">
    <a href="{{ route('riwayatKepemilikanHibahs.index') }}"><i class="fa fa-edit"></i><span>Riwayat Kepemilikan Hibahs</span></a>
</li>  --}}

{{--  <li class="{{ Request::is('suratKeteranganJandas*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganJandas.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Jandas</span></a>
</li>  --}}

{{-- <li class="{{ Request::is('suratKeteranganBelumPernahMenikahs*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganBelumPernahMenikahs.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Belum Pernah Menikahs</span></a>
</li> --}}

{{-- <li class="{{ Request::is('suratKeteranganHubunganKeluargas*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganHubunganKeluargas.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Hubungan Keluargas</span></a>
</li> --}}

{{-- <li class="{{ Request::is('suratKeteranganPanggilanCerais*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganPanggilanCerais.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Panggilan Cerais</span></a>
</li> --}}

{{-- <li class="{{ Request::is('suratKeterangans*') ? 'active' : '' }}">
    <a href="{{ route('suratKeterangans.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangans</span></a>
</li> --}}

{{-- <li class="{{ Request::is('suratKeteranganTentangKematians*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganTentangKematians.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Tentang Kematians</span></a>
</li> --}}

{{--  <li class="{{ Request::is('suratKeteranganKelahirans*') ? 'active' : '' }}">
    <a href="{{ route('suratKeteranganKelahirans.index') }}"><i class="fa fa-edit"></i><span>Surat Keterangan Kelahirans</span></a>
</li>  --}}

