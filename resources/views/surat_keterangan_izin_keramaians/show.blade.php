@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Izin Keramaian
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('surat_keterangan_izin_keramaians.show_fields')
                    <a href="{!! route('suratKeteranganIzinKeramaians.index') !!}" class="btn btn-default">Back</a>
                    @if ($suratKeteranganIzinKeramaian->status->status == "Diterima")
                        <a href="{!! route('keramaian.print',['id'=>$suratKeteranganIzinKeramaian->id]) !!}" class="btn btn-default">Print</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
