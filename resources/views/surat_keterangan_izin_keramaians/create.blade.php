@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Izin Keramaian
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'suratKeteranganIzinKeramaians.store']) !!}

                        @include('surat_keterangan_izin_keramaians.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
