<!-- UserName Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->user->username !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->nama !!}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->tempat_lahir !!}</p>
</div>

<!-- Tgl Lahir Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->tgl_lahir !!}</p>
</div>

<!-- Kewarganegaraan Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->kewarganegaraan !!}</p>
</div>

<!-- Agama Field -->
<div class="form-group">
    {!! Form::label('agama', 'Agama:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->agama !!}</p>
</div>

<!-- Status Perkawinan Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan', 'Status Perkawinan:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->status_perkawinan !!}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->alamat !!}</p>
</div>

<!-- Acara Kegiatan Field -->
<div class="form-group">
    {!! Form::label('acara_kegiatan', 'Acara Kegiatan:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->acara_kegiatan !!}</p>
</div>

<!-- Tgl Mulai Field -->
<div class="form-group">
    {!! Form::label('tgl_mulai', 'Tgl Mulai:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->tgl_mulai !!}</p>
</div>

<!-- Tgl Selesai Field -->
<div class="form-group">
    {!! Form::label('tgl_selesai', 'Tgl Selesai:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->tgl_selesai !!}</p>
</div>

<!-- Alamat Acara Field -->
<div class="form-group">
    {!! Form::label('alamat_acara', 'Alamat Acara:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->alamat_acara !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $suratKeteranganIzinKeramaian->status->status !!}</p>
</div>

