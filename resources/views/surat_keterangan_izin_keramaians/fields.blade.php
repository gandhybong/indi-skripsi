<div class="col-sm-6">
    <!-- Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama', 'Nama') !!}
        {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tempat Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tempat_lahir', 'Tempat Lahir') !!}
        {!! Form::text('tempat_lahir', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tgl Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tgl_lahir', 'Tgl Lahir') !!}
        {!! Form::date('tgl_lahir', null , ['class' => 'form-control','id'=>'tgl_lahir']) !!}
    </div>
    
    <!-- Kewarganegaraan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kewarganegaraan', 'Kewarganegaraan') !!}
        {!! Form::text('kewarganegaraan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Agama Field -->
    <div class="form-group col-sm-12">
        <label for="agama">Agama</label>
        <select class="form-control" id="agama" name="agama" >
            @isset($suratKeteranganIzinKeramaian)
                <option value="Islam" {{ (old('agama') == 'Islam') || $suratKeteranganIzinKeramaian->agama == 'Islam' ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') || $suratKeteranganIzinKeramaian->agama == 'Protestan' ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') || $suratKeteranganIzinKeramaian->agama == 'Katolik' ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') || $suratKeteranganIzinKeramaian->agama == 'Hindu' ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') || $suratKeteranganIzinKeramaian->agama == 'Buddha' ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') || $suratKeteranganIzinKeramaian->agama == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
            @endisset

            @empty($suratKeteranganIzinKeramaian)
                <option value="Islam" {{ (old('agama') == 'Islam') ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
            @endempty
        </select>
    </div>
    
    <!-- Status Perkawinan Field -->
    <div class="form-group col-sm-12">
        <label for="status_perkawinan">
            Status Perkawinan 
        </label>
        @isset($suratKeteranganIzinKeramaian)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') || $suratKeteranganIzinKeramaian->status_perkawinan == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') || $suratKeteranganIzinKeramaian->status_perkawinan == 'Kawin' ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') || $suratKeteranganIzinKeramaian->status_perkawinan == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') || $suratKeteranganIzinKeramaian->status_perkawinan == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endisset

        @empty($suratKeteranganIzinKeramaian)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endempty
    </div>
    
    <!-- Alamat Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat', 'Alamat') !!}
        {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="col-sm-6">
    <!-- Alamat Acara Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat_acara', 'Alamat Acara') !!}
        {!! Form::textarea('alamat_acara', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Acara Kegiatan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('acara_kegiatan', 'Acara Kegiatan') !!}
        {!! Form::text('acara_kegiatan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tgl Mulai Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tgl_mulai', 'Tgl Mulai') !!}
        {!! Form::date('tgl_mulai', null , ['class' => 'form-control','id'=>'tgl_mulai']) !!}
    </div>
    
    <!-- Tgl Selesai Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tgl_selesai', 'Tgl Selesai') !!}
        {!! Form::date('tgl_selesai', null , ['class' => 'form-control','id'=>'tgl_selesai']) !!}
    </div>
    
    @if(Route::current()->getName() == 'suratKeteranganIzinKeramaians.edit')
        <!-- Status Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status', 'Status') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganIzinKeramaian->status->status}}" readonly>
        </div>
    @endif
</div>

<div class="col-sm-12">
@if(Route::current()->getName() == 'suratKeteranganIzinKeramaians.edit')
    @if ( $suratKeteranganIzinKeramaian->status->status == "Draf" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
            {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
            <a href="{!! route('suratKeteranganIzinKeramaians.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @elseif ( $suratKeteranganIzinKeramaian->status->status == "Diterima" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{{ route('keramaian.print', ['id' => $suratKeteranganIzinKeramaian->id]) }}" class="btn btn-success" target="_blank" id="print">
                Print
            </a>
            <a href="{!! route('suratKeteranganIzinKeramaians.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('suratKeteranganIzinKeramaians.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @endif
@else
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Buat Surat Keterangan Izin Keramaian', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('suratKeteranganIzinKeramaians.index') !!}" class="btn btn-default">kembali</a>
    </div>
@endif
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_mulai').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#tgl_selesai').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection