@section( 'css' )
@include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganIzinKeramaians-table' ).DataTable({

        });
    } );
</script>
@endsection

<div class="table-responsive">
    <table class="table" id="suratKeteranganIzinKeramaians-table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Tempat Lahir</th>
                <th>Tgl Lahir</th>
                <th>Alamat</th>
                <th>Acara Kegiatan</th>
                <th>Tgl Mulai</th>
                <th>Tgl Selesai</th>
                <th>Alamat Acara</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganIzinKeramaians as $suratKeteranganIzinKeramaian)
            <tr>
                <td>{!! $suratKeteranganIzinKeramaian->nama !!}</td>
                <td>{!! $suratKeteranganIzinKeramaian->tempat_lahir !!}</td>
                <td style="word-wrap: break-word;min-width: 100px;max-width: 100px;">{!! $suratKeteranganIzinKeramaian->tgl_lahir !!}</td>
                <td>{!! $suratKeteranganIzinKeramaian->alamat !!}</td>
                <td>{!! $suratKeteranganIzinKeramaian->acara_kegiatan !!}</td>
                <td style="word-wrap: break-word;min-width: 100px;max-width: 100px;">{!! $suratKeteranganIzinKeramaian->tgl_mulai !!}</td>
                <td style="word-wrap: break-word;min-width: 100px;max-width: 100px;">{!! $suratKeteranganIzinKeramaian->tgl_selesai !!}</td>
                <td>{!! $suratKeteranganIzinKeramaian->alamat_acara !!}</td>
                <td>{!! $suratKeteranganIzinKeramaian->status->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganIzinKeramaians.destroy', $suratKeteranganIzinKeramaian->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('suratKeteranganIzinKeramaians.edit', [$suratKeteranganIzinKeramaian->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
