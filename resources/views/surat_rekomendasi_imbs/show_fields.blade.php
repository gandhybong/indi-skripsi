<!-- Kode Field -->
<div class="form-group">
    {!! Form::label('kode', 'Kode:') !!}
    <p>{{ $suratRekomendasiImb->kode }}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{{ $suratRekomendasiImb->nik }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $suratRekomendasiImb->nama }}</p>
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{{ $suratRekomendasiImb->jenis_kelamin }}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{{ $suratRekomendasiImb->tempat_lahir }}</p>
</div>

<!-- Tgl Lahir Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
    <p>{{ $suratRekomendasiImb->tgl_lahir }}</p>
</div>

<!-- Kewarganegaraan Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
    <p>{{ $suratRekomendasiImb->kewarganegaraan }}</p>
</div>

<!-- Agama Field -->
<div class="form-group">
    {!! Form::label('agama', 'Agama:') !!}
    <p>{{ $suratRekomendasiImb->agama }}</p>
</div>

<!-- Status Perkawinan Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan', 'Status Perkawinan:') !!}
    <p>{{ $suratRekomendasiImb->status_perkawinan }}</p>
</div>

<!-- Pekerjaan Field -->
<div class="form-group">
    {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
    <p>{{ $suratRekomendasiImb->pekerjaan }}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $suratRekomendasiImb->alamat }}</p>
</div>

<!-- Maksud Field -->
<div class="form-group">
    {!! Form::label('maksud', 'Maksud:') !!}
    <p>{{ $suratRekomendasiImb->maksud }}</p>
</div>

<!-- Keperluan Field -->
<div class="form-group">
    {!! Form::label('keperluan', 'Keperluan:') !!}
    <p>{{ $suratRekomendasiImb->keperluan }}</p>
</div>

<!-- Alamat Bangunan Field -->
<div class="form-group">
    {!! Form::label('alamat_bangunan', 'Alamat Bangunan:') !!}
    <p>{{ $suratRekomendasiImb->alamat_bangunan }}</p>
</div>

<!-- Luas Bangunan Field -->
<div class="form-group">
    {!! Form::label('luas_bangunan', 'Luas Bangunan:') !!}
    <p>{{ $suratRekomendasiImb->luas_bangunan }}</p>
</div>

<!-- Luas Tanah Field -->
<div class="form-group">
    {!! Form::label('luas_tanah', 'Luas Tanah:') !!}
    <p>{{ $suratRekomendasiImb->luas_tanah }}</p>
</div>

<!-- Nama Pemilik Tanah Field -->
<div class="form-group">
    {!! Form::label('nama_pemilik_tanah', 'Nama Pemilik Tanah:') !!}
    <p>{{ $suratRekomendasiImb->nama_pemilik_tanah }}</p>
</div>

<!-- Nomor Sertifikat Tanah Field -->
<div class="form-group">
    {!! Form::label('nomor_sertifikat_tanah', 'Nomor Sertifikat Tanah:') !!}
    <p>{{ $suratRekomendasiImb->nomor_sertifikat_tanah }}</p>
</div>

<!-- Bangunan Terbuat Dari Field -->
<div class="form-group">
    {!! Form::label('bangunan_terbuat_dari', 'Bangunan Terbuat Dari:') !!}
    <p>{{ $suratRekomendasiImb->bangunan_terbuat_dari }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratRekomendasiImb->status_id }}</p>
</div>

