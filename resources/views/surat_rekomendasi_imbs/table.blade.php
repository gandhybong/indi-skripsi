@section( 'css' )
    @include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratRekomendasiImbs-table' ).DataTable({

            });
        } );
    </script>
@endsection

<div class="table-responsive">
    <table class="table" id="suratRekomendasiImbs-table">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>Alamat Bangunan</th>
                <th>Luas Bangunan</th>
                <th>Luas Tanah</th>
                <th>Nama Pemilik Tanah</th>
                <th>Nomor Sertifikat Tanah</th>
                <th>Bangunan Terbuat Dari</th>
                <th>Status Id</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratRekomendasiImbs as $suratRekomendasiImb)
            <tr>
                <td>{{ $suratRekomendasiImb->kode }}</td>
                <td>{{ $suratRekomendasiImb->nik }}</td>
                <td>{{ $suratRekomendasiImb->nama }}</td>
                <td>{{ $suratRekomendasiImb->alamat_bangunan }}</td>
                <td>{{ $suratRekomendasiImb->luas_bangunan }}</td>
                <td>{{ $suratRekomendasiImb->luas_tanah }}</td>
                <td>{{ $suratRekomendasiImb->nama_pemilik_tanah }}</td>
                <td>{{ $suratRekomendasiImb->nomor_sertifikat_tanah }}</td>
                <td>{{ $suratRekomendasiImb->bangunan_terbuat_dari }}</td>
                <td>{{ $suratRekomendasiImb->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratRekomendasiImbs.destroy', $suratRekomendasiImb->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratRekomendasiImbs.edit', [$suratRekomendasiImb->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
