@if(Route::current()->getName() == 'suratRekomendasiImbs.edit')
<div class="col-sm-6">
    <!-- Kode Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kode', 'Kode:') !!}
        {!! Form::text('kode', null, ['class' => 'form-control','readonly']) !!}
    </div>
</div>
<div class="clearfix"></div>
@endif

<div class="col-sm-6">
    <!-- Nik Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik', 'Nik:') !!}
        {!! Form::text('nik', null, ['class' => 'form-control', 'maxlength' => '16']) !!}
    </div>
    
    <!-- Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama', 'Nama:') !!}
        {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Jenis Kelamin Field -->
    <div class="form-group col-sm-12">
        <label for="jenis_kelamin">
            Jenis Kelamin
        </label>
        @isset($suratRekomendasiImb)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki' || $suratRekomendasiImb->jenis_kelamin == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan' || $suratRekomendasiImb->jenis_kelamin == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endisset

        @empty($suratRekomendasiImb)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endempty
    </div>
    
    <!-- Tempat Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
        {!! Form::text('tempat_lahir', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tgl Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
        {!! Form::date('tgl_lahir', null, ['class' => 'form-control','id'=>'tgl_lahir']) !!}
    </div>
    
    <!-- Kewarganegaraan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
        {!! Form::text('kewarganegaraan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Agama Field -->
    <div class="form-group col-sm-12">
        <label for="agama">Agama</label>
        <select class="form-control" id="agama" name="agama" >
            @isset($suratRekomendasiImb)
                <option value="Islam" {{ (old('agama') == 'Islam') || $suratRekomendasiImb->agama == 'Islam' ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') || $suratRekomendasiImb->agama == 'Protestan' ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') || $suratRekomendasiImb->agama == 'Katolik' ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') || $suratRekomendasiImb->agama == 'Hindu' ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') || $suratRekomendasiImb->agama == 'Buddha' ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') || $suratRekomendasiImb->agama == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
            @endisset

            @empty($suratRekomendasiImb)
                <option value="Islam" {{ (old('agama') == 'Islam') ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
            @endempty
        </select>
    </div>
    
    <!-- Status Perkawinan Field -->
    <div class="form-group col-sm-12">
        <label for="status_perkawinan">
            Status Perkawinan 
        </label>
        @isset($suratRekomendasiImb)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') || $suratRekomendasiImb->status_perkawinan == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') || $suratRekomendasiImb->status_perkawinan == 'Kawin' ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') || $suratRekomendasiImb->status_perkawinan == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') || $suratRekomendasiImb->status_perkawinan == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endisset

        @empty($suratRekomendasiImb)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endempty
    </div>
    
    <!-- Pekerjaan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
        {!! Form::text('pekerjaan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Alamat Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat', 'Alamat:') !!}
        {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Maksud Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('maksud', 'Maksud:') !!}
        {!! Form::text('maksud', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Keperluan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('keperluan', 'Keperluan:') !!}
        {!! Form::text('keperluan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Alamat Bangunan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat_bangunan', 'Alamat Bangunan:') !!}
        {!! Form::text('alamat_bangunan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Luas Bangunan Field -->
    <div class="form-group col-sm-12">
        <label for="luas_bangunan">
            Luas Tanah (Dalam M<sup>2</sup>)
        </label>
        {!! Form::number('luas_bangunan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Luas Tanah Field -->
    <div class="form-group col-sm-12">
        <label for="luas_tanah">
            Luas Tanah (Dalam M<sup>2</sup>)
        </label>
        {!! Form::number('luas_tanah', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Nama Pemilik Tanah Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama_pemilik_tanah', 'Nama Pemilik Tanah:') !!}
        {!! Form::text('nama_pemilik_tanah', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Nomor Sertifikat Tanah Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nomor_sertifikat_tanah', 'Nomor Sertifikat Tanah:') !!}
        {!! Form::text('nomor_sertifikat_tanah', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Bangunan Terbuat Dari Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('bangunan_terbuat_dari', 'Bangunan Terbuat Dari:') !!}
        {!! Form::text('bangunan_terbuat_dari', null, ['class' => 'form-control']) !!}
    </div>
    
    @if(Route::current()->getName() == 'suratRekomendasiImbs.edit')
        <!-- Status Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status', 'Status') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratRekomendasiImb->status->status}}" readonly>
        </div>
    @endif
</div>

<div class="col-sm-12">
@if(Route::current()->getName() == 'suratRekomendasiImbs.edit')
    @if ( $suratRekomendasiImb->status->status == "Draf" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
            {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
            <a href="{!! route('suratRekomendasiImbs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @elseif ( $suratRekomendasiImb->status->status == "Diterima" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{{ route('imb.print', ['id' => $suratRekomendasiImb->id]) }}" class="btn btn-success" target="_blank" id="print">
                Print
            </a>
            <a href="{!! route('suratRekomendasiImbs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('suratRekomendasiImbs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @endif
@else
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Buat Surat Rekomendasi IMB', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('suratRekomendasiImbs.index') !!}" class="btn btn-default">kembali</a>
    </div>
@endif
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection