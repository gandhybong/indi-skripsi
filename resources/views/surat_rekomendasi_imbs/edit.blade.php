@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Rekomendasi Imb
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratRekomendasiImb, ['route' => ['suratRekomendasiImbs.update', $suratRekomendasiImb->id], 'method' => 'patch']) !!}

                        @include('surat_rekomendasi_imbs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection