@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Pernyataan Hibah
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratPernyataanHibah, ['route' => ['suratPernyataanHibahs.update', $suratPernyataanHibah->id], 'method' => 'patch']) !!}

                        @include('surat_pernyataan_hibahs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection