<div class="form-group col-sm-6">
    <!-- Nik Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik', 'NIK:') !!}
        {!! Form::text('nik', null, ['class' => 'form-control', 'maxlength' => '16']) !!}
    </div>
    
    <!-- Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama', 'Nama:') !!}
        {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tempat Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
        {!! Form::text('tempat_lahir', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tgl Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
        {!! Form::date('tgl_lahir', null, ['class' => 'form-control','id'=>'tgl_lahir']) !!}
    </div>
    
    <!-- Kewarganegaraan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
        {!! Form::text('kewarganegaraan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Agama Field -->
    <div class="form-group col-sm-12">
        <label for="agama">Agama</label>
        <select class="form-control" id="agama" name="agama" >
            @isset($suratPernyataanHibah)
                <option value="Islam" {{ (old('agama') == 'Islam') || $suratPernyataanHibah->agama == 'Islam' ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') || $suratPernyataanHibah->agama == 'Protestan' ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') || $suratPernyataanHibah->agama == 'Katolik' ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') || $suratPernyataanHibah->agama == 'Hindu' ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') || $suratPernyataanHibah->agama == 'Buddha' ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') || $suratPernyataanHibah->agama == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
            @endisset
    
            @empty($suratPernyataanHibah)
                <option value="Islam" {{ (old('agama') == 'Islam') ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
            @endempty
        </select>
    </div>
    
    <!-- Pekerjaan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
        {!! Form::text('pekerjaan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Alamat Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat', 'Alamat:') !!}
        {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
    </div>
    <!-- Letak Tanah Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('letak_tanah', 'Letak Tanah:') !!}
        {!! Form::text('letak_tanah', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Lebar Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('lebar', 'Lebar (Meter) :') !!}
        {!! Form::number('lebar', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Panjang Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('panjang', 'Panjang (Meter) :') !!}
        {!! Form::number('panjang', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Dihibahkan Kepada Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('dihibahkan_kepada', 'Dihibahkan Kepada:') !!}
        {!! Form::text('dihibahkan_kepada', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Untuk Dijadikan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('untuk_dijadikan', 'Untuk Dijadikan:') !!}
        {!! Form::text('untuk_dijadikan', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Batas Utara Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('batas_utara', 'Berbatasan dengan Tanah di Utara:') !!}
        {!! Form::text('batas_utara', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Batas Timur Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('batas_timur', 'Berbatasan dengan Tanah di Timur:') !!}
        {!! Form::text('batas_timur', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Batas Selatan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('batas_selatan', 'Berbatasan dengan Tanah di Selatan:') !!}
        {!! Form::text('batas_selatan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Batas Barat Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('batas_barat', 'Berbatasan dengan Tanah di Barat:') !!}
        {!! Form::text('batas_barat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-12">
        {!! Form::label('sejarah_singkat', 'Sejarah Singkat Tanah/Bangunan:') !!}
        <!-- Sejarah Dikuasai Oleh Field -->
        {!! Form::label('sejarah_dikuasai_oleh', 'Dikuasai Pertama Kali Oleh:') !!}
        {!! Form::text('sejarah_dikuasai_oleh', null, ['class' => 'form-control']) !!}
        
        <!-- Sejarah Tahun Dikuasai Field -->
        {!! Form::label('sejarah_tahun_dikuasai', 'Tahun Pertama Kali Dikuasai:') !!}
        {!! Form::text('sejarah_tahun_dikuasai', null, ['class' => 'form-control']) !!}
    
        *Jika Tidak Ada Riwayat Tidak Perlu Diisi
        <div id="items row">
            @isset($suratPernyataanHibah->riwayatKepemilikanHibahs)
                @if ($x = 1) @endif
                    @foreach ($suratPernyataanHibah->riwayatKepemilikanHibahs as $item)
                        <label>Dijual atau Dihibahkan:</label>
                        <input id="alasan" class="form-control" name="alasan[{{$item->id}}]" type="text" value="{{ $item->alasan }}" />
                        <label>Kepada:</label>
                        <input id="kepada_hibah" class="form-control" name="kepada_hibah[{{$item->id}}]" type="text" value="{{ $item->nama }}" />
                        <label>Hubungan Waris:</label>
                        <input id="tahun_hibah" class="form-control" name="tahun_hibah[{{$item->id}}]" type="text" value="{{ $item->tahun }}" />
            
                        <a href='#' class='remove_field'>
                            <i class='fa fa-times'></i>
                        </a>
                    @endforeach
                @if ($x++) @endif
            @endisset
            @empty($suratPernyataanHibah->riwayatKepemilikanHibahs)
                <label>Dijual atau Dihibahkan 1:</label>
                <input id="alasan" class="form-control" name="alasan[]" type="text" />
                <label>Kepada 1:</label>
                <input id="kepada_hibah" class="form-control" name="kepada_hibah[]" type="text" />
                <label>Tahun 1:</label>
                <input id="tahun_hibah" class="form-control" name="tahun_hibah[]" type="text" />
            @endempty
        </div>
        <button type="button" class="add_field_button">Tambahkan Ahli Waris</button>
    </div>
    
    <!-- Jabatan Saksi Pertama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('jabatan_saksi_pertama', 'Jabatan Saksi Pertama:') !!}
        {!! Form::text('jabatan_saksi_pertama', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Nama Saksi Pertama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama_saksi_pertama', 'Nama Saksi Pertama:') !!}
        {!! Form::text('nama_saksi_pertama', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Jabatan Saksi Kedua Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('jabatan_saksi_kedua', 'Jabatan Saksi Kedua:') !!}
        {!! Form::text('jabatan_saksi_kedua', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Nama Saksi Kedua Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama_saksi_kedua', 'Nama Saksi Kedua:') !!}
        {!! Form::text('nama_saksi_kedua', null, ['class' => 'form-control']) !!}
    </div>
    
    @if(Route::current()->getName() == 'suratPernyataanHibahs.edit')
        <!-- Status Id Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status_id', 'Status Id') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratPernyataanHibah->status->status}}" readonly>
        </div>
    @endif
</div>

<div class="form-group col-sm-12">
@if(Route::current()->getName() == 'suratPernyataanHibahs.edit')
    @if ( $suratPernyataanHibah->status->status == "Draf" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
            {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
            <a href="{!! route('suratPernyataanHibahs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @elseif ( $suratPernyataanHibah->status->status == "Diterima" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{{ route('hibah.print', ['id' => $suratPernyataanHibah->id]) }}" class="btn btn-success" target="_blank" id="print">
                Print
            </a>
            <a href="{!! route('suratPernyataanHibahs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('suratPernyataanHibahs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @endif
@else
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Buat Surat Keterangan Ahli Waris', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('suratPernyataanHibahs.index') !!}" class="btn btn-default">kembali</a>
    </div>
@endif
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $(document).ready(function() {
            var max_fields = 20; //maximum input boxes allowed
            var wrapper = $("#items"); //Fields wrapper
            var add_button = $(".add_field_button"); //Add button ID
            
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    console.log(x);
                    $(wrapper).append(
                        "<div class='form-group col-sm-12'>"+
                            "<div class='form-group col-sm-12'>"+
                                "<label>Dijual atau Dihibahkan :</label>"+
                                "<input id='alasan' class='form-control' name='alasan[]' required='required' type='text' />"+
                            "</div>"+
                            "<div class='form-group col-sm-12'>"+
                                "<label>Kepada:</label>"+
                                "<input id='kepada_hibah' class='form-control' name='kepada_hibah[]' required='required' type='text' />"+
                            "</div>"+
                            "<div class='form-group col-sm-12'>"+
                                "<label>Tahun:</label>"+
                                "<input id='tahun_hibah' class='form-control' name='tahun_hibah[]' required='required' type='text' />"+
                            "</div>"+
                            "<a href='#' class='remove_field'>"+
                                "<i class='fa fa-times'></i>"+
                            "</a>"+
                        "</div>"
                    ); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
                e.preventDefault(); $(this).parent('div').remove(); x--;
            });
        });
        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection
