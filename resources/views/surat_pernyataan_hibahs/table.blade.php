@section( 'css' )
    @include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratPernyataanHibahs-table' ).DataTable({

            });
        } );
    </script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratPernyataanHibahs-table">
        <thead>
            <tr>
                <th>Nik</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Letak Tanah</th>
                <th>Lebar</th>
                <th>Panjang</th>
                <th>Batas Utara</th>
                <th>Batas Timur</th>
                <th>Batas Selatan</th>
                <th>Batas Barat</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratPernyataanHibahs as $suratPernyataanHibah)
            <tr>
                <td>{{ $suratPernyataanHibah->nik }}</td>
                <td>{{ $suratPernyataanHibah->nama }}</td>
                <td>{{ $suratPernyataanHibah->alamat }}</td>
                <td>{{ $suratPernyataanHibah->letak_tanah }}</td>
                <td>{{ $suratPernyataanHibah->lebar }}</td>
                <td>{{ $suratPernyataanHibah->panjang }}</td>
                <td>{{ $suratPernyataanHibah->batas_utara }}</td>
                <td>{{ $suratPernyataanHibah->batas_timur }}</td>
                <td>{{ $suratPernyataanHibah->batas_selatan }}</td>
                <td>{{ $suratPernyataanHibah->batas_barat }}</td>
                <td>{{ $suratPernyataanHibah->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratPernyataanHibahs.destroy', $suratPernyataanHibah->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratPernyataanHibahs.edit', [$suratPernyataanHibah->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
