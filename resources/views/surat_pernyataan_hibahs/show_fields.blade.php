<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $suratPernyataanHibah->user_id }}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{{ $suratPernyataanHibah->nik }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $suratPernyataanHibah->nama }}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{{ $suratPernyataanHibah->tempat_lahir }}</p>
</div>

<!-- Tgl Lahir Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
    <p>{{ $suratPernyataanHibah->tgl_lahir }}</p>
</div>

<!-- Kewarganegaraan Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
    <p>{{ $suratPernyataanHibah->kewarganegaraan }}</p>
</div>

<!-- Agama Field -->
<div class="form-group">
    {!! Form::label('agama', 'Agama:') !!}
    <p>{{ $suratPernyataanHibah->agama }}</p>
</div>

<!-- Pekerjaan Field -->
<div class="form-group">
    {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
    <p>{{ $suratPernyataanHibah->pekerjaan }}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $suratPernyataanHibah->alamat }}</p>
</div>

<!-- Letak Tanah Field -->
<div class="form-group">
    {!! Form::label('Letak_tanah', 'Letak Tanah:') !!}
    <p>{{ $suratPernyataanHibah->letak_tanah }}</p>
</div>

<!-- Lebar Field -->
<div class="form-group">
    {!! Form::label('lebar', 'Lebar:') !!}
    <p>{{ $suratPernyataanHibah->lebar }}</p>
</div>

<!-- Panjang Field -->
<div class="form-group">
    {!! Form::label('panjang', 'Panjang:') !!}
    <p>{{ $suratPernyataanHibah->panjang }}</p>
</div>

<!-- Batas Utara Field -->
<div class="form-group">
    {!! Form::label('batas_utara', 'Batas Utara:') !!}
    <p>{{ $suratPernyataanHibah->batas_utara }}</p>
</div>

<!-- Batas Timur Field -->
<div class="form-group">
    {!! Form::label('batas_timur', 'Batas Timur:') !!}
    <p>{{ $suratPernyataanHibah->batas_timur }}</p>
</div>

<!-- Batas Selatan Field -->
<div class="form-group">
    {!! Form::label('batas_selatan', 'Batas Selatan:') !!}
    <p>{{ $suratPernyataanHibah->batas_selatan }}</p>
</div>

<!-- Batas Barat Field -->
<div class="form-group">
    {!! Form::label('batas_barat', 'Batas Barat:') !!}
    <p>{{ $suratPernyataanHibah->batas_barat }}</p>
</div>

<!-- Sejarah Dikuasai Oleh Field -->
<div class="form-group">
    {!! Form::label('sejarah_dikuasai_oleh', 'Sejarah Dikuasai Oleh:') !!}
    <p>{{ $suratPernyataanHibah->sejarah_dikuasai_oleh }}</p>
</div>

<!-- Sejarah Tahun Dikuasai Field -->
<div class="form-group">
    {!! Form::label('sejarah_tahun_dikuasai', 'Sejarah Tahun Dikuasai:') !!}
    <p>{{ $suratPernyataanHibah->sejarah_tahun_dikuasai }}</p>
</div>

<!-- Sejarah Dihibahkan Kepada Field -->
<div class="form-group">
    {!! Form::label('sejarah_dihibahkan_kepada', 'Sejarah Dihibahkan Kepada:') !!}
    <p>{{ $suratPernyataanHibah->sejarah_dihibahkan_kepada }}</p>
</div>

<!-- Sejarah Tahun Dihibahkan Field -->
<div class="form-group">
    {!! Form::label('sejarah_tahun_dihibahkan', 'Sejarah Tahun Dihibahkan:') !!}
    <p>{{ $suratPernyataanHibah->sejarah_tahun_dihibahkan }}</p>
</div>

<!-- Dihibahkan Kepada Field -->
<div class="form-group">
    {!! Form::label('dihibahkan_kepada', 'Dihibahkan Kepada:') !!}
    <p>{{ $suratPernyataanHibah->dihibahkan_kepada }}</p>
</div>

<!-- Untuk Dijadikan Field -->
<div class="form-group">
    {!! Form::label('untuk_dijadikan', 'Untuk Dijadikan:') !!}
    <p>{{ $suratPernyataanHibah->untuk_dijadikan }}</p>
</div>

<!-- Jabatan Saksi Pertama Field -->
<div class="form-group">
    {!! Form::label('jabatan_saksi_pertama', 'Jabatan Saksi Pertama:') !!}
    <p>{{ $suratPernyataanHibah->jabatan_saksi_pertama }}</p>
</div>

<!-- Nama Saksi Pertama Field -->
<div class="form-group">
    {!! Form::label('nama_saksi_pertama', 'Nama Saksi Pertama:') !!}
    <p>{{ $suratPernyataanHibah->nama_saksi_pertama }}</p>
</div>

<!-- Jabatan Saksi Kedua Field -->
<div class="form-group">
    {!! Form::label('jabatan_saksi_kedua', 'Jabatan Saksi Kedua:') !!}
    <p>{{ $suratPernyataanHibah->jabatan_saksi_kedua }}</p>
</div>

<!-- Nama Saksi Kedua Field -->
<div class="form-group">
    {!! Form::label('nama_saksi_kedua', 'Nama Saksi Kedua:') !!}
    <p>{{ $suratPernyataanHibah->nama_saksi_kedua }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratPernyataanHibah->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratPernyataanHibah->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratPernyataanHibah->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratPernyataanHibah->nomor_surat }}</p>
</div>

