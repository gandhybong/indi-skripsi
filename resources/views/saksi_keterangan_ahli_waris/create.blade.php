@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Saksi Keterangan Ahli Waris
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'saksiKeteranganAhliWaris.store']) !!}

                        @include('saksi_keterangan_ahli_waris.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
