<div class="table-responsive">
    <table class="table" id="saksiKeteranganAhliWaris-table">
        <thead>
            <tr>
                <th>Ahli Waris Id</th>
        <th>Nama</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($saksiKeteranganAhliWaris as $saksiKeteranganAhliWaris)
            <tr>
                <td>{{ $saksiKeteranganAhliWaris->ahli_waris_id }}</td>
            <td>{{ $saksiKeteranganAhliWaris->nama }}</td>
                <td>
                    {!! Form::open(['route' => ['saksiKeteranganAhliWaris.destroy', $saksiKeteranganAhliWaris->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('saksiKeteranganAhliWaris.show', [$saksiKeteranganAhliWaris->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('saksiKeteranganAhliWaris.edit', [$saksiKeteranganAhliWaris->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
