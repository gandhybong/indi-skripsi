@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Domisili
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('surat_keterangan_domisilis.show_fields')
                    <a href="{!! route('suratKeteranganDomisilis.index') !!}" class="btn btn-default">Back</a>
                    @if ($suratKeteranganDomisili->status->status == "Diterima")
                        <a href="{!! route('domisili.print',['id'=>$suratKeteranganDomisili->id]) !!}" class="btn btn-default">Print</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
