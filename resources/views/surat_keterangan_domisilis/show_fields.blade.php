<!-- Kode Field -->
<div class="form-group">
    {!! Form::label('kode', 'Kode:') !!}
    <p>{!! $suratKeteranganDomisili->kode !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $suratKeteranganDomisili->nama !!}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{!! $suratKeteranganDomisili->nik !!}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $suratKeteranganDomisili->alamat !!}</p>
</div>

<!-- Alamat Domisili Field -->
<div class="form-group">
    {!! Form::label('alamat_domisili', 'Alamat Domisili:') !!}
    <p>{!! $suratKeteranganDomisili->alamat_domisili !!}</p>
</div>

<!-- Dusun Field -->
<div class="form-group">
    {!! Form::label('Dusun', 'Dusun:') !!}
    <p>{!! $suratKeteranganDomisili->Dusun !!}</p>
</div>

<!-- Desa Field -->
<div class="form-group">
    {!! Form::label('Desa', 'Desa:') !!}
    <p>{!! $suratKeteranganDomisili->Desa !!}</p>
</div>

<!-- Kecamatan Field -->
<div class="form-group">
    {!! Form::label('Kecamatan', 'Kecamatan:') !!}
    <p>{!! $suratKeteranganDomisili->Kecamatan !!}</p>
</div>

<!-- Kabupaten Field -->
<div class="form-group">
    {!! Form::label('Kabupaten', 'Kabupaten:') !!}
    <p>{!! $suratKeteranganDomisili->Kabupaten !!}</p>
</div>

<!-- Maksud Field -->
<div class="form-group">
    {!! Form::label('Maksud', 'Maksud:') !!}
    <p>{!! $suratKeteranganDomisili->Maksud !!}</p>
</div>

<!-- Ketua Rt Field -->
<div class="form-group">
    {!! Form::label('ketua_rt', 'Ketua Rt:') !!}
    <p>{!! $suratKeteranganDomisili->ketua_rt !!}</p>
</div>

<!-- Kepala Dusun Field -->
<div class="form-group">
    {!! Form::label('kepala_dusun', 'Kepala Dusun:') !!}
    <p>{!! $suratKeteranganDomisili->kepala_dusun !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $suratKeteranganDomisili->status->status !!}</p>
</div>
