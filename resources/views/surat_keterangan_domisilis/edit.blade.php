@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Domisili
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganDomisili, ['route' => ['suratKeteranganDomisilis.update', $suratKeteranganDomisili->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_domisilis.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection