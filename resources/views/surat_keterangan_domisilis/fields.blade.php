
<div class="col-sm-6">
    @if(Route::current()->getName() == 'suratKeteranganDomisilis.edit')
        <!-- Kode Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('kode', 'Kode') !!}
            {!! Form::text('kode', null, ['class' => 'form-control','readonly']) !!}
        </div>
    @endif
    <!-- Nik Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik', 'Nomor Induk Kependudukan (NIK)') !!}
        {!! Form::text('nik', null, ['class' => 'form-control', 'maxlength' => '16']) !!}
    </div>
    
    <!-- Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama', 'Nama (Sesuai KTP)') !!}
        {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Alamat Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat', 'Alamat') !!}
        {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Alamat Domisili Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat_domisili', 'Alamat Domisili') !!}
        {!! Form::textarea('alamat_domisili', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Dusun Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('Dusun', 'Dusun') !!}
        {!! Form::text('Dusun', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Desa Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('Desa', 'Desa') !!}
        {!! Form::text('Desa', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Kecamatan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('Kecamatan', 'Kecamatan') !!}
        {!! Form::text('Kecamatan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Kabupaten Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('Kabupaten', 'Kabupaten') !!}
        {!! Form::text('Kabupaten', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="clearfix"></div>

<div class="col-sm-6">
    <!-- Maksud Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('Maksud', 'Tujuan Pembuatan Surat') !!}
        {!! Form::text('Maksud', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Ketua Rt Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('ketua_rt', 'Nama Ketua Rt') !!}
        {!! Form::text('ketua_rt', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Kepala Dusun Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kepala_dusun', 'Nama Kepala Dusun') !!}
        {!! Form::text('kepala_dusun', null, ['class' => 'form-control']) !!}
    </div>
    
    @if(Route::current()->getName() == 'suratKeteranganDomisilis.edit')
        <!-- Status Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status', 'Status') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganDomisili->status->status}}" readonly>
        </div>
    @endif
</div>

<div class="clearfix"></div>
<div class="col-sm-12">

@if(Route::current()->getName() == 'suratKeteranganDomisilis.edit')
    @if ( $suratKeteranganDomisili->status->status == "Draf" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
            {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
            <a href="{!! route('suratKeteranganDomisilis.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @elseif ( $suratKeteranganDomisili->status->status == "Diterima" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{{ route('domisili.print', ['id' => $suratKeteranganDomisili->id]) }}" class="btn btn-success" target="_blank" id="print">
                Print
            </a>
            <a href="{!! route('suratKeteranganDomisilis.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('suratKeteranganDomisilis.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @endif
@else
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Buat Surat Keterangan Domisili', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('suratKeteranganDomisilis.index') !!}" class="btn btn-default">Kembali</a>
    </div>
@endif
</div>

@section('scripts')
    <script type="text/javascript">
        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection
