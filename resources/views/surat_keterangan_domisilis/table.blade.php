@section( 'css' )
    @include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratKeteranganDomisilis-table' ).DataTable({

            });
        } );
    </script>
@endsection

<div class="table-responsive">
    <table class="table" id="suratKeteranganDomisilis-table">
        <thead>
            <tr>
                <th>Kode</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Alamat Domisili</th>
                <th>Dusun</th>
                <th>Desa</th>
                <th>Kecamatan</th>
                <th>Kabupaten</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganDomisilis as $suratKeteranganDomisili)
            <tr>
                <td>{!! $suratKeteranganDomisili->kode !!}</td>
                <td>{!! $suratKeteranganDomisili->nik !!}</td>
                <td>{!! $suratKeteranganDomisili->nama !!}</td>
                <td>{!! $suratKeteranganDomisili->alamat !!}</td>
                <td>{!! $suratKeteranganDomisili->alamat_domisili !!}</td>
                <td>{!! $suratKeteranganDomisili->Dusun !!}</td>
                <td>{!! $suratKeteranganDomisili->Desa !!}</td>
                <td>{!! $suratKeteranganDomisili->Kecamatan !!}</td>
                <td>{!! $suratKeteranganDomisili->Kabupaten !!}</td>
                <td>{!! $suratKeteranganDomisili->status->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganDomisilis.destroy', $suratKeteranganDomisili->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('suratKeteranganDomisilis.edit', [$suratKeteranganDomisili->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
