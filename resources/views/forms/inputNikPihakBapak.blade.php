<div class="form-group">
    <label>
        Nomor Induk Kependudukan (NIK) Pihak Bapak 
    </label>
    <input class="form-control" type="text" name="nik_pihak_bapak" id="nik_pihak_bapak" value="{{ old('nik_pihak_bapak') }}" autocomplete="off" >
    @error('nik_pihak_bapak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>