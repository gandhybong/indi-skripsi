<div class="form-group">
    <label>
        Tanggal Lahir Pihak Pertama
    </label>
    <input class="form-control" type="date" name="tgl_lahir_pihak_pertama" id="tgl_lahir_pihak_pertama" value="{{ old('tgl_lahir_pihak_pertama') }}">
    @error('tgl_lahir_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>