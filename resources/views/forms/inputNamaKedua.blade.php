<div class="form-group">
    <label>
        Nama Kedua
    </label>
    <input class="form-control" type="text" name="nama_kedua" id="nama_kedua" value="{{ old('nama_kedua') }}" autocomplete="off">
    @error('nama_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>