<div class="form-group">
    <label>
        Tempat Meninggal
    </label>
    <input class="form-control" type="text" name="tempat_meninggal" id="tempat_meninggal" value="{{ old('tempat_meninggal') }}" autocomplete="off">
    @error('tempat_meninggal')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>