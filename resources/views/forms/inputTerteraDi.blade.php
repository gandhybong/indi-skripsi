<div class="form-group">
    <label>
        Tertera Di
    </label>
    <input class="form-control" type="text" name="tertera_di" id="tertera_di" value="{{ old('tertera_di') }}" autocomplete="off">
    @error('tertera_di')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>