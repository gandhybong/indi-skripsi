<div class="form-group">
    <label for="nomor_sertifikat_tanah">
        Nomor Sertifikat Tanah
    </label>
    <input class="form-control" name="nomor_sertifikat_tanah" type="text" id="nomor_sertifikat_tanah" value="{{ old('nomor_sertifikat_tanah') }}" autocomplete="off">
    @error('nomor_sertifikat_tanah')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>