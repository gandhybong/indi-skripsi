<div class="form-group">
    <label>
        Bin / Binti
    </label>
    <input class="form-control" type="text" name="bin_binti" id="bin_binti" value="{{ old('bin_binti') }}" autocomplete="off">
    @error('bin_binti')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>