<div class="form-group">
    <label for="dusun">
        Dusun 
    </label>
    <input class="form-control" type="text" name="dusun" id="dusun" value="{{ old('dusun') }}" autocomplete="off">
    @error('dusun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>