<div class="form-group">
    <label for="luas_tanah">
        Luas Tanah (Dalam M<sup>2</sup>)
    </label>
    <input class="form-control" name="luas_tanah" type="number" id="luas_tanah" value="{{ old('luas_tanah') }}" autocomplete="off">
    @error('luas_tanah')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>