{!! Form::label('sejarah_singkat', 'Sejarah Singkat Tanah/Bangunan:') !!}
<div class="form-group col-sm-12">
    <!-- Sejarah Dikuasai Oleh Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('sejarah_dikuasai_oleh', 'Dikuasai Pertama Kali Oleh:') !!}
        {!! Form::text('sejarah_dikuasai_oleh', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Sejarah Tahun Dikuasai Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('sejarah_tahun_dikuasai', 'Tahun Pertama Kali Dikuasai:') !!}
        {!! Form::text('sejarah_tahun_dikuasai', null, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group col-sm-12">
        *Jika Tidak Ada Riwayat Tidak Perlu Diisi
        <div id="items">
            <div class="form-group col-sm-12">
                <div class="form-group col-sm-12">
                    <label>Dijual atau Dihibahkan 1:</label>
                    <input id="alasan" class="form-control" name="alasan[]" type="text" value={{ old('alasan.0') }}>
                </div>
                <div class="form-group col-sm-12">
                    <label>Kepada 1:</label>
                    <input id="kepada_hibah" class="form-control" name="kepada_hibah[]" type="text" value={{ old('kepada_hibah.0') }}>
                </div>
                <div class="form-group col-sm-12">
                    <label>Tahun 1:</label>
                    <input id="tahun_hibah" class="form-control" name="tahun_hibah[]" type="text" value={{ old('tahun_hibah.0') }}>
                </div>
            </div>
        </div>
        <button type="button" class="add_field_button">Tambahkan Ahli Waris</button>
    </div>
</div>