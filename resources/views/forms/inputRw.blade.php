<div class="form-group col-sm-6">
    <label>
        RW
    </label>
    <input class="form-control" type="text" name="rw" id="rw" value="{{ old('rw') }}" >
    @error('rw')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>