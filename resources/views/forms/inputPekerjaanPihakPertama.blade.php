<div class="form-group">
    <label>
        Pekerjaan Pihak Pertama
    </label>
    <input class="form-control" type="text" name="pekerjaan_pihak_pertama" id="pekerjaan_pihak_pertama" value="{{ old('pekerjaan_pihak_pertama') }}" >
    @error('pekerjaan_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>