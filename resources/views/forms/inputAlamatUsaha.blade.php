<div class="form-group">
    <label>
        Alamat Usaha
    </label>
    <textarea class="form-control" name="alamat_usaha" id="alamat_usaha" rows="3">{{ old('alamat_usaha') }}</textarea>
    @error('alamat_usaha')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>