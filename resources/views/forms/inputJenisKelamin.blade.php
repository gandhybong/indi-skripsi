<div class="form-group">
    <label>
        Jenis Kelamin 
    </label>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
            {{ (old('jenis_kelamin') == 'Laki-Laki') ? "checked" : "" }}
            >Laki-Laki
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
            {{ (old('jenis_kelamin') == 'Perempuan') ? "checked" : "" }}
            >Perempuan
        </label>
    </div>
    @error('jenis_kelamin')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>