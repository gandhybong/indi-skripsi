<div class="form-group">
    <label>
        Tanggal Meninggal
    </label>
    <input class="form-control" type="date" name="tgl_meninggal" id="tgl_meninggal" value="{{ old('tgl_meninggal') }}" autocomplete="off">
    @error('tgl_meninggal')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>