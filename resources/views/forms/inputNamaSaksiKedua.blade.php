<div class="form-group">
    <label>
        Nama Saksi Kedua
    </label>
    <input class="form-control" type="text" name="nama_saksi_kedua" id="nama_saksi_kedua" value={{ old('nama_saksi_kedua') }}>
    @error('nama_saksi_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>