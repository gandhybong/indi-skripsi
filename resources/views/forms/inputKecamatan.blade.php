<div class="form-group">
    <label for="kecamatan">
        Kecamatan 
    </label>
    <input class="form-control" type="text" name="kecamatan" id="kecamatan" value="{{ old('kecamatan') }}" autocomplete="off">
    @error('kecamatan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>