<div class="form-group">
    <label for="umur_pihak_pertama">
        Umur Pihak Pertama
    </label>
    <input class="form-control" name="umur_pihak_pertama" type="number" id="umur_pihak_pertama" value="{{ old('umur_pihak_pertama') }}" autocomplete="off">
    @error('umur_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>