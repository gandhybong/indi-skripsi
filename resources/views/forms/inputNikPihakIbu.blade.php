<div class="form-group">
    <label>
        Nomor Induk Kependudukan (NIK) Pihak Ibu 
    </label>
    <input class="form-control" type="text" name="nik_pihak_ibu" id="nik_pihak_ibu" value="{{ old('nik_pihak_ibu') }}" autocomplete="off" >
    @error('nik_pihak_ibu')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>