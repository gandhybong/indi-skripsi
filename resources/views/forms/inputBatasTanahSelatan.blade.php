<div class="form-group">
    <label>
        Batas Tanah Selatan
    </label>
    <input class="form-control" type="text" name="batas_selatan" id="batas_selatan" value={{ old('batas_selatan') }}>
    @error('batas_selatan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>