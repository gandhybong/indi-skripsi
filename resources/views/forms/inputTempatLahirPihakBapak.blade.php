<div class="form-group">
    <label>
        Tempat Lahir Pihak Bapak
    </label>
    <input class="form-control" type="text" name="tempat_lahir_pihak_bapak" id="tempat_lahir_pihak_bapak" value="{{ old('tempat_lahir_pihak_bapak') }}">
    @error('tempat_lahir_pihak_bapak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>