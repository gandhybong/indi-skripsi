<div class="form-group">
    <label for="agama_pihak_bapak">Agama Pihak Bapak</label>
    <select class="form-control" id="agama_pihak_bapak" name="agama_pihak_bapak">
        <option value="Islam" {{ (old('agama_pihak_bapak') == 'Islam') ? "checked" : "" }}>Islam</option>
        <option value="Protestan" {{ (old('agama_pihak_bapak') == 'Protestan') ? "checked" : "" }}>Protestan</option>
        <option value="Katolik" {{ (old('agama_pihak_bapak') == 'Katolik') ? "checked" : "" }}>Katolik</option>
        <option value="Hindu" {{ (old('agama_pihak_bapak') == 'Hindu') ? "checked" : "" }}>Hindu</option>
        <option value="Buddha" {{ (old('agama_pihak_bapak') == 'Buddha') ? "checked" : "" }}>Buddha</option>
        <option value="Khonghucu" {{ (old('agama_pihak_bapak') == 'Khonghucu') ? "checked" : "" }}>Khonghucu</option>
    </select>
    @error('agama_pihak_bapak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>