<div class="form-group">
    <label>
        Alamat (Sesuai KTP)
    </label>
    <textarea class="form-control" name="alamat" id="alamat" rows="3">{{ old('alamat') }}</textarea>
    @error('alamat')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>