<div class="form-group">
    <label>
        Tanggal Lahir 
    </label>
    <input class="form-control" type="date" name="tgl_lahir" id="tgl_lahir" value="{{ old('tgl_lahir') }}">
    @error('tgl_lahir')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>