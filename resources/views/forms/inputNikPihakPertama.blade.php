<div class="form-group">
    <label>
        Nomor Induk Kependudukan (NIK) Pihak Pertama
    </label>
    <input class="form-control" type="text" name="nik_pihak_pertama" id="nik_pihak_pertama" value="{{ old('nik_pihak_pertama') }}" autocomplete="off" >
    @error('nik_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>