<div class="form-group">
    <label>
        Letak tanah 
    </label>
    <input class="form-control" type="text" name="letak_tanah" id="letak_tanah" value={{ old('letak_tanah') }}>
    @error('letak_tanah')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>