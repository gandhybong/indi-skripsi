<div class="form-group">
    <label for="agama">Agama</label>
    <select class="form-control" id="agama" name="agama">
        <option value="Islam" {{ (old('agama') == 'Islam') ? "checked" : "" }}>Islam</option>
        <option value="Protestan" {{ (old('agama') == 'Protestan') ? "checked" : "" }}>Protestan</option>
        <option value="Katolik" {{ (old('agama') == 'Katolik') ? "checked" : "" }}>Katolik</option>
        <option value="Hindu" {{ (old('agama') == 'Hindu') ? "checked" : "" }}>Hindu</option>
        <option value="Buddha" {{ (old('agama') == 'Buddha') ? "checked" : "" }}>Buddha</option>
        <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') ? "checked" : "" }}>Khonghucu</option>
    </select>
    @error('agama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>