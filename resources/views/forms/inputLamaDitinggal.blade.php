<div class="form-group">
    <label for="lama_ditinggal">
        Lama Ditinggal
    </label>
    <input class="form-control" name="lama_ditinggal" type="number" id="lama_ditinggal" value="{{ old('lama_ditinggal') }}" autocomplete="off">
    @error('lama_ditinggal')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>