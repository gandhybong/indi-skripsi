<div class="form-group">
    <label>
        Alamat Acara 
    </label>
    <textarea class="form-control" name="alamat_acara" id="alamat_acara" rows="3">{{ old('alamat_acara') }}</textarea>
    @error('alamat_acara')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>