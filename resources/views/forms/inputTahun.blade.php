<div class="form-group">
    <label>
        Tahun
    </label>
    <input class="form-control" type="text" name="tahun" id="tahun" value="{{ old('tahun') }}" autocomplete="off">
    @error('tahun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>