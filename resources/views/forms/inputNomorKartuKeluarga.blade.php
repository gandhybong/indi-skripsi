<div class="form-group">
    <label>
        Nomor Kartu Keluarga (KK) 
    </label>
    <input class="form-control" type="text" name="nomor_kartu_keluarga" id="nomor_kartu_keluarga" value="{{ old('nomor_kartu_keluarga') }}" autocomplete="off" >
    @error('nomor_kartu_keluarga')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>