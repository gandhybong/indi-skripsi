<div class="form-group">
    <label>
        Tahun Berdiri Usaha
    </label>
    <input class="form-control" type="text" name="tahun_berdiri_usaha" id="tahun_berdiri_usaha" value="{{ old('tahun_berdiri_usaha') }}" autocomplete="off">
    @error('tahun_berdiri_usaha')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>