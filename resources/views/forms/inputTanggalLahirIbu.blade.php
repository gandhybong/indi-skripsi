<div class="form-group">
    <label>
        Tanggal Lahir Pihak Ibu
    </label>
    <input class="form-control" type="date" name="tgl_lahir_pihak_ibu" id="tgl_lahir_pihak_ibu" value="{{ old('tgl_lahir_pihak_ibu') }}">
    @error('tgl_lahir_pihak_ibu')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>