<div class="form-group">
    <label>
        Kebutuhan Administrasi
    </label>
    <input class="form-control" type="text" name="administrasi" id="administrasi" value="{{ old('administrasi') }}" autocomplete="off">
    @error('administrasi')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>