<div class="form-group">
    <label>
        Tanggal Mulai 
    </label>
    <input class="form-control" type="date" name="tgl_mulai" id="tgl_mulai" value="{{ old('tgl_mulai') }}">
    @error('tgl_mulai')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>