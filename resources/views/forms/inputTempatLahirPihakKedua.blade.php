<div class="form-group">
    <label>
        Tempat Lahir Pihak Kedua
    </label>
    <input class="form-control" type="text" name="tempat_lahir_pihak_kedua" id="tempat_lahir_pihak_kedua" value="{{ old('tempat_lahir_pihak_kedua') }}">
    @error('tempat_lahir_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>