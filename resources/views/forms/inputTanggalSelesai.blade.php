<div class="form-group">
    <label>
        Tanggal Selesai 
    </label>
    <input class="form-control" type="date" name="tgl_selesai" id="tgl_selesai" value="{{ old('tgl_selesai') }}">
    @error('tgl_selesai')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>