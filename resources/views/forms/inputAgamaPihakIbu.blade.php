<div class="form-group">
    <label for="agama_pihak_ibu">Agama Pihak Ibu</label>
    <select class="form-control" id="agama_pihak_ibu" name="agama_pihak_ibu">
        <option value="Islam" {{ (old('agama_pihak_ibu') == 'Islam') ? "checked" : "" }}>Islam</option>
        <option value="Protestan" {{ (old('agama_pihak_ibu') == 'Protestan') ? "checked" : "" }}>Protestan</option>
        <option value="Katolik" {{ (old('agama_pihak_ibu') == 'Katolik') ? "checked" : "" }}>Katolik</option>
        <option value="Hindu" {{ (old('agama_pihak_ibu') == 'Hindu') ? "checked" : "" }}>Hindu</option>
        <option value="Buddha" {{ (old('agama_pihak_ibu') == 'Buddha') ? "checked" : "" }}>Buddha</option>
        <option value="Khonghucu" {{ (old('agama_pihak_ibu') == 'Khonghucu') ? "checked" : "" }}>Khonghucu</option>
    </select>
    @error('agama_pihak_ibu')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>