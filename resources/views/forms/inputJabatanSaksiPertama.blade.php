<div class="form-group">
    <label>
        Jabatan Saksi Pertama
    </label>
    <input class="form-control" type="text" name="jabatan_saksi_pertama" id="jabatan_saksi_pertama" value={{ old('jabatan_saksi_pertama') }}>
    @error('jabatan_saksi_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>