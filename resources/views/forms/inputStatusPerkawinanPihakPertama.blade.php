<div class="form-group">
    <label for="status_perkawinan_pihak_pertama">
        Status Perkawinan Pihak Pertama
    </label>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Belum Kawin" {{ (old('status_perkawinan_pihak_pertama') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Kawin" {{ (old('status_perkawinan_pihak_pertama') == 'Kawin') ? "checked" : "" }}>Kawin
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Cerai Hidup" {{ (old('status_perkawinan_pihak_pertama') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Cerai Mati" {{ (old('status_perkawinan_pihak_pertama') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
        </label>
    </div>
    @error('status_perkawinan_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>