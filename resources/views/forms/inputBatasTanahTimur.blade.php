<div class="form-group">
    <label>
        Batas Tanah Timur
    </label>
    <input class="form-control" type="text" name="batas_timur" id="batas_timur" value={{ old('batas_timur') }}>
    @error('batas_timur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>