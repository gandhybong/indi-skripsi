<div class="form-group">
    <label for="status_perkawinan_pihak_kedua">
        Status Perkawinan Pihak Kedua
    </label>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Belum Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Kawin') ? "checked" : "" }}>Kawin
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Hidup" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Mati" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
        </label>
    </div>
    @error('status_perkawinan_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>