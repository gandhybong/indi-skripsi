<div class="form-group">
    <label>
        Batas Tanah Barat
    </label>
    <input class="form-control" type="text" name="batas_barat" id="batas_barat" value={{ old('batas_barat') }}>
    @error('batas_barat')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>