<div class="form-group">
    <label>
        Pekerjaan Pihak Ibu
    </label>
    <input class="form-control" type="text" name="pekerjaan_pihak_ibu" id="pekerjaan_pihak_ibu" value="{{ old('pekerjaan_pihak_ibu') }}" >
    @error('pekerjaan_pihak_ibu')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>