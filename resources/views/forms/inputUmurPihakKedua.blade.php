<div class="form-group">
    <label for="umur_pihak_kedua">
        Umur Pihak Kedua
    </label>
    <input class="form-control" name="umur_pihak_kedua" type="number" id="umur_pihak_kedua" value="{{ old('umur_pihak_kedua') }}" autocomplete="off">
    @error('umur_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>