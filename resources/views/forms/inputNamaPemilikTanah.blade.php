<div class="form-group">
    <label for="nama_pemilik_tanah">
        Nama Pemilik Tanah
    </label>
    <input class="form-control" name="nama_pemilik_tanah" type="text" id="nama_pemilik_tanah" value="{{ old('nama_pemilik_tanah') }}" autocomplete="off">
    @error('nama_pemilik_tanah')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>