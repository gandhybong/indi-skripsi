<div class="form-group">
    <label>
        Untuk Dijadikan
    </label>
    <input class="form-control" type="text" name="untuk_dijadikan" id="untuk_dijadikan" value={{ old('untuk_dijadikan') }}>
    @error('untuk_dijadikan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>