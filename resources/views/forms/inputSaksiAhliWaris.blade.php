<div class="form-group">
    <div class="form-group col-sm-6">
        <label>
            Saksi Pertama 
        </label>
        <input class="form-control" type="text" name="saksi_pertama" id="saksi_pertama" value="{{ old('saksi_pertama') }}" autocomplete="off">
        @error('saksi_pertama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group col-sm-6">
        <label>
            Saksi Kedua 
        </label>
        <input class="form-control" type="text" name="saksi_kedua" id="saksi_kedua" value="{{ old('saksi_kedua') }}" autocomplete="off">
        @error('saksi_kedua')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>