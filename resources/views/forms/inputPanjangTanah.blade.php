<div class="form-group">
    <label for="panjang">
        Panjang Tanah (Dalam M)
    </label>
    <input class="form-control" name="panjang" type="number" id="panjang" value="{{ old('panjang') }}" autocomplete="off">
    @error('panjang')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>