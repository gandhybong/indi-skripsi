<div class="form-group">
    <label for="desa">
        Desa 
    </label>
    <input class="form-control" type="text" name="desa" id="desa" value="{{ old('desa') }}" autocomplete="off">
    @error('desa')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>