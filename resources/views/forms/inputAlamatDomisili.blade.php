<div class="form-group">
    <label for="alamat_domisili">
        Alamat Domisili 
    </label>
    <textarea class="form-control" name="alamat_domisili" id="alamat_domisili" rows="3">{{ old('alamat_domisili') }}</textarea>
    @error('alamat_domisili')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>