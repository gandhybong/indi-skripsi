<div class="form-group">
    <label>
        Pekerjaan Pihak Bapak
    </label>
    <input class="form-control" type="text" name="pekerjaan_pihak_bapak" id="pekerjaan_pihak_bapak" value="{{ old('pekerjaan_pihak_bapak') }}" >
    @error('pekerjaan_pihak_bapak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>