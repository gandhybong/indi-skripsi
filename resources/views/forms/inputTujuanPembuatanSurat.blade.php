<div class="form-group">
    <label>
        Tujuan Pembuatan Surat 
    </label>
    <input class="form-control" type="text" name="maksud" id="maksud" value="{{ old('maksud') }}" >
    @error('maksud')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>