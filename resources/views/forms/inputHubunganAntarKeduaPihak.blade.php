<div class="form-group">
    <label>
        Hubungan Antara Kedua Pihak
    </label>
    <input class="form-control" type="text" name="hubungan_antara_kedua_pihak" id="hubungan_antara_kedua_pihak" value="{{ old('hubungan_antara_kedua_pihak') }}" autocomplete="off">
    @error('hubungan_antara_kedua_pihak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>