<div class="form-group">
    <label>
        Nama Lengkap Pihak Pertama (Sesuai KTP)
    </label>
    <input class="form-control" type="text" name="nama_pihak_pertama" id="nama_pihak_pertama" value="{{ old('nama_pihak_pertama') }}" autocomplete="off">
    @error('nama_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>