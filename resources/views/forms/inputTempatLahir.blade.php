<div class="form-group">
    <label>
        Tempat Lahir 
    </label>
    <input class="form-control" type="text" name="tempat_lahir" id="tempat_lahir" value="{{ old('tempat_lahir') }}">
    @error('tempat_lahir')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>