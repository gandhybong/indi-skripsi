<div class="form-group">
    <label>
        Jabatan Saksi Kedua
    </label>
    <input class="form-control" type="text" name="jabatan_saksi_kedua" id="jabatan_saksi_kedua" value={{ old('jabatan_saksi_kedua') }}>
    @error('jabatan_saksi_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>