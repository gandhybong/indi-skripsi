<div class="form-group">
    <label>
        Tanggal Lahir Pihak Kedua
    </label>
    <input class="form-control" type="date" name="tgl_lahir_pihak_kedua" id="tgl_lahir_pihak_kedua" value="{{ old('tgl_lahir_pihak_kedua') }}">
    @error('tgl_lahir_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>