<div class="form-group">
    <label>
        Kewarganegaraan 
    </label>
    <input class="form-control" type="text" name="kewarganegaraan" id="kewarganegaraan" value="Indonesia" readonly>
    @error('kewarganegaraan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>