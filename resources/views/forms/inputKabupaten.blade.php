<div class="form-group">
    <label for="kabupaten">
        Kabupaten 
    </label>
    <input class="form-control" type="text" name="kabupaten" id="kabupaten" value="{{ old('kabupaten') }}" autocomplete="off">
    @error('kabupaten')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>