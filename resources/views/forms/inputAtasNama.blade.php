<div class="form-group">
    <label>
        Atas Nama
    </label>
    <input class="form-control" type="text" name="atas_nama" id="atas_nama" value="{{ old('atas_nama') }}" autocomplete="off">
    @error('atas_nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>