<div class="form-group">
    <label>
        Kewarganegaraan Pihak Ibu 
    </label>
    <input class="form-control" type="text" name="kewarganegaraan_pihak_ibu" id="kewarganegaraan_pihak_ibu" value="Indonesia" readonly>
    @error('kewarganegaraan_pihak_ibu')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>