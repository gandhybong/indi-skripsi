<div class="form-group">
    <label>
        Nama Lengkap Pihak Kedua (Sesuai KTP)
    </label>
    <input class="form-control" type="text" name="nama_pihak_kedua" id="nama_pihak_kedua" value="{{ old('nama_pihak_kedua') }}" autocomplete="off">
    @error('nama_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>