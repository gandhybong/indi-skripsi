<div class="form-group">
    <label>
        Nama Lengkap (Sesuai KTP)
    </label>
    <input class="form-control" type="text" name="nama" id="nama" value="{{ old('nama') }}" autocomplete="off">
    @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>