<div class="form-group">
    <label>
        Nama Usaha
    </label>
    <input class="form-control" type="text" name="nama_usaha" id="nama_usaha" value="{{ old('nama_usaha') }}" autocomplete="off">
    @error('nama_usaha')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>