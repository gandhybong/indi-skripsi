<div class="form-group">
    <label>
        Nomor Induk Kependudukan (NIK) 
    </label>
    <input class="form-control" type="text" name="nik" id="nik" value="{{ old('nik') }}" autocomplete="off" >
    @error('nik')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>