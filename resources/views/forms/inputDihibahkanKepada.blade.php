<div class="form-group">
    <label>
        Dihibahkan Kepada
    </label>
    <input class="form-control" type="text" name="dihibahkan_kepada" id="dihibahkan_kepada" value={{ old('dihibahkan_kepada') }}>
    @error('dihibahkan_kepada')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>