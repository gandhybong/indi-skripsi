<div class="form-group">
    <label>
        Jenis Kelamin Pihak Kedua
    </label>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Laki-Laki" 
            {{ (old('jenis_kelamin_pihak_kedua') == 'Laki-Laki') ? "checked" : "" }}
            >Laki-Laki
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Perempuan"
            {{ (old('jenis_kelamin_pihak_kedua') == 'Perempuan') ? "checked" : "" }}
            >Perempuan
        </label>
    </div>
    @error('jenis_kelamin_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>