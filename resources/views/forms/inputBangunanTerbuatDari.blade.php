<div class="form-group">
    <label for="bangunan_terbuat_dari">
        Bangunan Terbuat Dari
    </label>
    <input class="form-control" name="bangunan_terbuat_dari" type="text" id="bangunan_terbuat_dari" value="{{ old('bangunan_terbuat_dari') }}" autocomplete="off">
    @error('bangunan_terbuat_dari')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>