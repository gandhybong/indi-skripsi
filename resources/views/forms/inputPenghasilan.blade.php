<div class="form-group">
    <label>
        Penghasilan (Dalam Rupiah)
    </label>
    <input class="form-control" type="number" min="0" name="penghasilan" id="penghasilan" value="{{ old('penghasilan') }}" >
    @error('penghasilan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>