<div class="form-group">
    <label>
        Tempat Lahir Pihak Ibu
    </label>
    <input class="form-control" type="text" name="tempat_lahir_pihak_ibu" id="tempat_lahir_pihak_ibu" value="{{ old('tempat_lahir_pihak_ibu') }}">
    @error('tempat_lahir_pihak_ibu')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>