<div class="form-group">
    <label>
        Nama Acara / Kegiatan 
    </label>
    <input class="form-control" type="text" name="acara_kegiatan" id="acara_kegiatan" value="{{ old('acara_kegiatan') }}" autocomplete="off">
    @error('acara_kegiatan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>