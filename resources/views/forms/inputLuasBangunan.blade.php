<div class="form-group">
    <label for="luas_bangunan">
        Luas Bangunan (Dalam M<sup>2</sup>)
    </label>
    <input class="form-control" name="luas_bangunan" type="number" id="luas_bangunan" value="{{ old('luas_bangunan') }}" autocomplete="off">
    @error('luas_bangunan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>