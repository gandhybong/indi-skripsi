<div class="form-group">
    <label for="alamat_bangunan">
        Alamat Bangunan
    </label>
    <input class="form-control" name="alamat_bangunan" type="text" id="alamat_bangunan" value="{{ old('alamat_bangunan') }}" autocomplete="off">
    @error('alamat_bangunan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>