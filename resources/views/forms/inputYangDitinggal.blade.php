<div class="form-group">
    <label>
        Yang Ditinggal
    </label>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="yang_ditinggalkan" id="yang_ditinggalkan" value="Suami" 
            {{ (old('yang_ditinggalkan') == 'Suami') ? "checked" : "" }}
            >Suami
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="yang_ditinggalkan" id="yang_ditinggalkan" value="Isteri"
            {{ (old('yang_ditinggalkan') == 'Isteri') ? "checked" : "" }}
            >Isteri
        </label>
    </div>
    @error('yang_ditinggalkan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>