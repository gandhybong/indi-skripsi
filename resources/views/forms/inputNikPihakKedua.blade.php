<div class="form-group">
    <label>
        Nomor Induk Kependudukan (NIK) Pihak Kedua 
    </label>
    <input class="form-control" type="text" name="nik_pihak_kedua" id="nik_pihak_kedua" value="{{ old('nik_pihak_kedua') }}" autocomplete="off" >
    @error('nik_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>