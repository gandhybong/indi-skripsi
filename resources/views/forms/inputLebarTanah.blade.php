<div class="form-group">
    <label for="lebar">
        Lebar Tanah (Dalam M)
    </label>
    <input class="form-control" name="lebar" type="number" id="lebar" value="{{ old('lebar') }}" autocomplete="off">
    @error('lebar')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>