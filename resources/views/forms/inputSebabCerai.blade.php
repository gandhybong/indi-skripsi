<div class="form-group">
    <label>
        Sebab Cerai
    </label>
    <select id='sebab_cerai' name="sebab_cerai">
        <option value="hidup">
            Cerai Hidup
        </option>
        <option value="mati">
            Cerai Mati
        </option>
    </select>
    @error('sebab_cerai')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>