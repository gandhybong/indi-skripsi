<div class="form-group">
    <label>
        Pekerjaan 
    </label>
    <input class="form-control" type="text" name="pekerjaan" id="pekerjaan" value="{{ old('pekerjaan') }}" >
    @error('pekerjaan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>