<div class="form-group">
    <label for="ketua_rt">
        Nama Ketua RT 
    </label>
    <input class="form-control" type="text" name="ketua_rt" id="ketua_rt" value="{{ old('ketua_rt') }}" autocomplete="off">
    @error('ketua_rt')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>