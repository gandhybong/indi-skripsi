<div class="form-group">
    <label>
        Pekerjaan Pihak Kedua
    </label>
    <input class="form-control" type="text" name="pekerjaan_pihak_kedua" id="pekerjaan_pihak_kedua" value="{{ old('pekerjaan_pihak_kedua') }}" >
    @error('pekerjaan_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>