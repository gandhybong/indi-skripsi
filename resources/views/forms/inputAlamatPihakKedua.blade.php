<div class="form-group">
    <label>
        Alamat Pihak Kedua (Sesuai KTP)
    </label>
    <textarea class="form-control" name="alamat_pihak_kedua" id="alamat_pihak_kedua" rows="3">{{ old('alamat_pihak_kedua') }}</textarea>
    @error('alamat_pihak_kedua')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>