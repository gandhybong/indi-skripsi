<div class="form-group">
    <label for="keperluan">
        Keperluan
    </label>
    <select class="form-control" id="keperluan" name="keperluan">
        <option value="Rumah Hunian" {{ (old('keperluan') == 'Rumah Hunian') ? "checked" : "" }}>Rumah Hunian</option>
        <option value="Ruko" {{ (old('keperluan') == 'Ruko') ? "checked" : "" }}>Ruko</option>
    </select>
    @error('keperluan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>