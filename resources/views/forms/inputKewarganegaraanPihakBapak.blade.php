<div class="form-group">
    <label>
        Kewarganegaraan Pihak Bapak 
    </label>
    <input class="form-control" type="text" name="kewarganegaraan_pihak_bapak" id="kewarganegaraan_pihak_bapak" value="Indonesia" readonly>
    @error('kewarganegaraan_pihak_bapak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>