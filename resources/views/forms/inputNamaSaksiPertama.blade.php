<div class="form-group">
    <label>
        Nama Saksi Pertama
    </label>
    <input class="form-control" type="text" name="nama_saksi_pertama" id="nama_saksi_pertama" value={{ old('nama_saksi_pertama') }}>
    @error('nama_saksi_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>