<div class="form-group">
    <label>
        Alamat Pihak Pertama (Sesuai KTP)
    </label>
    <textarea class="form-control" name="alamat_pihak_pertama" id="alamat_pihak_pertama" rows="3">{{ old('alamat_pihak_pertama') }}</textarea>
    @error('alamat_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>