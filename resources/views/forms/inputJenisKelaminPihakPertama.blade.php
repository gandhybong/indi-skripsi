<div class="form-group">
    <label>
        Jenis Kelamin Pihak Pertama
    </label>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_pertama" id="jenis_kelamin_pihak_pertama" value="Laki-Laki" 
            {{ (old('jenis_kelamin_pihak_pertama') == 'Laki-Laki') ? "checked" : "" }}
            >Laki-Laki
        </label>
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_pertama" id="jenis_kelamin_pihak_pertama" value="Perempuan"
            {{ (old('jenis_kelamin_pihak_pertama') == 'Perempuan') ? "checked" : "" }}
            >Perempuan
        </label>
    </div>
    @error('jenis_kelamin_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>