<div class="form-group">
    <label>
        Tempat Lahir Pihak Pertama
    </label>
    <input class="form-control" type="text" name="tempat_lahir_pihak_pertama" id="tempat_lahir_pihak_pertama" value="{{ old('tempat_lahir_pihak_pertama') }}">
    @error('tempat_lahir_pihak_pertama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>