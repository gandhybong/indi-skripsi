<div class="form-group">
    <label>
        Status Kedua Pihak 
    </label>
    <input class="form-control" type="text" name="status_kedua_pihak" id="status_kedua_pihak" value="{{ old('status_kedua_pihak') }}" >
    @error('status_kedua_pihak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>