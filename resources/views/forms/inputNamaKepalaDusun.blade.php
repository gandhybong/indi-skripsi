<div class="form-group">
    <label for="kepala_dusun">
        Nama Kepala Dusun 
    </label>
    <input class="form-control" type="text" name="kepala_dusun" id="kepala_dusun" value="{{ old('kepala_dusun') }}" autocomplete="off">
    @error('kepala_dusun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>