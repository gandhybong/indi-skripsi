<div class="form-group">
    <label>
        Tanggal Lahir Pihak Bapak
    </label>
    <input class="form-control" type="date" name="tgl_lahir_pihak_bapak" id="tgl_lahir_pihak_bapak" value="{{ old('tgl_lahir_pihak_bapak') }}">
    @error('tgl_lahir_pihak_bapak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>