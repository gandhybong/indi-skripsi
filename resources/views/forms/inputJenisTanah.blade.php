<div class="form-group">
    <label for="kabupaten">
        Jenis Tanah (Perumahan / Perkebunan)
    </label>
    <input class="form-control" type="text" name="jenis_tanah" id="jenis_tanah" value="{{ old('jenis_tanah') }}" autocomplete="off">
    @error('jenis_tanah')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>