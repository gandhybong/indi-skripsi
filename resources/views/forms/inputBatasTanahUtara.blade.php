<div class="form-group">
    <label>
        Batas Tanah Utara
    </label>
    <input class="form-control" type="text" name="batas_utara" id="batas_utara" value={{ old('batas_utara') }}>
    @error('batas_utara')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>