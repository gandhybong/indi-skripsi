<div class="form-group col-sm-6">
    <label>
        RT
    </label>
    <input class="form-control" type="text" name="rt" id="rt" value="{{ old('rt') }}" >
    @error('rt')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>