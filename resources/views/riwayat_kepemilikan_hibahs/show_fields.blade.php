<!-- Hibahan Id Field -->
<div class="form-group">
    {!! Form::label('hibahan_id', 'Hibahan Id:') !!}
    <p>{{ $riwayatKepemilikanHibah->hibahan_id }}</p>
</div>

<!-- Alasan Field -->
<div class="form-group">
    {!! Form::label('alasan', 'Alasan:') !!}
    <p>{{ $riwayatKepemilikanHibah->alasan }}</p>
</div>

<!-- Tahun Field -->
<div class="form-group">
    {!! Form::label('tahun', 'Tahun:') !!}
    <p>{{ $riwayatKepemilikanHibah->tahun }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $riwayatKepemilikanHibah->nama }}</p>
</div>

