<div class="table-responsive">
    <table class="table" id="riwayatKepemilikanHibahs-table">
        <thead>
            <tr>
                <th>Hibahan Id</th>
        <th>Alasan</th>
        <th>Tahun</th>
        <th>Nama</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($riwayatKepemilikanHibahs as $riwayatKepemilikanHibah)
            <tr>
                <td>{{ $riwayatKepemilikanHibah->hibahan_id }}</td>
            <td>{{ $riwayatKepemilikanHibah->alasan }}</td>
            <td>{{ $riwayatKepemilikanHibah->tahun }}</td>
            <td>{{ $riwayatKepemilikanHibah->nama }}</td>
                <td>
                    {!! Form::open(['route' => ['riwayatKepemilikanHibahs.destroy', $riwayatKepemilikanHibah->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('riwayatKepemilikanHibahs.show', [$riwayatKepemilikanHibah->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('riwayatKepemilikanHibahs.edit', [$riwayatKepemilikanHibah->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
