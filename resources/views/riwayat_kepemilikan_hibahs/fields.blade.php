<!-- Hibahan Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hibahan_id', 'Hibahan Id:') !!}
    {!! Form::number('hibahan_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Alasan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alasan', 'Alasan:') !!}
    {!! Form::text('alasan', null, ['class' => 'form-control']) !!}
</div>

<!-- Tahun Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tahun', 'Tahun:') !!}
    {!! Form::text('tahun', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('riwayatKepemilikanHibahs.index') }}" class="btn btn-default">Cancel</a>
</div>
