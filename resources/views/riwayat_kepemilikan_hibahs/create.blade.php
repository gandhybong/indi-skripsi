@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Riwayat Kepemilikan Hibah
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'riwayatKepemilikanHibahs.store']) !!}

                        @include('riwayat_kepemilikan_hibahs.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
