@section( 'scripts' )
@include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratKeteranganKelahirans-table' ).DataTable({

            });
        } );
    </script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKeteranganKelahirans-table">
        <thead>
            <tr>
                <th>Nik Pihak Bapak</th>
                <th>Nama Pihak Bapak</th>
                <th>Tempat Lahir Pihak Bapak</th>
                <th>Tgl Lahir Pihak Bapak</th>
                <th>Kewarganegaraan Pihak Bapak</th>
                <th>Agama Pihak Bapak</th>
                <th>Pekerjaan Pihak Bapak</th>
                <th>Alamat Pihak Bapak</th>
                <th>Anak Ke</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganKelahirans as $suratKeteranganKelahiran)
            <tr>
                <td>{{ $suratKeteranganKelahiran->nik_pihak_bapak }}</td>
                <td>{{ $suratKeteranganKelahiran->nama_pihak_bapak }}</td>
                <td>{{ $suratKeteranganKelahiran->tempat_lahir_pihak_bapak }}</td>
                <td>{{ $suratKeteranganKelahiran->tgl_lahir_pihak_bapak }}</td>
                <td>{{ $suratKeteranganKelahiran->kewarganegaraan_pihak_bapak }}</td>
                <td>{{ $suratKeteranganKelahiran->agama_pihak_bapak }}</td>
                <td>{{ $suratKeteranganKelahiran->pekerjaan_pihak_bapak }}</td>
                <td>{{ $suratKeteranganKelahiran->alamat_pihak_bapak }}</td>
                <td>{{ $suratKeteranganKelahiran->anak_ke }}</td>
                <td>{{ $suratKeteranganKelahiran->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganKelahirans.destroy', $suratKeteranganKelahiran->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganKelahirans.edit', [$suratKeteranganKelahiran->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
