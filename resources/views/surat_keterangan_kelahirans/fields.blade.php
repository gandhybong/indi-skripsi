<!-- Nik Pihak Bapak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nik_pihak_bapak', 'Nik Pihak Bapak:') !!}
    {!! Form::text('nik_pihak_bapak', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Pihak Bapak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_pihak_bapak', 'Nama Pihak Bapak:') !!}
    {!! Form::text('nama_pihak_bapak', null, ['class' => 'form-control']) !!}
</div>

<!-- Tempat Lahir Pihak Bapak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tempat_lahir_pihak_bapak', 'Tempat Lahir Pihak Bapak:') !!}
    {!! Form::text('tempat_lahir_pihak_bapak', null, ['class' => 'form-control']) !!}
</div>

<!-- Tgl Lahir Pihak Bapak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tgl_lahir_pihak_bapak', 'Tgl Lahir Pihak Bapak:') !!}
    {!! Form::date('tgl_lahir_pihak_bapak', null, ['class' => 'form-control','id'=>'tgl_lahir_pihak_bapak']) !!}
</div>

<!-- Kewarganegaraan Pihak Bapak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kewarganegaraan_pihak_bapak', 'Kewarganegaraan Pihak Bapak:') !!}
    {!! Form::text('kewarganegaraan_pihak_bapak', null, ['class' => 'form-control']) !!}
</div>

<!-- Agama Field -->
<div class="form-group col-sm-12">
    <label for="agama_pihak_bapak">Agama Pihak Bapak:</label>
    <select class="form-control" id="agama_pihak_bapak" name="agama_pihak_bapak" >
        @isset($suratKeteranganKelahiran)
            <option value="Islam" {{ (old('agama_pihak_bapak') == 'Islam') || $suratKeteranganKelahiran->agama_pihak_bapak == 'Islam' ? "selected" : "" }}>Islam</option>
            <option value="Protestan" {{ (old('agama_pihak_bapak') == 'Protestan') || $suratKeteranganKelahiran->agama_pihak_bapak == 'Protestan' ? "selected" : "" }}>Protestan</option>
            <option value="Katolik" {{ (old('agama_pihak_bapak') == 'Katolik') || $suratKeteranganKelahiran->agama_pihak_bapak == 'Katolik' ? "selected" : "" }}>Katolik</option>
            <option value="Hindu" {{ (old('agama_pihak_bapak') == 'Hindu') || $suratKeteranganKelahiran->agama_pihak_bapak == 'Hindu' ? "selected" : "" }}>Hindu</option>
            <option value="Buddha" {{ (old('agama_pihak_bapak') == 'Buddha') || $suratKeteranganKelahiran->agama_pihak_bapak == 'Buddha' ? "selected" : "" }}>Buddha</option>
            <option value="Khonghucu" {{ (old('agama_pihak_bapak') == 'Khonghucu') || $suratKeteranganKelahiran->agama_pihak_bapak == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
        @endisset

        @empty($suratKeteranganKelahiran)
            <option value="Islam" {{ (old('agama_pihak_bapak') == 'Islam') ? "selected" : "" }}>Islam</option>
            <option value="Protestan" {{ (old('agama_pihak_bapak') == 'Protestan') ? "selected" : "" }}>Protestan</option>
            <option value="Katolik" {{ (old('agama_pihak_bapak') == 'Katolik') ? "selected" : "" }}>Katolik</option>
            <option value="Hindu" {{ (old('agama_pihak_bapak') == 'Hindu') ? "selected" : "" }}>Hindu</option>
            <option value="Buddha" {{ (old('agama_pihak_bapak') == 'Buddha') ? "selected" : "" }}>Buddha</option>
            <option value="Khonghucu" {{ (old('agama_pihak_bapak') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
        @endempty
    </select>
</div>

<!-- Pekerjaan Pihak Bapak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pekerjaan_pihak_bapak', 'Pekerjaan Pihak Bapak:') !!}
    {!! Form::text('pekerjaan_pihak_bapak', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamat Pihak Bapak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alamat_pihak_bapak', 'Alamat Pihak Bapak:') !!}
    {!! Form::text('alamat_pihak_bapak', null, ['class' => 'form-control']) !!}
</div>

<!-- Nik Pihak Ibu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nik_pihak_ibu', 'Nik Pihak Ibu:') !!}
    {!! Form::text('nik_pihak_ibu', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Pihak Ibu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_pihak_ibu', 'Nama Pihak Ibu:') !!}
    {!! Form::text('nama_pihak_ibu', null, ['class' => 'form-control']) !!}
</div>

<!-- Tempat Lahir Pihak Ibu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tempat_lahir_pihak_ibu', 'Tempat Lahir Pihak Ibu:') !!}
    {!! Form::text('tempat_lahir_pihak_ibu', null, ['class' => 'form-control']) !!}
</div>

<!-- Tgl Lahir Pihak Ibu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tgl_lahir_pihak_ibu', 'Tgl Lahir Pihak Ibu:') !!}
    {!! Form::date('tgl_lahir_pihak_ibu', null, ['class' => 'form-control','id'=>'tgl_lahir_pihak_ibu']) !!}
</div>

<!-- Kewarganegaraan Pihak Ibu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kewarganegaraan_pihak_ibu', 'Kewarganegaraan Pihak Ibu:') !!}
    {!! Form::text('kewarganegaraan_pihak_ibu', null, ['class' => 'form-control']) !!}
</div>

<!-- Agama Field -->
<div class="form-group col-sm-12">
    <label for="agama_pihak_ibu">Agama Pihak Ibu:</label>
    <select class="form-control" id="agama_pihak_ibu" name="agama_pihak_ibu" >
        @isset($suratKeteranganKelahiran)
            <option value="Islam" {{ (old('agama_pihak_ibu') == 'Islam') || $suratKeteranganKelahiran->agama_pihak_ibu == 'Islam' ? "selected" : "" }}>Islam</option>
            <option value="Protestan" {{ (old('agama_pihak_ibu') == 'Protestan') || $suratKeteranganKelahiran->agama_pihak_ibu == 'Protestan' ? "selected" : "" }}>Protestan</option>
            <option value="Katolik" {{ (old('agama_pihak_ibu') == 'Katolik') || $suratKeteranganKelahiran->agama_pihak_ibu == 'Katolik' ? "selected" : "" }}>Katolik</option>
            <option value="Hindu" {{ (old('agama_pihak_ibu') == 'Hindu') || $suratKeteranganKelahiran->agama_pihak_ibu == 'Hindu' ? "selected" : "" }}>Hindu</option>
            <option value="Buddha" {{ (old('agama_pihak_ibu') == 'Buddha') || $suratKeteranganKelahiran->agama_pihak_ibu == 'Buddha' ? "selected" : "" }}>Buddha</option>
            <option value="Khonghucu" {{ (old('agama_pihak_ibu') == 'Khonghucu') || $suratKeteranganKelahiran->agama_pihak_ibu == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
        @endisset

        @empty($suratKeteranganKelahiran)
            <option value="Islam" {{ (old('agama_pihak_ibu') == 'Islam') ? "selected" : "" }}>Islam</option>
            <option value="Protestan" {{ (old('agama_pihak_ibu') == 'Protestan') ? "selected" : "" }}>Protestan</option>
            <option value="Katolik" {{ (old('agama_pihak_ibu') == 'Katolik') ? "selected" : "" }}>Katolik</option>
            <option value="Hindu" {{ (old('agama_pihak_ibu') == 'Hindu') ? "selected" : "" }}>Hindu</option>
            <option value="Buddha" {{ (old('agama_pihak_ibu') == 'Buddha') ? "selected" : "" }}>Buddha</option>
            <option value="Khonghucu" {{ (old('agama_pihak_ibu') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
        @endempty
    </select>
</div>

<!-- Pekerjaan Pihak Ibu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pekerjaan_pihak_ibu', 'Pekerjaan Pihak Ibu:') !!}
    {!! Form::text('pekerjaan_pihak_ibu', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamat Pihak Ibu Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alamat_pihak_ibu', 'Alamat Pihak Ibu:') !!}
    {!! Form::text('alamat_pihak_ibu', null, ['class' => 'form-control']) !!}
</div>

<!-- Anak Ke Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anak_ke', 'Anak Ke:') !!}
    {!! Form::number('anak_ke', null, ['class' => 'form-control', 'min' => '1']) !!}
</div>

<!-- Nama Anak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_anak', 'Nama Anak:') !!}
    {!! Form::text('nama_anak', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group col-sm-12">
    <label for="jenis_kelamin_anak">
        Jenis Kelamin Anak
    </label>
    @isset($suratKeteranganKelahiran)
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_anak" id="jenis_kelamin_anak" value="Laki-Laki" 
                {{ (old('jenis_kelamin_anak') == 'Laki-Laki' || $suratKeteranganKelahiran->jenis_kelamin_anak == 'Laki-Laki') ? "checked" : "" }}
                >Laki-Laki
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_anak" id="jenis_kelamin_anak" value="Perempuan"
                {{ (old('jenis_kelamin_anak') == 'Perempuan' || $suratKeteranganKelahiran->jenis_kelamin_anak == 'Perempuan') ? "checked" : "" }}
                >Perempuan
            </label>
        </div>
    @endisset

    @empty($suratKeteranganKelahiran)
    <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_anak" id="jenis_kelamin_anak" value="Laki-Laki" 
                {{ (old('jenis_kelamin_anak') == 'Laki-Laki') ? "checked" : "" }}
                >Laki-Laki
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_anak" id="jenis_kelamin_anak" value="Perempuan"
                {{ (old('jenis_kelamin_anak') == 'Perempuan') ? "checked" : "" }}
                >Perempuan
            </label>
        </div>
    @endempty

    @error('jenis_kelamin_anak')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>

<!-- Tempat Lahir Anak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tempat_lahir_anak', 'Tempat Lahir Anak:') !!}
    {!! Form::text('tempat_lahir_anak', null, ['class' => 'form-control']) !!}
</div>

<!-- Jam Lahir Anak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jam_lahir_anak', 'Jam Lahir Anak:') !!}
    {!! Form::text('jam_lahir_anak', null, ['class' => 'form-control','id'=>'jam_lahir_anak']) !!}
</div>

<!-- Tgl Lahir Anak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tgl_lahir_anak', 'Tgl Lahir Anak:') !!}
    {!! Form::date('tgl_lahir_anak', null, ['class' => 'form-control','id'=>'tgl_lahir_anak']) !!}
</div>

<!-- Kewarganegaraan Anak Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kewarganegaraan_anak', 'Kewarganegaraan Anak:') !!}
    {!! Form::text('kewarganegaraan_anak', null, ['class' => 'form-control']) !!}
</div>

<!-- Agama Field -->
<div class="form-group col-sm-12">
    <label for="agama_anak">Agama Anak:</label>
    <select class="form-control" id="agama_anak" name="agama_anak" >
        @isset($suratKeteranganKelahiran)
            <option value="Islam" {{ (old('agama_anak') == 'Islam') || $suratKeteranganKelahiran->agama_anak == 'Islam' ? "selected" : "" }}>Islam</option>
            <option value="Protestan" {{ (old('agama_anak') == 'Protestan') || $suratKeteranganKelahiran->agama_anak == 'Protestan' ? "selected" : "" }}>Protestan</option>
            <option value="Katolik" {{ (old('agama_anak') == 'Katolik') || $suratKeteranganKelahiran->agama_anak == 'Katolik' ? "selected" : "" }}>Katolik</option>
            <option value="Hindu" {{ (old('agama_anak') == 'Hindu') || $suratKeteranganKelahiran->agama_anak == 'Hindu' ? "selected" : "" }}>Hindu</option>
            <option value="Buddha" {{ (old('agama_anak') == 'Buddha') || $suratKeteranganKelahiran->agama_anak == 'Buddha' ? "selected" : "" }}>Buddha</option>
            <option value="Khonghucu" {{ (old('agama_anak') == 'Khonghucu') || $suratKeteranganKelahiran->agama_anak == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
        @endisset

        @empty($suratKeteranganKelahiran)
            <option value="Islam" {{ (old('agama_anak') == 'Islam') ? "selected" : "" }}>Islam</option>
            <option value="Protestan" {{ (old('agama_anak') == 'Protestan') ? "selected" : "" }}>Protestan</option>
            <option value="Katolik" {{ (old('agama_anak') == 'Katolik') ? "selected" : "" }}>Katolik</option>
            <option value="Hindu" {{ (old('agama_anak') == 'Hindu') ? "selected" : "" }}>Hindu</option>
            <option value="Buddha" {{ (old('agama_anak') == 'Buddha') ? "selected" : "" }}>Buddha</option>
            <option value="Khonghucu" {{ (old('agama_anak') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
        @endempty
    </select>
</div>

@if(Route::current()->getName() == 'suratKeteranganKelahirans.edit')
    <!-- Status Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('status', 'Status') !!}
        <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganKelahiran->status->status}}" readonly>
    </div>
@endif

<div class="col-sm-12">
@if(Route::current()->getName() == 'suratKeteranganKelahirans.edit')
    @if ( $suratKeteranganKelahiran->status->status == "Draf" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
            {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
            <a href="{!! route('suratKeteranganKelahirans.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @elseif ( $suratKeteranganKelahiran->status->status == "Diterima" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{{ route('keteranganKelahiran.print', ['id' => $suratKeteranganKelahiran->id]) }}" class="btn btn-success" target="_blank" id="print">
                Print
            </a>
            <a href="{!! route('suratKeteranganKelahirans.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('suratKeteranganKelahirans.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @endif
@else
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Buat Surat Keterangan Kelahiran', ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('suratKeteranganKelahirans.index') }}" class="btn btn-default">Kembali</a>
    </div>
@endif
</div>



@section('scripts')
    <script type="text/javascript">
        @if(session('status'))
            document.getElementById("print").click();
        @endif

        $('#tgl_lahir_pihak_bapak').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#tgl_lahir_pihak_ibu').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#tgl_lahir_anak').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#jam_lahir_anak').datetimepicker({
            format: 'HH:mm',      
        })

        $('#datetimepicker1').datetimepicker({
            format: 'HH:mm'
        });
    </script>
@endsection