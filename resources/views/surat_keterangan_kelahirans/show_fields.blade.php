<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $suratKeteranganKelahiran->user_id }}</p>
</div>

<!-- Nik Pihak Bapak Field -->
<div class="form-group">
    {!! Form::label('nik_pihak_bapak', 'Nik Pihak Bapak:') !!}
    <p>{{ $suratKeteranganKelahiran->nik_pihak_bapak }}</p>
</div>

<!-- Nama Pihak Bapak Field -->
<div class="form-group">
    {!! Form::label('nama_pihak_bapak', 'Nama Pihak Bapak:') !!}
    <p>{{ $suratKeteranganKelahiran->nama_pihak_bapak }}</p>
</div>

<!-- Tempat Lahir Pihak Bapak Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir_pihak_bapak', 'Tempat Lahir Pihak Bapak:') !!}
    <p>{{ $suratKeteranganKelahiran->tempat_lahir_pihak_bapak }}</p>
</div>

<!-- Tgl Lahir Pihak Bapak Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir_pihak_bapak', 'Tgl Lahir Pihak Bapak:') !!}
    <p>{{ $suratKeteranganKelahiran->tgl_lahir_pihak_bapak }}</p>
</div>

<!-- Kewarganegaraan Pihak Bapak Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan_pihak_bapak', 'Kewarganegaraan Pihak Bapak:') !!}
    <p>{{ $suratKeteranganKelahiran->kewarganegaraan_pihak_bapak }}</p>
</div>

<!-- Agama Pihak Bapak Field -->
<div class="form-group">
    {!! Form::label('agama_pihak_bapak', 'Agama Pihak Bapak:') !!}
    <p>{{ $suratKeteranganKelahiran->agama_pihak_bapak }}</p>
</div>

<!-- Pekerjaan Pihak Bapak Field -->
<div class="form-group">
    {!! Form::label('pekerjaan_pihak_bapak', 'Pekerjaan Pihak Bapak:') !!}
    <p>{{ $suratKeteranganKelahiran->pekerjaan_pihak_bapak }}</p>
</div>

<!-- Alamat Pihak Bapak Field -->
<div class="form-group">
    {!! Form::label('alamat_pihak_bapak', 'Alamat Pihak Bapak:') !!}
    <p>{{ $suratKeteranganKelahiran->alamat_pihak_bapak }}</p>
</div>

<!-- Nik Pihak Ibu Field -->
<div class="form-group">
    {!! Form::label('nik_pihak_ibu', 'Nik Pihak Ibu:') !!}
    <p>{{ $suratKeteranganKelahiran->nik_pihak_ibu }}</p>
</div>

<!-- Nama Pihak Ibu Field -->
<div class="form-group">
    {!! Form::label('nama_pihak_ibu', 'Nama Pihak Ibu:') !!}
    <p>{{ $suratKeteranganKelahiran->nama_pihak_ibu }}</p>
</div>

<!-- Tempat Lahir Pihak Ibu Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir_pihak_ibu', 'Tempat Lahir Pihak Ibu:') !!}
    <p>{{ $suratKeteranganKelahiran->tempat_lahir_pihak_ibu }}</p>
</div>

<!-- Tgl Lahir Pihak Ibu Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir_pihak_ibu', 'Tgl Lahir Pihak Ibu:') !!}
    <p>{{ $suratKeteranganKelahiran->tgl_lahir_pihak_ibu }}</p>
</div>

<!-- Kewarganegaraan Pihak Ibu Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan_pihak_ibu', 'Kewarganegaraan Pihak Ibu:') !!}
    <p>{{ $suratKeteranganKelahiran->kewarganegaraan_pihak_ibu }}</p>
</div>

<!-- Agama Pihak Ibu Field -->
<div class="form-group">
    {!! Form::label('agama_pihak_ibu', 'Agama Pihak Ibu:') !!}
    <p>{{ $suratKeteranganKelahiran->agama_pihak_ibu }}</p>
</div>

<!-- Pekerjaan Pihak Ibu Field -->
<div class="form-group">
    {!! Form::label('pekerjaan_pihak_ibu', 'Pekerjaan Pihak Ibu:') !!}
    <p>{{ $suratKeteranganKelahiran->pekerjaan_pihak_ibu }}</p>
</div>

<!-- Alamat Pihak Ibu Field -->
<div class="form-group">
    {!! Form::label('alamat_pihak_ibu', 'Alamat Pihak Ibu:') !!}
    <p>{{ $suratKeteranganKelahiran->alamat_pihak_ibu }}</p>
</div>

<!-- Anak Ke Field -->
<div class="form-group">
    {!! Form::label('anak_ke', 'Anak Ke:') !!}
    <p>{{ $suratKeteranganKelahiran->anak_ke }}</p>
</div>

<!-- Nama Anak Field -->
<div class="form-group">
    {!! Form::label('nama_anak', 'Nama Anak:') !!}
    <p>{{ $suratKeteranganKelahiran->nama_anak }}</p>
</div>

<!-- Jenis Kelamin Anak Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin_anak', 'Jenis Kelamin Anak:') !!}
    <p>{{ $suratKeteranganKelahiran->jenis_kelamin_anak }}</p>
</div>

<!-- Tempat Lahir Anak Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir_anak', 'Tempat Lahir Anak:') !!}
    <p>{{ $suratKeteranganKelahiran->tempat_lahir_anak }}</p>
</div>

<!-- Jam Lahir Anak Field -->
<div class="form-group">
    {!! Form::label('jam_lahir_anak', 'Jam Lahir Anak:') !!}
    <p>{{ $suratKeteranganKelahiran->jam_lahir_anak }}</p>
</div>

<!-- Tgl Lahir Anak Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir_anak', 'Tgl Lahir Anak:') !!}
    <p>{{ $suratKeteranganKelahiran->tgl_lahir_anak }}</p>
</div>

<!-- Kewarganegaraan Anak Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan_anak', 'Kewarganegaraan Anak:') !!}
    <p>{{ $suratKeteranganKelahiran->kewarganegaraan_anak }}</p>
</div>

<!-- Agama Anak Field -->
<div class="form-group">
    {!! Form::label('agama_anak', 'Agama Anak:') !!}
    <p>{{ $suratKeteranganKelahiran->agama_anak }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratKeteranganKelahiran->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratKeteranganKelahiran->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratKeteranganKelahiran->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratKeteranganKelahiran->nomor_surat }}</p>
</div>

