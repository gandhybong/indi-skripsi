<!-- Ahli Waris Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ahli_waris_id', 'Ahli Waris Id:') !!}
    {!! Form::number('ahli_waris_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Hubungan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hubungan', 'Hubungan:') !!}
    {!! Form::text('hubungan', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('ahliWarisKeterenganAhliWaris.index') }}" class="btn btn-default">Cancel</a>
</div>
