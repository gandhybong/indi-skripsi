<div class="table-responsive">
    <table class="table" id="ahliWarisKeterenganAhliWaris-table">
        <thead>
            <tr>
                <th>Ahli Waris Id</th>
        <th>Nama</th>
        <th>Hubungan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($ahliWarisKeterenganAhliWaris as $ahliWarisKeterenganAhliWaris)
            <tr>
                <td>{{ $ahliWarisKeterenganAhliWaris->ahli_waris_id }}</td>
            <td>{{ $ahliWarisKeterenganAhliWaris->nama }}</td>
            <td>{{ $ahliWarisKeterenganAhliWaris->hubungan }}</td>
                <td>
                    {!! Form::open(['route' => ['ahliWarisKeterenganAhliWaris.destroy', $ahliWarisKeterenganAhliWaris->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('ahliWarisKeterenganAhliWaris.show', [$ahliWarisKeterenganAhliWaris->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('ahliWarisKeterenganAhliWaris.edit', [$ahliWarisKeterenganAhliWaris->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
