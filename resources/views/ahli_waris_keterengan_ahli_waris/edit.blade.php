@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ahli Waris Keterengan Ahli Waris
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ahliWarisKeterenganAhliWaris, ['route' => ['ahliWarisKeterenganAhliWaris.update', $ahliWarisKeterenganAhliWaris->id], 'method' => 'patch']) !!}

                        @include('ahli_waris_keterengan_ahli_waris.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection