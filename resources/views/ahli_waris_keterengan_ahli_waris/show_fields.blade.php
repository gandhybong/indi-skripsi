<!-- Ahli Waris Id Field -->
<div class="form-group">
    {!! Form::label('ahli_waris_id', 'Ahli Waris Id:') !!}
    <p>{{ $ahliWarisKeterenganAhliWaris->ahli_waris_id }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $ahliWarisKeterenganAhliWaris->nama }}</p>
</div>

<!-- Hubungan Field -->
<div class="form-group">
    {!! Form::label('hubungan', 'Hubungan:') !!}
    <p>{{ $ahliWarisKeterenganAhliWaris->hubungan }}</p>
</div>

