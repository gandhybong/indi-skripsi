<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $suratKeterangan->user_id }}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{{ $suratKeterangan->nik }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $suratKeterangan->nama }}</p>
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{{ $suratKeterangan->jenis_kelamin }}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{{ $suratKeterangan->tempat_lahir }}</p>
</div>

<!-- Tgl Lahir Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
    <p>{{ $suratKeterangan->tgl_lahir }}</p>
</div>

<!-- Kewarganegaraan Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
    <p>{{ $suratKeterangan->kewarganegaraan }}</p>
</div>

<!-- Pekerjaan Field -->
<div class="form-group">
    {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
    <p>{{ $suratKeterangan->pekerjaan }}</p>
</div>

<!-- Agama Field -->
<div class="form-group">
    {!! Form::label('agama', 'Agama:') !!}
    <p>{{ $suratKeterangan->agama }}</p>
</div>

<!-- Status Perkawinan Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan', 'Status Perkawinan:') !!}
    <p>{{ $suratKeterangan->status_perkawinan }}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $suratKeterangan->alamat }}</p>
</div>

<!-- Administrasi Field -->
<div class="form-group">
    {!! Form::label('administrasi', 'Administrasi:') !!}
    <p>{{ $suratKeterangan->administrasi }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratKeterangan->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratKeterangan->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratKeterangan->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratKeterangan->nomor_surat }}</p>
</div>

