@section( 'css' )
@include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratKeterangans-table' ).DataTable({
                responsive: true
            });
        } );
    </script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKeterangans-table">
        <thead>
            <tr>
                <th>Nik</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Tgl Lahir</th>
                <th>Kewarganegaraan</th>
                <th>Pekerjaan</th>
                <th>Agama</th>
                <th>Status Perkawinan</th>
                <th>Alamat</th>
                <th>Administrasi</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeterangans as $suratKeterangan)
            <tr>
                <td>{{ $suratKeterangan->nik }}</td>
                <td>{{ $suratKeterangan->nama }}</td>
                <td>{{ $suratKeterangan->jenis_kelamin }}</td>
                <td>{{ $suratKeterangan->tempat_lahir }}</td>
                <td>{{ $suratKeterangan->tgl_lahir }}</td>
                <td>{{ $suratKeterangan->kewarganegaraan }}</td>
                <td>{{ $suratKeterangan->pekerjaan }}</td>
                <td>{{ $suratKeterangan->agama }}</td>
                <td>{{ $suratKeterangan->status_perkawinan }}</td>
                <td>{{ $suratKeterangan->alamat }}</td>
                <td>{{ $suratKeterangan->administrasi }}</td>
                <td>{{ $suratKeterangan->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeterangans.destroy', $suratKeterangan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeterangans.edit', [$suratKeterangan->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
