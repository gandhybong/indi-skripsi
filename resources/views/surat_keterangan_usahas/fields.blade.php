@if(Route::current()->getName() == 'suratKeteranganUsahas.edit')
<div class="col-sm-6">
    <!-- Kode Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kode', 'Kode:') !!}
        {!! Form::text('kode', null, ['class' => 'form-control','readonly']) !!}
    </div>
</div>
@endif

<div class="col-sm-6">
    <!-- Nik Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik', 'NIK:') !!}
        {!! Form::text('nik', null, ['class' => 'form-control', 'maxlength' => '16']) !!}
    </div>
    
    <!-- Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama', 'Nama:') !!}
        {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Jenis Kelamin Field -->
    <div class="form-group col-sm-12">
        <label for="jenis_kelamin">
            Jenis Kelamin
        </label>
        
        @isset($suratKeteranganUsaha)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki' || $suratKeteranganUsaha->jenis_kelamin == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan' || $suratKeteranganUsaha->jenis_kelamin == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endisset
    
        @empty($suratKeteranganUsaha)
        <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endempty
    </div>
    
    <!-- Tempat Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
        {!! Form::text('tempat_lahir', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tgl Lahir Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
        {!! Form::date('tgl_lahir', null, ['class' => 'form-control','id'=>'tgl_lahir']) !!}
    </div>
    
    <!-- Status Perkawinan Field -->
    <div class="form-group col-sm-12">
        <label for="status_perkawinan">
            Status Perkawinan 
        </label>
        @isset($suratKeteranganUsaha)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') || $suratKeteranganUsaha->status_perkawinan == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') || $suratKeteranganUsaha->status_perkawinan == 'Kawin' ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') || $suratKeteranganUsaha->status_perkawinan == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') || $suratKeteranganUsaha->status_perkawinan == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endisset

        @empty($suratKeteranganUsaha)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endempty
    </div>
    
    <!-- Kewarganegaraan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
        {!! Form::text('kewarganegaraan', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Agama Field -->
    <div class="form-group col-sm-12">
        <label for="agama">Agama</label>
        <select class="form-control" id="agama" name="agama" >
            @isset($suratKeteranganUsaha)
                <option value="Islam" {{ (old('agama') == 'Islam') || $suratKeteranganUsaha->agama == 'Islam' ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') || $suratKeteranganUsaha->agama == 'Protestan' ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') || $suratKeteranganUsaha->agama == 'Katolik' ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') || $suratKeteranganUsaha->agama == 'Hindu' ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') || $suratKeteranganUsaha->agama == 'Buddha' ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') || $suratKeteranganUsaha->agama == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
            @endisset

            @empty($suratKeteranganUsaha)
                <option value="Islam" {{ (old('agama') == 'Islam') ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
            @endempty
        </select>
    </div>
    
    <!-- Pekerjaan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
        {!! Form::text('pekerjaan', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Alamat Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat', 'Alamat:') !!}
        {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Nama Usaha Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama_usaha', 'Nama Usaha:') !!}
        {!! Form::text('nama_usaha', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Tahun Berdiri Usaha Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tahun_berdiri_usaha', 'Tahun Berdiri Usaha:') !!}
        {!! Form::text('tahun_berdiri_usaha', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Alamat Usaha Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat_usaha', 'Alamat Usaha:') !!}
        {!! Form::textarea('alamat_usaha', null, ['class' => 'form-control']) !!}
    </div>
    
    @if(Route::current()->getName() == 'suratKeteranganUsahas.edit')
        <!-- Status Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status', 'Status') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganUsaha->status->status}}" readonly>
        </div>
    @endif

</div>

<div class="col-sm-12">

@if(Route::current()->getName() == 'suratKeteranganUsahas.edit')
    @if ( $suratKeteranganUsaha->status->status == "Draf" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
            {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
            <a href="{!! route('suratKeteranganUsahas.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @elseif ( $suratKeteranganUsaha->status->status == "Diterima" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{{ route('usaha.print', ['id' => $suratKeteranganUsaha->id]) }}" class="btn btn-success" target="_blank" id="print">
                Print
            </a>
            <a href="{!! route('suratKeteranganUsahas.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('suratKeteranganUsahas.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @endif
@else
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Buat Surat Keterangan Usaha', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('suratKeteranganUsahas.index') !!}" class="btn btn-default">kembali</a>
    </div>
@endif
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection