@section( 'css' )
@include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganUsahas-table' ).DataTable({
            responsive: true
        });
    } );
</script>
@endsection

<div class="table-responsive">
    <table class="table" id="suratKeteranganUsahas-table">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Nik</th>
                <th>Nama</th>                
                <th>Pekerjaan</th>
                <th>Alamat</th>
                <th>Nama Usaha</th>
                <th>Tahun Berdiri Usaha</th>
                <th>Alamat Usaha</th>
                <th>Nomor Surat</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganUsahas as $suratKeteranganUsaha)
            <tr>
                <td>{{ $suratKeteranganUsaha->kode }}</td>
                <td>{{ $suratKeteranganUsaha->nik }}</td>
                <td>{{ $suratKeteranganUsaha->nama }}</td>
                <td>{{ $suratKeteranganUsaha->pekerjaan }}</td>
                <td>{{ $suratKeteranganUsaha->alamat }}</td>
                <td>{{ $suratKeteranganUsaha->nama_usaha }}</td>
                <td>{{ $suratKeteranganUsaha->tahun_berdiri_usaha }}</td>
                <td>{{ $suratKeteranganUsaha->alamat_usaha }}</td>
                <td>{{ $suratKeteranganUsaha->nomor_surat }}</td>
                <td>{{ $suratKeteranganUsaha->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganUsahas.destroy', $suratKeteranganUsaha->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganUsahas.edit', [$suratKeteranganUsaha->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
