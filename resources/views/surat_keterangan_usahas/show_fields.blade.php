<!-- Kode Field -->
<div class="form-group">
    {!! Form::label('kode', 'Kode:') !!}
    <p>{{ $suratKeteranganUsaha->kode }}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{{ $suratKeteranganUsaha->nik }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $suratKeteranganUsaha->nama }}</p>
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{{ $suratKeteranganUsaha->jenis_kelamin }}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{{ $suratKeteranganUsaha->tempat_lahir }}</p>
</div>

<!-- Tgl Lahir Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
    <p>{{ $suratKeteranganUsaha->tgl_lahir }}</p>
</div>

<!-- Status Perkawinan Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan', 'Status Perkawinan:') !!}
    <p>{{ $suratKeteranganUsaha->status_perkawinan }}</p>
</div>

<!-- Kewarganegaraan Field -->
<div class="form-group">
    {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
    <p>{{ $suratKeteranganUsaha->kewarganegaraan }}</p>
</div>

<!-- Agama Field -->
<div class="form-group">
    {!! Form::label('agama', 'Agama:') !!}
    <p>{{ $suratKeteranganUsaha->agama }}</p>
</div>

<!-- Pekerjaan Field -->
<div class="form-group">
    {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
    <p>{{ $suratKeteranganUsaha->pekerjaan }}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $suratKeteranganUsaha->alamat }}</p>
</div>

<!-- Nama Usaha Field -->
<div class="form-group">
    {!! Form::label('nama_usaha', 'Nama Usaha:') !!}
    <p>{{ $suratKeteranganUsaha->nama_usaha }}</p>
</div>

<!-- Tahun Berdiri Usaha Field -->
<div class="form-group">
    {!! Form::label('tahun_berdiri_usaha', 'Tahun Berdiri Usaha:') !!}
    <p>{{ $suratKeteranganUsaha->tahun_berdiri_usaha }}</p>
</div>

<!-- Alamat Usaha Field -->
<div class="form-group">
    {!! Form::label('alamat_usaha', 'Alamat Usaha:') !!}
    <p>{{ $suratKeteranganUsaha->alamat_usaha }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratKeteranganUsaha->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratKeteranganUsaha->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratKeteranganUsaha->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratKeteranganUsaha->nomor_surat }}</p>
</div>

