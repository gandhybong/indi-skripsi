@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Surat Keterangan Pajak</h1>
    </section>
    <div class="clearfix"></div>
    <br>
    <div class="col-sm-4">
        <a class="btn btn-primary" href="{!! route('suratKeteranganPajaks.create') !!}">Pengajuan Surat Keterangan Pajak</a>
    </div>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-sm-12">
                    @include('surat_keterangan_pajaks.table')
                </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

