@section( 'css' )
    @include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratKeteranganPajaks-table' ).DataTable({

            });
        } );
    </script>
@endsection

<div class="table-responsive">
    <table class="table" id="suratKeteranganPajaks-table">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Agama</th>
                <th>Status</th>
                <th>Pekerjaan</th>
                <th>Alamat</th>
                <th>Status Id</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganPajaks as $suratKeteranganPajak)
            <tr>
                <td>{!! $suratKeteranganPajak->kode !!}</td>
                <td>{!! $suratKeteranganPajak->nik !!}</td>
                <td>{!! $suratKeteranganPajak->nama !!}</td>
                <td>{!! $suratKeteranganPajak->jenis_kelamin !!}</td>
                <td>{!! $suratKeteranganPajak->agama !!}</td>
                <td>{!! $suratKeteranganPajak->status_perkawinan !!}</td>
                <td>{!! $suratKeteranganPajak->pekerjaan !!}</td>
                <td>{!! $suratKeteranganPajak->alamat !!}</td>
                <td>{!! $suratKeteranganPajak->status->status !!}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganPajaks.destroy', $suratKeteranganPajak->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('suratKeteranganPajaks.edit', [$suratKeteranganPajak->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
