@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Pajak
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganPajak, ['route' => ['suratKeteranganPajaks.update', $suratKeteranganPajak->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_pajaks.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection