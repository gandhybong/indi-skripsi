@section( 'css' )
    @include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#users-table' ).DataTable({

            });
        } );
    </script>
@endsection

<div class="table-responsive">
    <table class="table" id="users-table">
        <thead>
            <tr>
                <th>NIK</th>
                <th>User Name</th>
                <th>Name</th>
                <th>Alamat</th>
                <th>Tanggal Lahir</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{!! $user->nik !!}</td>
                <td>{!! $user->username !!}</td>
                <td>{!! $user->name !!}</td>
                <td>{!! $user->alamat !!}</td>
                <td>{!! date( 'Y-m-d', strtotime($user->tgl_lahir) ) !!}</td>
                <td>{!! $user->email !!}</td>
                <td>
                    {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
