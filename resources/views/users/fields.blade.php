<div class="col-sm-6">
    <!-- Nik Field -->
    <div class="form-group">
        {!! Form::label('nik', 'Nomor Induk Kependudukan (NIK)') !!}
        {!! Form::text('nik', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
    </div>

    <!-- Name Field -->
    <div class="form-group">
        {!! Form::label('name', 'Nama Pemohon (Sesuai KTP)') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
    </div>

    <!-- Birth Date Field -->
    <div class="form-group">
        {!! Form::label('tgl_lahir', 'Tanggal Lahir') !!}
        {!! Form::date('tgl_lahir', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
    </div>

    <!-- Alamat Field -->
    <div class="form-group">
        {!! Form::label('alamat', 'Alamat') !!}
        {!! Form::textarea('alamat', null, ['class' => 'form-control', 'rows' => '5', 'style' => 'resize: none;']) !!}
    </div>
</div>

<div class="col-sm-6">
    <!-- Username Field -->
    <div class="form-group">
        {!! Form::label('username', 'Username') !!}
        {!! Form::text('username', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
    </div>

    @if ( \Route::current()->getName() != 'users.edit' )
    <!-- Password Field -->
    <div class="form-group">
        {!! Form::label('password', 'Password:') !!}
        {!! Form::text('password', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
    </div>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    @if ( \Route::current()->getName == 'users.create' )
        {!! Form::submit('Buat Pengguna Baru', ['class' => 'btn btn-primary']) !!}
    @else
        {!! Form::submit('Perbaharui Pengguna', ['class' => 'btn btn-primary']) !!}
    @endif
    <a href="{!! route('users.index') !!}" class="btn btn-default">Batal</a>
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
        })
    </script>
@endsection
