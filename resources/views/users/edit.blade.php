@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Ubah Data Pemohon
        </h1>
    </section>

    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}

                        @include('users.fields')

                   {!! Form::close() !!}
               </div>
           </div>
        </div>
    </div>

    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => ['users.update.password', $user->id], 'method' => 'patch']) !!}

                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('password', 'Password') !!}
                                {!! Form::text('password', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>

                        <div class="col-sm-12">
                            {!! Form::submit('Perbaharui Password Pengguna', ['class' => 'btn btn-primary']) !!}
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection