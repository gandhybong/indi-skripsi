<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->user_id }}</p>
</div>

<!-- Nik Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('nik_pihak_pertama', 'Nik Pihak Pertama:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->nik_pihak_pertama }}</p>
</div>

<!-- Nama Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('nama_pihak_pertama', 'Nama Pihak Pertama:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->nama_pihak_pertama }}</p>
</div>

<!-- Jenis Kelamin Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin_pihak_pertama', 'Jenis Kelamin Pihak Pertama:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_pertama }}</p>
</div>

<!-- Tempat Lahir Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir_pihak_pertama', 'Tempat Lahir Pihak Pertama:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->tempat_lahir_pihak_pertama }}</p>
</div>

<!-- Tgl Lahir Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir_pihak_pertama', 'Tgl Lahir Pihak Pertama:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->tgl_lahir_pihak_pertama }}</p>
</div>

<!-- Status Perkawinan Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan_pihak_pertama', 'Status Perkawinan Pihak Pertama:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_pertama }}</p>
</div>

<!-- Pekerjaan Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('pekerjaan_pihak_pertama', 'Pekerjaan Pihak Pertama:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->pekerjaan_pihak_pertama }}</p>
</div>

<!-- Alamat Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('alamat_pihak_pertama', 'Alamat Pihak Pertama:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->alamat_pihak_pertama }}</p>
</div>

<!-- Nik Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('nik_pihak_kedua', 'Nik Pihak Kedua:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->nik_pihak_kedua }}</p>
</div>

<!-- Nama Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('nama_pihak_kedua', 'Nama Pihak Kedua:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->nama_pihak_kedua }}</p>
</div>

<!-- Jenis Kelamin Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin_pihak_kedua', 'Jenis Kelamin Pihak Kedua:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_kedua }}</p>
</div>

<!-- Tempat Lahir Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir_pihak_kedua', 'Tempat Lahir Pihak Kedua:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->tempat_lahir_pihak_kedua }}</p>
</div>

<!-- Tgl Lahir Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir_pihak_kedua', 'Tgl Lahir Pihak Kedua:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->tgl_lahir_pihak_kedua }}</p>
</div>

<!-- Status Perkawinan Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan_pihak_kedua', 'Status Perkawinan Pihak Kedua:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_kedua }}</p>
</div>

<!-- Pekerjaan Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('pekerjaan_pihak_kedua', 'Pekerjaan Pihak Kedua:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->pekerjaan_pihak_kedua }}</p>
</div>

<!-- Alamat Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('alamat_pihak_kedua', 'Alamat Pihak Kedua:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->alamat_pihak_kedua }}</p>
</div>

<!-- Hubungan Antara Kedua Pihak Field -->
<div class="form-group">
    {!! Form::label('hubungan_antara_kedua_pihak', 'Hubungan Antara Kedua Pihak:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->hubungan_antara_kedua_pihak }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratKeteranganHubunganKeluarga->nomor_surat }}</p>
</div>

