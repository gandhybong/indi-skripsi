@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Surat Keterangan Hubungan Keluarga
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($suratKeteranganHubunganKeluarga, ['route' => ['suratKeteranganHubunganKeluargas.update', $suratKeteranganHubunganKeluarga->id], 'method' => 'patch']) !!}

                        @include('surat_keterangan_hubungan_keluargas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection