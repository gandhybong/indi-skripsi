<!-- Nik Pihak Pertama Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nik_pihak_pertama', 'Nik Pihak Pertama:') !!}
    {!! Form::text('nik_pihak_pertama', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Pihak Pertama Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama_pihak_pertama', 'Nama Pihak Pertama:') !!}
    {!! Form::text('nama_pihak_pertama', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Kelamin Pihak Pertama Field -->
<div class="form-group col-sm-12">
    <label for="jenis_kelamin_pihak_pertama">
        Jenis Kelamin Pihak Pertama
    </label>
    @isset($suratKeteranganHubunganKeluarga)
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_pertama" id="jenis_kelamin_pihak_pertama" value="Laki-Laki" 
                {{ (old('jenis_kelamin_pihak_pertama') == 'Laki-Laki' || $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_pertama == 'Laki-Laki') ? "checked" : "" }}
                >Laki-Laki
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_pertama" id="jenis_kelamin_pihak_pertama" value="Perempuan"
                {{ (old('jenis_kelamin_pihak_pertama') == 'Perempuan' || $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_pertama == 'Perempuan') ? "checked" : "" }}
                >Perempuan
            </label>
        </div>
    @endisset

    @empty($suratKeteranganHubunganKeluarga)
    <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_pertama" id="jenis_kelamin_pihak_pertama" value="Laki-Laki" 
                {{ (old('jenis_kelamin_pihak_pertama') == 'Laki-Laki') ? "checked" : "" }}
                >Laki-Laki
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_pertama" id="jenis_kelamin_pihak_pertama" value="Perempuan"
                {{ (old('jenis_kelamin_pihak_pertama') == 'Perempuan') ? "checked" : "" }}
                >Perempuan
            </label>
        </div>
    @endempty
</div>

<!-- Tempat Lahir Pihak Pertama Field -->
<div class="form-group col-sm-12">
    {!! Form::label('tempat_lahir_pihak_pertama', 'Tempat Lahir Pihak Pertama:') !!}
    {!! Form::text('tempat_lahir_pihak_pertama', null, ['class' => 'form-control']) !!}
</div>

<!-- Tgl Lahir Pihak Pertama Field -->
<div class="form-group col-sm-12">
    {!! Form::label('tgl_lahir_pihak_pertama', 'Tgl Lahir Pihak Pertama:') !!}
    {!! Form::date('tgl_lahir_pihak_pertama', null, ['class' => 'form-control','id'=>'tgl_lahir_pihak_pertama']) !!}
</div>

<!-- Status Perkawinan Pihak Pertama Field -->
<div class="form-group col-sm-12">
    <label for="status_perkawinan_pihak_pertama">
        Status Perkawinan 
    </label>
    @isset($suratKeteranganHubunganKeluarga)
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Belum Kawin" {{ (old('status_perkawinan_pihak_pertama') == 'Belum Kawin') || $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_pertama == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Kawin" {{ (old('status_perkawinan_pihak_pertama') == 'Kawin') || $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_pertama == 'Kawin' ? "checked" : "" }}>Kawin
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Cerai Hidup" {{ (old('status_perkawinan_pihak_pertama') == 'Cerai Hidup') || $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_pertama == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Cerai Mati" {{ (old('status_perkawinan_pihak_pertama') == 'Cerai Mati') || $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_pertama == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
            </label>
        </div>
    @endisset

    @empty($suratKeteranganHubunganKeluarga)
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Belum Kawin" {{ (old('status_perkawinan_pihak_pertama') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Kawin" {{ (old('status_perkawinan_pihak_pertama') == 'Kawin') ? "checked" : "" }}>Kawin
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Cerai Hidup" {{ (old('status_perkawinan_pihak_pertama') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_pertama" id="status_perkawinan_pihak_pertama" value="Cerai Mati" {{ (old('status_perkawinan_pihak_pertama') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
            </label>
        </div>
    @endempty
</div>

<!-- Pekerjaan Pihak Pertama Field -->
<div class="form-group col-sm-12">
    {!! Form::label('pekerjaan_pihak_pertama', 'Pekerjaan Pihak Pertama:') !!}
    {!! Form::text('pekerjaan_pihak_pertama', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamat Pihak Pertama Field -->
<div class="form-group col-sm-12">
    {!! Form::label('alamat_pihak_pertama', 'Alamat Pihak Pertama:') !!}
    {!! Form::text('alamat_pihak_pertama', null, ['class' => 'form-control']) !!}
</div>

<!-- Nik Pihak Kedua Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nik_pihak_kedua', 'Nik Pihak Kedua:') !!}
    {!! Form::text('nik_pihak_kedua', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Pihak Kedua Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama_pihak_kedua', 'Nama Pihak Kedua:') !!}
    {!! Form::text('nama_pihak_kedua', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Kelamin Pihak Kedua Field -->
<div class="form-group col-sm-12">
    <label for="jenis_kelamin_pihak_kedua">
        Jenis Kelamin Pihak Kedua
    </label>
    @isset($suratKeteranganHubunganKeluarga)
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Laki-Laki" 
                {{ (old('jenis_kelamin_pihak_kedua') == 'Laki-Laki' || $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_kedua == 'Laki-Laki') ? "checked" : "" }}
                >Laki-Laki
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Perempuan"
                {{ (old('jenis_kelamin_pihak_kedua') == 'Perempuan' || $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_kedua == 'Perempuan') ? "checked" : "" }}
                >Perempuan
            </label>
        </div>
    @endisset

    @empty($suratKeteranganHubunganKeluarga)
    <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Laki-Laki" 
                {{ (old('jenis_kelamin_pihak_kedua') == 'Laki-Laki') ? "checked" : "" }}
                >Laki-Laki
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Perempuan"
                {{ (old('jenis_kelamin_pihak_kedua') == 'Perempuan') ? "checked" : "" }}
                >Perempuan
            </label>
        </div>
    @endempty
</div>

<!-- Tempat Lahir Pihak Kedua Field -->
<div class="form-group col-sm-12">
    {!! Form::label('tempat_lahir_pihak_kedua', 'Tempat Lahir Pihak Kedua:') !!}
    {!! Form::text('tempat_lahir_pihak_kedua', null, ['class' => 'form-control']) !!}
</div>

<!-- Tgl Lahir Pihak Kedua Field -->
<div class="form-group col-sm-12">
    {!! Form::label('tgl_lahir_pihak_kedua', 'Tgl Lahir Pihak Kedua:') !!}
    {!! Form::date('tgl_lahir_pihak_kedua', null, ['class' => 'form-control','id'=>'tgl_lahir_pihak_kedua']) !!}
</div>

<!-- Status Perkawinan Pihak Kedua Field -->
<div class="form-group col-sm-12">
    <label for="status_perkawinan_pihak_kedua">
        Status Perkawinan 
    </label>
    @isset($suratKeteranganHubunganKeluarga)
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Belum Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Belum Kawin') || $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_kedua == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Kawin') || $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_kedua == 'Kawin' ? "checked" : "" }}>Kawin
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Hidup" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Hidup') || $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_kedua == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Mati" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Mati') || $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_kedua == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
            </label>
        </div>
    @endisset

    @empty($suratKeteranganHubunganKeluarga)
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Belum Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Kawin') ? "checked" : "" }}>Kawin
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Hidup" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Mati" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
            </label>
        </div>
    @endempty
</div>

<!-- Pekerjaan Pihak Kedua Field -->
<div class="form-group col-sm-12">
    {!! Form::label('pekerjaan_pihak_kedua', 'Pekerjaan Pihak Kedua:') !!}
    {!! Form::text('pekerjaan_pihak_kedua', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamat Pihak Kedua Field -->
<div class="form-group col-sm-12">
    {!! Form::label('alamat_pihak_kedua', 'Alamat Pihak Kedua:') !!}
    {!! Form::text('alamat_pihak_kedua', null, ['class' => 'form-control']) !!}
</div>

<!-- Hubungan Antara Kedua Pihak Field -->
<div class="form-group col-sm-12">
    {!! Form::label('hubungan_antara_kedua_pihak', 'Hubungan Antara Kedua Pihak:') !!}
    {!! Form::text('hubungan_antara_kedua_pihak', null, ['class' => 'form-control']) !!}
</div>

<@if(Route::current()->getName() == 'suratKeteranganHubunganKeluargas.edit')
<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status_id', 'Status Id') !!}
    <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganHubunganKeluarga->status->status}}" readonly>
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    @if(Route::current()->getName() == 'suratKeteranganHubunganKeluargas.edit')
        @if ( $suratKeteranganHubunganKeluarga->status->status == "Draf" )
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
                {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
                <a href="{!! route('suratKeteranganHubunganKeluargas.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @elseif ( $suratKeteranganHubunganKeluarga->status->status == "Diterima" )
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                <a href="{{ route('hubunganKeluarga.print', ['id' => $suratKeteranganHubunganKeluarga->id]) }}" class="btn btn-success" target="_blank" id="print">
                    Print
                </a>
                <a href="{!! route('suratKeteranganHubunganKeluargas.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @else
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                <a href="{!! route('suratKeteranganHubunganKeluargas.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @endif
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Buat Surat Keterangan Janda', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('suratKeteranganHubunganKeluargas.index') }}" class="btn btn-default">Cancel</a>
        </div>
    @endif
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_lahir_pihak_pertama').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#tgl_lahir_pihak_kedua').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection