@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganHubunganKeluargas-table' ).DataTable({

        });
    } );
</script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKeteranganHubunganKeluargas-table">
        <thead>
            <tr>
                <th>User Id</th>
                <th>Nik P.Pertama</th>
                <th>Nama P.Pertama</th>
                <th>Jenis Kelamin P.Pertama</th>
                <th>Tempat Lahir P.Pertama</th>
                <th>Tgl Lahir P.Pertama</th>
                <th>Status Perkawinan P.Pertama</th>
                <th>Pekerjaan P.Pertama</th>
                <th>Alamat P.Pertama</th>
                <th>Nik P.Kedua</th>
                <th>Nama P.Kedua</th>
                <th>Jenis Kelamin P.Kedua</th>
                <th>Tempat Lahir P.Kedua</th>
                <th>Tgl Lahir P.Kedua</th>
                <th>Status Perkawinan P.Kedua</th>
                <th>Pekerjaan P.Kedua</th>
                <th>Alamat P.Kedua</th>
                <th>Hubungan Antara Kedua Pihak</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganHubunganKeluargas as $suratKeteranganHubunganKeluarga)
            <tr>
                <td>{{ $suratKeteranganHubunganKeluarga->user_id }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->nik_pihak_pertama }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->nama_pihak_pertama }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_pertama }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->tempat_lahir_pihak_pertama }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->tgl_lahir_pihak_pertama }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_pertama }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->pekerjaan_pihak_pertama }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->alamat_pihak_pertama }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->nik_pihak_kedua }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->nama_pihak_kedua }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->jenis_kelamin_pihak_kedua }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->tempat_lahir_pihak_kedua }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->tgl_lahir_pihak_kedua }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->status_perkawinan_pihak_kedua }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->pekerjaan_pihak_kedua }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->alamat_pihak_kedua }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->hubungan_antara_kedua_pihak }}</td>
                <td>{{ $suratKeteranganHubunganKeluarga->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganHubunganKeluargas.destroy', $suratKeteranganHubunganKeluarga->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganHubunganKeluargas.edit', [$suratKeteranganHubunganKeluarga->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
