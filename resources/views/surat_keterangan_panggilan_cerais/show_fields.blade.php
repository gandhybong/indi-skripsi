<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->user_id }}</p>
</div>

<!-- Nik Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('nik_pihak_pertama', 'Nik Pihak Pertama:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->nik_pihak_pertama }}</p>
</div>

<!-- Nama Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('nama_pihak_pertama', 'Nama Pihak Pertama:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->nama_pihak_pertama }}</p>
</div>

<!-- Umur Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('umur_pihak_pertama', 'Umur Pihak Pertama:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->umur_pihak_pertama }}</p>
</div>

<!-- Pekerjaan Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('pekerjaan_pihak_pertama', 'Pekerjaan Pihak Pertama:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->pekerjaan_pihak_pertama }}</p>
</div>

<!-- Alamat Pihak Pertama Field -->
<div class="form-group">
    {!! Form::label('alamat_pihak_pertama', 'Alamat Pihak Pertama:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->alamat_pihak_pertama }}</p>
</div>

<!-- Nik Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('nik_pihak_kedua', 'Nik Pihak Kedua:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->nik_pihak_kedua }}</p>
</div>

<!-- Nama Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('nama_pihak_kedua', 'Nama Pihak Kedua:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->nama_pihak_kedua }}</p>
</div>

<!-- Umur Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('umur_pihak_kedua', 'Umur Pihak Kedua:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->umur_pihak_kedua }}</p>
</div>

<!-- Pekerjaan Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('pekerjaan_pihak_kedua', 'Pekerjaan Pihak Kedua:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->pekerjaan_pihak_kedua }}</p>
</div>

<!-- Alamat Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('alamat_pihak_kedua', 'Alamat Pihak Kedua:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->alamat_pihak_kedua }}</p>
</div>

<!-- Yang Ditinggalkan Field -->
<div class="form-group">
    {!! Form::label('yang_ditinggalkan', 'Yang Ditinggalkan:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->yang_ditinggalkan }}</p>
</div>

<!-- Lama Dittingal Field -->
<div class="form-group">
    {!! Form::label('lama_ditinggal', 'Lama Dittingal:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->lama_ditinggal }}</p>
</div>

<!-- Nama Pembina Tki Field -->
<div class="form-group">
    {!! Form::label('nama_pembina_TKI', 'Nama Pembina Tki:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->nama_pembina_TKI }}</p>
</div>

<!-- Nip Pembina Tki Field -->
<div class="form-group">
    {!! Form::label('nip_pembina_TKI', 'Nip Pembina Tki:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->nip_pembina_TKI }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratKeteranganPanggilanCerai->nomor_surat }}</p>
</div>

