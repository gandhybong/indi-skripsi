<div class="form-group col-sm-6">
    <!-- Nik Pihak Pertama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik_pihak_pertama', 'Nik Pihak Pertama:') !!}
        {!! Form::text('nik_pihak_pertama', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Nama Pihak Pertama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama_pihak_pertama', 'Nama Pihak Pertama:') !!}
        {!! Form::text('nama_pihak_pertama', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Umur Pihak Pertama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('umur_pihak_pertama', 'Umur Pihak Pertama:') !!}
        {!! Form::number('umur_pihak_pertama', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Pekerjaan Pihak Pertama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('pekerjaan_pihak_pertama', 'Pekerjaan Pihak Pertama:') !!}
        {!! Form::text('pekerjaan_pihak_pertama', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Alamat Pihak Pertama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat_pihak_pertama', 'Alamat Pihak Pertama:') !!}
        {!! Form::text('alamat_pihak_pertama', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Nik Pihak Kedua Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik_pihak_kedua', 'Nik Pihak Kedua:') !!}
        {!! Form::text('nik_pihak_kedua', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Nama Pihak Kedua Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama_pihak_kedua', 'Nama Pihak Kedua:') !!}
        {!! Form::text('nama_pihak_kedua', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Umur Pihak Kedua Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('umur_pihak_kedua', 'Umur Pihak Kedua:') !!}
        {!! Form::number('umur_pihak_kedua', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Pekerjaan Pihak Kedua Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('pekerjaan_pihak_kedua', 'Pekerjaan Pihak Kedua:') !!}
        {!! Form::text('pekerjaan_pihak_kedua', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Alamat Pihak Kedua Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat_pihak_kedua', 'Alamat Pihak Kedua:') !!}
        {!! Form::text('alamat_pihak_kedua', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Yang Ditinggalkan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('yang_ditinggalkan', 'Yang Ditinggalkan (istri/suami) :') !!}
        {!! Form::text('yang_ditinggalkan', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Lama Dittingal Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('lama_ditinggal', 'Lama Dittingal dalam tahun:') !!}
        {!! Form::number('lama_ditinggal', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Nama Pembina Tki Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama_pembina_TKI', 'Nama Pembina Tki:') !!}
        {!! Form::text('nama_pembina_TKI', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Nip Pembina Tki Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nip_pembina_TKI', 'Nip Pembina Tki:') !!}
        {!! Form::text('nip_pembina_TKI', null, ['class' => 'form-control']) !!}
    </div>
</div>

<@if(Route::current()->getName() == 'suratKeteranganPanggilanCerais.edit')
<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status_id', 'Status Id') !!}
    <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganPanggilanCerai->status->status}}" readonly>
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    @if(Route::current()->getName() == 'suratKeteranganPanggilanCerais.edit')
        @if ( $suratKeteranganPanggilanCerai->status->status == "Draf" )
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
                {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
                <a href="{!! route('suratKeteranganPanggilanCerais.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @elseif ( $suratKeteranganPanggilanCerai->status->status == "Diterima" )
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                <a href="{{ route('PanggilanCerai.print', ['id' => $suratKeteranganPanggilanCerai->id]) }}" class="btn btn-success" target="_blank" id="print">
                    Print
                </a>
                <a href="{!! route('suratKeteranganPanggilanCerais.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @else
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                <a href="{!! route('suratKeteranganPanggilanCerais.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @endif
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('suratKeteranganPanggilanCerais.index') }}" class="btn btn-default">Cancel</a>
        </div>
    @endif
</div>

@section('scripts')
    <script type="text/javascript">
        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection
