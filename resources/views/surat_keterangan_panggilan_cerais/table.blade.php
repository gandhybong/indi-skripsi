@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganPanggilanCerais-table' ).DataTable({

        });
    } );
</script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKeteranganPanggilanCerais-table">
        <thead>
            <tr>
                <th>User Id</th>
                <th>Nik Pihak Pertama</th>
                <th>Nama Pihak Pertama</th>
                <th>Umur Pihak Pertama</th>
                <th>Pekerjaan Pihak Pertama</th>
                <th>Alamat Pihak Pertama</th>
                <th>Nik Pihak Kedua</th>
                <th>Nama Pihak Kedua</th>
                <th>Umur Pihak Kedua</th>
                <th>Pekerjaan Pihak Kedua</th>
                <th>Alamat Pihak Kedua</th>
                <th>Yang Ditinggalkan</th>
                <th>Lama Dittingal</th>
                <th>Nama Pembina Tki</th>
                <th>Nip Pembina Tki</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganPanggilanCerais as $suratKeteranganPanggilanCerai)
            <tr>
                <td>{{ $suratKeteranganPanggilanCerai->user_id }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->nik_pihak_pertama }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->nama_pihak_pertama }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->umur_pihak_pertama }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->pekerjaan_pihak_pertama }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->alamat_pihak_pertama }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->nik_pihak_kedua }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->nama_pihak_kedua }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->umur_pihak_kedua }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->pekerjaan_pihak_kedua }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->alamat_pihak_kedua }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->yang_ditinggalkan }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->lama_ditinggal }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->nama_pembina_TKI }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->nip_pembina_TKI }}</td>
                <td>{{ $suratKeteranganPanggilanCerai->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganPanggilanCerais.destroy', $suratKeteranganPanggilanCerai->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganPanggilanCerais.edit', [$suratKeteranganPanggilanCerai->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
