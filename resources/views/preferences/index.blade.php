@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Preferensi</h1>
    </section>
    <br>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <form action="{!! route('preferences.store') !!}" method="POST">
                    @csrf
                    <div class="form-group col-sm-6">
                        <label>Provinsi</label> 
                        <input type="text" name="Provinsi" id="Provinsi" class="form-control" value="{{$preferences['Provinsi']['value']}}">

                        <label>Kabupaten</label> 
                        <input type="text" name="Kabupaten" id="Kabupaten" class="form-control" value="{{$preferences['Kabupaten']['value']}}">

                        <label>Kecamatan</label> 
                        <input type="text" name="Kecamatan" id="Kecamatan" class="form-control" value="{{$preferences['Kecamatan']['value']}}">

                        <label>Desa</label> 
                        <input type="text" name="Desa" id="Desa" class="form-control" value="{{$preferences['Desa']['value']}}">

                        <label>Alamat</label> 
                        <input type="text" name="Alamat" id="Alamat" class="form-control" value="{{$preferences['Alamat']['value']}}">

                        <label>Kepala Desa</label> 
                        <input type="text" name="Kepala Desa" id="Kepala Desa" class="form-control" value="{{$preferences['Kepala Desa']['value']}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <input type="submit" name="submit" class="btn btn-primary" value="Simpan Preferensi">
                        <a href="{!! route('preferences.index') !!}" class="btn btn-default">Batal</a>
                    </div>
                </form>    
            </div>
        </div>
    </div>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="col-sm-12">
                    <h3 class="box-title">Pengelolaan Logo Desa</h3>
                    {!! Form::open(['route' => 'preferences.update.logo', 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        <div class="form-group">
                            {!! Form::label('', 'Logo saat ini') !!}
                            <br>
                            @if ( $preferences['Logo']['value'] == '' )
                            <img src="{{ asset('images/assets/placeholder.png') }}" width="300px">
                            @else
                            <img src="{{ asset($preferences['Logo']['value']) }}" width="300px">
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('logo', 'Pilih Logo') !!}
                            {!! Form::file('logo') !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Simpan Logo', ['class' => 'btn btn-primary']) !!}
                            {!! Form::button('Hapus Logo', ['class' => 'btn btn-danger', 'onclick' => 'destroyLogo()']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => 'preferences.destroy.logo', 'method' => 'delete', 'id' => 'form-destroy-logo']) !!}
    {!! Form::close() !!}
@endsection

@section( 'scripts' )
    <script type="text/javascript">
        function destroyLogo()
        {
            if ( confirm('Anda yakin akan menghapus logo?') )
            {
                $( '#form-destroy-logo' ).submit();
            }
        }
    </script>
@endsection
