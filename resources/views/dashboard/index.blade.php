@extends('layouts.app')

@section('css')
<style>
    .chart-style{
        height: 40vh;
        width : 80vw;
        position: relative;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <canvas id="myChart" style="position: relative; height:40vh; width:80vw">
            <p>
                Web Browser Anda Tidak Mendukung Tampilan Ini
            </p>
        </canvas>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <a class="btn btn-primary" href="{{ route('dashboard.index') }}" role="button">
            Surat Masuk Hari Ini
        </a>            
    </div>

    <div class="col-sm-3">
        <a class="btn btn-primary" href="{{ route('dashboard.index',['name' => 'minggu']) }}" role="button">
            Surat Masuk Minggu Ini
        </a>
    </div>

    <div class="col-sm-3">
        <a class="btn btn-primary" href="{{ route('dashboard.index',['name' => 'bulan']) }}" role="button">
            Surat Masuk Bulan Ini
        </a>
    </div>

    <div class="col-sm-3">
        <a class="btn btn-primary" href="{{ route('dashboard.index',['name' => 'tahun']) }}" role="button">
            Surat Masuk Tahun Ini
        </a>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('vendor/chartjs/Chart.bundle.js') }}"></script>
    <script src="{{ asset('vendor/chartjs/Chart.bundle.min.js') }}"></script>
    <script>
    var type = 'pie';
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type,
        data: {
            labels: @json($labelsSurat),
            datasets: [{
                label: '{{ $labelGrafik }}',
                data: @json($dataSurat),
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    function addData() {
        myChart.data.labels.pop();
        myChart.data.datasets.forEach((dataset) => {
            dataset.data.pop();
        });
        myChart.update();
    }

    function resetData(){
        myChart.reset();
        myChart.update();
    }
    </script>
@endsection