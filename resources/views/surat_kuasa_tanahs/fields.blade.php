@if(Route::current()->getName() == 'suratKuasaTanahs.edit')
    <!-- Kode Field -->
<div class="col-sm-6">
    <div class="form-group col-sm-12">
        {!! Form::label('kode', 'Kode:') !!}
        {!! Form::text('kode', null, ['class' => 'form-control','readonly']) !!}
    </div>
</div>
<div class="clearfix"></div>
@endif

<div class="col-sm-6">
    <div class="col-sm-12">
        <h3>
            Data Diri Pihak Pertama
        </h3>
    </div>

    <div class="col-sm-6">
        <!-- Nik Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('nik', 'Nik:') !!}
            {!! Form::text('nik', null, ['class' => 'form-control']) !!}
        </div>
    
        <!-- Nama Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('nama', 'Nama:') !!}
            {!! Form::text('nama', null, ['class' => 'form-control']) !!}
        </div>
    
        <!-- Jenis Kelamin Field -->
        <div class="form-group col-sm-12">
            <label for="jenis_kelamin">
                Jenis Kelamin
            </label>
            
            @isset($suratKuasaTanah)
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                        {{ (old('jenis_kelamin') == 'Laki-Laki' || $suratKuasaTanah->jenis_kelamin == 'Laki-Laki') ? "checked" : "" }}
                        >Laki-Laki
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                        {{ (old('jenis_kelamin') == 'Perempuan' || $suratKuasaTanah->jenis_kelamin == 'Perempuan') ? "checked" : "" }}
                        >Perempuan
                    </label>
                </div>
            @endisset
        
            @empty($suratKuasaTanah)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
            @endempty
        </div>
    
        <!-- Tempat Lahir Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
            {!! Form::text('tempat_lahir', null, ['class' => 'form-control']) !!}
        </div>
        <!-- Tgl Lahir Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
            {!! Form::date('tgl_lahir', null, ['class' => 'form-control','id'=>'tgl_lahir']) !!}
        </div>
    </div>
    
    <div class="col-sm-6">
        <div class="form-group col-sm-12">
            <label for="status_perkawinan">
                Status Perkawinan 
            </label>
            @isset($suratKuasaTanah)
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') || $suratKuasaTanah->status_perkawinan == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') || $suratKuasaTanah->status_perkawinan == 'Kawin' ? "checked" : "" }}>Kawin
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') || $suratKuasaTanah->status_perkawinan == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') || $suratKuasaTanah->status_perkawinan == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
                    </label>
                </div>
            @endisset
    
            @empty($suratKuasaTanah)
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') ? "checked" : "" }}>Kawin
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
                    </label>
                </div>
            @endempty
        </div>
        
        <!-- Pekerjaan Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
            {!! Form::text('pekerjaan', null, ['class' => 'form-control']) !!}
        </div>
    
        <!-- Alamat Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('alamat', 'Alamat:') !!}
            {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="col-sm-6">
    <h3>
        Data Diri Pihak Kedua
    </h3>
    <div class="col-sm-6">
        <!-- Nik Pihak Kedua Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('nik_pihak_kedua', 'Nik Pihak Kedua:') !!}
            {!! Form::text('nik_pihak_kedua', null, ['class' => 'form-control']) !!}
        </div>
    
        <!-- Nama Pihak Kedua Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('nama_pihak_kedua', 'Nama Pihak Kedua:') !!}
            {!! Form::text('nama_pihak_kedua', null, ['class' => 'form-control']) !!}
        </div>
    
        <!-- Jenis Kelamin Pihak Kedua Field -->
        <div class="form-group col-sm-12">
            <label for="jenis_kelamin_pihak_kedua">
                Jenis Kelamin
            </label>
            
            @isset($suratKuasaTanah)
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Laki-Laki" 
                        {{ (old('jenis_kelamin_pihak_kedua') == 'Laki-Laki' || $suratKuasaTanah->jenis_kelamin_pihak_kedua == 'Laki-Laki') ? "checked" : "" }}
                        >Laki-Laki
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Perempuan"
                        {{ (old('jenis_kelamin_pihak_kedua') == 'Perempuan' || $suratKuasaTanah->jenis_kelamin_pihak_kedua == 'Perempuan') ? "checked" : "" }}
                        >Perempuan
                    </label>
                </div>
            @endisset
        
            @empty($suratKuasaTanah)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Laki-Laki" 
                    {{ (old('jenis_kelamin_pihak_kedua') == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin_pihak_kedua" id="jenis_kelamin_pihak_kedua" value="Perempuan"
                    {{ (old('jenis_kelamin_pihak_kedua') == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
            @endempty
        </div>
    
        <!-- Tempat Lahir Pihak Kedua Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('tempat_lahir_pihak_kedua', 'Tempat Lahir Pihak Kedua:') !!}
            {!! Form::text('tempat_lahir_pihak_kedua', null, ['class' => 'form-control']) !!}
        </div>
    
        <!-- Tgl Lahir Pihak Kedua Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('tgl_lahir_pihak_kedua', 'Tgl Lahir Pihak Kedua:') !!}
            {!! Form::date('tgl_lahir_pihak_kedua', null, ['class' => 'form-control','id'=>'tgl_lahir_pihak_kedua']) !!}
        </div>
    </div>

    <div class="col-sm-6">
        <!-- Status Perkawinan Pihak Kedua Field -->
        <div class="form-group col-sm-12">
            <label for="status_perkawinan_pihak_kedua">
                Status Perkawinan 
            </label>
            @isset($suratKuasaTanah)
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Belum Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Belum Kawin') || $suratKuasaTanah->status_perkawinan_pihak_kedua == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Kawin') || $suratKuasaTanah->status_perkawinan_pihak_kedua == 'Kawin' ? "checked" : "" }}>Kawin
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Hidup" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Hidup') || $suratKuasaTanah->status_perkawinan_pihak_kedua == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Mati" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Mati') || $suratKuasaTanah->status_perkawinan_pihak_kedua == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
                    </label>
                </div>
            @endisset
    
            @empty($suratKuasaTanah)
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Belum Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Kawin" {{ (old('status_perkawinan_pihak_kedua') == 'Kawin') ? "checked" : "" }}>Kawin
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Hidup" {{ (old('status_perkawinan_pihak_kedua') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="status_perkawinan_pihak_kedua" id="status_perkawinan_pihak_kedua" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
                    </label>
                </div>
            @endempty
        </div>

        <!-- Pekerjaan Pihak Kedua Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('pekerjaan_pihak_kedua', 'Pekerjaan Pihak Kedua:') !!}
            {!! Form::text('pekerjaan_pihak_kedua', null, ['class' => 'form-control']) !!}
        </div>
    
        <!-- Alamat Pihak Kedua Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('alamat_pihak_kedua', 'Alamat Pihak Kedua:') !!}
            {!! Form::textarea('alamat_pihak_kedua', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="col-sm-6">
    <!-- Status Kedua Pihak Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('status_kedua_pihak', 'Status Kedua Pihak:') !!}
        {!! Form::text('status_kedua_pihak', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Jenis Tanah Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('jenis_tanah', 'Jenis Tanah:') !!}
        {!! Form::text('jenis_tanah', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Nomor Sertifikat Tanah Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nomor_sertifikat_tanah', 'Nomor Sertifikat Tanah:') !!}
        {!! Form::text('nomor_sertifikat_tanah', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Atas Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('atas_nama', 'Atas Nama:') !!}
        {!! Form::text('atas_nama', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="col-sm-6">
    <!-- Tahun Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('tahun', 'Tahun:') !!}
        {!! Form::text('tahun', null, ['class' => 'form-control']) !!}
    </div>
    
    <!-- Luas Tanah Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('luas_tanah', 'Luas Tanah:') !!}
        {!! Form::text('luas_tanah', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Maksud Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('maksud', 'Maksud:') !!}
        {!! Form::text('maksud', null, ['class' => 'form-control']) !!}
    </div>
    
    @if(Route::current()->getName() == 'suratKuasaTanahs.edit')
        <!-- Status Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status', 'Status') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratKuasaTanah->status->status}}" readonly>
        </div>
    @endif
</div>

<div class="col-sm-12">
@if(Route::current()->getName() == 'suratKuasaTanahs.edit')
    @if ( $suratKuasaTanah->status->status == "Draf" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
            {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
            <a href="{!! route('suratKuasaTanahs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @elseif ( $suratKuasaTanah->status->status == "Diterima" )
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{{ route('kuasa.print', ['id' => $suratKuasaTanah->id]) }}" class="btn btn-success" target="_blank" id="print">
                Print
            </a>
            <a href="{!! route('suratKuasaTanahs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            <a href="{!! route('suratKuasaTanahs.index') !!}" class="btn btn-default">Kembali</a>
        </div>
    @endif
@else
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Buat Surat Kuasa Tanah', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('suratKuasaTanahs.index') !!}" class="btn btn-default">kembali</a>
    </div>
@endif
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        $('#tgl_lahir_pihak_kedua').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection