@section( 'css' )
@include( 'layouts.datatables_css' )
@endsection

@section( 'scripts' )
    @include( 'layouts.datatables_js' )

    <script type="text/javascript">
        $( document ).ready( function() {
            $( '#suratKuasaTanahs-table' ).DataTable({
                responsive: true
            });
        } );
    </script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKuasaTanahs-table">
        <thead>
            <tr>
                <th>Kode</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Tgl Lahir</th>
                <th>Status Perkawinan</th>
                <th>Pekerjaan</th>
                <th>Alamat</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKuasaTanahs as $suratKuasaTanah)
            <tr>
                <td>{{ $suratKuasaTanah->kode }}</td>
                <td>{{ $suratKuasaTanah->nik }}</td>
                <td>{{ $suratKuasaTanah->nama }}</td>
                <td>{{ $suratKuasaTanah->jenis_kelamin }}</td>
                <td>{{ $suratKuasaTanah->tempat_lahir }}</td>
                <td>{{ $suratKuasaTanah->tgl_lahir }}</td>
                <td>{{ $suratKuasaTanah->status_perkawinan }}</td>
                <td>{{ $suratKuasaTanah->pekerjaan }}</td>
                <td>{{ $suratKuasaTanah->alamat }}</td>
                <td>{{ $suratKuasaTanah->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKuasaTanahs.destroy', $suratKuasaTanah->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKuasaTanahs.edit', [$suratKuasaTanah->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
