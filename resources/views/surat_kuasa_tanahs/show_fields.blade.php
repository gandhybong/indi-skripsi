<!-- Kode Field -->
<div class="form-group">
    {!! Form::label('kode', 'Kode:') !!}
    <p>{{ $suratKuasaTanah->kode }}</p>
</div>

<!-- Nik Field -->
<div class="form-group">
    {!! Form::label('nik', 'Nik:') !!}
    <p>{{ $suratKuasaTanah->nik }}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{{ $suratKuasaTanah->nama }}</p>
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{{ $suratKuasaTanah->jenis_kelamin }}</p>
</div>

<!-- Tempat Lahir Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
    <p>{{ $suratKuasaTanah->tempat_lahir }}</p>
</div>

<!-- Tgl Lahir Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
    <p>{{ $suratKuasaTanah->tgl_lahir }}</p>
</div>

<!-- Status Perkawinan Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan', 'Status Perkawinan:') !!}
    <p>{{ $suratKuasaTanah->status_perkawinan }}</p>
</div>

<!-- Pekerjaan Field -->
<div class="form-group">
    {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
    <p>{{ $suratKuasaTanah->pekerjaan }}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{{ $suratKuasaTanah->alamat }}</p>
</div>

<!-- Nik Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('nik_pihak_kedua', 'Nik Pihak Kedua:') !!}
    <p>{{ $suratKuasaTanah->nik_pihak_kedua }}</p>
</div>

<!-- Nama Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('nama_pihak_kedua', 'Nama Pihak Kedua:') !!}
    <p>{{ $suratKuasaTanah->nama_pihak_kedua }}</p>
</div>

<!-- Jenis Kelamin Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin_pihak_kedua', 'Jenis Kelamin Pihak Kedua:') !!}
    <p>{{ $suratKuasaTanah->jenis_kelamin_pihak_kedua }}</p>
</div>

<!-- Tempat Lahir Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('tempat_lahir_pihak_kedua', 'Tempat Lahir Pihak Kedua:') !!}
    <p>{{ $suratKuasaTanah->tempat_lahir_pihak_kedua }}</p>
</div>

<!-- Tgl Lahir Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('tgl_lahir_pihak_kedua', 'Tgl Lahir Pihak Kedua:') !!}
    <p>{{ $suratKuasaTanah->tgl_lahir_pihak_kedua }}</p>
</div>

<!-- Status Perkawinan Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('status_perkawinan_pihak_kedua', 'Status Perkawinan Pihak Kedua:') !!}
    <p>{{ $suratKuasaTanah->status_perkawinan_pihak_kedua }}</p>
</div>

<!-- Pekerjaan Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('pekerjaan_pihak_kedua', 'Pekerjaan Pihak Kedua:') !!}
    <p>{{ $suratKuasaTanah->pekerjaan_pihak_kedua }}</p>
</div>

<!-- Alamat Pihak Kedua Field -->
<div class="form-group">
    {!! Form::label('alamat_pihak_kedua', 'Alamat Pihak Kedua:') !!}
    <p>{{ $suratKuasaTanah->alamat_pihak_kedua }}</p>
</div>

<!-- Status Kedua Pihak Field -->
<div class="form-group">
    {!! Form::label('status_kedua_pihak', 'Status Kedua Pihak:') !!}
    <p>{{ $suratKuasaTanah->status_kedua_pihak }}</p>
</div>

<!-- Jenis Tanah Field -->
<div class="form-group">
    {!! Form::label('jenis_tanah', 'Jenis Tanah:') !!}
    <p>{{ $suratKuasaTanah->jenis_tanah }}</p>
</div>

<!-- Nomor Hak Milik Field -->
<div class="form-group">
    {!! Form::label('nomor_hak_milik', 'Nomor Hak Milik:') !!}
    <p>{{ $suratKuasaTanah->nomor_hak_milik }}</p>
</div>

<!-- Atas Nama Field -->
<div class="form-group">
    {!! Form::label('atas_nama', 'Atas Nama:') !!}
    <p>{{ $suratKuasaTanah->atas_nama }}</p>
</div>

<!-- Tahun Field -->
<div class="form-group">
    {!! Form::label('tahun', 'Tahun:') !!}
    <p>{{ $suratKuasaTanah->tahun }}</p>
</div>

<!-- Luas Field -->
<div class="form-group">
    {!! Form::label('luas', 'Luas:') !!}
    <p>{{ $suratKuasaTanah->luas }}</p>
</div>

<!-- Maksud Field -->
<div class="form-group">
    {!! Form::label('maksud', 'Maksud:') !!}
    <p>{{ $suratKuasaTanah->maksud }}</p>
</div>

<!-- Status Id Field -->
<div class="form-group">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $suratKuasaTanah->status_id }}</p>
</div>

<!-- Tanggal Pengajuan Field -->
<div class="form-group">
    {!! Form::label('tanggal_pengajuan', 'Tanggal Pengajuan:') !!}
    <p>{{ $suratKuasaTanah->tanggal_pengajuan }}</p>
</div>

<!-- Tanggal Terbit Field -->
<div class="form-group">
    {!! Form::label('tanggal_terbit', 'Tanggal Terbit:') !!}
    <p>{{ $suratKuasaTanah->tanggal_terbit }}</p>
</div>

<!-- Nomor Surat Field -->
<div class="form-group">
    {!! Form::label('nomor_surat', 'Nomor Surat:') !!}
    <p>{{ $suratKuasaTanah->nomor_surat }}</p>
</div>

