@extends('layouts.app')

@section('content')
    <div class="jumbotron">
        <div class="col-sm-12 col-md-4">
            <img src="{{ asset('images/icons/icon-512x512.png') }}" width="100%">
        </div>
        <div class="col-sm-12 col-md-8">
            <h1>Selamat Datang di NusAntara!</h1>
            <p>Indonesia yang baik, dimulai dari desa yang baik. Desa yang lebih modern dan lebih maju. Kami percaya bahwa pengelolaan administrasi yang baik adalah salah satu kunci menuju desa yang lebih baik dan berdikari. <b>NusAntara</b> hadir untuk memberikan dukungan kepada para perangkat desa agar mampu mengelola admistrasi desa dengan lebih kredibel dan terstruktur.</p>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12">
        <div class="antara">
            <a href="https://kalbar.antaranews.com/">
                <img src="https://kalbar.antaranews.com/img/logo_ant_kalbar.png" alt="" target="_BLANK">
            </a>
            <span>kolaborasi Antara Kalbar & Turbin Inovasi Indonesia</span>
        </div>
    </div>
@endsection