@section( 'scripts' )
@include( 'layouts.datatables_js' )

<script type="text/javascript">
    $( document ).ready( function() {
        $( '#suratKeteranganJandas-table' ).DataTable({

        });
    } );
</script>
@endsection
<div class="table-responsive">
    <table class="table" id="suratKeteranganJandas-table">
        <thead>
            <tr>
                <th>Nik</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Tempat Lahir</th>
                <th>Tgl Lahir</th>
                <th>Status Perkawinan</th>
                <th>Kewarganegaraan</th>
                <th>Agama</th>
                <th>Pekerjaan</th>
                <th>Alamat</th>
                <th>Rt</th>
                <th>Rw</th>
                <th>Sebab Cerai</th>
                <th>Administrasi</th>
                <th>Status Surat</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($suratKeteranganJandas as $suratKeteranganJanda)
            <tr>
                <td>{{ $suratKeteranganJanda->nik }}</td>
                <td>{{ $suratKeteranganJanda->nama }}</td>
                <td>{{ $suratKeteranganJanda->jenis_kelamin }}</td>
                <td>{{ $suratKeteranganJanda->tempat_lahir }}</td>
                <td>{{ $suratKeteranganJanda->tgl_lahir }}</td>
                <td>{{ $suratKeteranganJanda->status_perkawinan }}</td>
                <td>{{ $suratKeteranganJanda->kewarganegaraan }}</td>
                <td>{{ $suratKeteranganJanda->agama }}</td>
                <td>{{ $suratKeteranganJanda->pekerjaan }}</td>
                <td>{{ $suratKeteranganJanda->alamat }}</td>
                <td>{{ $suratKeteranganJanda->rt }}</td>
                <td>{{ $suratKeteranganJanda->rw }}</td>
                <td>{{ $suratKeteranganJanda->sebab_cerai }}</td>
                <td>{{ $suratKeteranganJanda->administrasi }}</td>
                <td>{{ $suratKeteranganJanda->status->status }}</td>
                <td>
                    {!! Form::open(['route' => ['suratKeteranganJandas.destroy', $suratKeteranganJanda->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('suratKeteranganJandas.edit', [$suratKeteranganJanda->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
