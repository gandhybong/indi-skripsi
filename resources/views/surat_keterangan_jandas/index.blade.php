@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Surat Keterangan Janda</h1>
    </section>
    <div class="clearfix"></div>
    <br>
    <div class="col-sm-4">
        <a class="btn btn-primary" href="{{ route('suratKeteranganJandas.create') }}">
            Pengajuan Surat Keterangan Janda Baru
        </a>
    </div>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-sm-12">
                    @include('surat_keterangan_jandas.table')
                </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

