<div class="col-sm-6">

    <!-- Nik Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nik', 'Nik:') !!}
        {!! Form::text('nik', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Nama Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('nama', 'Nama:') !!}
        {!! Form::text('nama', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Jenis Kelamin Field -->
    <div class="form-group col-sm-12">
        <label for="jenis_kelamin">
            Jenis Kelamin
        </label>
        @isset($suratKeteranganJanda)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki' || $suratKeteranganJanda->jenis_kelamin == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan' || $suratKeteranganJanda->jenis_kelamin == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endisset
    
        @empty($suratKeteranganJanda)
        <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" 
                    {{ (old('jenis_kelamin') == 'Laki-Laki') ? "checked" : "" }}
                    >Laki-Laki
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan"
                    {{ (old('jenis_kelamin') == 'Perempuan') ? "checked" : "" }}
                    >Perempuan
                </label>
            </div>
        @endempty
    </div>

    <!-- Tempat Lahir Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tempat_lahir', 'Tempat Lahir:') !!}
        {!! Form::text('tempat_lahir', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Tgl Lahir Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tgl_lahir', 'Tgl Lahir:') !!}
        {!! Form::date('tgl_lahir', null, ['class' => 'form-control','id'=>'tgl_lahir']) !!}
    </div>

    <!-- Status Perkawinan Field -->
    <div class="form-group col-sm-12">
        <label for="status_perkawinan">
            Status Perkawinan 
        </label>
        @isset($suratKeteranganJanda)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') || $suratKeteranganJanda->status_perkawinan == 'Belum Kawin' ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') || $suratKeteranganJanda->status_perkawinan == 'Kawin' ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') || $suratKeteranganJanda->status_perkawinan == 'Cerai Hidup' ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') || $suratKeteranganJanda->status_perkawinan == 'Cerai Mati' ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endisset
    
        @empty($suratKeteranganJanda)
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Belum Kawin" {{ (old('status_perkawinan') == 'Belum Kawin') ? "checked" : "" }} >Belum Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Kawin" {{ (old('status_perkawinan') == 'Kawin') ? "checked" : "" }}>Kawin
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Hidup" {{ (old('status_perkawinan') == 'Cerai Hidup') ? "checked" : "" }}>Cerai Hidup
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="status_perkawinan" id="status_perkawinan" value="Cerai Mati" {{ (old('status_perkawinan') == 'Cerai Mati') ? "checked" : "" }}>Cerai Mati
                </label>
            </div>
        @endempty
    </div>

    <!-- Kewarganegaraan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('kewarganegaraan', 'Kewarganegaraan:') !!}
        {!! Form::text('kewarganegaraan', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Agama Field -->
    <div class="form-group col-sm-12">
        <label for="agama">Agama</label>
        <select class="form-control" id="agama" name="agama" >
            @isset($suratKeteranganJanda)
                <option value="Islam" {{ (old('agama') == 'Islam') || $suratKeteranganJanda->agama == 'Islam' ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') || $suratKeteranganJanda->agama == 'Protestan' ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') || $suratKeteranganJanda->agama == 'Katolik' ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') || $suratKeteranganJanda->agama == 'Hindu' ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') || $suratKeteranganJanda->agama == 'Buddha' ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') || $suratKeteranganJanda->agama == 'Konghucu' ? "selected" : "" }}>Khonghucu</option>
            @endisset
    
            @empty($suratKeteranganJanda)
                <option value="Islam" {{ (old('agama') == 'Islam') ? "selected" : "" }}>Islam</option>
                <option value="Protestan" {{ (old('agama') == 'Protestan') ? "selected" : "" }}>Protestan</option>
                <option value="Katolik" {{ (old('agama') == 'Katolik') ? "selected" : "" }}>Katolik</option>
                <option value="Hindu" {{ (old('agama') == 'Hindu') ? "selected" : "" }}>Hindu</option>
                <option value="Buddha" {{ (old('agama') == 'Buddha') ? "selected" : "" }}>Buddha</option>
                <option value="Khonghucu" {{ (old('agama') == 'Khonghucu') ? "selected" : "" }}>Khonghucu</option>
            @endempty
        </select>
    </div>

    <!-- Pekerjaan Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('pekerjaan', 'Pekerjaan:') !!}
        {!! Form::text('pekerjaan', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Alamat Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('alamat', 'Alamat:') !!}
        {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Rt Field -->
    <div class="col-sm-6">
        <div class="row">
            <div class="form-group col-sm-6">
                {!! Form::label('rt', 'Rt:') !!}
                {!! Form::text('rt', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Rw Field -->
            <div class="form-group col-sm-6">
                {!! Form::label('rw', 'Rw:') !!}
                {!! Form::text('rw', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>

    <!-- Sebab Cerai Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('sebab_cerai', 'Sebab Cerai:') !!}
        <select id='sebab_cerai' name="sebab_cerai">
            <option value="hidup">
                Cerai Hidup
            </option>
            <option value="mati">
                Cerai Mati
            </option>
        </select>
    </div>

    <!-- Administrasi Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('administrasi', 'Administrasi:') !!}
        {!! Form::text('administrasi', null, ['class' => 'form-control']) !!}
    </div>

    @if(Route::current()->getName() == 'suratKeteranganJandas.edit')
        <!-- Status Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('status_id', 'Status Id') !!}
            <input class="form-control" name="status" id="status" type="text" value="{{$suratKeteranganJanda->status->status}}" readonly>
        </div>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    @if(Route::current()->getName() == 'suratKeteranganJandas.edit')
        @if ( $suratKeteranganJanda->status->status == "Draf" )
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::submit('Terima', ['class' => 'btn btn-primary', 'name' => 'terima', 'onClick' => 'redirect()']) !!}
                {!! Form::submit('Tolak', ['class' => 'btn btn-danger', 'name' => 'tolak']) !!}
                <a href="{!! route('suratKeteranganJandas.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @elseif ( $suratKeteranganJanda->status->status == "Diterima" )
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                <a href="{{ route('janda.print', ['id' => $suratKeteranganJanda->id]) }}" class="btn btn-success" target="_blank" id="print">
                    Print
                </a>
                <a href="{!! route('suratKeteranganJandas.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @else
            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                <a href="{!! route('suratKeteranganJandas.index') !!}" class="btn btn-default">Kembali</a>
            </div>
        @endif
    @else
        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Buat Surat Keterangan Janda', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('suratKeteranganJandas.index') }}" class="btn btn-default">Cancel</a>
        </div>
    @endif
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_lahir').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })

        @if(session('status'))
            document.getElementById("print").click();
        @endif
    </script>
@endsection
